#!/bin/bash

echo "dispatch.step_list = input balance statistics_unitcell model_statistics annulus
scaling.unit_cell=117 222 308 90 90 90
scaling.space_group=P212121
input.reflections_suffix=_groupA.refl
input.experiments_suffix=_refined.expt

input.keep_imagesets=True

merging.d_max=2.9
merging.d_min=2.5

statistics.annulus.d_max=2.9
statistics.annulus.d_min=2.5

output.output_dir=$1/annulus
" > annulus.phil


for d in $1/*; do
  if [ ! -f $d/srun.sh ]; then continue; fi
  echo "input.path=$PWD/$d/out" >> annulus.phil
done

let ntasks_max=$SLURM_NNODES*$SLURM_CPUS_ON_NODE
if [ $ntasks_max -gt 1024 ]; then ntasks=1024; else ntasks=$ntasks_max; fi
srun -N $SLURM_NNODES -n $ntasks cctbx.xfel.merge annulus.phil
