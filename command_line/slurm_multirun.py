from __future__ import division
# LIBTBX_SET_DISPATCHER_NAME psii_spread.slurm_multirun

from libtbx.phil import parse
from dials.util.options import ArgumentParser
import os
import subprocess
import libtbx.load_env
import tempfile


help_message = """
Handy script for dispatching many dials.stills_process jobs from a single slurm
interactive (salloc) session.

TODO example usage
"""

usage = """
$ salloc --nodes=64 --constraint=haswell --time=4:00:00 -A m3562 \\
    -q interactive --bb="capacity=500G access=striped type=scratch"
$ module load parallel
$ psii_spread.slurm_multirun nodes_per_run=7 todo_file=../../todo.txt \\
    output.output_dir=007
--where todo.txt contains a list of absolute path pairs [image file, phil file]
"""

master_phil_str = """
nodes_per_run = 4
  .type = int
  .help = process individual runs on this many nodes
todo_file = None
  .type = path
  .help = File with lines containing an image path and a phil path
output {
  output_dir = .
    .type = path
}
"""

phil_scope = parse(master_phil_str)


srun_template = """#!/bin/bash

source {conda_setpaths}

export HDF5_USE_FILE_LOCKING=FALSE

temp_out_dir=$DW_JOB_STRIPED/{i_run:03d}
mkdir -p $temp_out_dir/out
mkdir -p $temp_out_dir/log

srun -N {nodes_per_run} -n {tasks_per_run} --job-name=idx_{i_run:03d} \\
  dials.stills_process \\
    {phil_path} \\
    {image_path} \\
    mp.method=mpi \\
    output.output_dir=$temp_out_dir/out \\
    output.logging_dir=$temp_out_dir/log \\
    > $temp_out_dir/log/stdout.log \\
    2> $temp_out_dir/log/stderr.log

srun -n 1 -N 1 --job-name=tar_{i_run:03d} {transfer_path} &
"""

transfer_template = """#!/bin/bash

temp_out_dir=$DW_JOB_STRIPED/{i_run:03d}

cd $temp_out_dir
tar cf - * | tar -C {out_dir} -xf -
cd -
rm -r $temp_out_dir
"""

class Script:
  from dials.util.options import ArgumentParser

  usage = "TODO"

  def _validate_environment(self):
    nnodes = os.getenv('SLURM_NNODES')
    tasks_per_node = os.getenv('SLURM_TASKS_PER_NODE')
    head_node = os.getenv('SLURMD_NODENAME')
    assert nnodes is not None
    assert tasks_per_node is not None
    assert head_node is not None
    self.nnodes = int(nnodes)
    self.tasks_per_node = int(tasks_per_node.split('(')[0])
    self.head_node = head_node
    assert os.getenv('DW_JOB_STRIPED') is not None
    assert subprocess.call('parallel --version >/dev/null', shell=True) == 0


  def __init__(self):
    self.parser = ArgumentParser(usage=usage, phil=phil_scope, epilog=help_message)

  def run(self, args=None):
    params, options = self.parser.parse_args(args, show_diff_phil=True)

    self._validate_environment()

    # make todo list of (image, phil) pairs
    assert params.todo_file is not None
    todo = []
    with open(params.todo_file) as f:
      for l in f.readlines():
        image, phil = [os.path.abspath(x) for x in l.split()]
        assert os.path.isfile(image)
        assert os.path.isfile(phil)
        todo.append((image, phil))

    # create a todo file for parallel to use
    tf = tempfile.NamedTemporaryFile(delete=False)
    todo_filename = tf.name
    tf.close()

    # define some variables to fill in the srun template
    conda_setpaths = os.path.join(
        abs(libtbx.env.build_path), 'conda_setpaths.sh'
    )
    nodes_per_run = params.nodes_per_run
    tasks_per_run = nodes_per_run * self.tasks_per_node
    head_node = self.head_node

    # process the runlist
    for i_run, (image_path, phil_path) in enumerate(todo):
      out_dir = os.path.abspath(
          os.path.join(params.output.output_dir, "{:03d}".format(i_run))
      )
      os.makedirs(out_dir)

      srun_path = os.path.join(os.path.abspath(out_dir), 'srun.sh')
      transfer_path = os.path.join(os.path.abspath(out_dir), 'transfer.sh')

      transfer_text = transfer_template.format(**locals())
      srun_text = srun_template.format(**locals())
      with open(srun_path, 'w') as srun_file:
        srun_file.write(srun_text)
      with open(transfer_path, 'w') as transfer_file:
        transfer_file.write(transfer_text)
      os.chmod(srun_path, 0o755)
      os.chmod(transfer_path, 0o755)
      with open(todo_filename, 'a') as todo_file:
        todo_file.write(srun_path + '\n')

    # pipe the todo file into gnu parallel
    njobs = self.nnodes // (params.nodes_per_run - 1)
    subprocess.call("head {}".format(todo_filename), shell=True)
    subprocess.call(
        "cat {} | parallel --jobs {} --ungroup" .format(todo_filename, njobs),
        shell=True
    )

    print('Waiting for data transfers to finish')
    # TODO: monitor output of 'squeue -u$(whoami) -s'

if __name__=="__main__":
  script = Script()
  script.run()
