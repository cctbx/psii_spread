"""
Plot a series of histograms, one for each series of refls specified input glob,
in order to investigate the distribution of desired reflection property.
"""

from __future__ import absolute_import, division, print_function

import argparse
from glob import iglob
from pathlib import Path
from collections import deque
from itertools import chain, islice
import sys
from typing import Generator, Protocol, Sequence, Tuple, TypeVar, Union

from dials.array_family import flex
from dials.util.options import ArgumentParser
from dxtbx.model.experiment_list import ExperimentList
from iotbx.phil import parse
from libtbx.phil import scope, scope_extract
from xfel.merging.application.input.file_lister import PathPair, StemLocator
from xfel.merging.application.mpi_helper import mpi_helper as mpi_helper_class
from xfel.util.preference import flatten

import matplotlib.pyplot as plt
import numpy as np
from orderedset import OrderedSet


mpi_helper = mpi_helper_class()
T = TypeVar('T')


def sliding_pairs(sequence: Sequence[T]) -> Generator[Tuple[T], None, None]:
  it = iter(sequence)
  window = deque(islice(it, 1), maxlen=2)
  for x in it:
    window.append(x)
    yield tuple(window)


def parse_phil(phil_scope: Union[str, scope]) -> \
    Tuple[scope_extract, argparse.Namespace]:
  """Parse phil scope or scope-describing string into parameters and options"""
  import libtbx.load_env  # noqa, implicit import
  phil_scope = parse(phil_scope) if isinstance(phil_scope, str) else phil_scope
  ap = ArgumentParser(usage=f"\n libtbx.python {sys.argv[0]}", phil=phil_scope)
  # Parse the command line. quick_parse is required for MPI compatibility
  params_, options_ = ap.parse_args(show_diff_phil=True, quick_parse=True)
  return params_, options_



phil_scope_str = """
input {
  glob = None
    .type = str
    .multiple = True
    .help = A single glob pointing to all associated .expt and .refl files.
    .help = Each glob will correspond to a single plotted histogram.
    .help = Expt and refl files must end with .expt and .refl, respectively.
}
"""


Dividable = Union[float, np.ndarray]


class PathPairLike(Protocol):
  expt_path: str
  refl_path: str
  expt_stem: str
  refl_stem: str
  expt_suffix: str
  refl_suffix: str

  @classmethod
  def from_dir_expt_name(cls, base_path: str, expt_filename: str) -> 'PathPairLike':
    ...

  @classmethod
  def from_dir_refl_name(cls, base_path: str, refl_filename: str) -> 'PathPairLike':
    ...


def list_input_pairs(input_glob: str) -> list[PathPairLike]:
  """Simplified xfel.merging.application.input.file_lister.list_input_pairs"""
  path_pairs = []
  PathPair.expt_suffix = '.expt'
  PathPair.refl_suffix = '.refl'

  def load_path_if_expt_or_refl(path_: str, filename_: str) -> None:
    if filename_.endswith(PathPair.expt_suffix):
      path_pairs.append(PathPair.from_dir_expt_name(path_, filename_))
    if filename_.endswith(PathPair.refl_suffix):
      path_pairs.append(PathPair.from_dir_refl_name(path_, filename_))

  for path in [Path(p) for p in iglob(input_glob)]:
    if path.is_dir():
      for file_path in path.iterdir():
        load_path_if_expt_or_refl(str(path), str(file_path.name))
    else:
      load_path_if_expt_or_refl(str(path.parent), str(path.name))

  # Merge every matching pair of PathPair(expt, None) + PathPair(None, refl)
  # in path_pairs with common stub name into a single PathPair(expt, refl)
  matching_fully_paths, expt_singlets, refl_singlets = [], StemLocator(), StemLocator()
  for path_pair in OrderedSet(path_pairs):
    if path_pair.expt_path and path_pair.refl_path:
      matching_fully_paths.append(path_pair)
    elif path_pair.expt_path and not path_pair.refl_path:
      expt_singlets[path_pair.expt_stem] = path_pair.expt_path
    elif path_pair.refl_path and not path_pair.expt_path:
      refl_singlets[path_pair.refl_stem] = path_pair.refl_path
  unpaired_expt_stems = OrderedSet(expt_singlets)
  unpaired_refl_stems = OrderedSet(refl_singlets)
  unpaired_matching_stems = unpaired_expt_stems.intersection(unpaired_refl_stems)
  matching_stem_paths = [PathPair(expt_singlets[c], refl_singlets[c]) for c in unpaired_matching_stems]

  # For expts/refls w/o matching stem, just sort both name and pray they match
  remaining_expt_stems = unpaired_expt_stems - unpaired_matching_stems
  remaining_refl_stems = unpaired_refl_stems - unpaired_matching_stems
  matching_hopefully_paths = [PathPair(expt_singlets[es], refl_singlets[rs])
                              for es, rs in zip(remaining_expt_stems, remaining_refl_stems)]
  return matching_fully_paths + matching_stem_paths + matching_hopefully_paths


def half_inverse(d: Dividable) -> Dividable:
  return 0.5 / d


class DHistogramArtist:
  n_bins = 100
  d_lims = [2.25, 50.0]
  d_midpoints = [3.2, 5.0]

  def __init__(self) -> None:
    self.fig, self.sax = plt.subplots()
    self.sax.set_xlabel('sint/l [1/A]')
    self.sax.set_xticks(self.s_stops)
    self.sax.xaxis.tick_top()
    self.sax.xaxis.set_label_position('top')
    self.dax = self.sax.secondary_xaxis('bottom',
                                        functions=(half_inverse, half_inverse))
    self.dax.set_xlabel('d [A]')
    self.dax.set_xticks(self.d_stops)
    for s_midpoint in self.s_midpoints:
      self.sax.axvline(s_midpoint, color='k')

  def add_histogram(self, d_spacings: Sequence, label: str = None) -> None:
    s_spacings = half_inverse(np.array(d_spacings, dtype=float))
    self.sax.hist(s_spacings, bins=self.s_bins, histtype='step', label=label)

  def show(self):
    self.sax.legend()
    plt.show()

  @property
  def s_bins(self) -> list[int]:
    s_spans = np.diff(self.s_stops)
    s_target_fractions = s_spans / sum(s_spans)
    s_bin_counts = (self.n_bins * s_target_fractions).astype(int)
    while sum(s_bin_counts) < self.n_bins:
      s_surplus = (s_bin_counts / self.n_bins) - s_target_fractions
      s_bin_counts[np.argmin(s_surplus)] += 1
    while sum(s_bin_counts) > self.n_bins:
      s_surplus = (s_bin_counts / self.n_bins) - s_target_fractions
      s_bin_counts[np.argmax(s_surplus)] -= 1
    s_bins = []
    for (start, stop), s_bin_count \
            in zip(sliding_pairs(self.s_stops), s_bin_counts):
      s_bins += list(np.linspace(start, stop, s_bin_count, endpoint=False))
    s_bins.append(self.s_stops[-1])
    return s_bins

  @property
  def d_stops(self) -> np.ndarray:
    return np.array([min(self.d_lims), *sorted(self.d_midpoints),
                     max(self.d_lims)], dtype=float)

  @property
  def s_stops(self) -> np.ndarray:
    return half_inverse(self.d_stops[::-1])

  @property
  def s_midpoints(self) -> np.ndarray:
    return half_inverse(np.array(self.d_midpoints, dtype=float))


def read_d_spacings(expt_path: Path, refl_path: Path) -> list[float]:
  """Calculate and return a flat list of d_spacing floats"""
  expts = ExperimentList.from_file(expt_path, check_format=False)
  refls = flex.reflection_table.from_file(refl_path)
  d_spacings = []
  for expt, indices in refls.iterate_experiments_and_indices(expts):
    refl = refls.select(indices)
    uc = expt.crystal.get_unit_cell()
    d_spacings.extend(list(uc.d(refl['miller_index'])))
  return d_spacings


def run(parameters) -> None:
  dha = DHistogramArtist() if mpi_helper.rank == 0 else None

  for ig in parameters.input.glob:
    path_pairs = mpi_helper.comm.bcast(list_input_pairs(ig), root=0)
    path_pairs: list[PathPairLike]

    d_spacings = flatten([read_d_spacings(ep, rp) for ep, rp
                          in path_pairs[mpi_helper.rank::mpi_helper.size]])
    d_spacings = mpi_helper.comm.reduce(d_spacings, root=0, op=mpi_helper.MPI.SUM)
    if mpi_helper.rank == 0:
      dha.add_histogram(d_spacings, label=ig)
  if mpi_helper.rank == 0:
    dha.show()


params = []
if __name__ == '__main__':
  params, options = parse_phil(phil_scope_str)
  if '-h' in options or '--help' in options:
    print(__doc__)
    exit()
  run(params)
