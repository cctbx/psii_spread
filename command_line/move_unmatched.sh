#!/bin/bash

for d in $1/*; do
  if [ -f $d/srun.sh ]; then
    total=0
    unmatched=0
    mkdir $d/out/unmatched
    for f in $d/out/*_integrated.refl; do
      let total=total+1
      if [ ! -e ${f%_integrated.refl}_ucfiltered.refl ]; then
        let unmatched=unmatched+1
        mv ${f%_integrated.refl}* $d/out/unmatched/
      fi
    done
    echo "$d: $unmatched out of $total removed by filter"
  fi
done
