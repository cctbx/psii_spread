#!/bin/bash

rm todo_jfm.sh 2>/dev/null
rm payload_jfm.sh 2>/dev/null

for d in $1/*; do
  if [ ! -f $d/srun.sh ]; then continue; fi

  if [[ $(ls $d/out/*_integrated.refl 2>/dev/null |wc -l) -eq 0 ]]; then
    continue
  fi


  echo "echo process $d; \
    cctbx.xfel.jungfrau_metrics \
    input.predictions_glob=$PWD/$d/out/*_integrated.refl \
    input.observations_glob=$PWD/$d/out/*_ucfiltered.refl \
    input.expt_glob=$PWD/$d/out/*_integrated.expt \
    output.observations_filtered_replaces=ucfiltered \
    > $PWD/$d/out/jfm.out \
    2>$PWD/$d/out/jfm.err" >> todo_jfm.sh

done


echo '#!/bin/bash

module load parallel

if [[ -z "${SLURM_NODEID}" ]]; then
  echo "need \$SLURM_NODEID set"
  exit
fi

if [[ -z "${SLURM_NNODES}" ]]; then
  echo "need \$SLURM_NNODES set"
  exit
fi

cat $1 |                                               \
awk -v NNODE="$SLURM_NNODES" -v NODEID="$SLURM_NODEID" \
'\''NR % NNODE == NODEID'\'' |                               \
parallel -k --lb
' > payload_jfm.sh
chmod +x payload_jfm.sh

srun --no-kill -n $SLURM_NNODES -N $SLURM_NNODES --wait=0 payload_jfm.sh todo_jfm.sh

rm todo_jfm.sh 2>/dev/null
rm payload_jfm.sh 2>/dev/null
