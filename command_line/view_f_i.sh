#!/bin/bash

root=$1
n_file=$2
i_image=$3
refl_suffix=$4

pushd $root
folder=$(grep run_000$n_file */srun.sh |cut -c -3)
popd

echo view image

refl=$root/$folder/out/*_00${i_image}_$refl_suffix
expt=$root/$folder/out/*_00${i_image}_refined.expt
echo refl=$refl
echo expt=$expt
dials.image_viewer $refl $expt load_models=False

