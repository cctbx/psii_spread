from __future__ import division
import os, sys
from dials.array_family import flex
from cctbx.miller import match_indices
INT2DIR = sys.argv[1] # the integrated.refl directory
QADIR = sys.argv[2] # the reindexed.refl directory
n_total = 0
n_file = 0
n_reindex = 0
n_matches = 0
n_strong_no_integration = 0
n_integrated = 0
n_common = 0
n_weak = 0

import cProfile
pr = cProfile.Profile()
pr.enable()

for item in os.listdir(QADIR):
  if "ucfiltered.refl" in item:
    strfile = os.path.join(QADIR,item)
    strong_refls = flex.reflection_table.from_file(strfile)
    nrefls = len(strong_refls)
    ri = reindex_miller = strong_refls["miller_index"]
    select_indexed = reindex_miller != (0,0,0)
    reindex = (select_indexed).count(True)
    strong_and_indexed = strong_refls.select(select_indexed)

    print (strfile, nrefls, reindex)
    n_total += nrefls
    n_file += 1
    n_reindex += reindex

    integration_file = os.path.join(INT2DIR,item.replace("ucfiltered.refl","integrated.refl"))
    int_refls = flex.reflection_table.from_file(integration_file)
    ii = integration_miller = int_refls["miller_index"]
    MM = match_indices(strong_and_indexed["miller_index"], int_refls["miller_index"])
    print("  Strong+indexed",reindex, "integrated",len(ii), "in common",len(MM.pairs()),
            "indexed but no integration", len(MM.singles(0)), "integrated weak", len(MM.singles(1)))
    n_integrated += len(ii)
    n_common += len(MM.pairs())
    n_strong_no_integration += len(MM.singles(0))
    n_weak += len(MM.singles(1))

    P = MM.pairs()
    A = P.column(0)
    B = P.column(1)
    strong_and_indexed = strong_and_indexed.select(A)
    int_refls = int_refls.select(B)
    # transfer over the calculated positions from integrate2 to strong refls

    strong_and_indexed["xyzcal.mm"] = int_refls["xyzcal.mm"]
    strong_and_indexed["xyzcal.px"] = int_refls["xyzcal.px"]
    strong_and_indexed["delpsical.rad"] = int_refls["delpsical.rad"]

    output = os.path.join(QADIR,item.replace("ucfiltered.refl","groupA.refl"))
    strong_and_indexed.as_file(output)
print ("Grand total is %d from %d files of which %d reindexed"%(n_total,n_file, n_reindex))
print ("TOT Strong+indexed",n_reindex, "integrated",n_integrated, "in common",n_common, "indexed but no integration", n_strong_no_integration, "integrated weak", n_weak)

pr.disable()
pr.dump_stats(os.path.join(QADIR, 'groupA.prof'))
