#!/bin/bash

psii_spread_cl=$(dirname "$0")

rm todo_groupA.txt 2>/dev/null

for d in $1/*; do
  if [ -f $d/srun.sh ]; then
    echo "libtbx.python $psii_spread_cl/groupA.py $d/out $d/out > $d/groupA_ucfiltered.log" >> todo_groupA.txt
  fi
done

module load parallel
parallel -j 20 < todo_groupA.txt

echo -e "RUN\t\tSTRONG+IDX\tINTEGRATED\tGROUP_A\tGROUP_B\tWEAK"

for d in $1/*; do
  if [ -f $d/srun.sh ]; then
    run=$(egrep -o 'run_000[[:digit:]]{3}' $d/srun.sh)
    result=$(tail -1 $d/groupA_ucfiltered.log |
      awk '{ printf "%d\t\t%d\t\t%d\t%d\t%d\n", $3, $5, $8, $13, $16 }')
    echo -e "$run\t$result"
  fi
done

