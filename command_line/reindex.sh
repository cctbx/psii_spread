#!/bin/bash

rm payload_reindex.sh 2>/dev/null
rm todo_reindex.sh 2>/dev/null
for f in $(find $PWD/$1 -name *_refined.expt); do root=${f%_refined.expt}; echo dials.index output.log=None ${root}_refined.expt ${root}_strong.refl indexing.method=None indexing.stills.refine_all_candidates=False refinement.reflections.outlier.algorithm=null refinement.parameterisation.detector.fix=all indexing.refinement_protocol.mode=repredict_only output.experiments=${root}_reindexed.expt output.reflections=${root}_reindexed.refl >> todo_reindex.txt; done



echo '#!/bin/bash

module load parallel

if [[ -z "${SLURM_NODEID}" ]]; then
  echo "need \$SLURM_NODEID set"
  exit
fi

if [[ -z "${SLURM_NNODES}" ]]; then
  echo "need \$SLURM_NNODES set"
  exit
fi

cat $1 |                                               \
awk -v NNODE="$SLURM_NNODES" -v NODEID="$SLURM_NODEID" \
'\''NR % NNODE == NODEID'\'' |                               \
parallel
' > payload_reindex.sh
chmod +x payload_reindex.sh

srun --no-kill -n $SLURM_NNODES -N $SLURM_NNODES --wait=0 \
  payload_reindex.sh todo_reindex.txt >/dev/null
