#!/bin/bash

for f in $(find $PWD/$1 -name *_reindexed.expt); do root=${f%_reindexed.expt}; echo "cctbx.xfel.filter_experiments_by_uc_old ${root}_reindexed.expt ${root}_reindexed.refl cluster.covariance.file=$2 output.experiments=${root}_ucfiltered.expt output.reflections=${root}_ucfiltered.refl" >> todo_ucfilter.txt; done

echo '#!/bin/bash

module load parallel

if [[ -z "${SLURM_NODEID}" ]]; then
  echo "need \$SLURM_NODEID set"
  exit
fi

if [[ -z "${SLURM_NNODES}" ]]; then
  echo "need \$SLURM_NNODES set"
  exit
fi

cat $1 |                                               \
awk -v NNODE="$SLURM_NNODES" -v NODEID="$SLURM_NODEID" \
'\''NR % NNODE == NODEID'\'' |                               \
parallel
' > payload_ucfilter.sh
chmod +x payload_ucfilter.sh

srun --no-kill -n $SLURM_NNODES -N $SLURM_NNODES --wait=0 \
  payload_reindex.sh todo_ucfilter.txt >/dev/null 2>/dev/null
