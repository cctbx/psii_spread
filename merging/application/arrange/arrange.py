from collections import UserList, deque
from copy import deepcopy
from enum import Enum
from itertools import chain, islice
from numbers import Integral
import re
from pathlib import Path
from typing import Generator, Iterable, Protocol, Sequence, TypeVar, Union

import numpy as np
import pandas as pd

from dials.array_family import flex
from dxtbx.model import Experiment, ExperimentList
from xfel.merging.application.worker import worker

from path_counter import PathCounter, RecursivePathCounter


T = TypeVar('T')
GuideLongSequence = Sequence
class Logger(Protocol):
  def log(self, msg: str) -> None: ...
  def log_main(self, msg: str) -> None: ...


def flex_concat(r: Sequence[flex.reflection_table]) -> flex.reflection_table:
  """Safely concatenate zero-to-several flex.reflection_tables"""
  return flex.reflection_table.concat(r) if r else flex.reflection_table()


def sliding_pairs(sequence: Sequence[T]) -> Generator[tuple[T], None, None]:
  """Yield elements of sequence as subsequent pairs: [ABCD]->[AB],[BC],[CD]"""
  it = iter(sequence)
  window = deque(islice(it, 1), maxlen=2)
  for x in it:
    window.append(x)
    yield tuple(window)


class COMM(Protocol):
  """Simple Typing Protocol for mpi4py communicator such as MPI.COMM_WORLD"""
  rank: int
  size: int
  def allgather(self, item: T) -> list[T]: ...  # noqa
  def alltoall(self, sequence: Sequence[T]) -> Sequence[T]: ...  # noqa


def sliced_alltoall(comm: COMM,  # noqa
                    sent: list[Sequence[T]],
                    slices: int = 1,
                    ) -> list[Sequence[T]]:
  """Perform all-to-all in `slices` to avoid mpi4py OverflowError"""
  # known issue: this can still be insufficient if many slices have 1 pair only
  slice_bounds = [np.linspace(0, len(sent[rank]), slices+1, dtype=int)
                  for rank in range(comm.size)]
  recv: list[T] = [None, ] * comm.size
  for slice_i in range(slices):
    sent_slice = []
    for target_rank, pairs in enumerate(sent):
      slice_start = slice_bounds[target_rank][slice_i]
      slice_stop = slice_bounds[target_rank][slice_i+1]
      sent_slice.append(pairs[slice_start:slice_stop])
    recv_slice = comm.alltoall(sent_slice)
    for rank, recv_slice_from_rank in enumerate(recv_slice):
      if recv[rank] is None:
        recv[rank] = recv_slice_from_rank
      else:
        recv[rank] += recv_slice_from_rank
  return recv


class Pairs:
  """Efficiently store, slice, and join ordered expt/refl pairs"""

  @classmethod
  def from_pairs(cls, pairs_iterable: Iterable['Pairs']) -> 'Pairs':
    """Create a new single instance of Pairs from an iterable of Pairs"""
    expts = ExperimentList()
    refls = []
    for pairs in pairs_iterable:
      for expt, refl in pairs:
        expts.append(expt)
        refls.append(refl)
    return Pairs(expts, flex_concat(refls))

  def __init__(self,
               expts: ExperimentList = ExperimentList(),
               refls: flex.reflection_table = flex.reflection_table()) -> None:
    self.expts = expts
    self.refls = refls

  def __add__(self, other) -> 'Pairs':
    """Make a new `Pairs` by concatenating both expts and refls in donors"""
    expts = ExperimentList([*self.expts, *other.expts])
    refls = flex_concat([deepcopy(self.refls), deepcopy(other.refls)])
    return Pairs(expts, refls)

  def __len__(self) -> int:
    return len(self.expts)

  def __iter__(self) -> 'Pairs':
    self._n = 0
    return self

  def __next__(self) -> tuple[Experiment, flex.reflection_table]:
    if self._n < len(self):
      self._n += 1
      return self[self._n - 1]
    raise StopIteration

  def __getitem__(self,
                  item: Union[slice, GuideLongSequence[bool], Sequence[int], int],
                  ) -> Union['Pairs', tuple[Experiment, flex.reflection_table]]:
    """Slice with slice/bool mask/index sequence or get expt&refl with int"""
    if isinstance(item, slice):
      return self[np.arange(len(self))[item]]
    elif hasattr(item, '__iter__') and hasattr(item, '__len__'):
      if len(item) == 0:
        return Pairs()
      elif all(isinstance(i, bool) for i in item):
        assert len(item) == len(self), f'{len(item)=} must be = {len(self)=}'
        return self[np.arange(len(self))[np.array(item)]]
      elif all(isinstance(i, Integral) for i in item):
        item = [int(i) for i in item]
        expts = ExperimentList([self.expts[i] for i in item])
        refls = [self.refls.select(self.refls['id'] == i) for i in item]
        return Pairs(expts, flex_concat(refls))
    elif isinstance(item, Integral):
      item = int(item)
      return self.expts[item], self.refls.select(self.refls["id"] == item)
    raise NotImplementedError(f'Unknown iterator {item=} of {type(item)=}')

  def mask(self, mask: GuideLongSequence[bool]) -> None:
    """Modify self so that only elements where mask is True remain"""
    assert len(mask) == len(self)
    self.expts = ExperimentList([e for e, m in zip(self.expts, mask) if m])
    self.refls = flex_concat([self.refls.select(self.refls['id'] == i)
                              for i, m in enumerate(mask) if m])


class Guide(UserList[int]):
  """
  A class representing target position of all Pairs, typically on this rank.
  The length of an individual guide on a rank should be equal to the number
  of expt/refl pairs handled by this rank. The value on i-th position should
  be equal to the overall position of this expt/refl pair across all ranks.

  Guide([0, 1, 2, 25, 26]) describes five expt/refl pairs handled by a rank.
  First three of these pairs should be sent to the ranks handling positions
  0, 1, and 2 (typically rank 0) while preserving their order; the fourth
  and fifth pair should be sent to rank(s) handling positions 25 and 26.
  """
  logger: Logger = None

  @classmethod
  def ordered(cls, rank: int, expt_counts_by_rank: Sequence[int]) -> 'Guide':
    """Return Guide representing current order of Pairs across ranks"""
    start: int = sum(expt_counts_by_rank[:rank])
    stop: int = start + expt_counts_by_rank[rank]
    return Guide(np.arange(start, stop, 1, int))

  def outer(self, comm: COMM) -> 'Guide':
    """A guide summarizing target positions of all Pairs across all ranks"""
    return Guide(chain(*comm.allgather(self)))

  def global_mask(self, comm: COMM, mask: GuideLongSequence[bool]) -> None:
    """Drop elements, afterwards reindex `self` based on `mask` across ranks"""
    masked_self = Guide(np.array(self, dtype=int)[np.array(mask, dtype=bool)])
    all_masked_selves = comm.allgather(masked_self)
    flattened_masked_selves = Guide(chain(*all_masked_selves))
    collapsed_masked_selves = np.argsort(np.argsort(flattened_masked_selves))
    start: int = sum(len(g) for g in all_masked_selves[:comm.rank])
    self.data = list(collapsed_masked_selves[start:start+len(masked_self)])

  def global_sort(self,
                  comm: COMM,
                  keys: GuideLongSequence,
                  reverse: bool,
                  ) -> None:
    """Sort elements of `self` based on values of `keys` across ranks"""
    if len(keys) != len(self):
      raise ValueError(f'{len(keys)=} and {len(self)=} must be equal!')
    gathered_keys = comm.allgather(keys)
    start: int = sum(len(k) for k in gathered_keys[:comm.rank])
    all_keys = list(chain(*gathered_keys))
    indices_that_sort_all_keys = np.argsort(all_keys)
    if reverse:
      indices_that_sort_all_keys = indices_that_sort_all_keys[::-1]
    target_positions_of_all_keys = np.argsort(indices_that_sort_all_keys)
    self.data = list(target_positions_of_all_keys[start:start + len(keys)])

  def arrange(self,
              comm: COMM,
              pairs: Pairs,
              slices: int = 1,
              ) -> tuple[ExperimentList, flex.reflection_table]:
    """Arrange Pairs across comm ranks according to the order in Guides"""
    if self.logger is not None:
      self.logger.log("This rank's guide: " + str(self))
    guides = comm.allgather(self)
    total_item_count = sum(len(g) for g in guides)
    rank_boundaries = np.linspace(0, total_item_count, comm.size+1, dtype=int)
    guide_array = np.array(self, dtype=int)
    sent_pairs, recv_pos = [], []
    # Calculate which pairs will be sent to each rank
    for i, (rank_start, rank_stop) in enumerate(sliding_pairs(rank_boundaries)):
      idx = np.where((rank_start <= guide_array) & (guide_array < rank_stop))[0]
      p = pairs[idx]
      if self.logger is not None:
        identifiers = str([e.identifier for e in p.expts])
        self.logger.log(f"Pairs sent to rank {i}: " + identifiers)
      sent_pairs.append(p)
    # Calculate which pairs will be received from each rank
    rank_start = rank_boundaries[comm.rank]
    rank_stop = rank_boundaries[comm.rank + 1]
    for rank_guide in guides:
      recv_pos.append([p for p in rank_guide if rank_start <= p < rank_stop])
    # All-to-all all pairs between the ranks
    if self.logger is not None:
      self.logger.log('#pairs sent/rank: ' + str([len(s) for s in sent_pairs]))
    recv_pairs = comm.alltoall(sent_pairs) if slices == 1 else \
      sliced_alltoall(comm, sent_pairs, slices)
    if self.logger is not None:
      self.logger.log('#pairs recv/rank: ' + str([len(s) for s in recv_pairs]))
    # Recreate order of received pairs based on their order calculated before
    expts = [None, ] * (rank_stop - rank_start)
    refls = [None, ] * (rank_stop - rank_start)
    for i, (rank_recv, rank_pos) in enumerate(zip(recv_pairs, recv_pos)):
      for (expt, refl), pos in zip(rank_recv, rank_pos):
        if self.logger is not None:
          self.logger.log(f'Recv from rank {i:4d} for position {pos:6d}: '
                          + expt.identifier)
        expts[pos - rank_start] = expt
        refls[pos - rank_start] = refl
    return ExperimentList(expts), flex_concat(refls)


class Arrange(worker):
  """Generalized load_balance worker capable of filtering and sorting input"""

  class FilterMode(Enum):
    off = 'off'
    filter = 'filter'
    match = 'match'

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super().__init__(params=params, mpi_helper=mpi_helper,
                     mpi_logger=mpi_logger)

  def __repr__(self):
    return 'Arrange (filter, sort, balance) input data load'

  def eir_counts(self,
                 expts: ExperimentList,
                 refls: flex.reflection_table,
                 verbose: bool = True,
                 ) -> pd.DataFrame:
    """Calculate expts, imagesets, and refls' distribution across all ranks"""
    expt_count = len(expts) if expts else 0
    refl_count = len(refls) if refls else 0
    imageset_count = sum(len(i) for i in expts.imagesets()) if expts else 0
    eir_count = np.array([expt_count, imageset_count, refl_count])
    eir_counts = self.mpi_helper.comm.allgather(eir_count)
    eir_counts = pd.DataFrame(np.array(eir_counts, dtype=int))
    eir_counts.columns = 'experiments images reflections'.split()
    if self.mpi_helper.rank == 0 and verbose:
      for col_name, col in eir_counts.items():
        self.logger.main_log(f'{str(col_name).title():11} (total,min,max): '
                             f'{sum(col)}, {min(col)}, {max(col)}')
    return eir_counts

  def tally(self, paths: Iterable[Union[str, Path]]) -> PathCounter:
    """Recursively tally paths and log their local and global distribution"""
    local_counter = RecursivePathCounter(paths)
    self.logger.log('\n' + str(local_counter.without_common_ancestors))
    global_counter = PathCounter(self.mpi_helper.count(local_counter))
    if self.mpi_helper.rank == 0:
      self.logger.main_log('\n' + str(global_counter.without_common_ancestors))
    return local_counter

  def run(self,
          expts: ExperimentList,
          refls: flex.reflection_table,
          ) -> tuple[ExperimentList, flex.reflection_table]:
    """Run the main worker: arrange (filter, sort, balance) input data load"""
    # Prepare
    if self.mpi_helper.rank == 0:
      self.logger.main_log('Data distribution before load balancing:')
    eri_counts = self.eir_counts(expts, refls, verbose=True)
    guide = Guide.ordered(self.mpi_helper.rank, eri_counts.experiments)
    if self.params.input.parallel_file_load.balance_verbose:
      Guide.logger = self.logger
    imageset_paths = np.array([expt.imageset.paths()[0] for expt in expts])
    pairs = Pairs(expts, refls)
    # Tally
    if self.params.arrange.tally.by == 'imageset_path':
      _ = self.tally(imageset_paths)
    # Filter
    filter_mode = self.FilterMode(self.params.arrange.filter.mode)
    if filter_mode is not self.FilterMode.off:
      if (pattern := self.params.arrange.filter.imageset_path) is not None:
        self.logger.log(f'Escaped {filter_mode.name} pattern: "{pattern}"')
        regex = re.compile(pattern)
        mask = np.array([bool(regex.search(ip)) for ip in imageset_paths], dtype=bool)
        mask = mask if filter_mode is self.FilterMode.match else ~mask
        guide.global_mask(self.mpi_helper.comm, mask)
        imageset_paths = imageset_paths[mask]
        pairs.mask(mask)
        self.logger.log(f'Filtered out {sum(~mask)} expt/refl pairs')
      if self.mpi_helper.rank == 0:
        self.logger.main_log('Data distribution after filtering:')
      _ = self.eir_counts(pairs.expts, pairs.refls, verbose=True)
    # Sort
    if self.params.arrange.sort.by == 'imageset_path':
      reverse = self.params.arrange.sort.reverse
      guide.global_sort(self.mpi_helper.comm, imageset_paths, reverse=reverse)
    # Balance
    slices = self.params.input.parallel_file_load.balance_mpi_alltoall_slices
    expts, refls = guide.arrange(self.mpi_helper.comm, pairs, slices)
    if self.mpi_helper.rank == 0:
      self.logger.main_log('Data distribution after load balancing:')
    _ = self.eir_counts(expts, refls, verbose=True)
    return expts, refls


