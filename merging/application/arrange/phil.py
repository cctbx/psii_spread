from __future__ import division

# arrange uses also selected parallel_file_load scope keywords
# from cctbx_project/xfel/merging/application/phil/phil.py
phil_str = """
arrange {
  filter {
    mode = *off filter match
      .type = choice
      .help = Filtering mode: off, filter out, or match only files with matching name.
    imageset_path = None
      .type = str
      .help = Regex expression with part of imageset paths to be filtered/matched.
  }
  sort {
    by = *None imageset_path
      .type = choice
      .help = If not None, property of expt used as sort key before balancing.
    reverse = False
      .type = bool
      .help = If true, reverse the order after sort (sort last-to-first).
  }
  tally {
    by = *None imageset_path
        .type = choice
        .help = If not None, property of expt to be (recursively) tallied.
  }
}
"""
