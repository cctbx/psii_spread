# ~~~~~~~~~~~~~~~~~ Required boilerplate for custom workers ~~~~~~~~~~~~~~~~~ #
import inspect
import os
import sys

current_file = inspect.getfile(inspect.currentframe())
current_dir = os.path.dirname(os.path.abspath(current_file))
sys.path.insert(0, current_dir)
# You may import directly from the directory containing
# this module. It's not necessary to reset the path.
# ~~~~~~~~~~~~~~~~~~~~~~~ End of required boilerplate ~~~~~~~~~~~~~~~~~~~~~~~ #

from arrange import Arrange
from xfel.merging.application.worker import factory as factory_base      # noqa


class factory(factory_base):
  """Factory class for arranging (balance+sort) expts & refls between ranks"""
  @staticmethod
  def from_parameters(params, additional_info=(),
                      mpi_helper=None, mpi_logger=None):
    """Initiate new instance of arrange worker based on params phil."""
    return [Arrange(params, mpi_helper, mpi_logger)]
