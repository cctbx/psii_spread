from collections import Counter
from pathlib import Path
from typing import Union


class PathCounter(Counter[Path, int]):
  """Count `pathlib.Path` objects and display them nicely using `str()`"""
  def __setitem__(self, key: Union[str, Path], value: int) -> None:
    super().__setitem__(Path(key), value)

  def __str__(self) -> str:
    fmt = f'{{:{len(str(self.most_common(1)[0][1])) if self else 1}d}} {{!s}}'
    return '\n'.join(fmt.format(v, k) for k, v in sorted(self.items()))

  @property
  def without_common_ancestors(self):
    max_value = self.most_common(1)[0][1] if self else 0
    common_ancestors = [k for k, v in self.items() if v == max_value]
    nearest_common_ancestor = sorted(common_ancestors)[-1] if self else None
    return PathCounter({k: v for k, v in self.items()
                        if v < max_value or k == nearest_common_ancestor})


class RecursivePathCounter(PathCounter):
  """When adding to `self[path]`, add to all of path's parents as well"""
  def __setitem__(self, path: Union[str, Path], value: int) -> None:
    for path_ in reversed([(pp := Path(path)), *pp.parents]):
      super().__setitem__(path_, super().__getitem__(path_) + value)


