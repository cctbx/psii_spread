from __future__ import division
from xfel.merging.application.worker import worker
from dials.array_family import flex
from xfel.merging.application.utils.data_counter import data_counter
from dxtbx.model.experiment_list import ExperimentList

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.

class pretty(worker):

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super(pretty, self).__init__(params=params, mpi_helper=mpi_helper, mpi_logger=mpi_logger)
    self.position_shifts = flex.double()

  def __repr__(self):
    return """Special worker for deciding which diffraction patterns are nice
    in the sense that we can probably model them with diffBragg"""

  def run(self, experiments, reflections):
    new_reflections = []
    new_experiments = ExperimentList()
    #loop through experiments
    for iid, expt in enumerate(experiments):
      src_refls = reflections.select(reflections["id"] == iid)

      if len(src_refls)%2 == {"even":0, "odd":1}[self.params.pretty.criterion]:
          src_refls['id'] = flex.int(len(src_refls), len(new_experiments))
          new_reflections.append(src_refls)
          new_experiments.append(expt)

    if len(new_reflections) > 0:
      new_reflections = flex.reflection_table.concat(new_reflections)
    else:
      new_reflections = flex.reflection_table()
    self.mpi_helper.comm.barrier()

    data_counter(self.params).count(new_experiments, new_reflections)
    return new_experiments, new_reflections
