from __future__ import division

phil_str = """
pretty {
  integration {
    refl = None
      .type = str
      .help = directory glob containing the integration.refls files to be tapped for the
      .help = SPREAD shoeboxes.
    expt = None
      .type = str
  }
  length = 10
    .type = int
    .help = please ask Nick
  criterion = even *odd
    .type = choice
    .help = tell you later
}
"""
