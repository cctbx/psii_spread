from __future__ import division
from xfel.merging.application.worker import worker
from dials.array_family import flex
import math
from scitbx.matrix import col
from dxtbx.model.experiment_list import ExperimentList

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.

class drefine(worker):

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super(drefine, self).__init__(params=params, mpi_helper=mpi_helper, mpi_logger=mpi_logger)

  def __repr__(self):
    return "Refinement options prior to unit cell filter"
    """
    The worker wraps dials refinement, and as such the "refinement" phil space supplies the parameters
    """
  def dials_refine(self, experiment, input_reflections, verbose=True):
    from dials.algorithms.refinement.refiner import RefinerFactory
    experiments = ExperimentList(); experiments.append(experiment)
    reflections = flex.reflection_table()
    reflections.extend(input_reflections)
    reflections.reset_ids()

    refiner = RefinerFactory.from_parameters_data_experiments(self.params,
      reflections, experiments)
    Initial_position = refiner.get_experiments()[0].detector[0].get_beam_centre_lab(refiner.get_experiments()[0].beam.get_s0())
    Initial_position_00 = refiner.get_experiments()[0].detector[0].get_lab_coord((0,0))

    #print("II",refiner.get_experiments()[0].crystal)
    #print("II",reflections[0])
    history = refiner.run()
    NEWR = refiner.predict_for_reflection_table(reflections=reflections)
    #print("JJ",NEWR[0])

    #print(list(reflections.keys()))
    #print(list(NEWR.keys()))
    #for item in history["rmsd"]:
    #  print("XX","%5.2f %5.2f %8.5f"%(item[0],item[1],180.*item[2]/math.pi))

    #print("XX",refiner.selection_used_for_refinement().count(True),"spots used for refinement")
    Final_position = refiner.get_experiments()[0].detector[0].get_beam_centre_lab(refiner.get_experiments()[0].beam.get_s0())
    Final_position_00 = refiner.get_experiments()[0].detector[0].get_lab_coord((0,0))
    #print("XX",refiner.get_experiments()[0].crystal)
    if verbose:
      print("Distance initial %.3f mm, final %.3f mm, Z change %.3f mm"%(-Initial_position[2], -Final_position[2], (-Final_position[2])-(-Initial_position[2])) +
          " Z shift %.3fmm"%( (col(Final_position_00)-col(Initial_position_00))[2] ) +
          " Total XYZ shift %.3fmm"%((col(Final_position_00)-col(Initial_position_00)).length())
      )
    return refiner.get_experiments()[0], NEWR

  def run(self, experiments, reflections):
      new_experiments = ExperimentList()
      new_reflections = []
      for iid, expt in enumerate(experiments):
        outlier = 0
        refl = reflections.select(reflections["id"] == iid)

        expt, refl = self.dials_refine(expt,refl)

        new_experiments.append(expt)
        new_reflections.append(refl)
      new_reflections = flex.reflection_table.concat(new_reflections)


      from xfel.merging.application.utils.data_counter import data_counter
      data_counter(self.params).count(new_experiments, new_reflections)
      return new_experiments, new_reflections
