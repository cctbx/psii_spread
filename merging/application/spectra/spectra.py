from __future__ import division
from dials.array_family import flex
from dxtbx.model import Experiment, ExperimentList
from xfel.merging.application.worker import worker

import pickle
import numpy as np

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.

def get_cdf(global_shots, all_energies, global_weights, nbins=37):
  assert nbins < len(all_energies) # can't make bins more granular than spectrum sampling
  cdf = flex.double([global_weights[0]])
  for weight in global_weights[1:]:
    cdf.append(cdf[-1]+weight)
  assert len(cdf) == len(global_weights)
  maxcdf = cdf[-1]
  cdf_quantiles = [(1./(2*nbins))*(2*p)*maxcdf for p in range(1,nbins)]
  assert len(cdf_quantiles) == nbins-1
  global_weights_quantiles = []
  all_energies_quantiles = []

  # one-pass through all data to find equal areas under the spectrum curve
  iq = 0
  for idx in range(len(all_energies)-1):
    cum_lower = cdf[idx]; cum_upper = cdf[idx+1]
    while (cum_lower <= cdf_quantiles[iq] < cum_upper):

      wts_lower = global_weights[idx]; wts_upper = global_weights[idx+1]
      energy_lower = all_energies[idx]; energy_upper = all_energies[idx+1]
      # linear interpolation gets bin boundaries
      m_slope = (cum_lower - cum_upper) / (energy_lower - energy_upper)
      b_intcpt = cum_lower - m_slope * energy_lower
      all_energies_quantiles.append( (cdf_quantiles[iq] - b_intcpt) / m_slope )
      # once again for interpolated photon intensity
      m_slope = (cum_lower - cum_upper) / (wts_lower - wts_upper)
      b_intcpt = cum_lower - m_slope * wts_lower
      global_weights_quantiles.append( (cdf_quantiles[iq] - b_intcpt) / m_slope )
      iq += 1
      if iq == nbins-1: break
    if iq == nbins-1: break
  return cdf_quantiles, global_weights_quantiles, all_energies_quantiles

  #plot is only for debug
  from matplotlib import pyplot as plt
  plt.plot(all_energies_quantiles, cdf_quantiles, "r.")
  #for iq,quant in enumerate(n2_quantiles):
  #  plt.plot([6541,6543],[quant,quant],"r-")
  for icum,cum in enumerate(cdf_quantiles):
    plt.plot([6519,6561],[cum,cum],"-",color="limegreen")
  plt.plot(all_energies,cdf,"-",color="blue")
  plt.title('Cumulative distribution over %d shots'%(global_shots))
  plt.xlabel('Energy (eV)')
  plt.ylabel('CDF')
  plt.show()
  return cdf_quantiles, global_weights_quantiles, all_energies_quantiles

class Spectra(worker):
  """Discover the incident sprectra from imagesets, combine them all and stash information"""

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super().__init__(params=params, mpi_helper=mpi_helper,
                     mpi_logger=mpi_logger)

  def __repr__(self):
    return 'Discover the incident sprectra from imagesets, combine them all and stash information'

  def run(self,
          experiments: ExperimentList,
          reflections: flex.reflection_table,
          ) -> tuple[ExperimentList, flex.reflection_table]:
      all_energies = None
      all_weights = None
      all_shots = 0
      for experiment in experiments:
        print(experiment.beam)
        if True: # self.mpi_helper.rank == 0: # trial plot on rank 0
          from dxtbx.imageset import ImageSetFactory

          iset = experiment.imageset
          assert len(iset) == 1
          image_index = iset.indices()[0]
          path = iset.paths()[0]

          CC = ImageSetFactory.make_imageset(filenames=[path], single_file_indices=[image_index])
          spectrum = CC.get_spectrum(0)
          if all_weights is None:
            all_weights = spectrum.get_weights()
          else:
            all_weights += spectrum.get_weights()
          if all_energies is None:
            all_energies = spectrum.get_energies_eV()
          else:
            assert all_energies == spectrum.get_energies_eV()
          all_shots += 1
      global_weights = self.mpi_helper.comm.allreduce(all_weights)
      global_shots = self.mpi_helper.comm.allreduce(all_shots)
      if self.mpi_helper.rank == 0:
        print("DATA nshots",global_shots)
        print("DATA energies",list(all_energies))
        print("DATA weights",list(global_weights))
        # write them to a file
        with open("incident_spectra.pickle","wb") as F:
          pickle.dump(dict(
          global_shots=global_shots, all_energies=all_energies, global_weights=global_weights), F)
        # and stash them in common store
        self.common_store["store_incident_spectra"] = dict(
          global_shots=global_shots, all_energies=all_energies, global_weights=global_weights)
        if False: # only for debug
          from matplotlib import pyplot as plt
          plt.plot(  all_energies,all_weights,"-", color="orange")
          plt.plot(all_energies,global_weights,"-",color="blue")
          plt.title('Rank %d with %d shots'%(self.mpi_helper.rank,global_shots))
          plt.xlabel('Energy (eV)')
          plt.ylabel('Counts')
          plt.show()

      return experiments, reflections

if __name__=="__main__":
  filein = "/global/cfs/cdirs/lcls/sauter/LY99/spread/SIM/11refine/34848955/incident_spectra.pickle"
  nbins = int(sys.argv[1]) # use 32 for 1 eV coverage
  with open(filein,"rb") as F:
    fdata = pickle.load(F)
    global_shots = fdata["global_shots"]
    all_energies = fdata["all_energies"]
    global_weights = fdata["global_weights"]
  cdf_quantiles, global_weights_quantiles, all_energies_quantiles=get_cdf(
                      global_shots, all_energies, global_weights, nbins=nbins)
  from matplotlib import pyplot as plt
  for energy,weight in zip(all_energies_quantiles,global_weights_quantiles):
    plt.plot([energy, energy],[0,weight],"-",color="limegreen")
  plt.plot(all_energies,global_weights,"-",color="blue")
  plt.title('Incident spectra with %d equal-flux bins over %d shots'%(nbins,global_shots))
  plt.xlabel('Energy (eV)')
  plt.ylabel('Counts')
  plt.show()
