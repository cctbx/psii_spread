from __future__ import division
from dials.array_family import flex
from dxtbx.model import Experiment, ExperimentList
from xfel.merging.application.worker import worker

import pickle
import numpy as np

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.

def get_cdf(global_shots, all_energies, global_weights, nbins=37):
  pass
  cdf = flex.double([global_weights[0]])
  for weight in global_weights[1:]:
    cdf.append(cdf[-1]+weight)
  assert len(cdf) == len(global_weights)
  maxcdf = cdf[-1]
  even_spaced_cums = [(1./(2*nbins))*(2*p+1)*maxcdf for p in range(nbins)]
  np_quantiles = np.quantile( a = cdf, q = [(1./(2*nbins))*(2*p+1) for p in range(nbins)], method = "interpolated_inverted_cdf")
  n2_quantiles = np.quantile( a = global_weights, q = [(1./(2*nbins))*(2*p+1) for p in range(nbins)], method = "interpolated_inverted_cdf")



  #quantiles = [np.quantile(cdf, (1./(2*nbins))*(2*p+1)) for p in range(nbins)]
  from matplotlib import pyplot as plt
  #plt.plot(quantiles, even_spaced_cums, "r.")
  for iq,quant in enumerate(n2_quantiles):
    plt.plot([6541,6543],[quant,quant],"r-")
  for icum,cum in enumerate(even_spaced_cums):
    plt.plot([6539,6541],[cum,cum],"-",color="limegreen")
  plt.plot(all_energies,cdf,"-",color="blue")
  plt.title('Cumulative distribution over %d shots'%(global_shots))
  plt.xlabel('Energy (eV)')
  plt.ylabel('CDF')
  plt.show()


class Spectra(worker):
  """Discover the incident sprectra from imagesets, combine them all and stash information"""

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super().__init__(params=params, mpi_helper=mpi_helper,
                     mpi_logger=mpi_logger)

  def __repr__(self):
    return 'Discover the incident sprectra from imagesets, combine them all and stash information'

  def run(self,
          experiments: ExperimentList,
          reflections: flex.reflection_table,
          ) -> tuple[ExperimentList, flex.reflection_table]:
      all_energies = None
      all_weights = None
      all_shots = 0
      for experiment in experiments:
        print(experiment.beam)
        if True: # self.mpi_helper.rank == 0: # trial plot on rank 0
          from dxtbx.imageset import ImageSetFactory

          iset = experiment.imageset
          assert len(iset) == 1
          image_index = iset.indices()[0]
          path = iset.paths()[0]

          CC = ImageSetFactory.make_imageset(filenames=[path], single_file_indices=[image_index])
          spectrum = CC.get_spectrum(0)
          if all_weights is None:
            all_weights = spectrum.get_weights()
          else:
            all_weights += spectrum.get_weights()
          if all_energies is None:
            all_energies = spectrum.get_energies_eV()
          else:
            assert all_energies == spectrum.get_energies_eV()
          all_shots += 1
      global_weights = self.mpi_helper.comm.allreduce(all_weights)
      global_shots = self.mpi_helper.comm.allreduce(all_shots)
      if self.mpi_helper.rank == 0:
        print("DATA nshots",global_shots)
        print("DATA energies",list(all_energies))
        print("DATA weights",list(global_weights))
        # write them to a file
        with open("incident_spectra.pickle","wb") as F:
          pickle.dump((global_shots, all_energies, global_weights), F)
        # and stash them in common store
        self.common_store["store_incident_spectra"] = global_shots, all_energies, global_weights
        if False: # only for debug
          from matplotlib import pyplot as plt
          plt.plot(  all_energies,all_weights,"-", color="orange")
          plt.plot(all_energies,global_weights,"-",color="blue")
          plt.title('Rank %d with %d shots'%(self.mpi_helper.rank,global_shots))
          plt.xlabel('Energy (eV)')
          plt.ylabel('Counts')
          plt.show()

      return experiments, reflections

if __name__=="__main__":
  filein = "/global/cfs/cdirs/lcls/sauter/LY99/spread/SIM/11refine/34848955/incident_spectra.pickle"
  with open(filein,"rb") as F:
    global_shots, all_energies, global_weights = pickle.load(F)
  get_cdf(global_shots, all_energies, global_weights, nbins=16)
  from matplotlib import pyplot as plt
  for energy,weight in zip(all_energies,global_weights):
    plt.plot([energy, energy],[0,weight],"-",color="green")
  #plt.plot(all_energies,global_weights,"-",color="blue")
  plt.title('Incident spectra average over %d shots'%(global_shots))
  plt.xlabel('Energy (eV)')
  plt.ylabel('Counts')
  plt.show()
