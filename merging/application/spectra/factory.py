# ~~~~~~~~~~~~~~~~~ Required boilerplate for custom workers ~~~~~~~~~~~~~~~~~ #
import inspect
import os
import sys

current_file = inspect.getfile(inspect.currentframe())
current_dir = os.path.dirname(os.path.abspath(current_file))
sys.path.insert(0, current_dir)
# You may import directly from the directory containing
# this module. It's not necessary to reset the path.
# ~~~~~~~~~~~~~~~~~~~~~~~ End of required boilerplate ~~~~~~~~~~~~~~~~~~~~~~~ #

from spectra import Spectra
from xfel.merging.application.worker import factory as factory_base      # noqa


class factory(factory_base):
  """Factory class for combining all incident spectra into cumulative plot"""
  @staticmethod
  def from_parameters(params, additional_info=(),
                      mpi_helper=None, mpi_logger=None):
    """Initiate new instance of spectra worker based on params phil."""
    return [Spectra(params, mpi_helper, mpi_logger)]
