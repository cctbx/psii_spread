from __future__ import division

phil_str = """
substitute {
  apply = True
    .type = bool
    .help = If False, filter input to match to substitutes, but do not replace
  input = None
    .type = str
    .multiple = True
    .help = path glob to the refls which should be tapped for new shoe boxes
}
"""
