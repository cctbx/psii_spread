from __future__ import division

import functools
from glob import glob
from typing import Callable, Dict, List, NamedTuple, Tuple

from xfel.merging.application.worker import worker
from dials.array_family import flex
from xfel.merging.application.utils.data_counter import data_counter
from dxtbx.model import Experiment, ExperimentList
from cctbx.miller import match_indices


class LocatedPair(NamedTuple):
  """NamedTuple documenting the original order of expt/refl pair in input
  as well as, optionally, paths to the file containing substitute refls"""
  original_iid: int
  expt: Experiment
  refl: flex.reflection_table
  substitute_path: str = ''


def log_start_and_end(func: Callable) -> Callable:
  """Method wrapper to automatically log START and END of their execution"""
  @functools.wraps(func)
  def wrapped_func(*args, **kwargs):
    self = args[0]
    self.log("START: " + func.__doc__, level=0)
    return_value = func(*args, **kwargs)
    self.log("END:   " + func.__doc__, level=0)
    return return_value
  return wrapped_func


class Substitute(worker):
  """Generalised and version of the specific worker. It is more general and
  does not require specific file structure, but might be slower. Works much
  faster if ran after input.parallel_file_load.balance=global2 balancer."""

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    self.uuid_map = {}
    super().__init__(params=params, mpi_helper=mpi_helper,
                     mpi_logger=mpi_logger)

  def __repr__(self):
    return "Substitute expts or refls with equivalents from alternative source"

  def run(self,
          experiments: ExperimentList,
          reflections: flex.reflection_table,
          ) -> Tuple[ExperimentList, flex.reflection_table]:
    self.uuid_map = self.generate_uuid_map()
    new_expts, new_refls = self.substitute_refls(experiments, reflections)
    self.mpi_helper.comm.barrier()
    data_counter(self.params).count(new_expts, new_refls)
    return new_expts, new_refls

  @log_start_and_end
  def substitute_refls(self,
                       expts: ExperimentList,
                       refls: flex.reflection_table,
                       ) -> Tuple[ExperimentList, flex.reflection_table]:
    """Substitute input refls with Miller-matched ones from substitute input"""
    new_pairs = []
    self.log(f"# Assigned expts:   {len(expts):9d}", level=1)
    self.log(f"# Assigned refls:   {len(refls):9d}", level=1)
    located_pairs = self.pack_and_sort_pairs(expts, refls)
    for lp in located_pairs:
      uuid = lp.expt.identifier
      sub_refls = self.read_substitute_refl(lp.substitute_path)
      sub_refls = sub_refls.select_on_experiment_identifiers([uuid])
      self.log(f"Experiment uuid: {uuid}", level=0)
      self.log(f"# Original refls:   {len(lp.refl):9d}", level=0)
      self.log(f"# Substitute refls: {len(sub_refls):9d}", level=0)
      matches = match_indices(lp.refl['miller_index'], sub_refls['miller_index'])
      if self.params.substitute.apply:
        sub_selection = matches.pairs().column(1)
        sub_refls = sub_refls.select(sub_selection)
      else:
        input_selection = matches.pairs().column(0)
        sub_refls = lp.refl.select(input_selection)
      self.log(f"# Matching refls:   {len(sub_refls):9d}", level=0)
      if len(sub_refls) > 10:        # basic sanity check
        new_pairs.append(LocatedPair(lp.original_iid, lp.expt, sub_refls))
    expts, refls = self.unpack_and_unsort_pairs(new_pairs)
    self.log(f"# Returned expts:   {len(expts):9d}", level=1)
    self.log(f"# Returned refls:   {len(refls):9d}", level=1)
    return expts, refls

  @log_start_and_end
  def generate_uuid_map(self) -> Dict[str, str]:
    """Generate a dictionary with {uuid: substitute refl file path} pairs"""
    sub_paths = self.get_substitute_paths()
    mpi_rank = self.mpi_helper.rank
    mpi_size = self.mpi_helper.size
    rank_sub_paths = sub_paths[mpi_rank::mpi_size]
    self.log(f"Generate uuid_map for {len(rank_sub_paths)} out of"
             f"{len(sub_paths)} substitute paths", level=1)
    uuid_pairs = list()
    for rsp in rank_sub_paths:
      rank_sub_refl = self.read_substitute_refl(rsp)
      uuids = rank_sub_refl.experiment_identifiers().values()
      uuid_pairs.extend([(uuid, rsp) for uuid in uuids])
    self.log(f"Reported {len(uuid_pairs):9d} uuid_map entries", level=1)
    uuid_pairs_gathered = self.mpi_helper.comm.gather(uuid_pairs, root=0)
    uuid_pairs = list()
    if mpi_rank == 0:
      for upg in uuid_pairs_gathered:
        uuid_pairs.extend(upg)
    uuid_map = {p[0]: p[1] for p in uuid_pairs} if mpi_rank == 0 else dict()
    uuid_map = self.mpi_helper.comm.bcast(uuid_map, root=0)
    self.log(f"Received {len(uuid_map):9d} uuid_map entries", level=1)
    return uuid_map

  def get_substitute_paths(self) -> List[str]:
    substitute_paths = []
    for si in self.params.substitute.input:
      substitute_paths.extend(glob(si))
    return substitute_paths

  @log_start_and_end
  def pack_and_sort_pairs(self,
                          expts: ExperimentList,
                          refls: flex.reflection_table
                          ) -> List[LocatedPair]:
    """Pack and sort expt/refl pairs using substitute paths as the key"""
    located_pairs = []
    for iid, expt in enumerate(expts):
      assert len(expt.imageset.paths()) == 1 and len(expt.imageset) == 1
      refl = refls.select(refls['id'] == iid)
      if uuid := self.uuid_map.get(expt.identifier, False):
        located_pairs.append(LocatedPair(iid, expt, refl, uuid))
    located_pairs.sort(key=lambda p: p.substitute_path)
    return located_pairs

  @log_start_and_end
  def unpack_and_unsort_pairs(self,
                              located_pairs: List[LocatedPair]
                              ) -> Tuple[ExperimentList, flex.reflection_table]:
    """Unpack expt/refl pairs and return them in the original order"""
    expts = ExperimentList()
    refls = []
    located_pairs.sort(key=lambda p: p.original_iid)
    for located_pair in located_pairs:
      expts.append(located_pair.expt)
      refls.append(located_pair.refl)
    refls = flex.reflection_table.concat(refls) \
        if refls else flex.reflection_table()
    return expts, refls

  @functools.lru_cache(maxsize=1)
  def read_substitute_refl(self, path: str) -> flex.reflection_table:
    self.log(f"Reading file: {path}", level=0)
    return flex.reflection_table.from_file(path)

  def log(self, text: str, level: int = 0) -> None:
    if self.params.output.log_level <= level:
      self.logger.log(text)
