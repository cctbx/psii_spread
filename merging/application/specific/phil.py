from __future__ import division

phil_str = """
specific {
  integration {
    refl = None
      .type = str
      .help = directory glob containing the integration.refls files to be tapped for the
      .help = SPREAD shoeboxes.
    expt = None
      .type = str
  }
}
"""
