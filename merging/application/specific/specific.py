from __future__ import division
from xfel.merging.application.worker import worker
from dials.array_family import flex
from xfel.merging.application.utils.data_counter import data_counter
from dxtbx.model.experiment_list import ExperimentList
from cctbx import miller

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.

class specific(worker):

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super(specific, self).__init__(params=params, mpi_helper=mpi_helper, mpi_logger=mpi_logger)
    self.position_shifts = flex.double()

  def __repr__(self):
    return "Replace strong indexed shoeboxes with integrated prediction shoeboxes"

  def run(self, experiments, reflections):
    new_reflections = []
    new_experiments = ExperimentList()
    #loop through experiments
    for iid, expt in enumerate(experiments):
      iset = expt.imageset
      assert len(iset) == 1 # handle one at a time, even if multiple lattices/image
      path = iset.paths()[0]
      indx = iset.indices()[0]

      # parse out the run number specific to this case
      run = int(path.split('/')[-2])
      integ_refls_file = self.params.specific.integration.refl%(run,indx)
      integ_expt_file = self.params.specific.integration.expt%(run,indx)
      if self.mpi_helper.rank==0:
        print("Looking for ",integ_refls_file)

      src_refls = reflections.select_on_experiment_identifiers([expt.identifier])

      read_refls = flex.reflection_table.from_file(integ_refls_file)
      read_expts = ExperimentList.from_file(integ_expt_file)

      if self.mpi_helper.rank==0:
        print("source reflections",len(src_refls))

      dst_refls = read_refls.select_on_experiment_identifiers([expt.identifier])
      if self.mpi_helper.rank==0: print("destination reflections",len(dst_refls))

      #print(list(src_refls.keys()), list(dst_refls.keys()))

      matches = miller.match_indices(src_refls['miller_index'], dst_refls['miller_index'])
      pairs = matches.pairs()
      src_selection = pairs.column(0)
      dst_selection = pairs.column(1)
      if self.mpi_helper.rank==0:
        print("matched dst reflections", len(dst_selection))

      dst_refls = dst_refls.select(dst_selection)
      if len(dst_refls) > 10: # basic sanity check
        new_reflections.append(dst_refls) # to go with expts.
        new_experiments.append(expt)
    new_reflections = flex.reflection_table.concat(new_reflections)
    self.mpi_helper.comm.barrier()

    data_counter(self.params).count(new_experiments, new_reflections)
    return new_experiments, new_reflections
