from __future__ import annotations
from __future__ import division

import logging
import math

from dials.array_family import flex
from libtbx import Auto
from scitbx.matrix import col, sqr

logger = logging.getLogger(__name__)
"""
Class to determine effective domain size for a crystal given a set of indexed reflections
"""

def one_panel(axes, expt, refls, params):
            """Determine optimal domain size model (monochromatic).  Assume mosaic rotation is fixed."""
            all_crystals = []
            nv_acceptance_flags = flex.bool(len(refls["id"]))
            from dxtbx.model import MosaicCrystalSauter2014

            reference_FW = 2.0 * expt.crystal.get_half_mosaicity_deg()
            reference_dom_sz = expt.crystal.get_domain_size_ang()
            excursion_rad = refls["delpsical.rad"]
            delta_psi_deg = excursion_rad * 180.0 / math.pi
            mean_excursion = flex.mean(delta_psi_deg)

            crystal = MosaicCrystalSauter2014(expt.crystal)
            expt.crystal = crystal
            beam = expt.beam
            miller_indices = refls["miller_index"]
            diffs = refls['xyzobs.px.value']-refls['xyzcal.px']

            two_thetas = crystal.get_unit_cell().two_theta(
                miller_indices, beam.get_wavelength(), deg=True
            )
            dspacings = crystal.get_unit_cell().d(miller_indices)

            # First -- try to get a reasonable envelope for the observed excursions.
            # minimum of three regions; maximum of 50 measurements in each bin
            logger.info("fitting parameters on %d spots", len(excursion_rad))
            n_bins = min(max(3, len(excursion_rad) // 25), 50)
            bin_sz = len(excursion_rad) // n_bins
            logger.info("nbins %s bin_sz %s", n_bins, bin_sz)
            order = flex.sort_permutation(two_thetas)
            two_thetas_env = flex.double()
            dspacings_env = flex.double()
            excursion_rads_env = flex.double()
            for x in range(0, n_bins):
                subset = order[x * bin_sz : (x + 1) * bin_sz]
                two_thetas_env.append(flex.mean(two_thetas.select(subset)))
                dspacings_env.append(flex.mean(dspacings.select(subset)))
                excursion_rads_env.append(
                    flex.max(flex.abs(excursion_rad.select(subset)))
                )

            # Second -- parameter fit
            # solve the normal equations
            sum_inv_u_sq = flex.sum(dspacings_env * dspacings_env)
            sum_inv_u = flex.sum(dspacings_env)
            sum_te_u = flex.sum(dspacings_env * excursion_rads_env)
            sum_te = flex.sum(excursion_rads_env)
            Normal_Mat = sqr((sum_inv_u_sq, sum_inv_u, sum_inv_u, len(dspacings_env)))
            Vector = col((sum_te_u, sum_te))
            solution = Normal_Mat.inverse() * Vector
            s_ang = 1.0 / (2 * solution[0])
            logger.info("Best LSQ fit Scheerer domain size is %9.2f ang", s_ang)

            k_degrees = solution[1] * 180.0 / math.pi
            logger.info(
                "The LSQ full mosaicity is %8.5f deg; half-mosaicity %9.5f",
                2 * k_degrees,
                k_degrees,
            )

            # Get default eta_rad if not Auto, else derive from expt.crystal
            eta_rad = h if (h := params.trumpet.eta_rad) is not Auto \
              else max(reference_FW * math.pi / 180., 1e-15)

            # coerce the estimates to be positive for max-likelihood
            lower_limit_domain_size = (
                math.pow(crystal.get_unit_cell().volume(), 1.0 / 3.0) * 3
            )  # params.refinement.domain_size_lower_limit

            d_estimate = max(s_ang, lower_limit_domain_size)

            from serialtbx.mono_simulation.max_like import block_size_simplex
            try:
              M = block_size_simplex(
                d_i=dspacings,
                psi_i=excursion_rad,
                eta_rad=eta_rad,
                Deff=d_estimate,
              )
            except ValueError as e:
              print("BLOCKSIZE REFINEMENT SIMPLEX VALUEERROR %f %f"%( s_ang, lower_limit_domain_size))
              return 1 # indicates outlier
            logger.info(
                "ML: mosaicity FW=%4.2f deg, Dsize=%5.0fA on %d spots",
                M.x[1] * 180.0 / math.pi,
                2.0 / M.x[0],
                len(two_thetas),
            )
            tan_phi_rad_ML = dspacings / (2.0 / M.x[0])
            tan_phi_deg_ML = tan_phi_rad_ML * 180.0 / math.pi
            tan_outer_deg_ML = tan_phi_deg_ML + 0.5 * M.x[1] * 180.0 / math.pi

            # Only set the flags for those reflections that were indexed for this lattice
            nv_acceptance_flags = flex.abs(delta_psi_deg) < tan_outer_deg_ML,

            # params.refinement.mosaic.enable_AD14F7B: # Excursion vs resolution fit
            print(
                    "ML: mosaicity FW=%5.3f %5.3f°, Dsize=%8.0f  %8.0fÅ on %d spots %s"%(
M.x[1] * 180.0 / math.pi,
reference_FW,
2.0 / M.x[0],
reference_dom_sz,
len(two_thetas),
expt.identifier
                    )
                )

            axes.plot(two_thetas, delta_psi_deg, "bo")
            minplot = flex.min(two_thetas)
            axes.plot([0, minplot], [mean_excursion, mean_excursion], "k-")
            LR = flex.linear_regression(two_thetas, delta_psi_deg)
            model_y = LR.slope() * two_thetas + LR.y_intercept()
            axes.plot(two_thetas, model_y, "k-")
            outlier_condition = (2.0 / M.x[0]) < params.trumpet.outlier.deff

            axes.set_title(
                    "rotation FW=%5.3f°, Dsize=%5.0fÅ on %d spots"
                    % (M.x[1] * 180.0 / math.pi, 2.0 / M.x[0], len(two_thetas)),
                    color = ('r' if outlier_condition else 'k')
                )
            axes.plot(two_thetas, tan_phi_deg_ML, "r.")
            axes.plot(two_thetas, -tan_phi_deg_ML, "r.")
            axes.plot(two_thetas, tan_outer_deg_ML, "g.")
            axes.plot(two_thetas, -tan_outer_deg_ML, "g.")
            axes.set_xlim([0, params.trumpet.AD1TF7B_MAX2T])
            axes.set_ylim([-params.trumpet.AD1TF7B_MAXDP, params.trumpet.AD1TF7B_MAXDP])
            axes.set_xlabel("2θ(°)"); axes.set_ylabel("ΔΨ(°)")

            # set the new conditions
            expt.crystal.set_half_mosaicity_deg(M.x[1] * 90./math.pi)
            expt.crystal.set_domain_size_ang(2.0 / M.x[0])

            return int(outlier_condition)

