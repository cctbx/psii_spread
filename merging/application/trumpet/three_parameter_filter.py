from __future__ import division
from dials.array_family import flex
from scitbx.matrix import col
import math
from math import sqrt
import numpy as np


class deliverable2: # derived from annulus/rmsd2
  def __init__(self,axes, experiment, reflections, params, TF, DF, verbose=False):
    self.params = params
    self.uc = self.params.scaling.unit_cell

    from cctbx.crystal import symmetry
    S = symmetry(unit_cell=self.params.scaling.unit_cell,
                 space_group_info=self.params.scaling.space_group)

    #deliverable 2:  resolution bins of total rms offset, radial offset, tangential offset, correlation
    reflections["offset vector"] = reflections["xyzcal.px"]-reflections["xyzobs.px.value"]
    # the z component needs to be zeroed out because it's treated inconsistently between dials & merge
    parts = reflections["offset vector"].parts()
    reflections["offset vector"] = flex.vec2_double([(parts[0][idx],parts[1][idx])
                                                    for idx in range(len(reflections))])

    reflections["squared diff"] = reflections["offset vector"].dot(reflections["offset vector"])

    # geometry
    E0 = experiment
    B = E0.beam
    D = E0.detector
    s0_vector = -col(B.get_sample_to_source_direction())
    """
    S1_vector
    better to just project these two vectors on to the detector, then convert mm to pixels, then take the
    vector diff in pixels to get radial direction.  Then rotate 90 degrees on the normal to get tangential.
    """
    s0_on_panel_px = flex.vec2_double([D[ipn].get_ray_intersection_px(s0_vector)
                     for ipn in reflections["panel"]])
    s1_on_panel_px = flex.vec2_double(
                     [D[reflections["panel"][idx]].get_ray_intersection_px(reflections["s1"][idx])
                     for idx in range(len(reflections))])
    radial_direction_unit = (s1_on_panel_px - s0_on_panel_px).each_normalize()
    tangential_direction_unit = radial_direction_unit.rotate_around_origin(math.pi/2.)

    radial_offset = (reflections["offset vector"].dot(radial_direction_unit)) # scalar offset length, not vector
    tangential_offset = reflections["offset vector"].dot(tangential_direction_unit) # scalar offset length
    radial_sq_diff = radial_offset * radial_offset
    tangential_sq_diff = tangential_offset * tangential_offset

    SIG_CUT = params.trumpet.residuals.cutoff
    reflections["tangential_outlier"] = (tangential_sq_diff > (SIG_CUT*TF)**2)
    if verbose: print ("%d total reflections, %d are tangential outliers"%(len(reflections),
           reflections["tangential_outlier"].count(True)))

    # not sure why but my library function uses a 1000x prefactor
    three_feature_vector = [(1000.*radial_offset[ix],1000.*tangential_offset[ix],reflections['delpsical.rad'][ix])
                            for ix in range(len(reflections))]

    reflections["3-feature_outlier"] = (flex.double(DF.emp_cov.mahalanobis(three_feature_vector)) > SIG_CUT**2)
    if verbose: print ("%d total reflections, %d are 3-feature outliers"%(len(reflections),
           reflections["3-feature_outlier"].count(True)))
    self.both_outlier = reflections["tangential_outlier"] | reflections["3-feature_outlier"]
    if verbose: print ("%d total reflections, %d are both outliers"%(len(reflections), self.both_outlier.count(True)))
    self.outlier_condition = int( self.both_outlier.count(True) > params.trumpet.outlier.tff )
    if params.trumpet.plot_all.enable:
      def div1000(x,pos):
        return "%.1f"%(x/1000.) # because my library function uses a 1000x prefactor
      axes.set_title( "Observed vs. Calculated spot position" )
      #axes.plot(1000.*radial_offset, 1000.*tangential_offset, "r.") # simple plot without deltapsi colors
      axes.set_xlabel("ΔR (pixels)"); axes.set_ylabel("ΔT (pixels)")
      axes.xaxis.set_major_formatter(div1000)
      axes.yaxis.set_major_formatter(div1000)

      from sklearn.covariance import EmpiricalCovariance as EC
      cov = DF.emp_cov # three-feature covariance fit
      cov_slice = EC()

      cov_slice.location_ = np.array([ cov.location_[0], cov.location_[1] ])
      cov_slice.covariance_ = np.array([ cov.covariance_[ 0,0 ], cov.covariance_[ 0,1 ],
                                           cov.covariance_[ 1,0 ], cov.covariance_[ 1,1 ] ])
      cov_slice.covariance_ = cov_slice.covariance_.reshape((2,2))
      cov_slice.precision_ = np.array([ cov.precision_[ 0,0 ], cov.precision_[ 0,1 ],
                                           cov.precision_[ 1,0 ], cov.precision_[ 1,1 ] ])
      cov_slice.precision_ = cov_slice.precision_.reshape((2,2))

      import matplotlib as mpl
      # Show contours of the distance functions
      xx, yy = np.meshgrid(
          np.linspace((cov_slice.location_[0]-5*sqrt(cov_slice.covariance_[0,0])),
                      (cov_slice.location_[0]+5*sqrt(cov_slice.covariance_[0,0])), 100),
          np.linspace((cov_slice.location_[1]-5*sqrt(cov_slice.covariance_[1,1])),
                      (cov_slice.location_[1]+5*sqrt(cov_slice.covariance_[1,1])), 100),
        )
      zz = np.c_[xx.ravel(), yy.ravel()]
      mahal_emp_cov = cov_slice.mahalanobis(zz)
      mahal_emp_cov = mahal_emp_cov.reshape(xx.shape)
      emp_cov_contour = axes.contour(xx, yy, np.sqrt(mahal_emp_cov),
                                  levels=[1.,2.,3.,4.],
                                  cmap=mpl.pyplot.cm.PuBu_r,
                                  norm = mpl.colors.Normalize(vmin=0.0, vmax=5.0), #sensible color range
                                  linestyles='dashed')
      message = "%d reflections, %d are %.0fσ outliers"%(len(reflections),
                self.both_outlier.count(True), params.trumpet.residuals.cutoff)
      axes.annotate(message,
                     xy = (cov_slice.location_[0]-4.0*sqrt(cov_slice.covariance_[0,0]),
                           cov_slice.location_[1]-6.0*sqrt(cov_slice.covariance_[1,1])),color="blue"
                 )
      axes.set_xlim((cov_slice.location_[0]-5*sqrt(cov_slice.covariance_[0,0]),
                     cov_slice.location_[0]+5*sqrt(cov_slice.covariance_[0,0])))
      axes.set_ylim((cov_slice.location_[0]-5*sqrt(cov_slice.covariance_[0,0]),
                     cov_slice.location_[0]+5*sqrt(cov_slice.covariance_[0,0])))

      data = reflections['delpsical.rad'] * (180/math.pi)

      from matplotlib.colors import Normalize
      # initialize the color map
      vmin=-0.05; vmax=0.05
      norm = Normalize(vmin=vmin, vmax=vmax)

      colormap="RdYlGn_r" # https://matplotlib.org/examples/color/colormaps_reference.html
      cmap = mpl.colormaps[colormap]
      from matplotlib import cm
      sm = cm.ScalarMappable(norm=norm, cmap=cmap)
      color_vals = np.linspace(vmin, vmax, 11)
      sm.set_array(color_vals) # needed for colorbar

      # as in detector_residuals: norm, cmap, color_vals, sm
      axes.scatter(1000.*radial_offset, 1000.*tangential_offset,
                   c = data, norm=norm, cmap = cmap, linewidths=0, s=params.trumpet.residuals.dot_size)

      if sm is not None and color_vals is not None:
        colorbar_units = "°"
        cb = axes.figure.colorbar(sm, ticks=color_vals, ax=axes)
        cb.ax.set_yticklabels(["%3.2f%s"%(i,colorbar_units) for i in color_vals], fontsize=7)
