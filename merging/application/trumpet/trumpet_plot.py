from __future__ import division
from xfel.merging.application.worker import worker
from dials.array_family import flex
import math
from scitbx.matrix import col
from dxtbx.model.experiment_list import ExperimentList

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.
import three_parameter_filter, block_size, pixel_energy_histograms

class trumpet_plot(worker):

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super(trumpet_plot, self).__init__(params=params, mpi_helper=mpi_helper, mpi_logger=mpi_logger)
    self.position_shifts = flex.double()

  def __repr__(self):
    return "Calculate and analyze the trumpet plot"

  def cell_stats(self):
      #boilerplate to read covariance matrix
      from uc_metrics.clustering.util import get_population_permutation # implicit import
      import pickle

      class Empty: pass
      if self.mpi_helper.rank == 0:
        with open(self.params.filter.unit_cell.cluster.covariance.file,'rb') as F:
          data = pickle.load(F)
          E=Empty()
          E.features_ = data["features"]
          E.sample_name = data["sample"]
          E.output_info = data["info"]
          pop=data["populations"]
          self.logger.main_log("Focusing on cluster component %d from previous analysis of %d cells"%(
            self.params.filter.unit_cell.cluster.covariance.component, len(pop.labels)))
          self.logger.main_log("%s noise %d order %s"%(pop.populations, pop.n_noise_, pop.main_components))

          legend = pop.basic_covariance_compact_report(feature_vectors=E).getvalue()
          self.logger.main_log(legend)
          self.logger.main_log("Applying Mahalanobis cutoff of %.3f"%(self.params.filter.unit_cell.cluster.covariance.mahalanobis))
        transmitted = data
      else:
        transmitted = None
      # distribute cluster information to all ranks
      self.cluster_data = self.mpi_helper.comm.bcast(transmitted, root=0)
      # pull out the index numbers of the unit cell parameters to be used for covariance matrix
      self.cluster_data["idxs"]=[["a","b","c","alpha","beta","gamma"].index(F) for F in self.cluster_data["features"]]

  def cell_one_panel(self, axes, expt, refls):
    import numpy as np
    from math import sqrt
    experiment_unit_cell = expt.crystal.get_unit_cell()
    P = experiment_unit_cell.parameters()
    features = [P[idx] for idx in self.cluster_data["idxs"]]
    features = np.array(features).reshape(1,-1)
    cov=self.cluster_data["populations"].fit_components[self.params.filter.unit_cell.cluster.covariance.component]
    m_distance = sqrt(cov.mahalanobis(features))
    message = ""
    for iif, f in enumerate(features[0]):
      #self.cluster_data["features"]=['a','c']
      #features[0]=np.array([106,307])
      message = message + " %s=%6.1f"%(self.cluster_data["features"][iif], f)
    message = message +" at %4.2fσ"%m_distance
    axes.scatter([features[0][0]], [features[0][1]], 6, color="blue")

    jitem = self.params.filter.unit_cell.cluster.covariance.component
    ax = [0,1] # self.cluster_data['idxs'] # = [0,2]
    from sklearn.covariance import EmpiricalCovariance as EC
    cov_slice = EC()
    cov_slice.location_ = np.array([ cov.location_[ax[0]], cov.location_[ax[1]] ])
    cov_slice.covariance_ = np.array([ cov.covariance_[ ax[0],ax[0] ], cov.covariance_[ ax[0],ax[1] ],
                                           cov.covariance_[ ax[1],ax[0] ], cov.covariance_[ ax[1],ax[1] ] ])
    cov_slice.covariance_ = cov_slice.covariance_.reshape((2,2))
    cov_slice.precision_ = np.array([ cov.precision_[ ax[0],ax[0] ], cov.precision_[ ax[0],ax[1] ],
                                           cov.precision_[ ax[1],ax[0] ], cov.precision_[ ax[1],ax[1] ] ])
    cov_slice.precision_ = cov_slice.precision_.reshape((2,2))
    axes.scatter( [cov_slice.location_[0]], [cov_slice.location_[1]], 6.0, color="orange", label="mean")

    import matplotlib as mpl
    # Show contours of the distance functions
    xx, yy = np.meshgrid(
          np.linspace(cov_slice.location_[0]-5*sqrt(cov_slice.covariance_[0,0]), cov_slice.location_[0]+5*sqrt(cov_slice.covariance_[0,0]), 100),
          np.linspace(cov_slice.location_[1]-5*sqrt(cov_slice.covariance_[1,1]), cov_slice.location_[1]+5*sqrt(cov_slice.covariance_[1,1]), 100),
        )
    zz = np.c_[xx.ravel(), yy.ravel()]
    mahal_emp_cov = cov_slice.mahalanobis(zz)
    mahal_emp_cov = mahal_emp_cov.reshape(xx.shape)
    emp_cov_contour = axes.contour(xx, yy, np.sqrt(mahal_emp_cov),
                                  levels=[1.,2.,3.,4.],
                                  cmap=mpl.pyplot.cm.PuBu_r,
                                  norm = mpl.colors.Normalize(vmin=0.0, vmax=5.0), #sensible color range
                                  linestyles='dashed')
    axes.annotate(message,
                     xy = (cov_slice.location_[0]-4.0*sqrt(cov_slice.covariance_[0,0]),
                           cov_slice.location_[1]-4.0*sqrt(cov_slice.covariance_[1,1])),color="blue"
                 )

    axes.set_xlabel("$%s-$axis (Å)"%(self.cluster_data["features"][0]))
    axes.set_ylabel("$%s-$axis (Å)"%(self.cluster_data["features"][1]))
    axes.set_xlim([cov_slice.location_[0]-5*sqrt(cov_slice.covariance_[0,0]),
                   cov_slice.location_[0]+5*sqrt(cov_slice.covariance_[0,0])])
    axes.set_ylim([cov_slice.location_[1]-5*sqrt(cov_slice.covariance_[1,1]),
                   cov_slice.location_[1]+5*sqrt(cov_slice.covariance_[1,1])])
    outlier_condition = m_distance > self.params.trumpet.outlier.ucell
    if outlier_condition:
      axes.set_xlabel("$%s-$axis (Å)"%(self.cluster_data["features"][0]), color = 'r')

    return int(outlier_condition)

  def dials_refine(self, experiment, input_reflections, verbose=False):
    from dials.algorithms.refinement.refiner import RefinerFactory
    experiments = ExperimentList(); experiments.append(experiment)
    reflections = flex.reflection_table()
    reflections.extend(input_reflections)
    reflections.reset_ids()

    refiner = RefinerFactory.from_parameters_data_experiments(self.params,
      reflections, experiments)
    Initial_position = refiner.get_experiments()[0].detector[0].get_beam_centre_lab(refiner.get_experiments()[0].beam.get_s0())
    Initial_position_00 = refiner.get_experiments()[0].detector[0].get_lab_coord((0,0))

    #print("II",refiner.get_experiments()[0].crystal)
    #print("II",reflections[0])
    history = refiner.run()
    NEWR = refiner.predict_for_reflection_table(reflections=reflections)
    #print("JJ",NEWR[0])

    #print(list(reflections.keys()))
    #print(list(NEWR.keys()))
    #for item in history["rmsd"]:
    #  print("XX","%5.2f %5.2f %8.5f"%(item[0],item[1],180.*item[2]/math.pi))

    #print("XX",refiner.selection_used_for_refinement().count(True),"spots used for refinement")
    Final_position = refiner.get_experiments()[0].detector[0].get_beam_centre_lab(refiner.get_experiments()[0].beam.get_s0())
    Final_position_00 = refiner.get_experiments()[0].detector[0].get_lab_coord((0,0))
    #print("XX",refiner.get_experiments()[0].crystal)
    self.position_shifts.append((-Final_position[2])-(-Initial_position[2]))
    if verbose:
      print("Distance initial %.3f mm, final %.3f mm, Z change %.3f mm"%(-Initial_position[2], -Final_position[2], (-Final_position[2])-(-Initial_position[2])) +
          " Z shift %.3fmm"%( (col(Final_position_00)-col(Initial_position_00))[2] ) +
          " Total XYZ shift %.3fmm"%((col(Final_position_00)-col(Initial_position_00)).length())
      )

    return refiner.get_experiments()[0], NEWR

  def run(self, experiments, reflections):
    if self.params.filter.unit_cell.cluster.covariance.file is not None:
      # if file, read self.cluster_data from file and apply the Mahalanobis filter
      self.cell_stats()
    else:
      # if no file, recover the self.cluster_data in memory from previously run worker
      # but do not apply the Mahalanobis filter (as it has already been done)
      self.cluster_data = self.common_store["cluster_data"]

    from xfel.merging.application.utils.data_counter import data_counter
    if self.params.trumpet.debug_rank_0 and self.mpi_helper.rank != 0:
      data_counter(self.params).count(experiments, reflections)
      return experiments, reflections

    if self.params.trumpet.plot_all.enable:
      new_experiments = ExperimentList()
      new_reflections = []
      peh = pixel_energy_histograms.all_panels()
      import matplotlib
      import matplotlib.pyplot as plt
      import matplotlib.gridspec as gridspec
      if self.params.trumpet.plot_all.savepng:
        matplotlib.use('Agg')
        fig = plt.figure(figsize=(14,8))
        gs = gridspec.GridSpec(nrows=2, ncols=3, height_ratios=[1, 1])
      pixel_energy_outliers = 0
      block_size_outliers = 0
      unit_cell_outliers = 0
      radial_transverse_outliers = 0
      for iid, expt in enumerate(experiments):
        outlier = 0
        refl = reflections.select(reflections["id"] == iid)

        if self.params.trumpet.dials_refine: # Here the expt is modified with a new detector and cell
          # Normally set dials_refine to False since we now use the tsrefine & drefine workers
          expt, refl = self.dials_refine(expt,refl)

        if not self.params.trumpet.plot_all.savepng:
          fig = plt.figure(figsize=(14,8))
          gs = gridspec.GridSpec(nrows=2, ncols=3, height_ratios=[1, 1])
        axspec = fig.add_subplot(gs[0, :])

        pixel_energy_outlier = peh.one_panel(axspec, expt, refl, self.params)
        outlier += pixel_energy_outlier; pixel_energy_outliers += pixel_energy_outlier
        axtrump = fig.add_subplot(gs[1, 0])

        block_size_outlier = block_size.one_panel(axtrump, expt, refl, self.params) # Expt is modified with a new block size
        outlier += block_size_outlier; block_size_outliers += block_size_outlier
        axcell = fig.add_subplot(gs[1, 1])

        unit_cell_outlier = self.cell_one_panel(axcell, expt, refl)
        outlier += unit_cell_outlier; unit_cell_outliers += unit_cell_outlier
        axresid = fig.add_subplot(gs[1, 2])
        DD = three_parameter_filter.deliverable2(
            axresid, expt, refl, self.params,
            TF = self.common_store["store_transverse_sigma"], # tangential offset sigma (global), previously calculated
            DF = self.common_store["store_three_param_fit"]
        )

        radial_transverse_outlier = DD.outlier_condition
        outlier += radial_transverse_outlier; radial_transverse_outliers += radial_transverse_outlier
        # ad hoc filter:  lattice is untrustworthy if two outlier criteria are met.
        if outlier >= 2:
          axspec.annotate("Rejected outlier",xy=(0.8,0.95),
            xycoords="figure fraction",size='large',color='red')
        if self.mpi_helper.rank == 0:
          if self.params.trumpet.plot_all.savepng:
            fig.savefig("%03d.png"%iid)
            fig.clf()
          else: plt.show()
        # optionally reject outliers in the saved files
        if outlier >= 2 and self.params.trumpet.outlier.remove:
          pass
        else:
          new_experiments.append(expt)
          new_reflections.append(refl)
      #tally up the outlier criteria
      pixel_energy_outliers = self.mpi_helper.sum(pixel_energy_outliers)
      block_size_outliers = self.mpi_helper.sum(block_size_outliers)
      unit_cell_outliers = self.mpi_helper.sum(unit_cell_outliers)
      radial_transverse_outliers = self.mpi_helper.comm.reduce(radial_transverse_outliers, self.mpi_helper.MPI.SUM)
      if self.mpi_helper.rank==0:

        self.logger.main_log(
        "lattices flagged for misalignment of pixel-energy histogram: %d\n"%pixel_energy_outliers +
        "lattices flagged outside the block size cutoff: %d\n"%block_size_outliers +
        "lattices flagged for non-conforming unit cell: %d\n"%unit_cell_outliers +
        "lattices flagged for outliers on the three-feature plot: %d\n"%radial_transverse_outliers +
        "\n"
        )

      new_reflections = flex.reflection_table.concat(new_reflections)

      if False: # Create plots showing per-shot detector movement from dials.refine
        position_shifts = self.mpi_helper.aggregate_flex(self.position_shifts, flex.double)
        from matplotlib import pyplot as plt
        if self.mpi_helper.rank == 0:
          plt.hist(position_shifts)
          plt.title("Z Shifts from per-shot detector refinement hierarchy 0")
          plt.xlabel("Z-shift (mm)")
          plt.ylabel("Count")
          stats = flex.mean_and_variance(position_shifts)
          plt.annotate("Mean %.3f Stddev %.3f"%(stats.mean(),stats.unweighted_sample_standard_deviation()),xy=(0.1,0.1))
          plt.show()

      # done with all ranks, aggregate all moments
      if False:
       moment2_fee = self.mpi_helper.aggregate_flex(peh.moment2_fee, flex.double)
       moment2_brg = self.mpi_helper.aggregate_flex(peh.moment2_brg, flex.double)
       moment3_fee = self.mpi_helper.aggregate_flex(peh.moment3_fee, flex.double)
       moment3_brg = self.mpi_helper.aggregate_flex(peh.moment3_brg, flex.double)
       if self.mpi_helper.rank == 0:
        print("Plotting 2nd-moment results from %d lattices"%(len(moment2_fee)))
        print("Plotting 2nd-moment resxlts from %d lattices"%(len(moment2_brg)))
        from matplotlib import pyplot as plt
        #from IPython import embed; embed()
        plt.scatter(moment2_fee,moment2_brg,c="red")
        plt.title("standard deviation results from %d lattices"%(len(moment2_fee)))
        plt.xlabel("Std dev FEE spectra (eV)")
        plt.ylabel("Std dev Bragg distribution (eV)")
        plt.show()
        plt.scatter(moment3_fee,moment3_brg,c="blue")
        plt.title("skewness results from %d lattices"%(len(moment3_fee)))
        plt.xlabel("Skewness FEE spectra")
        plt.ylabel("Skewness Bragg distribution")
        plt.show()

    data_counter(self.params).count(new_experiments, new_reflections)
    return new_experiments, new_reflections

  def run_with_refinery_predictions(self, experiments, reflections):
    if self.mpi_helper.rank != 0:
      from xfel.merging.application.utils.data_counter import data_counter
      data_counter(self.params).count(experiments, reflections)
      return experiments, reflections

    from dials.algorithms.refinement import RefinerFactory
    refiner = RefinerFactory.from_parameters_data_experiments(
                self.params, reflections, experiments
    )
    print("running refiner")
    refiner.run()
    print("ran")
    experiments = refiner.get_experiments()
    predicted = refiner.predict_for_indexed()
    # note that we are changing the predictions given by the input refls:
    reflections["xyzcal.mm"] = predicted["xyzcal.mm"]
    reflections["xyzcal.px"] = predicted["xyzcal.px"] # added by NKS for completeness
    #
    # check to make sure before/after are the same?
    #
    reflections["entering"] = predicted["entering"]
    centroids = reflections.select(refiner.selection_used_for_refinement())

    from dials.algorithms.indexing.nave_parameters import NaveParameters

    nv = NaveParameters(
                params=self.params,
                experiments=experiments,
                reflections=centroids,
                refinery=refiner,
                graph_verbose=True,
    )
    nv()
    acceptance_flags_nv = nv.nv_acceptance_flags

    from xfel.merging.application.utils.data_counter import data_counter
    data_counter(self.params).count(experiments, reflections)
    return experiments, reflections
