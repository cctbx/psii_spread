from __future__ import division
import numpy as np
from matplotlib import pyplot as plt
from dxtbx.model.experiment_list import ExperimentList
from dxtbx.imageset import ImageSetFactory
from dials.array_family import flex
from cctbx import factor_ev_angstrom
import math
from libtbx.test_utils import approx_equal

def weighted_mean(var, wts):
    """Calculates the weighted mean, these 4 functions taken from stackoverflow"""
    return np.average(var, weights=wts)


def weighted_variance(var, wts):
    """Calculates the weighted variance"""
    return np.average((var - weighted_mean(var, wts))**2, weights=wts)

def weighted_skew(var, wts):
    """Calculates the weighted skewness"""
    return (np.average((var - weighted_mean(var, wts))**3, weights=wts) /
            weighted_variance(var, wts)**(1.5))

def weighted_kurtosis(var, wts):
    """Calculates the weighted skewness"""
    return (np.average((var - weighted_mean(var, wts))**4, weights=wts) /
            weighted_variance(var, wts)**(2))

class all_panels:
 """Handle and store pixel energy histogram evaluation and its statistics"""
 def __init__(self):
  self.moment2_fee = flex.double()
  self.moment2_brg = flex.double()
  self.moment3_fee = flex.double()
  self.moment3_brg = flex.double()

 def one_panel(self, axes, expt, refls, params):
  """Calc. and draw energy plot+histogram on axes for single expt/refls pair"""
  all_wavelengths = flex.double()
  all_weights = flex.double()
  detector = expt.detector
  xtal = expt.crystal
  uc = xtal.get_unit_cell()
  ds = uc.d(refls["miller_index"])
  s0 = expt.beam.get_s0() # this has length 1/λ.  Is it OK?

  for panel_id, shoebox, bbox, d in zip(refls["panel"], refls["shoebox"],
                                        refls["bbox"], ds):
    panel = detector[panel_id]
    shoebox_shape = shoebox.data.focus()[1:]
    x1, x2, y1, y2, _, _ = bbox
    two_thetas = flex.double()
    for s in range(shoebox_shape[0]):
      for f in range(shoebox_shape[1]):
        two_theta = panel.get_two_theta_at_pixel(s0, (f + x1, s + y1))
        two_thetas.append(two_theta)
    sel = shoebox.mask.as_1d() == 5
    two_thetas = two_thetas.select(sel)
    all_wavelengths.extend(2 * d * flex.sin(two_thetas / 2))
    signal = shoebox.data - shoebox.background  # bg=0 if from indexed/refined
    all_weights.extend(signal.as_1d().as_double().select(sel))

  all_energies = factor_ev_angstrom / all_wavelengths
  mean_energy = factor_ev_angstrom / expt.beam.get_wavelength()
  y, x, _ = axes.hist(
      all_energies.as_numpy_array(),
      weights=all_weights.as_numpy_array(),
      bins=params.trumpet.spectrum.nbins,
      range=params.trumpet.spectrum.range
    )
  pixels_weighted_mean = flex.mean_weighted(all_energies, all_weights)
  y_fullscale = np.max(y)
  def plot_notch(x_, color='black'):
      axes.plot([x_, x_], [0, 0.05 * y_fullscale], '-', color=color)
  plot_notch(pixels_weighted_mean, color='black')

  iset = expt.imageset
  assert len(iset) == 1
  image_index = iset.indices()[0]
  path = iset.paths()[0]
  """In this scenario, the input worker is asked to keep_imagesets but not read_image_headers.
    The downstream worker then reconstructs the imageset and reads the spectra and raw data arrays."""
    # Re-generate the image sets using their format classes so we can read the raw data
    # Use the experiments one at a time to not use up memory

  CC = ImageSetFactory.make_imageset(filenames=iset.paths(), single_file_indices=expt.imageset.indices())
  spectrum = CC.get_spectrum(0)

  normed_spectrum_weights = spectrum.get_weights() / flex.max(spectrum.get_weights())
  scaled_spectrum_weights = normed_spectrum_weights * y_fullscale

  axes.plot(
      (params.trumpet.spectrum.recalibration + spectrum.get_energies_eV()),
      scaled_spectrum_weights.as_numpy_array(),
      "-", color="orange")
  spectrum_weighted_mean = flex.mean_weighted(spectrum.get_energies_eV(), scaled_spectrum_weights)
  assert approx_equal(mean_energy, spectrum_weighted_mean,  # agreement of spectrum and beam objects
                      eps=params.trumpet.spectrum.energy_delta_tolerance)
  plot_notch(params.trumpet.spectrum.recalibration + spectrum_weighted_mean, 'orange')

  axes.set_title('Energy histograms: %s, event %d, refls %d'%(path,image_index,len(refls)))
  axes.set_xlabel('Energy (eV)')
  axes.set_ylabel('Counts')
  axes.legend(['pixel weighted mean','FEE spectrum', 'spectrum mean',  'Weighted histogram of all shoebox pixels'])

  print ("eV diff: %4.2f eV"%(pixels_weighted_mean - mean_energy))

  A=params.trumpet.spectrum.recalibration + spectrum_weighted_mean
  B=pixels_weighted_mean
  C_fee = flex.mean_and_variance(spectrum.get_energies_eV(), scaled_spectrum_weights)
  D_brg = flex.mean_and_variance(all_energies, all_weights)
  C = C_fee.gsl_stats_wsd()
  D = D_brg.gsl_stats_wsd()
  self.moment2_fee.append(C)
  self.moment2_brg.append(D)
  #assert approx_equal(C, math.sqrt(weighted_variance(spectrum.get_energies_eV(), scaled_spectrum_weights)),eps=0.02)
  #assert approx_equal(D, math.sqrt(weighted_variance(all_energies, all_weights)),eps=0.02)
  E = weighted_skew(spectrum.get_energies_eV(), scaled_spectrum_weights)
  F = weighted_skew(all_energies, all_weights)
  self.moment3_fee.append(E)
  self.moment3_brg.append(F)
  G = weighted_kurtosis(spectrum.get_energies_eV(), scaled_spectrum_weights)
  H = weighted_kurtosis(all_energies, all_weights)

  analysis = """
            FEE    Bragg
Mean (eV)  %6.1f %6.1f  Δ=%.1f
Stddev (eV)%6.1f %6.1f
Skewness   %6.1f %6.1f
Kurtosis-3 %6.1f %6.1f"""%(A,B,B-A,C,D,E,F,G-3.,H-3.)
  # Negative skew: left tail is longer; positive skew: right tail is longer
  # Note kurtosis>3 has fatter tails than normal distribution (thin peak); kurtosis<3 have thinner tails (broad peak)
  axes.annotate(analysis, xy = (params.trumpet.spectrum.range[0], 0.4*y_fullscale), color="blue", fontname="monospace")
  outlier_condition = (abs(B-A)/C) > params.trumpet.outlier.stddev
  if outlier_condition: axes.set_xlabel("Energy (eV), outlier", color='r')
  return int(outlier_condition)

def run():  # as originally encoded as a standalone by ASB.
  all_diffs = flex.double()
  all_mean_energies = flex.double()
  RECALIBRATION = 1.95 # hypothesize that FEE spectra should be adjusted to FEE + RECALIBRATION
  plot=True
  if plot:
    fig = plt.figure(figsize=(14,4))
  for expt_id in range(112):
    localpath = "split_%03d"%expt_id
    expt = ExperimentList.from_file(localpath + ".expt")[0]
    reflections = flex.reflection_table.from_file(localpath + ".refl")

  #for expt_id, expt in enumerate(expts):
    all_wavelengths = flex.double()
    all_weights = flex.double()

    refls = reflections.select(reflections['id'] == 0) #expt_id)

    detector = expt.detector

    xtal = expt.crystal
    uc = xtal.get_unit_cell()
    d = uc.d(refls["miller_index"])
    s0 = expt.beam.get_s0() # this has length 1/λ.  Is it OK?

    for i in range(len(refls)):
      panel = detector[refls["panel"][i]]
      sb = refls["shoebox"][i]
      sb_shape = sb.data.focus()[1:]
      x1, x2, y1, y2, _, _ = refls["bbox"][i]

      tt = flex.double()
      for s in range(sb_shape[0]):
        for f in range(sb_shape[1]): # control: reverse the 0,1 order
            two_theta = panel.get_two_theta_at_pixel(s0, (f + x1, s + y1))
            tt.append(two_theta)
            #energy = factor_ev_angstrom / (2 * d[i] * math.sin(two_theta / 2))
            #if d[i]<2.6: print ("pixel %d, %d %7.1f eV"%(s,f,energy)) # 2.9 eV/pixel?
      sel = sb.mask.as_1d() == 5
      tt = tt.select(sel)

      all_wavelengths.extend(2 * d[i] * flex.sin(tt / 2))
      all_weights.extend(sb.data.as_1d().as_double().select(sel)) # how do we know both arrays traverse the sb in the same order?

    all_energies = factor_ev_angstrom / all_wavelengths
    mean_energy = factor_ev_angstrom / expt.beam.get_wavelength()
    if plot:
      y, x, _ = plt.hist(
      all_energies.as_numpy_array(),
      weights=all_weights.as_numpy_array(),
      bins=60,range=(7060.0,7180.0)
    )
    pixels_weighted_mean = flex.mean_weighted(all_energies, all_weights)
    if plot: plt.plot([pixels_weighted_mean, pixels_weighted_mean], [0, 250], "-", color="black")

    iset = expt.imageset
    assert len(iset) == 1
    image_index = iset.indices()[0]
    path = iset.paths()[0]
    spectrum = iset.get_spectrum(0)

    normed_spectrum_weights = spectrum.get_weights() / flex.max(spectrum.get_weights())
    if plot:
      scaled_spectrum_weights = normed_spectrum_weights * np.max(y)


      plt.plot(
      (RECALIBRATION + spectrum.get_energies_eV()),
#      spectrum.get_energies_eV().as_numpy_array(),
      scaled_spectrum_weights.as_numpy_array(),
      "-", color="orange")
      spectrum_weighted_mean = flex.mean_weighted(spectrum.get_energies_eV(), scaled_spectrum_weights)

      assert approx_equal(mean_energy, spectrum_weighted_mean) # agreement of spectrum and beam objects

      plt.plot([RECALIBRATION + spectrum_weighted_mean, RECALIBRATION + spectrum_weighted_mean], [0, 250], "-", color="orange")

      plt.title('Energy histograms: %s, event %d, refls %d'%(path,image_index,len(refls)))
      plt.xlabel('Energy (eV)')
      plt.ylabel('Counts')
      plt.legend(['pixel weighted mean','FEE spectrum', 'spectrum mean',  'Weighted histogram of all shoebox pixels'])
      #fig.savefig("%03d.png"%expt_id)
      plt.show()
      fig.clf()
      #fig = plt.figure(figsize=(14,4))
    print ("eV diff: %4.2f eV"%(pixels_weighted_mean - mean_energy))
    all_diffs.append(pixels_weighted_mean - mean_energy)
    all_mean_energies.append(RECALIBRATION + mean_energy)
  exit()

  print ("Mean diff %4.2f eV"%flex.mean(all_diffs))
  y, x, _ = plt.hist(
      all_mean_energies.as_numpy_array(),
      bins=25,
    )
  plt.show()

if __name__=="__main__":
  run()
