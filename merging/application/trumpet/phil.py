from __future__ import division

phil_str = """
trumpet {
  dials_refine = False
    .type = bool
    .help = Try to improve the model for a single experiment prior to computing statistics
  debug_rank_0 = False
    .type = bool
    .help = return if rank>0, allows simplified debug
  plot_all {
    enable = False
      .type = bool
      .help = enable the matplotlib output
    savepng = True
      .type = bool
      .help = instead of plotting each event to screen, save as individual *.png files
      .help = PNG can later be concatenated into animated gif with convert -delay 60 -loop 0 *.png annulus_hits.gif
  }
  spectrum {
    recalibration = 0.0
      .type = float
      .help = On the plot, add recalibration (eV) to the FEE spectrum.
      .help = Represents a global offset already determined
    nbins = 60
      .type = int
      .help = Number of energy bins for the histogram of strong-spot pixel energies
    range = None
      .type = floats(size=2)
      .help = Energy range in eV for computing the histogram bins
    energy_delta_tolerance = 0.000001
      .type = float
      .help = Maximum absolute difference between mean beam energy derived from
      .help = spectrum and wavelength in eV, which will not raise assert error
  }
  eta_rad = 0.00001745
    .type = float
    .help = fixed value of eta, expressed in radians.
    .help = the default is equal to 0.001 degrees. Reasonable default for well ordered crystal.
    .help = If "Auto", use individual per-expt values from the input files.
  AD1TF7B_MAX2T = 50.0
    .type = float
    .help = Sauter 2014 Acta D trumpet plot Fig. 7B, maximum two theta angle expressed in degrees.
  AD1TF7B_MAXDP = 0.2
    .type = float
    .help = Sauter 2014 Acta D trumpet plot Fig. 7B, maximum delta psi angle expressed in degrees.
  residuals {
    cutoff = 5.
      .type = float
      .help = sigma (mahalanobis) cutoff for determining outliers on the radial,tangential,delta-psi plot
    dot_size = 24.
      .type = float
      .help = For plotting the delta-R delta-T residuals
  }
  outlier {
    stddev=1.
      .type = float
      .help = Absolute difference between FEE spectrum and implied Bragg spectrum in units of FEE spectrum std dev
    deff=2000.
      .type = float
      .help = Domain size in Angstrom below this value
    ucell=2.
      .type = float
      .help = unit cell outside this many Mahalanobis intervals
    tff=0
      .type = int
      .help = greater than this number of outlier residuals in the three-feature fit (radial, transverse, delta psi)
    remove=False
      .type = bool
      .help = If True, remove experiment from output if it hits two of the four above criteria
  }
}
include scope dials.algorithms.indexing.indexer.phil_scope
include scope dials.algorithms.refinement.refiner.phil_scope
"""
