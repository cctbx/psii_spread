from __future__ import absolute_import, division, print_function

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.
from trumpet_plot import trumpet_plot
from xfel.merging.application.worker import factory as factory_base

class factory(factory_base):
  """ Factory class for calculating and analyzing the trumpet plot. """
  @staticmethod
  def from_parameters(params, additional_info=[], mpi_helper=None, mpi_logger=None):
    """ """
    return [trumpet_plot(params, mpi_helper, mpi_logger)]

