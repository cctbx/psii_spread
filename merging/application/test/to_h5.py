from dxtbx.model.experiment_list import ExperimentListFactory
from dxtbx.imageset import ImageSetFactory
from simtbx.nanoBragg import utils
import numpy as np

experiments_filename = "split_000.expt"
def show_spectrum(spec):
  from matplotlib import pyplot as plt
  plt.plot(spec.get_energies_eV(), spec.get_weights())
  plt.show()

def run():
  experiments = ExperimentListFactory.from_json_file(experiments_filename, check_format = False)
  experiment = experiments[0]
  print("Data from", experiment.imageset.paths()[0], "event", experiment.imageset.indices()[0])

  #regeneration of the Imageset, as done in cctbx.xfel.merge
  current_imageset = ImageSetFactory.make_imageset(experiment.imageset.paths())
  event_index = experiment.imageset.indices()[0]
  experiment.imageset = current_imageset[event_index:event_index+1]
  spectrum = experiment.imageset.get_spectrum(0)
  #show_spectrum(spectrum)
  diffraction = experiment.imageset.get_raw_data(0)
  assert diffraction[0].focus() == (176,192) # flex.double panel dimensions (slow,fast)
  assert len(diffraction) == 64 # number of panels in the ePix10K-2M
  diffraction_data = [diffraction[pid].as_numpy_array() for pid in range(len(diffraction))]
  assert diffraction_data[0].shape == (176,192) # numpy array panel dimentions (slow,fast)
  assert len(experiment.detector) == 64 # number of panels in the dxtbx model
  assert experiment.detector[0].get_image_size() == (192,176)
  # there seems to be a potential problem.  Dxtbx states the panel size as (fast,slow)

  num_output_images = 1
  beam_dict = experiment.beam.to_dict()
  det_dict = experiment.detector.to_dict()
  try:
        beam_dict.pop("spectrum_energies")
        beam_dict.pop("spectrum_weights")
  except Exception: pass
  comp = dict(compression='lzf')
  with utils.H5AttributeGeomWriter("%s.h5"%(experiment.identifier),
        image_shape=(64,176,192), num_images=num_output_images,
        detector=det_dict, beam=beam_dict, dtype=np.float32,
        detector_and_beam_are_dicts=True, compression_args=comp) as writer:
        writer.add_image(diffraction_data)

if __name__=="__main__":
  run()
