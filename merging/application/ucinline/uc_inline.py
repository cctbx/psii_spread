from __future__ import division
from xfel.merging.application.worker import worker

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.

class uc_inline(worker):

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super(uc_inline, self).__init__(params=params, mpi_helper=mpi_helper, mpi_logger=mpi_logger)

  def __repr__(self):
    return "Filter on multivariate-Gaussian unit cell model without prior uc_metrics call"
    """
    The purpose of the worker is to condense the multistep manual process into a single automatic step:
    1) produce the tdata list (list of unit cells)
    2) produce the uc_metrics plot (optionally save plot, optionally write the covariance file)
    3) optionally apply unit cell filter based on the covariance matrix.
    In order to run this automatically we must know the clustering behavior ahead of time.
    Relevant parameters are:
    tdata.output_path=<filename for unit cells> # Optionally write unit cells to file. Extension automatically set to *.tdata.
    uc_metrics.cluster.dbscan.eps=2.0 # the distance parameter normally entered interactively in the GUI
    uc_metrics.file_name=None # The input tdata. Normally None, or provide file name to override
    uc_metrics.space_group=Pmmm # Mandatory, only accept cells having this symmetry
    uc_metrics.feature_vector=a,b,c
    uc_metrics.write_covariance=False
    uc_metrics.plot.outliers=True
    """

  def run(self, experiments, reflections):
    # first produce the tdata list
    from xfel.merging.application.tdata.cell_listing import simple_cell_listing
    SCL = simple_cell_listing(params=self.params)
    SCL.run(all_experiments=experiments, all_reflections=reflections)
    if self.mpi_helper.rank == 0:
      all_results = SCL.global_results
    # done with tdata list

    # Now look at clustering
    if self.mpi_helper.rank == 0:
      from uc_metrics.clustering.step_dbscan3d import dbscan_plot_manager
      from uc_metrics.command_line.dbscan import wrap_2D_features
      self.params.uc_metrics.input.iterable = all_results
      plots = dbscan_plot_manager(self.params.uc_metrics)
      number_of_features = len(self.params.uc_metrics.input.feature_vector.split(","))
      # simplistic method for finding the plot type!
      if self.params.uc_metrics.show_plot:
        if number_of_features <= 2:
          wrap_2D_features(plots)
        else:
          plots.wrap_3D_features()
      covariance_dict = plots.covariance_model_as_dict()
    self.mpi_helper.comm.barrier()
    # done with clustering

    # apply covariance model as filter
    experiment_ids_to_remove = []
    removed_for_unit_cell = 0
    removed_for_space_group = 0
    if True:
      from xfel.merging.application.filter.experiment_filter import experiment_filter
      EF = experiment_filter(params=self.params, mpi_helper=self.mpi_helper, mpi_logger=self.logger)
      class Empty: pass
      if self.mpi_helper.rank == 0:
        data = covariance_dict
        E=Empty()
        E.features_ = data["features"]
        E.sample_name = data["sample"]
        E.output_info = data["info"]
        pop=data["populations"]
        self.logger.main_log("Focusing on cluster component %d from previous analysis of %d cells"%(
          self.params.filter.unit_cell.cluster.covariance.component, len(pop.labels)))
        self.logger.main_log("%s noise %d order %s"%(pop.populations, pop.n_noise_, pop.main_components))
        legend = pop.basic_covariance_compact_report(feature_vectors=E).getvalue()
        self.logger.main_log(legend)
        self.logger.main_log(
            "Applying Mahalanobis cutoff of %.3f"%(self.params.filter.unit_cell.cluster.covariance.mahalanobis))
        transmitted = data
      else:
        transmitted = None
      # distribute cluster information to all ranks
      EF.cluster_data = self.mpi_helper.comm.bcast(transmitted, root=0)
      # pull out the index numbers of the unit cell parameters to be used for covariance matrix
      EF.cluster_data["idxs"]=[["a","b","c","alpha","beta","gamma"].index(F) for F in EF.cluster_data["features"]]
      self.common_store["cluster_data"] = EF.cluster_data # stash the information for subsequent worker
      for experiment in experiments:
        if self.params.ucinline.cluster_only: # for debug
          continue
        if not EF.check_cluster(experiment):
          experiment_ids_to_remove.append(experiment.identifier)
          removed_for_unit_cell += 1
# END OF COVARIANCE FILTER, start reporting the filter results:
    input_len_expts = len(experiments)
    input_len_refls = len(reflections)
    new_experiments, new_reflections = experiment_filter.remove_experiments(experiments, reflections, experiment_ids_to_remove)

    removed_reflections = input_len_refls - len(new_reflections)
    assert removed_for_space_group + removed_for_unit_cell == input_len_expts - len(new_experiments)

    self.logger.log("Experiments rejected because of unit cell dimensions: %d"%removed_for_unit_cell)
    self.logger.log("Experiments rejected because of space group %d"%removed_for_space_group)
    self.logger.log("Reflections rejected because of rejected experiments: %d"%removed_reflections)

    # MPI-reduce total counts
    comm = self.mpi_helper.comm
    MPI = self.mpi_helper.MPI
    total_removed_for_unit_cell  = comm.reduce(removed_for_unit_cell, MPI.SUM, 0)
    total_removed_for_space_group  = comm.reduce(removed_for_space_group, MPI.SUM, 0)
    total_reflections_removed  = comm.reduce(removed_reflections, MPI.SUM, 0)

    # rank 0: log total counts
    if self.mpi_helper.rank == 0:
      self.logger.main_log("Total experiments rejected because of unit cell dimensions: %d"%total_removed_for_unit_cell)
      self.logger.main_log("Total experiments rejected because of space group %d"%total_removed_for_space_group)
      self.logger.main_log("Total reflections rejected because of rejected experiments %d"%total_reflections_removed)

    from xfel.merging.application.utils.data_counter import data_counter
    data_counter(self.params).count(new_experiments, new_reflections)
    return new_experiments, new_reflections
