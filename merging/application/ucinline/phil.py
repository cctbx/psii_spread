from __future__ import division

phil_str = """
ucinline {
  cluster_only = False
    .type = bool
    .help = if True, do not filter the experiments
}
uc_metrics {
  include scope uc_metrics.clustering.step1.phil_scope
}
"""
