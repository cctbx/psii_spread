from __future__ import division
from collections import Counter
from itertools import chain
from LS49.work2_for_aca_lsq.remake_range_intensities_with_complex import gen_fmodel_with_complex
from dxtbx.model.experiment_list import ExperimentList
from dials.array_family import flex
import numpy as np
from dxtbx import flumpy
from cctbx.xray import structure_factors
from cctbx import xray
from scitbx.matrix import sqr
from scitbx.lbfgs.tst_mpi_split_evaluator import mpi_split_evaluator_run
from simtbx import get_exascale
import scitbx
from scipy import constants
import traceback as tb
from typing import Annotated, List
import sys,os
import pickle,io
ENERGY_CONV = 1e10 * constants.c * constants.h / constants.electron_volt

# Carry over boilerplate from LS49 project; eventually refactor
from LS49.sim import step5_pad
from LS49 import ls49_big_data
step5_pad.big_data = ls49_big_data
from LS49.sim.step5_pad import data
local_data = data()

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.
import utils
from crystal_detail_interface import CrystalDetailSingleton
from new_global_fdp_refinery import rank_0_fit_all_f_mix_in
from dxtbx.imageset import ImageSetFactory


NClassesLongList = Annotated[List, 'len == n_classes']
NEnergyChannelsLongList = Annotated[List, 'len == len(energy_channels)']


class intensities_mix_in: #modeled after LS49.work2_for_aca_lsq.remake_range_intensities_with_complex
  def get_hi_sym_structures(self):
    from iotbx import pdb
    # assume we get the scaling.model pdb filepath. Should be model phenix-refined against spread data
    # note: the unit cell for Fmodel is taken from the PDB file!
    # Hi-symmetry models.  a) whole pdb, b) no Fe, c) FE1 only, d) FE2 only
    self.mpi_helper.comm.barrier()
    pdb_whole_model = pdb.input(file_name = self.params.scaling.model)
    pdb_text = pdb_whole_model.as_pdb_string()

    if self.params.exafel.metal == "MMO2": # Two chemically distinct Fe sites
      tokens = pdb_text.split("\n")
      btokens = []; ctokens = []; dtokens = []
      for token in tokens:
        splits = token.split()
        if len(splits)==12:
          if splits[11]!="FE": btokens.append(token) # For MMO just pure luck that it worked out
                                                     # FIXME: PDB columns don't always have in between
                                                     # white space go back and must fix this another way
        else: btokens.append(token)
        if token.find("ATOM")==0: continue
        if token.find("HETATM")==0:
          if splits[2].find("FE")!=0: continue       # specialized for MMO, does not work for ferredoxin
          if splits[5]=="601": print(splits); ctokens.append(token); continue # MMO-specific criteria for two atom classes
          if splits[5]=="602": dtokens.append(token); continue # must go back and hide this behind a phil parameter
        ctokens.append(token); dtokens.append(token)
      pdb_text_b = "\n".join(btokens); print (len(tokens),len(btokens),len(ctokens),len(dtokens))
      pdb_text_c = "\n".join(ctokens);
      pdb_text_d = "\n".join(dtokens);
      self.n_classes = 2 # specific for MMO, two classes of Fe atoms; better to generalize behind a phil parameter
      hi_sym_structures = []
      for tag,lines in zip(["pdb_text", "pdb_text_b", "pdb_text_c", "pdb_text_d"],
                          [pdb_text, pdb_text_b, pdb_text_c, pdb_text_d]):
        pdb_inp = pdb.input(source_info=None,lines = lines)
        xray_structure = pdb_inp.xray_structure_simple()
        xray_structure.show_summary(prefix="%s "%tag)
        hi_sym_structures.append(xray_structure) # Hi-symmetry models.  a) whole pdb, b) no Fe, c) FE1 only, d) FE2 only
      self.labels = ["601","602"] # to later set new_model_intensities
      # case-specific starting models are set to the iron edge for now, change to Mn later
      self.case_specific_starting_models = [local_data.get("Fe_metallic_model")]*self.n_classes

    elif self.params.exafel.metal == "MMO1": # Two chemically same Fe sites
      tokens = pdb_text.split("\n")
      btokens = []; ctokens = []
      for token in tokens:
        splits = token.split()
        if len(splits)==12:
          if splits[11]!="FE": btokens.append(token) # For MMO just pure luck that it worked out
                                                     # FIXME: PDB columns don't always have in between
                                                     # white space go back and must fix this another way
        else: btokens.append(token)
        if token.find("ATOM")==0: continue
        if token.find("HETATM")==0:
          if splits[2].find("FE")!=0: continue       # specialized for MMO, does not work for ferredoxin
          if splits[5] in ["601","602"]: print(splits); ctokens.append(token); continue # MMO-specific criteria for one atom class
        ctokens.append(token);
      pdb_text_b = "\n".join(btokens); print (len(tokens),len(btokens),len(ctokens))
      pdb_text_c = "\n".join(ctokens);
      self.n_classes = 1 # specific for MMO, two classes of Fe atoms; better to generalize behind a phil parameter
      hi_sym_structures = []
      for tag,lines in zip(["pdb_text", "pdb_text_b", "pdb_text_c"],
                          [pdb_text, pdb_text_b, pdb_text_c]):
        pdb_inp = pdb.input(source_info=None,lines = lines)
        xray_structure = pdb_inp.xray_structure_simple()
        xray_structure.show_summary(prefix="%s "%tag)
        hi_sym_structures.append(xray_structure) # Hi-symmetry models.  a) whole pdb, b) no Fe, c) FE only
      self.labels = ["FE"] # to later set new_model_intensities
      # case-specific starting models are set to the iron edge for now, change to Mn later
      self.case_specific_starting_models = [local_data.get("Fe_metallic_model")]*self.n_classes

    elif self.params.exafel.metal == "PSII":  # 8 chemically same PSII Mn sites
      tokens = pdb_text.split("\n")
      btokens, ctokens = [], []
      for token in tokens:
        splits = token.split()
        if token.startswith(('ATOM', 'HETATM', 'ANISOU')):
          if not token.endswith('Mn'):
            btokens.append(token)
        else: btokens.append(token)
        if token.startswith('ATOM'): continue
        if token.startswith(('HETATM', 'ANISOU')):
          if splits[-1] != 'Mn':
            continue
          assert token[23:26] == '418'  # should work for current ref. (only?)
          print(token)
          ctokens.append(token)
          continue
        ctokens.append(token)
      print(f'{len(tokens)=}, {len(btokens)=}, {len(ctokens)=}')
      pdb_text_b = "\n".join(btokens)
      pdb_text_c = "\n".join(ctokens)
      self.n_classes = 1 # single atom class for all Mn atoms
      hi_sym_structures = []
      for tag,lines in zip(["pdb_text", "pdb_text_b", "pdb_text_c"],
                           [pdb_text, pdb_text_b, pdb_text_c]):
        pdb_inp = pdb.input(source_info=None, lines = lines)
        xray_structure = pdb_inp.xray_structure_simple()
        xray_structure.show_summary(prefix="%s "%tag)
        hi_sym_structures.append(xray_structure) # Hi-symmetry models.  a) whole pdb, b) no Mn, c) Mn only
      self.labels = ["MN"] # to later set new_model_intensities
      self.case_specific_starting_models = [local_data.get("Mn_metallic_model")] * self.n_classes

    elif self.params.exafel.metal == "PSII4":  # 4 classes of PSII Mn sites
      tokens = pdb_text.split("\n")
      btokens, ctokens, dtokens, etokens, ftokens = [], [], [], [], []
      for token in tokens:
        splits = token.split()
        if token.startswith(('ATOM', 'HETATM', 'ANISOU')):
          if not token.endswith('Mn'):
            btokens.append(token)
        else: btokens.append(token)
        if token.startswith('ATOM'): continue
        if token.startswith(('HETATM', 'ANISOU')):
          if splits[-1] != 'Mn':
            continue
          assert token[23:26] == '418'  # should work for current ref. (only?)
          print(token)
          token_map = {'1': ctokens, '2': dtokens, '3': etokens, '4': ftokens}
          token_map[token[14]].append(token)  # append to diff. tokens for Mn#
          continue
        ctokens.append(token); dtokens.append(token)
        etokens.append(token); ftokens.append(token)
      print(f'{len(tokens)=}, {len(btokens)=}, {len(ctokens)=}, '
            f'{len(dtokens)=}, {len(etokens)=}, {len(ftokens)=}')
      pdb_text_b = "\n".join(btokens)
      pdb_text_c = "\n".join(ctokens)
      pdb_text_d = "\n".join(dtokens)
      pdb_text_e = "\n".join(etokens)
      pdb_text_f = "\n".join(ftokens)
      self.n_classes = 4 # four atom classes for four distinct Mn sites
      hi_sym_structures = []
      for tag,lines in zip(["pdb_text", "pdb_text_b", "pdb_text_c", "pdb_text_d", "pdb_text_e", "pdb_text_f"],
                           [pdb_text, pdb_text_b, pdb_text_c, pdb_text_d, pdb_text_e, pdb_text_f]):
        pdb_inp = pdb.input(source_info=None, lines = lines)
        xray_structure = pdb_inp.xray_structure_simple()
        xray_structure.show_summary(prefix="%s "%tag)
        hi_sym_structures.append(xray_structure) # Hi-symmetry models.  a) whole pdb, b) no Mn, c) Mn1 only, d) Mn2 only, e) Mn3 only, f) Mn4 only
      self.labels = ["MN1", "MN2", "MN3", "MN4"] # to later set new_model_intensities
      self.case_specific_starting_models = [local_data.get("Mn_metallic_model")]*self.n_classes

    self.mpi_helper.comm.barrier() # migrate this to a b-cast if needed
    return hi_sym_structures

  def encapsulated_unpickle(self,path,mpi_rank):
      # pickle file > 1 GB cannot be simply unpickled and broadcasted.
      # try broadcasting the bytestring first and then unpickling in each rank.
      chunks_from_file = []
      if mpi_rank == 0:
        with open(path,"rb") as F:
          while 1:
            print (len(chunks_from_file))
            staged = F.read(1000000000) #largest safe chunk for MPI broadcast
            if staged == b"": break
            chunks_from_file.append( staged )
        n_chunks = len(chunks_from_file)
      else:
        n_chunks = None
      self.mpi_helper.comm.barrier()
      n_chunks = self.mpi_helper.comm.bcast(n_chunks, root=0)

      chunks_for_stream=[]
      for i_chunk in range(n_chunks):
        chunk=None
        if mpi_rank==0:
          chunk = chunks_from_file[i_chunk]
        chunks_for_stream.append( self.mpi_helper.comm.bcast(chunk, root = 0) )

      big_buffer = b"".join(chunks_for_stream)

      F = io.BytesIO(big_buffer)
      Fmodel_indices = pickle.load(F)
      result = pickle.load(F)
      return Fmodel_indices, result

  def get_static_fcalcs(self,energy_channels): # expressed in eV please
    import time
    b0=time.time()

    if self.params.exafel.static_fcalcs.action == "read":
      Fmodel_indices, result = self.encapsulated_unpickle(
        self.params.exafel.static_fcalcs.path, self.logical_rank)
      if self.logical_rank == 0:
        with open(self.params.exafel.static_fcalcs.whole_path,"rb") as F:
          static_miller_array = pickle.load(F)
      else:
        static_miller_array = None
      self.mpi_helper.comm.barrier()
      self.static_miller_array = self.mpi_helper.comm.bcast(static_miller_array, root=0)

    else: # write
      whole_structure = self.hi_sym_structures[0]
      base_structure = self.hi_sym_structures[1]
      epsilon = 0.05
      direct_algo_res_limit = self.params.statistics.annulus.d_min-epsilon
      k_sol = self.params.scaling.pdb.k_sol

      energy = energy_channels[0] # lowest energy channel, below the edge
      GF_whole7070 = gen_fmodel_with_complex.from_structure(whole_structure,energy
                   ).from_parameters(resolution = direct_algo_res_limit,
                                     algorithm="fft",
                                     k_sol = k_sol)
      GF_whole7070 = GF_whole7070.as_P1_primitive()
      f_container7070 = GF_whole7070.get_fmodel()
      Fmodel_whole7070 = f_container7070.f_model
      if self.logical_rank == 0:
        with open(self.params.exafel.static_fcalcs.whole_path,"wb") as F:
          pickle.dump(Fmodel_whole7070, F)
      else: Fmodel_whole7070 = None
      self.mpi_helper.comm.barrier()
      self.static_miller_array = self.mpi_helper.comm.bcast(Fmodel_whole7070, root=0)

      Fmodel_indices = self.static_miller_array.indices() # common structure defines the indices
      #shortly will use these indices to make a lookup dictionary

      F_bulk = f_container7070.fmodel.arrays.core.data.f_bulk
      F_bulk.reshape(flex.grid((Fmodel_indices.size(),1))) # in-place reshape, non-standard

      result = flex.complex_double(flex.grid((Fmodel_indices.size(),len(energy_channels))))

      # common structure to represent the wavelength-dependent non-Fe diffraction (bulk+atoms)
      for incr, energy in enumerate( energy_channels ): # expressed in eV please
        print(incr,energy)
        GF_non_Fe = gen_fmodel_with_complex.from_structure(base_structure,energy
                   ).from_parameters(resolution = direct_algo_res_limit,
                                     algorithm="fft",
                                     k_sol = k_sol)
        GF_non_Fe = GF_non_Fe.as_P1_primitive()
        f_container = GF_non_Fe.get_fmodel() # the get_fmodel step is most time consuming
        Fcalc_non_Fe = f_container.fmodel.f_calc().data()
        Fcalc_non_Fe.reshape(flex.grid((Fmodel_indices.size(),1))) # in-place reshape, non-standard
        result.matrix_paste_block_in_place((F_bulk + Fcalc_non_Fe),0,incr)

      # result holds a table of complex double structure factors.  Rows are Miller indices H.
      # columns are F_H(energy, 100 channels) for F(bulk) + F(non-Fe atoms). Thus this is
      # the energy-dependent portion of the calculation that is not dependent on the iron model.
      if self.logical_rank == 0:
        with open(self.params.exafel.static_fcalcs.path,"wb") as F:
          pickle.dump(Fmodel_indices,F); pickle.dump(result,F)
      self.mpi_helper.comm.barrier()
    this_focus = result.focus()
    assert this_focus[0] == len(Fmodel_indices)
    assert this_focus[1] == len(energy_channels)
    begin = time.time()
    HKL_lookup = {}
    for imiller, miller_index in enumerate(Fmodel_indices):
      HKL_lookup[miller_index] = imiller
    end = time.time()
    print ("looks OK with read time %.2f and dict time %.2f"%(begin-b0, end-begin))
    return HKL_lookup, Fmodel_indices, result

  def _split_sub_communicator(self, verbose=False):
    """Divide comm world into sub-communicators of size 32 or below (PR !17)"""
    cr = self.mpi_helper.rank
    cs = self.mpi_helper.size
    colors_count = int(np.ceil(cs / 32))
    colors = np.linspace(0, colors_count, num=cs, endpoint=False, dtype=int)
    sub_color = colors[cr]
    sub_rank = cr - list(colors).index(sub_color)
    if verbose:
      print(f'Splitting comm world into sub-worlds: sub-world rank={sub_rank},'
            f' size={Counter(colors)[sub_color]}, color={sub_color}')
    return self.mpi_helper.comm.Split(sub_color, sub_rank)

  def get_intensity_structure(self,
                              energy_channels: List[float],
                              static_base: flex.complex_double,
                              preset_starting_models: NClassesLongList,
                              labels: NEnergyChannelsLongList
                              ) -> flex.double:
    """
    Calculate new structural information for use in further refinement.

    Return a flex.double table with intensities and its partial derivatives,
    with `len(self.static_miller_array)` rows
    and `(1 + 2 * self.n_classes) * len(channels)` columns.
    Individual rows hold information from subsequent Miller indices H. First
    `n_channels` columns hold m squared amplitudes of structure factors F_H.
    The next `2*self.n_classes` groups of columns (`n_channels` columns each)
    hold partial derivatives of F_H with respect to fp(metal1), fdp(metal1),
    fp(metal2), fdp(metal2), and so on for each metal class:

    ┌───────┬───────────┬───────────┬───────────┬───────────┬───┬───────────┐
    │Struct.│Intensities│ Metal 1   │ Metal 1   │ Metal 2   │ … │ Metal N   │
    │Factors│ |F_H|**2  │ ∂F_H/∂fp  │ ∂F_H/∂fdp │ ∂F_H/∂fp  │ … │ ∂F_H/∂fp  │
    ├───────┼──┬──┬──┬──┼──┬──┬──┬──┼──┬──┬──┬──┼──┬──┬──┬──┼───┼──┬──┬──┬──┤
    │       │E1│E2│… │En│E1│E2│… │En│E1│E2│… │En│E1│E2│… │En│ … │E1│E2│… │En│
    ├───────┼──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴───┴──┴──┴──┴──┤
    │ H1    │    <-- (1 + 2 * self.n_classes) * len(channels) columns -->   │
    │ H2    │ ^                                                             │
    │ …     │ len(self.static_miller_array) rows                            │
    │ Hm    │ v                                                             │
    └───────┴───────────────────────────────────────────────────────────────┘

    Calculations of amplitudes**2 and derivatives for each energy channel E_i
    are performed in parallel, independently on each <32-rank sub-communicator.
    Parallelizing calculations significantly speeds them up, whereas limiting
    the sub-comm size to 32 prevents a diagnosed issue with communication
    slowdown observed for `Allgatherv`; see SPREAD PR !17 for more details.

    Thus, this is the energy-dependent portion of the calculation
    that is dependent on the metal model.
    """
    ms = self.static_miller_array.set()
    cs = self.static_miller_array.crystal_symmetry()
    d_min = self.params.statistics.annulus.d_min - 0.05  # epsilon
    algo = structure_factors.from_scatterers(crystal_symmetry=cs, d_min=d_min)
    n_rows = self.Fmodel_indices.size()
    n_channels = len(energy_channels)
    n_cols_per_channel = 1 + 2 * self.n_classes
    n_cols = n_channels * n_cols_per_channel

    metals: NClassesLongList = []
    for i_class in range(self.n_classes):
      metal = gen_fmodel_with_complex.from_structure(
        self.hi_sym_structures[2 + i_class],
        energy_channels[0]).from_parameters(algorithm="direct")
      metals.append(metal.as_P1_primitive())

    # Distribute workload across MPI ranks: Split comm world into sub-worlds.
    # Until `Free()`, ranks work only on some channels within their sub-world.
    sub_comm = self._split_sub_communicator(verbose=True)
    channels = list(range(n_channels))
    allotments = {r: channels[r::sub_comm.size] for r in range(sub_comm.size)}
    results = []

    for i_channel in allotments[sub_comm.rank]:
      energy = energy_channels[i_channel]  # expressed in eV
      print(" gradients", i_channel, energy)
      for i_class in range(self.n_classes):
        metals[i_class].reset_specific_at_energy(
          label_has=labels[i_class],
          tables=preset_starting_models[i_class],
          newvalue=energy)

      # Get Fcalc @ energy: first bulk + non-metal ampl., then add metal factors
      f_calc_total = static_base.matrix_copy_block(i_row=0, i_column=i_channel,
                                                   n_rows=n_rows, n_columns=1)
      for i_class in range(self.n_classes):
        f_calc_metal = algo(xray_structure=metals[i_class].xray_structure,
                            miller_set=ms, algorithm="direct").f_calc().data()
        f_calc_metal.reshape(flex.grid((n_rows, 1)))
        f_calc_total += f_calc_metal
      results.append(np.array(flex.norm(f_calc_total), dtype=np.double)) # flex.norm(a+bi) --> a*a + b*b
      f_calc_a, f_calc_b = f_calc_total.parts()

      grad_flags_dict = dict(site=False, u_iso=False, u_aniso=False,
                             occupancy=False, fp=True, fdp=True)
      grad_flags = xray.structure_factors.gradient_flags(**grad_flags_dict)

      for i_class, metal in enumerate(metals):
        xray.set_scatterer_grad_flags(
          scatterers = metal.xray_structure.scatterers(),
          **{key: getattr(grad_flags, key) for key in grad_flags_dict.keys()})
        sf = xray.ext.each_hkl_gradients_direct(
          ms.unit_cell(), ms.space_group(), ms.indices(),
          metal.xray_structure.scatterers(), None,
          metal.xray_structure.scattering_type_registry(),
          metal.xray_structure.site_symmetry_table(), 0)
        sf.d_fcalc_d_fp().reshape(flex.grid((n_rows, 1)))
        sf.d_fcalc_d_fdp().reshape(flex.grid((n_rows, 1)))
        vec2s = [sf.d_fcalc_d_fp(), sf.d_fcalc_d_fdp()]
        for vec2 in vec2s:
          vec2_a, vec2_b = vec2.parts()
          partial_I_partial_q = 2. * (f_calc_a * vec2_a + f_calc_b * vec2_b)
          results.append(np.array(partial_I_partial_q, dtype=np.double))

    # Gather results within each comm sub-world and return to "main" comm world
    sub_comm.barrier()
    results = np.concatenate(results, dtype=np.double) \
      if results else np.empty(0, dtype=np.double)
    gathered = np.empty(n_rows * n_cols, dtype=np.double)
    sizes = [len(allotments[rank]) * n_cols_per_channel * n_rows for rank in range(sub_comm.size)]
    offsets = np.zeros(sub_comm.size, dtype=int)
    offsets[1:] = np.cumsum(sizes)[:-1]
    sub_comm.Allgatherv(results, [gathered, sizes, offsets, self.mpi_helper.MPI.DOUBLE])
    sub_comm.Free()

    # Learn current column order, reshape & reorder results back, return as flex
    self.mpi_helper.comm.barrier()
    channel_order = list(chain(*allotments.values()))
    column_order = list(chain(*[range(c, n_cols, n_channels) for c in channel_order]))
    gathered = gathered.reshape(n_cols, ms.indices().size())
    gathered = gathered[np.argsort(column_order)]
    gathered = flumpy.from_numpy(gathered)
    gathered.matrix_transpose_in_place()
    print('Done with calculating, gathering intensities and its derivatives')
    return gathered


class prepare(intensities_mix_in, rank_0_fit_all_f_mix_in):
  def __init__(self, old_experiments, old_reflections, params, mpi_helper, mpi_logger, plot=True):
    self.mpi_helper = mpi_helper
    self.mpi_logger = mpi_logger
    self.params = params
    self.uc = self.params.scaling.unit_cell
    print("Rank %d on host %s"%(self.mpi_helper.rank, os.environ.get("SLURMD_NODENAME","")))
    self.logical_rank = self.mpi_helper.rank
    self.logical_size = self.mpi_helper.size
    self.hi_sym_structures = self.get_hi_sym_structures()
    _ = CrystalDetailSingleton(params, old_experiments)

    self.result_expt = ExperimentList()
    self.result_refl = flex.reflection_table()

    # Seemingly we need the spectrum energies before ever calculating model Intensities
    # Therefore we have to instantiate and hold all the imagesets! More than I bargained for!
    current_imageset_path = None
    hold_all_refl = []
    spectra = []
    for expt_id, expt in enumerate(old_experiments):
      print("Instantiate imageset of",expt.identifier)
      assert len(expt.imageset.paths()) == 1 and len(expt.imageset) == 1
      if expt.imageset.paths()[0] != current_imageset_path:
        current_imageset_path = expt.imageset.paths()[0]
        current_imageset = ImageSetFactory.make_imageset(expt.imageset.paths())
      idx = expt.imageset.indices()[0]
      expt.imageset = current_imageset[idx:idx+1]
      spectra.append( expt.imageset.get_spectrum(0) )
      refl = old_reflections.select(old_reflections['id']==expt_id)
      hold_all_refl.append(refl)
      expt.crystal = self.ensure_p1(expt.crystal) # unknown what would happen if we failed to
                                                  # get the refl table to primitive !!! XXX FIXME

    # validation of spectra; could potentially be adapted if spectrometer x-scales are not all equal
    energies, fluxes = utils.basic_run_manager.get_modified_spectrum(spec = spectra[0],
                       energy_offset_eV = self.params.exafel.debug.energy_offset_eV,
                       energy_stride_eV = self.params.exafel.debug.energy_stride_eV,
                       energy_plot = self.params.exafel.debug.energy_plot)
    for idx,spec in enumerate(spectra[1:]):
           ener,flux = utils.basic_run_manager.get_modified_spectrum(spec = spec,
                       energy_offset_eV = self.params.exafel.debug.energy_offset_eV,
                       energy_stride_eV = self.params.exafel.debug.energy_stride_eV,
                       energy_plot = self.params.exafel.debug.energy_plot)
           assert list(ener) == list(energies)

    self.HKL_lookup, self.Fmodel_indices, self.static_fcalcs = self.get_static_fcalcs(energies)
    # HKL_lookup: dictionary keyed on Miller index
    # Fmodel_indices: flex.miller_index
    # static_fcalcs: flex.complex_double
    # static_miller_array: miller array with type complex double

    #next task is to do the get_intensity_structure at the beginning of the macrocycle.
    # macrocycle 1 ---------------------------------------------------------
    # generate model_intensities table based on initial conditions
    current_model_intensities = self.get_intensity_structure(
      energies, self.static_fcalcs,
      preset_starting_models = self.case_specific_starting_models,
      labels = self.labels)
    this_focus = current_model_intensities.focus()

    assert this_focus[0] == len(self.Fmodel_indices)
    n_columns = 1 + 2 * self.n_classes
    assert this_focus[1] == n_columns * len(energies)
    print ("CHECK 1")

    # set up the amplitudes in the same style as exa3A. DONE
    self.setup_gpu_amplitudes(energies, current_model_intensities)
    assert self.gpu_polychromatic_channels_singleton.get_nchannels() == len(energies)
    print ("CHECK 2 with %d channels to GPU"%(self.gpu_polychromatic_channels_singleton.get_nchannels() ))

    # instantiate a list of utils.basic_run_managers, one for each lattice. Two subissues
    #   how to insert the polychromatic amplitudes instead of mono? DONE, use edge="poly"
    #   how to insert the spectra. DONE, there is a mono/poly option on refine_bragg_single_lattice

    # refine the G+abc for each lattice.  Make sure it is on all shoebox pixels, not border
    all_lattice_managers = []
    all_lattice_models = []
    all_lattice_errors = Counter()
    for expt_id, expt in enumerate(old_experiments):
      print("Beginning G+abc of",expt.identifier)
      try:
        run_mgr, run_mdl = self.thin_exa1(expt, hold_all_refl[expt_id])
      except RuntimeError as e:
        print("LATTICE RuntimeError",expt.imageset.paths()[0],expt.imageset.indices()[0])
        tb.print_exc(file=sys.stdout)
        all_lattice_errors[repr(e)] += 1
      except AssertionError as e:
        print("LATTICE AssertionError",e)
        tb.print_exc(file=sys.stdout)
        all_lattice_errors[repr(e)] += 1
      else:
        all_lattice_managers.append(run_mgr)
        all_lattice_models.append(run_mdl)
      print("Done with G+abc of",expt.identifier,"\n")

    print("CHECK 3 with %d good lattice managers and %d exceptions:" %
          (len(all_lattice_managers), sum(all_lattice_errors.values())))
    for error_repr, error_count in all_lattice_errors.items():
      print("- %4d x " % error_count + error_repr)

    # -----------------------------------------------------------------------
    # using the local version of rank_0_fit_all_f
    self.rank_0_fit_all_f_setup( starting_models = self.case_specific_starting_models, energies = energies )

    self.reinitialize(self.logical_rank, self.logical_size,
                 all_lattice_managers,
                 all_lattice_models,
                 HKL_lookup = self.HKL_lookup,
                 static_fcalcs = self.static_fcalcs,
                 model_intensities = current_model_intensities,
                 )
    self.set_macrocycle(1)

    minimizer = mpi_split_evaluator_run(target_evaluator=self,
        termination_params=scitbx.lbfgs.termination_parameters(
        traditional_convergence_test=True,
        traditional_convergence_test_eps=1.e-2,
        max_calls=self.params.sauter20.LLG_evaluator.max_calls)
    )

    self.mpi_helper.comm.barrier()
    # 2nd macrocycle---------------------------------------------------------

    if True:
      #Make sure the Gscale refinement is restartable with a hotstart to reflect the last values of G.
      self.mpi_helper.comm.barrier()
      model_ok=0
      for expt_id, expt in enumerate(old_experiments):
        print("Second G refinement",expt.identifier)
        run_mgr = all_lattice_managers[expt_id]
        run_mdl = all_lattice_models[expt_id]
        # skip initialization, just run again
        run_mdl.run()
        model_ok+=1
        print("Done2nd G of",expt.identifier,"\n")
      print ("CHECK 4 with %d lattice models re-refined"%model_ok)
      self.mpi_helper.comm.barrier()

    del self.gpu_polychromatic_channels_singleton
    # explicitly push Kokkos allocation device_Fhkl out of scope

  def setup_gpu_amplitudes(self,energy_channels, current_intensities):

    if self.params.exafel.debug.write_mtz and self.mpi_helper.rank==0:
        for numerical_key  in range(len(energy_channels)): # polychromatic model
          wavelength = ENERGY_CONV / energy_channels[numerical_key]
          temp_miller = self.static_miller_array.customized_copy(
            indices = self.Fmodel_indices,
            data = flex.sqrt(
                      current_intensities.matrix_copy_block(i_row=0,i_column=numerical_key,
                      n_rows=self.Fmodel_indices.size(),n_columns=1).as_1d()),
            ).set_observation_type_xray_amplitude()
          temp_miller.write_mtz(
            "sw1_structure_factors_%04d_%04d.mtz"%(numerical_key,energy_channels[numerical_key]),
            wavelength=wavelength)
    self.mpi_helper.comm.barrier()

    gpu_instance = get_exascale("gpu_instance", self.params.exafel.context)
    gpu_energy_channels = get_exascale("gpu_energy_channels", self.params.exafel.context)
    deviceId = (self.mpi_helper.rank)%int(os.environ.get("CCTBX_GPUS_PER_NODE","1"))
    self.gpu_run = gpu_instance( deviceId = deviceId )
    self.gpu_polychromatic_channels_singleton = gpu_energy_channels ( deviceId = deviceId )
    assert self.gpu_polychromatic_channels_singleton.get_nchannels() == 0 #uninitialized
    for x in range(len(energy_channels)): # polychromatic model
          self.gpu_polychromatic_channels_singleton.structure_factors_to_GPU_direct(
           x, self.Fmodel_indices, flex.sqrt(
                      current_intensities.matrix_copy_block(i_row=0,i_column=x,
                      n_rows=self.Fmodel_indices.size(),n_columns=1).as_1d()
           )) # amplitude = sqrt(intensity)

  def update_gpu_amplitudes(self,energy_channels, current_intensities):

    assert self.gpu_polychromatic_channels_singleton.get_nchannels() == len(self.energies)
    for x in range(len(energy_channels)): # polychromatic model
          self.gpu_polychromatic_channels_singleton.structure_factors_replace_GPU_direct(
           x, self.Fmodel_indices, flex.sqrt(
                      current_intensities.matrix_copy_block(i_row=0,i_column=x,
                      n_rows=self.Fmodel_indices.size(),n_columns=1).as_1d()
           )) # amplitude = sqrt(intensity)

  def ensure_p1(self, Crystal):
    high_symm_info = Crystal.get_space_group().info()
    self.cb_op = high_symm_info.change_of_basis_op_to_primitive_setting()
    dterm = sqr(self.cb_op.c().r().as_double()).determinant()
    if dterm != 1:
      Crystal = Crystal.change_basis(self.cb_op)
    return Crystal

  def thin_exa1(self, expt, refl):
    M = utils.basic_run_manager.from_mask_file_expt_refl(
              self.params.exafel.trusted_mask, expt, refl)
    M.gpu_channels_singleton = self.gpu_polychromatic_channels_singleton
    M.general_trusted_and_refl_mask(self.params.exafel)
    M.data_structures_for_refls()
    M.get_image_res_data() # get experimental data
    M.spread_modify_shoeboxes() # superimpose dials planar fit over the shoeboxes
    # put in proposal planes here instead of down below
    M.minimize_planes_single_lattice(raw_pixels=M.view["exp_data"], params= self.params,
                                     publish_to="plane_shoeboxes")
    M.define_greenlist(background_from="plane_shoeboxes")
    M.Z_statistics(experimental=M.view["exp_data"],
                   model=M.view["plane_shoeboxes"],
                   report=False,
                   method="border",
                   plot=False)
    # z-mean is slightly higher in the second call, Bragg spot is adding positive intensity at border
    # Z-mean is slightly positive and gets worse at higher angle.
    if self.params.exafel.skin:
      M.miller_array_diffBragg = self.sfall_primitive_setting
    M.flattened_exa_sim(mod_expt = expt, rank=self.mpi_helper.rank)
    POL = M.refine_bragg_single_lattice(raw_pixels=M.view["exp_data"], params= self.params,
                                  background_from="plane_shoeboxes", publish_to=None, edge="poly")
    bragg_proposal, background_proposal = POL.get_adu_model()

    M.view["bragg_plus_background"] = M.reusable_rmsd_from_whitelist(
      bragg_proposal=bragg_proposal, background_proposal=background_proposal,
      label="ersatz_mcmc",plot=self.params.exafel.model.plot,
      legend="Gradient-refined simulation: %s "%expt.identifier) # Plot 3
    M.Z_statistics(experimental=M.view["exp_data"],
                   model=M.view["bragg_plus_background"],
                   report=False,
                   method="border",
                   plot=False)
    M.view["Z_plot"] = M.Z_statistics(experimental=M.view["exp_data"],
                                           model=M.view["bragg_plus_background"],
                                           report=True,
                                           method="all",
                                           plot=False)

    if self.params.exafel.debug.lastfiles:
      M.write_hdf5(filenm = "%s.h5"%(expt.identifier))
      M.resultant_mask_to_file("%s.mask"%(expt.identifier))
      utils.write_expt_and_refl_table(expt, refl, exptpath="local22.expt", reflpath="local22.refl")
    return M, POL
    # OK to return at this point after getting the G+abc
    # returning the run manager rather than expt + refl because we want to reuse it.
    # better if we can instantiate M and hold it, then make calls for G+abc later
    # do this refactor after we get G+abc at first.

    # C-language memory management
    M.nB.free_all()
    if M.dB: M.dB.free_all()
    print ("Free'd C-variables")
    return expt, M.refl_table

if __name__ == "__main__":
  class Empty: pass
  def nothing(): pass
  C =intensities_mix_in() #modeled after LS49.work2_for_aca_lsq.remake_range_intensities_with_complex
  C.mpi_helper = Empty(); C.mpi_helper.comm = Empty(); C.mpi_helper.comm.barrier = nothing
  C.params = Empty(); C.params.exafel = Empty(); C.params.exafel.metal="PSII4"
  C.params.scaling = Empty(); C.params.scaling.model="7RF1_refine_030_Aa_refine_032_refine_034.pdb"
  C.get_hi_sym_structures()
