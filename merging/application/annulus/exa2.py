from __future__ import division
from LS49.adse13_187.adse13_221.basic_runner import basic_run_manager
from LS49.sim.util_fmodel import gen_fmodel
from dxtbx.model.experiment_list import ExperimentList
from dials.array_family import flex
from scitbx.matrix import sqr
from simtbx import get_exascale
from scipy import constants
ENERGY_CONV = 1e10 * constants.c * constants.h / constants.electron_volt

msg = "Run exascale API for SPREAD trial 1"
# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.
import utils
from dxtbx.imageset import ImageSetFactory

class prepare:
  def __init__(self, old_experiments, old_reflections, params, mpi_helper, mpi_logger, plot=True):
    self.mpi_helper = mpi_helper
    self.mpi_logger = mpi_logger
    self.params = params
    self.uc = self.params.scaling.unit_cell
    self.setup_amplitudes()
    self.result_expt = ExperimentList()
    self.result_refl = flex.reflection_table()
    # Re-generate the image sets using their format classes so we can read the raw data
    # Integrate the experiments one at a time to not use up memory
    current_imageset = None
    current_imageset_path = None
    for expt_id, expt in enumerate(old_experiments):
      assert len(expt.imageset.paths()) == 1 and len(expt.imageset) == 1
      if expt.imageset.paths()[0] != current_imageset_path:
        current_imageset_path = expt.imageset.paths()[0]
        current_imageset = ImageSetFactory.make_imageset(expt.imageset.paths())
      idx = expt.imageset.indices()[0]
      expt.imageset = current_imageset[idx:idx+1]
      S = expt.imageset.get_spectrum(0) # code not yet correct for polychromatic model
      refl = old_reflections.select(old_reflections['id']==expt_id)
      oneexpt, onerefl = self.thin_exa1(expt, refl)
      self.result_expt.append(oneexpt)
      self.result_refl.extend(onerefl)
      expt.imageset = None
    self.result_refl.reset_ids()
    del self.gpu_channels_singleton # confirmed destruction before calling kokkos::finalize()

  def setup_amplitudes(self):
    wavelength_A = 1.74 # general ballpark X-ray wavelength in Angstroms
    wavlen = flex.double([wavelength_A])
    epsilon = 0.05
    direct_algo_res_limit = self.params.statistics.annulus.d_min-epsilon
    GF = gen_fmodel(resolution=direct_algo_res_limit,
       pdb_text=open(self.params.scaling.model,"r").read(),
       algorithm="fft",wavelength=wavelength_A)
    GF.set_k_sol(0.435)
    GF.make_P1_primitive()
    sfall_channels = {}
    for x in range(len(wavlen)):
      sfall_channels[x]=GF.get_amplitudes()
    # there is an MPI-assisted algorithm for polychromatic beam

    gpu_instance = get_exascale("gpu_instance", self.params.exafel.context)
    gpu_energy_channels = get_exascale("gpu_energy_channels", self.params.exafel.context)
    self.gpu_run = gpu_instance( deviceId = int(os.environ.get("CCTBX_RECOMMEND_DEVICE", 0)) )
    self.gpu_channels_singleton = gpu_energy_channels (
         deviceId = int(os.environ.get("CCTBX_RECOMMEND_DEVICE", 0)))
    assert self.gpu_channels_singleton.get_nchannels() == 0 #uninitialized
    for x in range(1): # later, len(flux)): # code not yet correct for polychromatic model
          self.gpu_channels_singleton.structure_factors_to_GPU_direct(
           x, sfall_channels[x].indices(), sfall_channels[x].data())

  def ensure_p1(self, Crystal):
    high_symm_info = Crystal.get_space_group().info()
    self.cb_op = high_symm_info.change_of_basis_op_to_primitive_setting()
    dterm = sqr(self.cb_op.c().r().as_double()).determinant()
    if dterm != 1:
      Crystal = Crystal.change_basis(self.cb_op)
    return Crystal

  def thin_exa1(self, expt, refl):
    self.mpi_logger.log(expt.crystal.as_str())
    M = utils.basic_run_manager.from_mask_file_expt_refl(
              self.params.exafel.trusted_mask, expt, refl)
    M.gpu_channels_singleton = self.gpu_channels_singleton
    M.general_trusted_and_refl_mask(self.params.exafel)
    M.data_structures_for_refls()
    M.get_image_res_data() # get experimental data
    M.spread_modify_shoeboxes() # superimpose dials planar fit over the shoeboxes
    expt.crystal = self.ensure_p1(expt.crystal)
    self.mpi_logger.log(expt.crystal.as_str())
    nanobragg_sim = M.thin_exa_sim(mod_expt = expt) # initial Bragg simulation
    M.view["bragg_plus_background"] = M.reusable_rmsd(
      proposal=nanobragg_sim, label="ersatz_mcmc",plot=self.params.exafel.model.plot,
      legend="Unnormalized simulation: %s "%expt.identifier) # Plot 3
    M.view["Z_plot"] = M.Z_statistics(experimental=M.view["exp_data"],
                                           model=M.view["bragg_plus_background"],
                                           plot=False)
    # key point implemented here is to improve the Rossmann background over
    # DIALS model before proceeding to global minimization
    M.minimize_planes(experimental=M.view["exp_data"])
    M.Z_statistics(experimental=M.view["exp_data"],
                                           model=M.view["bragg_plus_background"],
                                           plot=False)

    if self.params.exafel.debug.lastfiles:
      M.write_hdf5(filenm = "%s.h5"%(expt.identifier))
      M.resultant_mask_to_file("%s.mask"%(expt.identifier))
      utils.write_expt_and_refl_table(expt, refl, exptpath="local22.expt", reflpath="local22.refl")
    return expt, M.refl_table

