from __future__ import division
from collections import Counter
from xfel.merging.application.worker import worker
from dials.array_family import flex
from dxtbx.model.experiment_list import ExperimentList

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.
import rmsd1, rmsd2, ds1, exa1, exa2, exa3, exa3A, sw1, utils
import planes_one_lattice # implicit import used by utils
"""
Enabling spread_roi.enable activates the following behaviors:
  requirement for shoeboxes, which is satisfied by input.persistent_refl_cols=shoebox
  prints out total lattices and shoebox pixels
  the persistent reflection table gains a column indicating if reflection is used for spread
  optionally filter reflections to take only strong spots with minimum I/sigma
  requires each lattice to have a minimum number of ROI spots
  avoid a bug found in annulus_statistics.py, must map_to_asu before counting unique indices
  a more verbose description of counting the unique Miller indices, completness & multiplicity
"""


class spread_roi(worker):

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super(spread_roi, self).__init__(params=params, mpi_helper=mpi_helper, mpi_logger=mpi_logger)

  def __repr__(self):
    return "Calculate reflection statistics in a resolution annulus"

  def test_product_0(self, experiments, reflections):
    """In this scenario, the input method is asked to keep_imagesets and read_image_headers.
    We then count the total number of spectra and raw data arrays available.  The total count
    reported in the main log should agree with the number of lattices passing the unit cell filter."""
    icount = 0
    for expt in experiments:
      iset = expt.imageset
      print("ZZZ test_product_0 rank",self.mpi_helper.rank, "image",iset.paths()[0],
            "spec", iset.get_spectrum(0).get_energies_eV().size(), "data",iset[0][0].size())
      icount += 1
    self.logger.log("Test_product_0 reads %d spectra and data arrays in rank %d"%(icount, self.mpi_helper.rank))
    icount = self.mpi_helper.comm.reduce(icount, self.mpi_helper.MPI.SUM)
    if self.mpi_helper.rank == 0:
      self.logger.main_log("Test_product_0 reads %d spectra and data arrays"%(icount))

    return icount, experiments, reflections

  def test_product_00(self, experiments, reflections):
    """In this scenario, the input worker is asked to keep_imagesets but not read_image_headers.
    The downstream worker then reconstructs the imageset and reads the spectra and raw data arrays.  The total count
    reported in the main log should agree with the number of lattices passing the unit cell filter."""
    # Re-generate the image sets using their format classes so we can read the raw data
    # Integrate the experiments one at a time to not use up memory
    from dxtbx.imageset import ImageSetFactory
    all_integrated = flex.reflection_table()
    current_imageset = None
    current_imageset_path = None
    icount = 0
    for expt_id, expt in enumerate(experiments):
      assert len(expt.imageset.paths()) == 1 and len(expt.imageset) == 1
      if expt.imageset.paths()[0] != current_imageset_path:
        current_imageset_path = expt.imageset.paths()[0]
        current_imageset = ImageSetFactory.make_imageset(expt.imageset.paths())
      idx = expt.imageset.indices()[0]
      expt.imageset = current_imageset[idx:idx+1]

      print("ZZZ test_product_00 rank",self.mpi_helper.rank, "image",expt.imageset.paths()[0],
            "spec", expt.imageset.get_spectrum(0).get_energies_eV().size(), "data",expt.imageset[0][0].size())
      expt.imageset = None # manage data lifetime to conserve memory
      """Regarding the memory budget on Perlmutter: 32 ranks/node gives a budget of 8.0 GiB/rank.
      0.4 GB used prior to annulus worker.  1.6 GB upon loading the first spectrum.
      0.166 GB each time accessing a new imageset[0].  After processing 30 images, this is 5 GB, about the limit.
      14.7 Mpx * 8 byte double should take only 118 MB, so it is not clear what the other 48 MB contains.
      """
      icount += 1
    self.logger.log("Test_product_00 reads %d spectra and data arrays in rank %d"%(icount, self.mpi_helper.rank))

    icount  = self.mpi_helper.comm.reduce(icount, self.mpi_helper.MPI.SUM)
    if self.mpi_helper.rank == 0:
      self.logger.main_log("Test_product_00 reads %d spectra and data arrays"%(icount))

    return icount, experiments, reflections

  def asu_analysis(self, miller_indices, unit_cell, space_group_info):
    nobs = len(miller_indices)

    from cctbx.crystal import symmetry
    S = symmetry(unit_cell=unit_cell,space_group_info=space_group_info)
    S.show_summary()

    ref_no_merge_anom = S.build_miller_set(anomalous_flag=True,
      d_min=self.params.statistics.annulus.d_min, d_max=self.params.statistics.annulus.d_max)

    ref_merge_anom = S.build_miller_set(anomalous_flag=False,
      d_min=self.params.statistics.annulus.d_min, d_max=self.params.statistics.annulus.d_max)

    exp_no_merge_anom = S.miller_set(anomalous_flag=True, indices=miller_indices)
    asu_no_merge = exp_no_merge_anom.map_to_asu()

    exp_merge_anom = S.miller_set(anomalous_flag=False, indices=miller_indices)
    asu_merge = exp_merge_anom.map_to_asu()
    self.logger.main_log("Not merging Friedel mates:")
    A = len(set(asu_no_merge.indices())); B = len(set(ref_no_merge_anom.indices()))
    self.logger.main_log("  %d unique indices of %d theoretical, %4.1f%% complete with multiplicity %5.1f"%(
      A, B, 100.*A/B, nobs/B
    ))
    self.logger.main_log("Merging Friedel mates:")
    A = len(set(asu_merge.indices())); B = len(set(ref_merge_anom.indices()))
    self.logger.main_log("  %d unique indices of %d theoretical, %4.1f%% complete with multiplicity %5.1f\n"%(
      A, B, 100.*A/B, nobs/B
    ))

  def define_spread_annulus(self, experiments, reflections):
    """In this scenario, there are no imagesets or read_image_headers.
    The worker defines and flags the set of shoeboxes for subsequent spread analysis.
    RMSD statistics are reported, both for the spread shoeboxes and for the whole set of reflections."""

    uc = self.params.scaling.unit_cell
    reflections["spread_roi"] = flex.bool(len(reflections),True)

    all_refl_in_annulus = []
    all_d_spacings = uc.d(reflections['miller_index'])

    d_min = self.params.statistics.annulus.d_min
    d_max = self.params.statistics.annulus.d_max
    global_in_annulus = (all_d_spacings > d_min) & (all_d_spacings < d_max)

    if self.params.spread_roi.strong:
      I_over_sigma = reflections['intensity.sum.value.unmodified'] / \
                     flex.sqrt(reflections['intensity.sum.variance.unmodified'])
      global_strong_spots = I_over_sigma > self.params.spread_roi.strong

    n_annulus_high_res_tail = 0
    n_annulus_low_res_tail = 0
    n_in_annulus = 0

    #print("global_in_annulus",global_in_annulus.count(True))
    #print("strong",(global_in_annulus & global_strong_spots).count(True)))
    N_annulus_checksum = 0
    N_annulus_strong = 0
    N_annulus_enough = 0
    n_lattice_only_low_res = 0
    n_lattice_insufficient_annulus_strong = 0

    for expt_id, expt in enumerate(experiments):
      # Make sure we are merging all the same space group
      # somewhat confusing to test space group equality
      # self.params.scaling.space_group is already a space_group_info (it is how phil works)
      esi = expt.crystal.get_crystal_symmetry().space_group_info()
      psi = self.params.scaling.space_group
      assert str(esi) == str(psi)

      self.logger.log('Annulus statistics: ' + expt.identifier)
      refl = reflections.select(reflections['id']==expt_id)
      dspacings = uc.d(refl['miller_index'])
      gt = dspacings > d_min # not the high resolution tail
      lt = dspacings < d_max # not the low resolution tail
      n_annulus_high_res_tail += gt.count(False)
      n_annulus_low_res_tail += lt.count(False)
      if lt.count(False) == len(refl):  n_lattice_only_low_res += 1

      refl_selection = gt & lt
      n_in_annulus += refl_selection.count(True)
      expt_in_annulus = refl_selection.count(True)
      N_annulus_checksum += expt_in_annulus

      if self.params.spread_roi.strong:
        I_over_sigma = refl['intensity.sum.value.unmodified'] / \
                     flex.sqrt(refl['intensity.sum.variance.unmodified'])
        refl_selection &= I_over_sigma > self.params.spread_roi.strong
      N_annulus_strong += refl_selection.count(True)

      if refl_selection.count(True) < self.params.spread_roi.min_spots:
        refl_selection = flex.bool(refl_selection.size(), False)
      N_annulus_enough += refl_selection.count(True)

      refl = refl.select(refl_selection)
      all_refl_in_annulus.append(refl)
      if refl_selection.count(True)==0:  n_lattice_insufficient_annulus_strong += 1
      iset = expt.imageset
      self.logger.log(
          '{}:{} : {} spots in annulus'.format(
              iset.paths()[0], iset.indices()[0], refl.size()))
      #utils.write_expt_and_refl_table(expt, refl, exptpath="local19.expt", reflpath="local19.refl") # for debug

      # for the master reflections table: gnarly boolean algebra to incorporate new criterion
      # 4 criteria are considered altogether: in_this_experiment, in_annulus, strong_spots, min_spots
      not_in_this_experiment = reflections['id']!=expt_id
      reflections["spread_roi"] &= ((global_in_annulus) | (not_in_this_experiment))

      # additional criterion of strong spots:
      if self.params.spread_roi.strong:
        reflections["spread_roi"] &= ((global_strong_spots) | (not_in_this_experiment))
      if refl_selection.count(True) < self.params.spread_roi.min_spots:
        reflections["spread_roi"] &= not_in_this_experiment
    if all_refl_in_annulus:
      all_refl_in_annulus = flex.reflection_table.concat(all_refl_in_annulus)
      # Extract shoebox size here
      all_refl_in_annulus["shoebox_size"] = flex.int(
        [item.data.size() for item in all_refl_in_annulus["shoebox"]]
      )
      # drop the shoebox column to avoid out-of-memory in rank 0
      del all_refl_in_annulus["shoebox"]
    else:
      all_refl_in_annulus = flex.reflection_table()

    n_lattice_input = self.mpi_helper.sum(len(experiments))
    n_lattice_only_low_res = self.mpi_helper.sum(n_lattice_only_low_res)
    n_lattice_insufficient_annulus_strong = self.mpi_helper.sum(n_lattice_insufficient_annulus_strong)

    n_annulus_high_res_tail = self.mpi_helper.sum(n_annulus_high_res_tail)
    n_annulus_low_res_tail = self.mpi_helper.sum(n_annulus_low_res_tail)
    n_in_annulus = self.mpi_helper.sum(n_in_annulus)
    N_annulus_checksum = self.mpi_helper.sum(N_annulus_checksum)
    N_annulus_strong = self.mpi_helper.sum(N_annulus_strong)
    N_annulus_enough = self.mpi_helper.sum(N_annulus_enough)

    in_roi_counter = Counter(reflections["spread_roi"])
    in_roi_counters = self.mpi_helper.comm.gather(in_roi_counter, root=0)

    shoebox_sizes = self.mpi_helper.aggregate_flex(all_refl_in_annulus.get('shoebox_size'))
    miller_indices = self.mpi_helper.aggregate_flex(all_refl_in_annulus.get('miller_index'))
    expt_identifiers = self.mpi_helper.aggregate_flex(
      all_refl_in_annulus.experiment_identifiers().values())
    intensities = self.mpi_helper.aggregate_flex(all_refl_in_annulus.get('intensity.sum.value.unmodified'))
    variances = self.mpi_helper.aggregate_flex(all_refl_in_annulus.get('intensity.sum.variance.unmodified'))

    if self.mpi_helper.rank==0:
      print("input lattices: %d"%n_lattice_input)
      print("lattice removed having only low res reflections: %d"%n_lattice_only_low_res)
      print("lattice removed having insufficent annulus refls: %d"%(n_lattice_insufficient_annulus_strong-n_lattice_only_low_res))

      print("final lattices: %d"%(len(set(expt_identifiers))))
      print("total shoeboxes: {}".format(len(shoebox_sizes)))
      print("total shoebox pixels: %d"%(sum(shoebox_sizes)))

      assert n_in_annulus == N_annulus_checksum
      annulus_breakdown = """Breakdown of %d reflections:
      Low-resolution tail: %d; High-resolution tail: %d; in %5.3f-%5.3fÅ annulus: %d"""%(
      n_annulus_high_res_tail + n_in_annulus + n_annulus_low_res_tail,
      n_annulus_low_res_tail, n_annulus_high_res_tail, self.params.statistics.annulus.d_max,
      self.params.statistics.annulus.d_min, n_in_annulus) + """
      Annulus & strong: %d; annulus & strong & min_spots(%d): %d"""%(
      N_annulus_strong, self.params.spread_roi.min_spots, N_annulus_enough)
      print(annulus_breakdown)
      print()

      self.logger.main_log(
        "input lattices: %d\n"%n_lattice_input +
        "lattice removed having only low res reflections: %d\n"%n_lattice_only_low_res +
        "lattice removed having insufficent annulus refls: %d\n"%(n_lattice_insufficient_annulus_strong-n_lattice_only_low_res) +
        "final lattices: %d\n"%(len(set(expt_identifiers))) +
        "total shoeboxes: {}\n".format( len(shoebox_sizes) ) +
        "total shoebox pixels: %d\n"%(sum(shoebox_sizes)) +
        annulus_breakdown +
        "\n"
      )

      self.asu_analysis(miller_indices=miller_indices, unit_cell=uc,
                        space_group_info = self.params.scaling.space_group)
      print()
      if self.params.spread_roi.plot:
        I_over_sigma = intensities / flex.sqrt(variances)
        from matplotlib import pyplot as plt
        plt.hist(I_over_sigma,bins=205,range=(-5.0,200.0))
        plt.show()

      all_in_roi_counter = sum(in_roi_counters, Counter())
      msg = f"Global total of refls {sum(all_in_roi_counter.values())}, " \
            f"of which the SPREAD ROI contains {all_in_roi_counter[True]}"
      print(msg)
      self.logger.main_log(msg)

  def rmsd_deliverables(self, experiments, reflections, model="xyzcal.px", filter_zero=None):
    #additional analysis
    rmsd1.deliverable1(experiments, reflections, params=self.params, mpi_helper=self.mpi_helper, mpi_logger=self.logger, model=model, filter_zero=filter_zero)

    #deliverable #2 add a binner to RMSD
    if self.mpi_helper.rank==0:
      print("spread ROI")
      self.logger.main_log("spread ROI")
    bins2 = rmsd2.deliverable2(experiments, reflections, n_bins=self.params.statistics.n_bins,
            d_max=self.params.statistics.annulus.d_max, d_min=self.params.statistics.annulus.d_min,
            params=self.params, mpi_helper=self.mpi_helper, mpi_logger=self.logger, model=model, filter_zero=filter_zero)
    #spread ROI, one bin
    bins2 = rmsd2.deliverable2(experiments, reflections, n_bins=1,
            d_max=self.params.statistics.annulus.d_max, d_min=self.params.statistics.annulus.d_min,
            params=self.params, mpi_helper=self.mpi_helper, mpi_logger=self.logger, header=False, model=model, filter_zero=filter_zero)
    if self.mpi_helper.rank==0:
      print("\nall data")
      self.logger.main_log("\nall data")
    bins2 = rmsd2.deliverable2(experiments, reflections, n_bins=self.params.statistics.n_bins,
            d_max=self.params.merging.d_max, d_min=self.params.merging.d_min,
            params=self.params, mpi_helper=self.mpi_helper, mpi_logger=self.logger, model=model, filter_zero=filter_zero)
    #all data, one bin
    bins2 = rmsd2.deliverable2(experiments, reflections, n_bins=1,
            d_max=self.params.merging.d_max, d_min=self.params.merging.d_min,
            params=self.params, mpi_helper=self.mpi_helper, mpi_logger=self.logger, header=False, model=model, filter_zero=filter_zero)
    store_transverse_sigma = bins2.DF["rms_tangential_offset"]; assert len(store_transverse_sigma)==1; store_transverse_sigma=store_transverse_sigma[0]
    store_three_param_fit = bins2.TF
    self.common_store["store_transverse_sigma"] = store_transverse_sigma
    self.common_store["store_three_param_fit"] = store_three_param_fit

  @staticmethod
  def annulus_filter(experiments, reflections):
    '''Remove experiments having no ROI reflections. Remove corresponding reflections'''

    new_experiments = ExperimentList()
    new_reflections = flex.reflection_table()

    for expt_id, expt in enumerate(experiments):
      refl = reflections.select(reflections['id']==expt_id)
      if refl["spread_roi"].count(True) == 0: continue # no reflections are in the SPREAD roi
      new_experiments.append(expt)
      new_reflections.extend(refl)
    new_reflections.reset_ids()

    return new_experiments, new_reflections

  def run(self, experiments, reflections):
    if self.params.exafel.scenario=="0":
      self.test_product_0(experiments, reflections)
      return experiments, reflections

    elif self.params.exafel.scenario=="00":
      self.test_product_00(experiments, reflections)
      return experiments, reflections

    elif self.params.exafel.scenario=="1":
      self.define_spread_annulus(experiments, reflections)
      self.rmsd_deliverables(experiments, reflections)
      new_experiments, new_reflections = self.annulus_filter(experiments, reflections)
      #return experiments, reflections
      # Do we have any data left?
      from xfel.merging.application.utils.data_counter import data_counter
      data_counter(self.params).count(new_experiments, new_reflections)
      return new_experiments, new_reflections

    elif self.params.exafel.scenario=="2":
      self.define_spread_annulus(experiments, reflections)
      self.rmsd_deliverables(experiments, reflections)
      self.gpu_holder = exa2.prepare(experiments, reflections, params=self.params,
                        mpi_helper=self.mpi_helper, mpi_logger=self.logger)
      self.rmsd_deliverables(experiments, self.gpu_holder.result_refl,
                             model="ersatz_mcmc_xyzcal.px")
      return experiments, reflections

    elif self.params.exafel.scenario=="3":
      self.define_spread_annulus(experiments, reflections)
      self.rmsd_deliverables(experiments, reflections)
      self.gpu_holder = exa3.prepare(experiments, reflections, params=self.params,
                        mpi_helper=self.mpi_helper, mpi_logger=self.logger)
      self.rmsd_deliverables(experiments, self.gpu_holder.result_refl,
                             model="ersatz_mcmc_xyzcal.px")
      return experiments, reflections

    elif self.params.exafel.scenario=="3A":
      self.define_spread_annulus(experiments, reflections)
      self.rmsd_deliverables(experiments, reflections)
      self.gpu_holder = exa3A.prepare(experiments, reflections, params=self.params,
                        mpi_helper=self.mpi_helper, mpi_logger=self.logger)
      self.rmsd_deliverables(self.gpu_holder.result_expt, self.gpu_holder.result_refl,
                             model="ersatz_mcmc_xyzcal.px",filter_zero="ersatz_mcmc_shoebox_sum")
      self.mpi_helper.comm.barrier()
      # Do we have any data? boilerplate
      from xfel.merging.application.utils.data_counter import data_counter
      data_counter(self.params).count(self.gpu_holder.result_expt, self.gpu_holder.result_refl)
      return self.gpu_holder.result_expt, self.gpu_holder.result_refl

    elif self.params.exafel.scenario=="S1": # spread week 1
      self.define_spread_annulus(experiments, reflections)
      self.rmsd_deliverables(experiments, reflections)
      self.gpu_holder = sw1.prepare(experiments, reflections, params=self.params,
                        mpi_helper=self.mpi_helper, mpi_logger=self.logger)
      return experiments, reflections
      self.rmsd_deliverables(self.gpu_holder.result_expt, self.gpu_holder.result_refl,
                             model="ersatz_mcmc_xyzcal.px",filter_zero="ersatz_mcmc_shoebox_sum")
      self.mpi_helper.comm.barrier()
      # Do we have any data? boilerplate
      from xfel.merging.application.utils.data_counter import data_counter
      data_counter(self.params).count(self.gpu_holder.result_expt, self.gpu_holder.result_refl)
      self.mpi_helper.comm.barrier()
      return self.gpu_holder.result_expt, self.gpu_holder.result_refl

    elif self.params.exafel.scenario=="ds1":
      self.define_spread_annulus(experiments, reflections)
      self.rmsd_deliverables(experiments, reflections)
      #run items through diffBragg Stage 1
      self.gpu_holder = ds1.prepare(experiments, reflections, params=self.params, mpi_helper=self.mpi_helper)
      self.mpi_helper.comm.barrier()
      result_expt = self.gpu_holder.result_expt
      result_refl = self.gpu_holder.result_refl
      self.rmsd_deliverables(result_expt, result_refl,
                             model="xyzcal.px")
      self.mpi_helper.comm.barrier()
      return result_expt, result_refl

    self.define_spread_annulus(experiments, reflections)
    self.rmsd_deliverables(experiments, reflections)
    #run items through exascale api 1
    exa = exa1.prepare(experiments, reflections, params=self.params, mpi_helper=self.mpi_helper,
           mpi_logger=self.logger)
    self.rmsd_deliverables(experiments, exa.result_refl, model="ersatz_mcmc_xyzcal.px")

    #end of additional analysis
    return experiments, reflections

# items in the data structures, for documentation purposes only:
retained_keys = ['id', 'flags', 'intensity.sum.value', 'intensity.sum.value.unmodified', 'intensity.sum.variance', 'intensity.sum.variance.unmodified', 'miller_index', 's1', 'shoebox_size']

shoebox_dir=['all_foreground_valid', 'allocate', 'background', 'bayesian_intensity', 'bbox', 'beam_vectors', 'centroid_all', 'centroid_all_minus_background', 'centroid_foreground', 'centroid_foreground_minus_background', 'centroid_masked', 'centroid_masked_minus_background', 'centroid_strong', 'centroid_strong_minus_background', 'centroid_valid', 'centroid_valid_minus_background', 'coords', 'count_mask_values', 'data', 'deallocate', 'does_bbox_contain_bad_pixels', 'flat', 'flatten', 'is_allocated', 'is_bbox_within_image_volume', 'is_consistent', 'mask', 'offset', 'panel', 'size', 'size_flat', 'summed_intensity', 'values', 'xoffset', 'xsize', 'yoffset', 'ysize', 'zoffset', 'zsize']
