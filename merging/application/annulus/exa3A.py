from __future__ import division
from LS49.adse13_187.adse13_221.basic_runner import basic_run_manager
from LS49.sim.util_fmodel import gen_fmodel
from dxtbx.model.experiment_list import ExperimentList
from dials.array_family import flex
from scitbx.matrix import sqr
from simtbx import get_exascale
from scipy import constants
import traceback as tb
import sys,os
ENERGY_CONV = 1e10 * constants.c * constants.h / constants.electron_volt

# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.
import utils
from dxtbx.imageset import ImageSetFactory
from crystal_detail_interface import CrystalDetailSingleton


class prepare:
  def __init__(self, old_experiments, old_reflections, params, mpi_helper, mpi_logger, plot=True):
    self.mpi_helper = mpi_helper
    self.mpi_logger = mpi_logger
    self.params = params
    self.uc = self.params.scaling.unit_cell
    print("Rank %d on host %s"%(self.mpi_helper.rank, os.environ.get("SLURMD_NODENAME","")))
    self.setup_amplitudes()
    _ = CrystalDetailSingleton(params, old_experiments)
    self.result_expt = ExperimentList()
    self.result_refl = flex.reflection_table()
    # Re-generate the image sets using their format classes so we can read the raw data
    # Integrate the experiments one at a time to not use up memory
    current_imageset = None
    current_imageset_path = None
    for expt_id, expt in enumerate(old_experiments):
      print("Beginning analysis of",expt.identifier)
      assert len(expt.imageset.paths()) == 1 and len(expt.imageset) == 1
      if expt.imageset.paths()[0] != current_imageset_path:
        current_imageset_path = expt.imageset.paths()[0]
        current_imageset = ImageSetFactory.make_imageset(expt.imageset.paths())
      idx = expt.imageset.indices()[0]
      expt.imageset = current_imageset[idx:idx+1]
      S = expt.imageset.get_spectrum(0) # code not yet correct for polychromatic model
      refl = old_reflections.select(old_reflections['id']==expt_id)
      try:
        oneexpt, onerefl = self.thin_exa1(expt, refl)
        self.result_expt.append(oneexpt)
        self.result_refl.extend(onerefl)
      except RuntimeError as e:
        print("ENCOUNTER RuntimeError",current_imageset_path,idx); tb.print_exc(file=sys.stdout)
      except AssertionError as e:
        print("ENCOUNTER Epsilon?",e); tb.print_exc(file=sys.stdout)
      expt.imageset = None
      print("Done with",expt.identifier,"\n")
    self.result_refl.reset_ids()
    del self.gpu_channels_singleton # explicitly push Kokkos allocation device_Fhkl out of scope
    mpi_helper.comm.barrier()

  def setup_amplitudes(self):
    wavelength_A = 1.74 # general ballpark X-ray wavelength in Angstroms
    wavlen = flex.double([wavelength_A])
    epsilon = 0.05
    direct_algo_res_limit = self.params.statistics.annulus.d_min-epsilon
    GF = gen_fmodel(resolution=direct_algo_res_limit,
       pdb_text=open(self.params.scaling.model,"r").read(),
       algorithm="fft",wavelength=wavelength_A)
    GF.set_k_sol(0.435)
    if self.params.exafel.skin:
      self.sfall_orig_setting = GF.get_amplitudes()
      print("debug setup_amplitudes in original high symm", self.sfall_orig_setting.crystal_symmetry())
    GF.make_P1_primitive()
    sfall_channels = {}
    for x in range(len(wavlen)):
      sfall_channels[x]=GF.get_amplitudes()
    # there is an MPI-assisted algorithm for polychromatic beam
    if self.params.exafel.skin:
      self.sfall_primitive_setting = sfall_channels[0]
      print("debug setup_amplitudes in P1", sfall_channels[x].crystal_symmetry())
    if self.params.exafel.debug.write_mtz and self.mpi_helper.rank==0:
      with open("structure.pdb","w") as FF:
        FF.write(GF.xray_structure.as_pdb_file())
      for numerical_key in sfall_channels.keys():
        sfall_channels[numerical_key].write_mtz("exa3A_structure_factors_%04d.mtz"%numerical_key,
        wavelength = wavelength_A)
    self.mpi_helper.comm.barrier()

    gpu_instance = get_exascale("gpu_instance", self.params.exafel.context)
    gpu_energy_channels = get_exascale("gpu_energy_channels", self.params.exafel.context)
    deviceId = (self.mpi_helper.rank)%int(os.environ.get("CCTBX_GPUS_PER_NODE","1"))
    self.gpu_run = gpu_instance( deviceId = deviceId )
    self.gpu_channels_singleton = gpu_energy_channels ( deviceId = deviceId )
    assert self.gpu_channels_singleton.get_nchannels() == 0 #uninitialized
    for x in range(1): # later, len(flux)): # code not yet correct for polychromatic model
          self.gpu_channels_singleton.structure_factors_to_GPU_direct(
           x, sfall_channels[x].indices(), sfall_channels[x].data())

  def ensure_p1(self, Crystal):
    high_symm_info = Crystal.get_space_group().info()
    self.cb_op = high_symm_info.change_of_basis_op_to_primitive_setting()
    dterm = sqr(self.cb_op.c().r().as_double()).determinant()
    if dterm != 1:
      Crystal = Crystal.change_basis(self.cb_op)
    return Crystal

  def thin_exa1(self, expt, refl):
    M = utils.basic_run_manager.from_mask_file_expt_refl(
              self.params.exafel.trusted_mask, expt, refl)
    M.gpu_channels_singleton = self.gpu_channels_singleton
    M.general_trusted_and_refl_mask(self.params.exafel)
    M.data_structures_for_refls()
    M.get_image_res_data() # get experimental data
    M.spread_modify_shoeboxes() # superimpose dials planar fit over the shoeboxes
    expt.crystal = self.ensure_p1(expt.crystal)
    # put in proposal planes here instead of down below
    M.minimize_planes_single_lattice(raw_pixels=M.view["exp_data"], params= self.params,
                                     publish_to="plane_shoeboxes")
    M.define_greenlist(background_from="plane_shoeboxes")
    M.Z_statistics(experimental=M.view["exp_data"],
                   model=M.view["plane_shoeboxes"],
                   report=False,
                   method="border",
                   plot=False)
    # z-mean is slightly higher in the second call, Bragg spot is adding positive intensity at border
    # Z-mean is slightly positive and gets worse at higher angle.
    if self.params.exafel.skin:
      M.miller_array_diffBragg = self.sfall_primitive_setting
    M.flattened_exa_sim(mod_expt = expt, rank=self.mpi_helper.rank)
    POL = M.refine_bragg_single_lattice(raw_pixels=M.view["exp_data"], params= self.params,
                                  background_from="plane_shoeboxes", publish_to=None)
    bragg_proposal, background_proposal = POL.get_adu_model()

    M.view["bragg_plus_background"] = M.reusable_rmsd_from_whitelist(
      bragg_proposal=bragg_proposal, background_proposal=background_proposal, label="ersatz_mcmc",plot=self.params.exafel.model.plot,
      legend="Gradient-refined simulation: %s "%expt.identifier) # Plot 3
    M.Z_statistics(experimental=M.view["exp_data"],
                   model=M.view["bragg_plus_background"],
                   report=False,
                   method="border",
                   plot=False)
    M.view["Z_plot"] = M.Z_statistics(experimental=M.view["exp_data"],
                                           model=M.view["bragg_plus_background"],
                                           report=True,
                                           method="all",
                                           plot=False)

    if self.params.exafel.debug.lastfiles:
      M.write_hdf5(filenm = "%s.h5"%(expt.identifier))
      M.resultant_mask_to_file("%s.mask"%(expt.identifier))
      utils.write_expt_and_refl_table(expt, refl,
                                      exptpath="%s.expt" % expt.identifier,
                                      reflpath="%s.refl" % expt.identifier)
    # C-language memory management
    M.nB.free_all()
    if M.dB: M.dB.free_all()
    print ("Free'd C-variables")
    return expt, M.refl_table

