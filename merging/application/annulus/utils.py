from __future__ import division
from dials.array_family import flex
import pickle, copy, math, os
from time import time

from dials.algorithms.shoebox import MaskCode
from LS49.adse13_187.adse13_221.data_slots import application_slots
from LS49.adse13_187.cyto_batch import multipanel_sim
import scitbx.lbfgs
from simtbx.nanoBragg.nanoBragg_crystal import NBcrystal
from simtbx.nanoBragg.nanoBragg_beam import NBbeam
from simtbx.nanoBragg.sim_data import SimData
from scipy import constants
ENERGY_CONV = 10000000000.0 * constants.c * constants.h / constants.electron_volt
from scipy.signal import convolve2d
import numpy as np
from libtbx.development.timers import Profiler
from simtbx import get_exascale
from crystal_detail_interface import CrystalDetailSingleton


def write_expt_and_refl_table(expt, refl, exptpath=None, reflpath=None):
  # takes a single experiment, and a refl table with rows from that experiment
  # side effect: modifies the refl id column
  # local expt to file
  from dxtbx.model.experiment_list import ExperimentList
  L = ExperimentList()
  L.append(expt)
  if exptpath is not None:
      L.as_file(exptpath)
      # local refl to pickle
  if reflpath is not None:
      if len(refl):
        refl.as_file(reflpath)

class basic_run_manager:
  CONVOLUTION_DONUT = np.array([(0, 1, 0), (1, 0, 1), (0, 1, 0)])

  def __init__(self,trusted_mask,refl_table,expt):
    self.trusted_mask = trusted_mask
    self.refl_table = refl_table
    self.expt = expt
    self.view = application_slots
    self.n_panels = len(self.expt.detector)

  @classmethod
  def from_mask_file_expt_refl(cls, trusted_mask_file, expt, refl):
    with open(trusted_mask_file,"rb") as M:
      mask = pickle.load(M)
    refl_table = refl
    return cls(mask,refl_table,expt)

  def get_shoebox_flag_mask(self, shoebox: flex.bool(),
                            min_neighbors: int = 3) -> flex.bool():
    """Prepare a 1d mask for shoebox which accepts only valid used pixels. Also
    flip any `True` pixels to `False` if they have less than `min_neighbors`"""
    flags = shoebox.mask.as_1d()
    flags_valid = (flags & MaskCode.Valid) == MaskCode.Valid
    flags_fg = (flags & MaskCode.Foreground) == MaskCode.Foreground
    flags_bgu = (flags & MaskCode.BackgroundUsed) == MaskCode.BackgroundUsed
    accepted_flags = flags_valid & (flags_bgu | flags_fg)
    accepted_2d = np.array(accepted_flags).reshape(shoebox.mask.focus()[1:3])
    true_neighbors = convolve2d(accepted_2d, self.CONVOLUTION_DONUT, mode='same') \
      if min_neighbors else np.zeros_like(accepted_2d)
    return flex.bool((accepted_2d & (true_neighbors >= min_neighbors)).ravel())

  def general_trusted_and_refl_mask(self, params):
    """As in lunus_wrap, but generalized for SPREAD experiments and updated to
    make `params` a class member. Border thickness is a phil parameter.
    Panel size not hard-coded; it comes from the expt."""
    self.params = params
    size = len(self.refl_table)
    border = self.params.shoebox_border
    self.flag_mask = []
    self.shoebox_mask = []
    for midx in range(self.n_panels):
      false_field = flex.bool(flex.grid(self.trusted_mask[midx].focus()), False)
      self.shoebox_mask.append(false_field)
      self.flag_mask.append(~false_field)

    # generate flag mask based on phil parameter trusted_pixels
    if params.trusted_pixels == 'all':
      pass
    elif params.trusted_pixels in {'used', 'used_trimmed'}:
      min_neighbors = 0 if params.trusted_pixels == 'used' else 3
      for panel, sb in zip(self.refl_table['panel'], self.refl_table['shoebox']):
        slow_size, fast_size = self.shoebox_mask[panel].focus()[:2]
        this_sb_flag_mask = self.get_shoebox_flag_mask(sb, min_neighbors)
        bb = sb.bbox
        for i_slow in range(max(0, bb[2]-border), min(slow_size, bb[3]+border)):
          for i_fast in range(max(0, bb[0]-border), min(fast_size, bb[1]+border)):
            index_on_panel = i_slow * fast_size + i_fast
            index_in_sb = (i_slow - bb[2]) * (bb[1] - bb[0]) + (i_fast - bb[0])
            self.flag_mask[panel][index_on_panel] &= this_sb_flag_mask[index_in_sb]
    else:
      raise KeyError(f'Unknown value of {params.trusted_pixels=}')

    n_pixel_visited = 0
    n_pixel_masked = 0
    n_pixel_invalid = 0
    for panel, sb in zip(self.refl_table['panel'], self.refl_table['shoebox']):
      slow_size, fast_size = self.shoebox_mask[panel].focus()[:2]
      bb = sb.bbox
      for i_slow in range(max(0, bb[2]-border), min(slow_size, bb[3]+border)):
        for i_fast in range(max(0, bb[0]-border), min(fast_size, bb[1]+border)):
          n_pixel_visited += 1
          index_on_panel = i_slow * fast_size + i_fast
          if not self.trusted_mask[panel][index_on_panel]:
            n_pixel_masked += 1
          elif not self.flag_mask[panel][index_on_panel]:
            n_pixel_invalid += 1
          else:
            self.shoebox_mask[panel][index_on_panel] = True

    self.resultant = []
    for midx in range(self.n_panels):
      newmask = self.trusted_mask[midx].__and__(self.shoebox_mask[midx])
      self.resultant.append(newmask)
    print(n_pixel_visited, "pixels were visited 1st in %d shoeboxes (with borders)" % size)
    print(n_pixel_masked, "were rejected due to trusted mask")
    print(n_pixel_invalid, "were rejected due to shoebox flag mask")

    self.monolithic_mask_whole_detector_as_1D_bool = flex.bool()
    for ipnl in range(len(self.resultant)):
      pnl = self.resultant[ipnl]
      self.monolithic_mask_whole_detector_as_1D_bool.extend(pnl.as_1d())
    panel0 = self.expt.detector[0].get_image_size()
    assert len(self.monolithic_mask_whole_detector_as_1D_bool)==self.n_panels*panel0[0]*panel0[1]

    # Exascale API setup: convert active-pixel-bools to whitelist-size_t
    self.monolithic_mask_whole_dectector_as_int_whitelist = \
      self.monolithic_mask_whole_detector_as_1D_bool.iselection()

  def resultant_mask_to_file(self,file_name):
    all_done = tuple(self.resultant)
    with open(file_name,"wb") as M:
      pickle.dump(all_done,M)

  def data_structures_for_refls(self):
    """This function sets up some data structures (spots_*) allowing us to index into the spots
    and pixels of interest.  These will be repeatedly used during parameter refinement
    to calculate target function and intermediate statistics.
    """
    Z = self.refl_table
    indices = Z['miller_index']
    CRYS = self.expt.crystal
    UC = CRYS.get_unit_cell()
    strong_resolutions = UC.d(indices)
    order = flex.sort_permutation(strong_resolutions, reverse=True)
    Z["spots_order"] = order
    self.spots_pixels = flex.size_t()
    spots_offset = flex.int(len(order),-1)
    spots_size = flex.int(len(order),-1)
    slow_size = self.expt.detector[0].get_image_size()[1]
    fast_size = self.expt.detector[0].get_image_size()[0]
    panel_size = slow_size * fast_size
    border = self.params.shoebox_border

    panels = Z['panel']
    shoeboxes = Z['shoebox']
    n_pixel_visited = 0
    n_pixel_masked = 0
    n_pixel_invalid = 0
    for oidx in range(len(order)): #loop through the shoeboxes in correct order
      sidx = order[oidx] # index into the Miller indices
      i_panel = panels[sidx]
      bb = shoeboxes[sidx].bbox
      first_position = spots_offset[sidx] = self.spots_pixels.size()
      for i_slow in range(max(0, bb[2]-border), min(slow_size, bb[3]+border)):
        for i_fast in range(max(0, bb[0]-border), min(fast_size, bb[1]+border)):
          n_pixel_visited += 1
          index_on_panel = i_slow * fast_size + i_fast
          if not self.trusted_mask[i_panel][index_on_panel]:
            n_pixel_masked += 1
          elif not self.flag_mask[i_panel][index_on_panel]:
            n_pixel_invalid += 1
          else:
            self.spots_pixels.append(i_panel * panel_size + index_on_panel)
      spot_size = spots_size[sidx] = self.spots_pixels.size() - first_position
    Z["spots_offset"] = spots_offset
    Z["spots_size"] = spots_size
    print(n_pixel_visited, "pixels were visited 2nd in %d shoeboxes (with borders)" % len(order))
    print(n_pixel_masked, "were rejected due to trusted mask")
    print(n_pixel_invalid, "were rejected due to shoebox pixel mask")
    print(len(self.spots_pixels), "were left in target")

    # Future use: guard against overlapping shoeboxes for Exascale API
    self.relevant_whitelist_order = flex.size_t()
    self.relevant_whitelist_millers = flex.miller_index()
    for sidx in range(len(self.refl_table["spots_offset"])):#loop through the shoeboxes
      for pidx in range(self.refl_table["spots_offset"][sidx],
                        self.refl_table["spots_offset"][sidx]+self.refl_table["spots_size"][sidx]):
        self.relevant_whitelist_order.append(self.spots_pixels[pidx])
        self.relevant_whitelist_millers.append(indices[sidx])

  def get_image_res_data(self):
      exp_data = self.expt.imageset.get_raw_data(0)
      assert len(exp_data) == self.n_panels # 256 Jungfrau panels
      self.view["exp_data"] = [exp_data[pid].as_numpy_array() for pid in range(len(exp_data))]

  def write_hdf5(self,filenm):
      from simtbx.nanoBragg import utils
      panel0 = self.expt.detector[0].get_image_size()
      expected_img_sh = (self.n_panels,panel0[1],panel0[0])
      num_output_images = len([1 for key in self.view if self.view[key] is not None])
      print("Saving HDF5 data of shape %s to file %s"%(expected_img_sh,filenm))
      beam_dict = self.expt.beam.to_dict()
      det_dict = self.expt.detector.to_dict()
      try:
        beam_dict.pop("spectrum_energies")
        beam_dict.pop("spectrum_weights")
      except Exception: pass
      comp = dict(compression='lzf' )
      with utils.H5AttributeGeomWriter(filenm,
                                image_shape=expected_img_sh, num_images=num_output_images,
                                detector=det_dict, beam=beam_dict, dtype=np.float32,
                                detector_and_beam_are_dicts=True, compression_args=comp) as writer:
        for key in self.view:
          if self.view[key] is not None:
            data = self.view[key]
            if type(data) == type([]):
              assert len(data) == self.n_panels
              writer.add_image(self.view[key])
            elif type(data)==type(flex.double()):
              assert data.focus() == expected_img_sh
              writer.add_image(self.view[key].as_numpy_array())
            else:
              raise Exception("unknown array type")
            #img_sh = self.view[key].shape; assert img_sh == expected_img_sh

  def spread_modify_shoeboxes(self, verbose=False): # and printing the shoeboxes in verbose mode
    # For SPREAD. Background image from experimental data(not lunus for now). Replace shoeboxes with dials plane fit.
    # limitation--can't have a non-zero border.  sort this out later.
    exp_data = self.expt.imageset.get_raw_data(0) # experimental data, a tuple over 2D flex.int panels
    Z = self.refl_table
    P = panels = Z['panel']
    S = shoeboxes = Z['shoebox']
    size = len(Z)
    # input exp_data to 3D flex.double output
    self.view["plane_shoeboxes"] = flex.double()
    for panel_item in exp_data:
      self.view["plane_shoeboxes"].extend(panel_item.as_1d().as_double())
    self.view["plane_shoeboxes"].reshape(flex.grid(len(exp_data), panel_item.focus()[0], panel_item.focus()[1]))
    gain = self.expt.detector[0].get_gain()
    for sidx in range(size): #loop through the shoeboxes
      ipanel = P[sidx]
      false_field = self.shoebox_mask[ipanel]
      slow_size = false_field.focus()[0]
      fast_size = false_field.focus()[1]
      bbox = S[sidx].bbox
      islow_limits = (max(0,bbox[2]), min(slow_size,bbox[3]))
      ifast_limits = (max(0,bbox[0]), min(fast_size,bbox[1]))
      if verbose:
       # print out the res-data
       for islow in range(islow_limits[0], islow_limits[1]):
        fast_count=0
        fast_sum=0
        for ifast in range(ifast_limits[0], ifast_limits[1]):
          value = exp_data[ipanel][islow*fast_size + ifast]
          print("%6.0f"%value, end="")
          fast_count+=1
          fast_sum+=value
        print(" =%6.0f"%(fast_sum/fast_count))
       print()
       # same print out, but coming from the double precision output version
       for islow in range(islow_limits[0], islow_limits[1]):
        fast_count=0
        fast_sum=0
        for ifast in range(ifast_limits[0], ifast_limits[1]):
          value = self.view["plane_shoeboxes"][ipanel,islow,ifast]
          print("%6.0f"%value, end="")
          fast_count+=1
          fast_sum+=value
        print(" =%6.0f"%(fast_sum/fast_count))
       print()
      # now abstract the dials planar fit from the shoebox object
      SS = S[sidx]
      SSB = gain * SS.background
      # set and print out the fit shoebox
      for idxslow, islow in enumerate(range(islow_limits[0], islow_limits[1])):
        fast_count=0
        fast_sum=0
        for idxfast, ifast in enumerate(range(ifast_limits[0], ifast_limits[1])):
          value = SSB[0,idxslow,idxfast]
          self.view["plane_shoeboxes"][ipanel,islow,ifast]=value
          if verbose: print("%6.0f"%value, end="")
          fast_count+=1
          fast_sum+=value
        if verbose: print(" =%6.0f"%(fast_sum/fast_count))
      if verbose: print()
      if verbose:
       # print out the data minus background-fit shoebox
       for islow in range(islow_limits[0], islow_limits[1]):
        fast_count=0
        fast_sum=0
        for ifast in range(ifast_limits[0], ifast_limits[1]):
          value = exp_data[ipanel][islow*fast_size + ifast]-self.view["plane_shoeboxes"][ipanel,islow,ifast]
          print("%6.0f"%value, end="")
          fast_count+=1
          fast_sum+=value
        print(" =%6.0f"%(fast_sum/fast_count))
       print("---")
       #input()

  def thin_exa_sim(self, mod_expt):
    return self.job_runner(expt=mod_expt, alt_expt=mod_expt, params=self.params,
      mask_array = self.monolithic_mask_whole_detector_as_1D_bool
      ) # returns simulated image as numpy array

  # from case_run::case_job_runner:
  def job_runner(self,expt,alt_expt,params,mask_array=None,i_exp=0,spectra={},mos_aniso=None):

    # Fixed hyperparameters
    mosaic_spread_samples = 250
    beamsize_mm = 0.000886226925452758 # sqrt beam focal area
    spot_scale = 1.
    oversample = 1  # factor 1,2, or 3 probably enough
    verbose = 0  # leave as 0, unless debug
    shapetype = "gauss_argchk"

    if mask_array is not None:
      assert type(mask_array) is flex.bool # type check intending to convert active-pixel-bools to whitelist-ints
      active_pixels = flex.size_t()
      for i, x in enumerate(mask_array):
        if x: active_pixels.append(i)
      mask_array = active_pixels

    detector = expt.detector
    flat = True  # enforce that the camera has 0 thickness
    if flat:
        from dxtbx_model_ext import SimplePxMmStrategy
        for panel in detector:
            panel.set_px_mm_strategy(SimplePxMmStrategy())
            panel.set_mu(0)
            panel.set_thickness(0)
        assert detector[0].get_thickness() == 0

    alt_crystal = alt_expt.crystal

    beam = expt.beam
    spec = expt.imageset.get_spectrum(0)
    energies_raw = spec.get_energies_eV().as_numpy_array()
    weights_raw = spec.get_weights().as_numpy_array()
    from LS49.adse13_187.adse13_221.explore_spectrum import method3
    energies, weights, _ = method3(energies_raw, weights_raw,); weights = 5e1 * weights
    energies = list(energies); weights = list(weights)

    device_Id = 0
    if self.gpu_channels_singleton is not None:
      device_Id = self.gpu_channels_singleton.get_deviceID()

    print("\n<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>")
    print("\tBreakdown:")
    for shapetype in ["gauss_argchk"]:
      BEG=time()
      if params.debug.verbose: print (self.gpu_channels_singleton.get_deviceID(),"device",shapetype)
      Famp_is_uninitialized = ( self.gpu_channels_singleton.get_nchannels() == 0 )
      if Famp_is_uninitialized:
        F_P1 = self.amplitudes
        for x in range(1):  # in this scenario, amplitudes are independent of lambda
          self.gpu_channels_singleton.structure_factors_to_GPU_direct(
          x, F_P1.indices(), F_P1.data())
      assert self.gpu_channels_singleton.get_nchannels() == 1

      # Variable parameters
      crystal_detail = CrystalDetailSingleton().get_detail(expt.identifier)
      alt_crystal.set_A(crystal_detail.Amats)
      mosaic_spread = crystal_detail.eta
      Ncells_abc = crystal_detail.ncells
      if params.debug.verbose:
        print(f'{mosaic_spread=}')
        print(f'{Ncells_abc=}')

      JF16M_numpy_array, TIME_BG, TIME_BRAGG, _ = multipanel_sim(
        CRYSTAL=alt_crystal, DETECTOR=detector, BEAM=beam,
        Famp = self.gpu_channels_singleton,
        energies=energies, fluxes=weights,
        cuda=True,
        oversample=oversample, Ncells_abc=Ncells_abc,
        mos_dom=mosaic_spread_samples, mos_spread=mosaic_spread,
        mos_aniso = mos_aniso,
        beamsize_mm=beamsize_mm,
        profile=shapetype,
        show_params=False,
        time_panels=False, verbose=verbose,
        spot_scale_override=spot_scale,
        include_background=False,
        mask_file=mask_array,
        context = params.context
      )
      TIME_EXA = time()-BEG

      print("\t\tExascale: time for bkgrd sim: %.4fs; Bragg sim: %.4fs; total: %.4fs" % (TIME_BG, TIME_BRAGG, TIME_EXA))
    print("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>\n")
    return JF16M_numpy_array

  @staticmethod
  def get_modified_spectrum(spec,energy_offset_eV,energy_stride_eV=0.5,energy_plot=False):
    # spec in an instance of dxtbx.model.Spectrum
    # later, migrate this to a method in the Format class?
    energies = list((spec.get_energies_eV() + energy_offset_eV).as_numpy_array())
    fluxes = list(spec.get_weights().as_numpy_array())
    print ("input %d energies from %.2f to %.2f, spaced %.2f eV"%(
           len(energies),energies[0],energies[-1],(energies[-1]-energies[0])/(len(energies)-1)))

    # assume spec is already divided into ~100 useful energy channels
    initial_length = len(spec.get_energies_eV())
    if initial_length < 4: raise ValueError
    # condense to coarsely-spaced spectrum
    stride=int(initial_length/((energies[-1]-energies[0])/energy_stride_eV))
    new_energies=[]
    new_fluxes=[]
    step=0; sum_energy=0.; sum_fluxes=0.
    for item in range(stride * (initial_length//stride)):
        step+=1; sum_energy+=energies[item]; sum_fluxes+=fluxes[item]
        if step==stride:
          new_energies.append(sum_energy/stride)
          new_fluxes.append(max(sum_fluxes/stride,0.)) # can't process 'negative' X-ray flux
          step=0; sum_energy=0.; sum_fluxes=0.
    print ("output %d energies from %.2f to %.2f, spaced %.2f eV"%(
           len(new_energies),new_energies[0],new_energies[-1],(new_energies[-1]-new_energies[0])/(len(new_energies)-1)))
    if energy_plot:
      from matplotlib import pyplot as plt
      plt.plot(energies,fluxes,'b.')
      plt.plot(new_energies, new_fluxes, 'r-')
      plt.show()
    return new_energies, new_fluxes

  def flattened_exa_sim(self, mod_expt, rank=0):
    expt = mod_expt
    alt_expt = mod_expt
    eid = expt.identifier

    # crystal concepts
    mosaic_method="double_uniform"
    crystal_detail = CrystalDetailSingleton().get_detail(eid)
    CRYSTAL = alt_expt.crystal
    CRYSTAL.set_A(crystal_detail.Amats)
    nbCrystal = NBcrystal(False)
    nbCrystal.dxtbx_crystal = CRYSTAL # is primitive form 'C 1 2/m 1 (x-y,x+y,z)'
    nbCrystal.symbol = CRYSTAL.get_space_group().info().type().lookup_symbol()
    nbCrystal.thick_mm = 0.01                   # Fixed hyperparamter for crystal_size_mm
    nbCrystal.xtal_shape = "gauss_argchk"       # Fixed hyperparameter for profile_shapetype
    nbCrystal.n_mos_domains = 250               # n_mosaic_domains_half_spread, will be doubled by nbCrystal
    nbCrystal.anisotropic_mos_spread_deg = None # Fixed hyperparameter for mos_aniso
    nbCrystal.Ncells_abc = crystal_detail.ncells
    nbCrystal.mos_spread_deg = crystal_detail.eta

    # beam concepts
    BEAM = expt.beam
    spec = expt.imageset.get_spectrum(0)
    energies, fluxes = self.get_modified_spectrum(spec = spec,
                       energy_offset_eV = self.params.debug.energy_offset_eV,
                       energy_stride_eV = self.params.debug.energy_stride_eV,
                       energy_plot = self.params.debug.energy_plot)

    nbBeam = NBbeam()
    nbBeam.size_mm = 0.000886226925452758 # beamsize_mm: sqrt beam focal area
    nbBeam.unit_s0 = BEAM.get_unit_s0()
    wavelengths = ENERGY_CONV / np.array(energies)
#test pattern on fluzes; XXX XXX FIXME this block is just for testing just for testing
#    for itest in range(len(fluxes)):
#      if itest%10 == 0:
#        fluxes[itest] *= 50.
#      else:
#        fluxes[itest] *= 0.
# yes, test pattern looks correct XXX XXX FIXME this block is just for testing just for testing
    self.nbBeam_spectrum = nbBeam.spectrum = list(zip(wavelengths, fluxes))

    # detector concepts
    oversample = 1  # factor 1,2, or 3 probably enough
    readout_noise=3
    psf_fwhm=0
    gain=1
    DETECTOR = expt.detector
    flat = True  # enforce that the camera has 0 thickness
    if flat:
        from dxtbx_model_ext import SimplePxMmStrategy
        for panel in DETECTOR:
            panel.set_px_mm_strategy(SimplePxMmStrategy())
            panel.set_mu(0)
            panel.set_thickness(0)
        assert DETECTOR[0].get_thickness() == 0

    # nanoBragg simdata concepts
    spot_scale_override = 1.
    verbose = 0  # leave as 0, unless debug
    S = SimData(use_default_crystal = False)
    S.detector = DETECTOR
    S.beam = nbBeam
    S.crystal = nbCrystal
    S.panel_id = 0 # remove the loop, use C++ iteration over detector panels
    S.add_air = False
    S.air_path_mm = 0
    S.add_water = False
    S.water_path_mm = 0.005
    S.readout_noise = readout_noise
    S.gain = gain
    S.psf_fwhm = psf_fwhm
    S.include_noise = False
    S.Umats_method = dict(double_random=0, double_uniform=5)[mosaic_method]
    Famp = self.gpu_channels_singleton
    S.instantiate_nanoBragg(verbose=verbose, oversample=oversample, interpolate=0,
      device_Id=Famp.get_deviceID(),default_F=0, adc_offset=0)
    SIM = S.D # the nanoBragg instance
    self.nB = SIM # hang on to this instance for later memory management
    if spot_scale_override is not None: SIM.spot_scale = spot_scale_override

    P = Profiler("Exascale setup")
    # Exascale API setup: amplitudes
    assert Famp.get_nchannels() > 0 # allow for either edge=="mono" or "poly"
    assert Famp.get_deviceID()==SIM.device_Id

    # Exascale API setup: simulation and detector
    context = self.params.context
    self.gpu_simulation = get_exascale("exascale_api_small_whitelist", context)(nanoBragg=SIM)
    self.gpu_simulation.allocate() # presumably done once for each image
    self.gpu_detector_type = get_exascale("gpu_detector_small_whitelist",context)
    self.gpu_detector = self.gpu_detector_type(deviceId=SIM.device_Id, detector=DETECTOR,
                        beam=BEAM)
    mask_array = self.monolithic_mask_whole_dectector_as_int_whitelist
    self.gpu_detector.each_image_allocate(n_pixels=mask_array.size())
    # self.gpu_simulation.show() # for debug

    self.dB = None
    if self.params.skin:
      DS1 = SimData(use_default_crystal = False)
      DS1.detector = DETECTOR
      DS1.beam = nbBeam
      nbCrystal.n_mos_domains = nbCrystal.n_mos_domains * 2 # account for nanoB/diffB interface inconsistency
      nbCrystal.miller_array = self.miller_array_diffBragg
      # miller_array contains     unit_cell=(44.97743879, 44.97743879, 47.2, 74.97887135, 74.97887135, 83.33055257),
      # space_group_symbol="P 1"
      # miller_array_high_symmetry     unit_cell=(44.97743879, 44.97743879, 47.2, 74.97887135, 74.97887135, 83.33055257),
      # space_group_symbol="C 1 2/m 1 (x-y,x+y,z)"
      DS1.crystal = nbCrystal
      DS1.panel_id = 0 # remove the loop, use C++ iteration over detector panels
      DS1.add_air = False
      DS1.air_path_mm = 0
      DS1.add_water = False
      DS1.water_path_mm = 0.005
      DS1.readout_noise = readout_noise
      DS1.gain = gain
      DS1.psf_fwhm = psf_fwhm
      DS1.include_noise = False
      DS1.Umats_method = dict(double_random=0, double_uniform=5)[mosaic_method]
      Famp = self.gpu_channels_singleton
      DS1.instantiate_diffBragg(verbose=verbose, oversample=oversample, interpolate=0,
        device_Id=rank%int(os.environ.get("CCTBX_GPUS_PER_NODE","1")),
        default_F=0, adc_offset=0, use_diffBragg=True)
      SIM_DS1 = DS1.D # the diffBragg instance
      if spot_scale_override is not None: SIM_DS1.spot_scale = spot_scale_override

      if "rot" in self.params.refpar.label:
        rotX, rotY, rotZ = 0, 1, 2
        SIM_DS1.refine(rotX)  # rotX
        SIM_DS1.refine(rotY)  # rotY
        SIM_DS1.refine(rotZ)  # rotZ

      SIM_DS1.initialize_managers()
      SIM_DS1.vectorize_umats()
      self.dB = SIM_DS1
      # self.dB.show_params() # for debug

    """
    Design goals:
     - for crystal, beam, detector, collect concept-related parameters together for initialization
     - identify things done once only, and collect them in a setup call to be used prior to minimization
     - segregate the mask_whole_detector_bool, mask_whole_detector_intlist, and relevant_whitelist_order
     - truncate the flattened_exa_sim as it seems its main purpose is to set up the exascale API
     - provide a new execute method that would reproduce the legacy function
     start
     - but separately pull together a G-optimizer written from the top down.
     - we may need a target_evaluation class in the API that registers specific pixelwise and summed targets
     - need to double check if we can have successive guesses for the per-image scale factor and
         apply them separately to the base value.  Might need a redesign.
     - this should all be done on the relevant whitelist without invoking the JF16M_numpy_array
    """
    ############### set up and execute functionality seem to be divided here ##########
  def specific_execute(self, per_image_scale_factor = 1):
    x = 0 # only one energy channel
    exit ("development exit 1 for spread XXX FIXME")
    mask_array = self.monolithic_mask_whole_dectector_as_int_whitelist

    ichannels = flex.int(len(self.nbBeam_spectrum), x)

    P = Profiler("%40s"%"select energy_multichannel_mask_allpanel with int_whitelist")
    self.gpu_simulation.add_energy_multichannel_mask_allpanel(
        ichannels = ichannels,
        gpu_amplitudes = self.gpu_channels_singleton,
        gpu_detector = self.gpu_detector,
        pixel_active_list_ints = mask_array )

    self.gpu_detector.scale_in_place(per_image_scale_factor) # apply scale directly on GPU

    packed_numpy = self.gpu_detector.get_raw_pixels()
    self.gpu_detector.scale_in_place(0) # reset
    return packed_numpy.as_numpy_array()

  def legacy_execute(self):
    BEG=time()
    skip_numpy=False
    x = 0 # only one energy channel
    exit ("development exit 2 for spread XXX FIXME")

    # Exascale API setup: masks
    mask_array = self.monolithic_mask_whole_dectector_as_int_whitelist
    if False: # old school, use nanoBragg sources to directly define add spots
      P = Profiler("%40s"%"select energy_channel_mask_allpanel with int_whitelist")
      self.gpu_simulation.add_energy_channel_mask_allpanel(
        channel_number = x, gpu_amplitudes = self.gpu_channels_singleton, gpu_detector = self.gpu_detector,
        pixel_active_list_ints = mask_array )
    else: # new way, use many F_amplitude channels
      ichannels = flex.int(len(self.nbBeam_spectrum), x)
      P = Profiler("%40s"%"select energy_multichannel_mask_allpanel with int_whitelist")
      self.gpu_simulation.add_energy_multichannel_mask_allpanel(
        ichannels = ichannels, gpu_amplitudes = self.gpu_channels_singleton, gpu_detector = self.gpu_detector,
        pixel_active_list_ints = mask_array )
    TIME_BRAGG = time()-P.start_el

    per_image_scale_factor = 1.
    self.gpu_detector.scale_in_place(per_image_scale_factor) # apply scale directly on GPU

    if skip_numpy:
      P = Profiler("%40s"%"get short whitelist values")
      whitelist_only = self.gpu_detector.get_whitelist_raw_pixels(RM.whitelist_focused_idx)
      self.gpu_detector.scale_in_place(0) # reset
      # whitelist_only, flex_double pixel values
      # RM.whitelist_focused_idx, flex.size_t detector addresses

      P = Profiler("%40s"%"each image free cuda")
      self.gpu_detector.each_image_free()
      return whitelist_only, TIME_BG, TIME_BRAGG, S.exascale_mos_blocks or None

    packed_numpy = self.gpu_detector.get_raw_pixels()
    self.gpu_detector.scale_in_place(0) # reset
    self.gpu_detector.each_image_free() # no op in kokkos
    JF16M_numpy_array = packed_numpy.as_numpy_array()
    TIME_EXA = time()-BEG

    print("\t\tExascale: time for Bragg sim: %.4fs; total: %.4fs" % (TIME_BRAGG, TIME_EXA))
    print("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>\n")
    return JF16M_numpy_array

  def per_shoebox_whitelist_iterator(self, sidx):
    """given a shoebox id, iterate through all its whitelisted pixels"""
    Z = self.refl_table
    SOFF = Z["spots_offset"]
    SSIZ = Z["spots_size"]
    # take some care with the order of indices, as dxtbx expresses panels as (fast,slow)
    fast_size = self.expt.detector[0].get_image_size()[0]
    slow_size = self.expt.detector[0].get_image_size()[1]
    panel_size=slow_size*fast_size
    for idxpx in self.spots_pixels[SOFF[sidx]:SOFF[sidx]+SSIZ[sidx]]:
      ipanel = idxpx//panel_size; panelpx = idxpx%panel_size
      islow = panelpx//fast_size; ifast = panelpx%fast_size
      yield ipanel, islow, ifast

  def per_shoebox_border_iterator(self, sidx, width=1):
    """same as whitelist for shoebox id, but iterate pixels over rectangular border"""
    Z = self.refl_table
    SOFF = Z["spots_offset"]
    SSIZ = Z["spots_size"]
    fast_size = self.expt.detector[0].get_image_size()[0]
    slow_size = self.expt.detector[0].get_image_size()[1]
    panel_size=slow_size*fast_size
    bbox = self.refl_table[sidx]["bbox"]
    for idxpx in self.spots_pixels[SOFF[sidx]:SOFF[sidx]+SSIZ[sidx]]:
      ipanel = idxpx//panel_size; panelpx = idxpx%panel_size
      islow = panelpx//fast_size; ifast = panelpx%fast_size
      if (islow > (bbox[2]-1+width) and islow < (bbox[3]-width)) and (
          ifast > (bbox[0]-1+width) and ifast < (bbox[1]-width)): continue
      yield ipanel, islow, ifast

  def per_lattice_whitelist_iterator(self):
    """iterate through all shoebox_id and all whitelisted pixels"""
    Z = self.refl_table
    SOFF = Z["spots_offset"]
    SSIZ = Z["spots_size"]
    fast_size = self.expt.detector[0].get_image_size()[0]
    slow_size = self.expt.detector[0].get_image_size()[1]
    panel_size=slow_size*fast_size
    for sidx in range(len(self.refl_table)): #loop through the shoeboxes
      for idxpx in self.spots_pixels[SOFF[sidx]:SOFF[sidx]+SSIZ[sidx]]:
        ipanel = idxpx//panel_size; panelpx = idxpx%panel_size
        islow = panelpx//fast_size; ifast = panelpx%fast_size
        yield ipanel, islow, ifast, sidx

  def simple_rmsd(self,calc_data="xyzcal.px",plot=False,legend="",filter_zero=None):
    """Function does a rudimentary plot of model vs. experimental spot position, and optionally
    a plot of deviation vs. Bragg angle.
    """
    Z = self.refl_table
    devs = flex.double()
    sqdevs = flex.double()
    for oidx in range(len(Z["spots_order"])): #loop through the shoeboxes in correct order
      sidx = Z["spots_order"][oidx] # index into the Miller indices
      if filter_zero is not None and Z[filter_zero][sidx]==0: continue
      calc = Z[calc_data][sidx][0:2]
      obs  = Z['xyzobs.px.value'][sidx][0:2]
      sqdev= (calc[0]-obs[0])**2 + (calc[1]-obs[1])**2
      dev  = math.sqrt(sqdev)
      devs.append(dev)
      sqdevs.append(sqdev)
      #print("%20s %6.2fpx"%(Z["miller_index"][sidx], dev))
    rmsd = math.sqrt(flex.mean(sqdevs))
    print (legend+"The rmsd is %6.3f px"%rmsd,"on %d shoeboxes"%(len(sqdevs)), flush=True)
    if plot:
      from matplotlib import pyplot as plt
      plt.plot(range(len(devs)),devs)
      running_range = range(15,len(devs),15)
      plt.plot(running_range, [flex.mean(devs[I-15:I]) for I in running_range], "r-",
         label="RMSD=%6.2fpx"%math.sqrt(flex.mean(sqdevs)))
      plt.title("Model vs. Experimental Bragg spot position")
      plt.xlabel("Spots ordered by increasing Bragg angle →")
      plt.ylabel("Deviation in pixels")
      plt.legend(loc='upper left')
      plt.show()
    return rmsd

  def reusable_rmsd(self,proposal,label,output_refl=None,plot=False,legend=""):
    """Function analyzes proposal data consisting of proposed Bragg spots
    Function has the more expansive purpose of analyzing the data
    and storing statistics: the data center of mass in the shoebox, and the data sum, to be
    used later for normalizing the model in each shoebox.
    """
    proposal_ctr_of_mass = flex.vec3_double()
    proposal_shoebox_sum = flex.double()
    mockup_simulation = copy.deepcopy(self.view["plane_shoeboxes"])
    from scitbx.matrix import col
    for sidx in range(len(self.refl_table)): #loop through the shoeboxes
      SUM_VEC = col((0.,0.))
      SUM_wt = 0.
      for ipanel, islow, ifast in self.per_shoebox_whitelist_iterator(sidx):
        proposal_value = max(0.0000001,proposal[ipanel][islow,ifast])
          #workaround for the "Paley" bug. If spot is not predicted, give it some nonzero intensity
        SUM_VEC = SUM_VEC + float(proposal_value) * col((float(islow),float(ifast)))
        SUM_wt += proposal_value
        mockup_simulation[ipanel,islow,ifast] += proposal_value
      if SUM_wt > 0.: c_o_m = SUM_VEC/SUM_wt
      else: c_o_m=col((0.,0.)); print("SIDX has zero pixels")
      # there is a half pixel offset in our understanding of position
      proposal_ctr_of_mass.append((c_o_m[1]+0.5,c_o_m[0]+0.5,0.0))
      proposal_shoebox_sum.append(SUM_wt)
    self.refl_table[label+"_xyzcal.px"] = proposal_ctr_of_mass
    self.refl_table[label+"_shoebox_sum"] = proposal_shoebox_sum
    self.simple_rmsd(calc_data=label+"_xyzcal.px",plot=plot,legend=legend) # toggle for plotting

    return mockup_simulation

  def reusable_rmsd_from_whitelist(self,bragg_proposal,background_proposal,label,output_refl=None,plot=False,legend=""):
    """reusable_rmsd_from_whitelist analyzes proposal data consisting of Bragg + background
    Analyzing the shoebox center of mass, and shoebox sum, stored for later.
    """
    from scitbx.matrix import col
    print("there are %d zero intensities in the Bragg model"%((bragg_proposal == 0.0).count(True)))
    print("there are %d negative intensities in the Bragg model"%((bragg_proposal < 0.0).count(True)))
    print('Bragg range',flex.min(bragg_proposal), flex.max(bragg_proposal))
    print('bckgd range',flex.min(background_proposal), flex.max(background_proposal))

    assert ((bragg_proposal) < 0.0).count(True) == 0
    # assert ((bragg_proposal + background_proposal) <= 0.0).count(True) == 0
    # check for the "Paley" bug. Proposal values must have positive intensity
    # However, can't assert this because background sometimes has negative pixels.
    # instead, flag those shoeboxes as negative

    proposal_ctr_of_mass = flex.vec3_double(len(self.refl_table)) # index over shoeboxes
    proposal_shoebox_sum = flex.double(len(self.refl_table))
    background_positive = flex.bool(len(self.refl_table), True)
    mockup_simulation = copy.deepcopy(self.view["plane_shoeboxes"])
    zeroes = 0
    for proposal_value, background, (ipanel, islow, ifast, sidx) in zip(
          bragg_proposal, background_proposal, self.per_lattice_whitelist_iterator()):
        proposal_ctr_of_mass[sidx] = col(proposal_ctr_of_mass[sidx]
                      ) + float(proposal_value) * col((float(islow),float(ifast),0))
        proposal_shoebox_sum[sidx] += float(proposal_value)

        mockup_simulation[ipanel,islow,ifast] = proposal_value + background
        if background < 0. : background_positive[sidx] = False

    for sidx in range(len(self.refl_table)): #loop through the shoeboxes
        if proposal_shoebox_sum[sidx]>0.0 and background_positive[sidx]:
          c_o_m = col(proposal_ctr_of_mass[sidx]) / proposal_shoebox_sum[sidx]
        else:
          zeroes+=1
          c_o_m = col(proposal_ctr_of_mass[sidx])
          proposal_shoebox_sum[sidx] = 0.0 # value indicates inactive shoebox
        proposal_ctr_of_mass[sidx] = (c_o_m[1]+0.5,c_o_m[0]+0.5,0.0)
        # there is a half pixel offset in our understanding of position

    self.refl_table[label+"_xyzcal.px"] = proposal_ctr_of_mass
    self.refl_table[label+"_shoebox_sum"] = proposal_shoebox_sum
    self.simple_rmsd(calc_data=label+"_xyzcal.px",plot=plot,legend=legend,filter_zero=label+"_shoebox_sum") # toggle for plotting
    print("%d of %d shoeboxes had zero model"%(zeroes, len(self.refl_table)))

    return mockup_simulation

  def define_greenlist(self,background_from):
    """As a result of computing a model background with Rossmann planes, some of the model
       pixels can be negative.  These should be treated as outliers.  An example is when
       Bragg signal has leaked into the pixels considered to be background.  Here we
       create a new filter, the "greenlist" that starts from the whitelist (pixels in
       refl_table shoeboxes) and identifies pixels with safe, positive background.
       11/1/22. Revisit.  The filter doesn't seem to be operating correctly.
       While flagging some negative pixels and counting the shoeboxes, the negative
       pixels don't seem to be removed.  The cost function then gets a logarithm of
       a negative number, and returns "nan"; optimization then diverges, and is caught by an assert.
    """
    background_adu = self.view[background_from].select(self.relevant_whitelist_order)
    if flex.min(background_adu) > 0.:
      self.greenlist = None; return

    self.greenlist = flex.bool(len(background_adu), True)
    background_positive = flex.bool(len(self.refl_table), True)
    for background, (ipanel, islow, ifast, sidx) in zip(
          background_adu, self.per_lattice_whitelist_iterator()):
        if background <= 0. : background_positive[sidx] = False
    print('Debugb bckgd range %7.2f - %7.2f, removing %d shoeboxes'%(
      flex.min(background_adu), flex.max(background_adu), background_positive.count(False)))

    idx = -1
    for background, (ipanel, islow, ifast, sidx) in zip(
          background_adu, self.per_lattice_whitelist_iterator()):
        idx += 1
        if not background_positive[sidx]:
           self.greenlist[idx] = False

  def Z_statistics(self,experimental, model, report=True, plot=False, method="border", legend=""):
    # XXX Fixme.  Change to a static method? Create a Z-reporter object with plot method.
    iterator = {"border":self.per_shoebox_border_iterator,
                "all":self.per_shoebox_whitelist_iterator}[method]
    Z_plot = []
    sigma_pixel_photons = []
    format_offset = self.params.debug.format_offset # only for SMV offset of 10
    model = model.as_numpy_array() # only way I know to get the panel slices
    gain = self.expt.detector[0].get_gain() # assumes detector-wide gain
    for x in range(self.n_panels):
      exp_panel_adu = experimental[x]
      abs_exp_panel_adu = np.abs(exp_panel_adu)
      abs_exp_panel_photons = abs_exp_panel_adu/gain # convert to photons
      poisson_noise_sigma_photons = np.sqrt(abs_exp_panel_photons)
      poisson_noise_sigma_photons = np.where(poisson_noise_sigma_photons==0., 1., poisson_noise_sigma_photons)
      sigma_pixel_photons.append(poisson_noise_sigma_photons)
      if report:
        diff_panel_photons = (model[x] - experimental[x])/gain
        offset_Z = gain * ((diff_panel_photons/poisson_noise_sigma_photons)*0.1 + 1.0) + format_offset
        # apparently the viewer requests data in photons, fool it by multiplying here by gain
        Z_plot.append(offset_Z)

    self.proposal_shoebox_mean_Z = flex.double()
    self.proposal_shoebox_sigma_Z = flex.double()
    all_Z_values = flex.double()
    sauter_eq_15_likelihood = flex.double()

    for sidx in range(len(self.refl_table)): #loop through the shoeboxes
      shoebox_Z_values = flex.double()
      for ipanel, islow, ifast in iterator(sidx):
        if model[ipanel][islow,ifast]<=0: continue
        std_dev_denominator_photons = sigma_pixel_photons[ipanel][islow,ifast]
        diff_panel_photons = (model[ipanel][islow,ifast] - experimental[ipanel][islow,ifast])/gain
        Z = (diff_panel_photons/std_dev_denominator_photons)
        shoebox_Z_values.append(Z)
        all_Z_values.append(Z)
        sauter_eq_15_likelihood.append( (model[ipanel][islow,ifast]/gain) -
          (experimental[ipanel][islow,ifast]/gain) * math.log(model[ipanel][islow,ifast]/gain))
      # avoid situation of <2 pixels in shoebox
      if len(shoebox_Z_values) > 1:
        stats = flex.mean_and_variance(shoebox_Z_values)
        self.proposal_shoebox_mean_Z.append( stats.mean() )
        self.proposal_shoebox_sigma_Z.append( stats.unweighted_sample_standard_deviation() )
      else:
        self.proposal_shoebox_mean_Z.append( -1 ) # dummy values, what else to do?
        self.proposal_shoebox_sigma_Z.append( -1 )
    self.refl_table["mean_Z"] = self.proposal_shoebox_mean_Z
    self.refl_table["sigma_Z"] = self.proposal_shoebox_sigma_Z
    print("proposal negative log likelihood %10f"%(flex.sum(sauter_eq_15_likelihood)))
    stats = flex.mean_and_variance(all_Z_values)
    self.mnz = stats.mean()
    self.sgz = stats.unweighted_sample_standard_deviation()
    print(legend+"proposal mean Z=%.2f, sigma Z=%.2f"%(self.mnz, self.sgz),"on %d pixels"%(len(all_Z_values)))
    if plot:
      self.plot_Z()
    if report:
      return Z_plot

  def plot_Z(self):
      from matplotlib import pyplot as plt
      plt.plot(range(len(self.refl_table)),
               self.proposal_shoebox_mean_Z.select(self.refl_table["spots_order"]),"r-",
               label="mean Z (all=%.2f)"%(self.mnz))
      plt.plot(range(len(self.refl_table)),
               self.proposal_shoebox_sigma_Z.select(self.refl_table["spots_order"]),
               label="std_dev Z (all=%.2f)"%(self.sgz))
      plt.title("Z_distribution in each shoebox")
      plt.xlabel("Spots ordered by increasing Bragg angle →")
      plt.ylabel("Z-value")
      plt.legend(loc='upper right')
      plt.show()

  def minimize_planes(self, experimental):
    # separate LBFGS minimization for each shoebox (for unit test only)
    from LS49.adse13_187.adse13_221.smooth_fit import fit_background_one_spot
    gain = self.expt.detector[0].get_gain()
    for sidx in range(len(self.refl_table)): #loop through the shoeboxes
      bbox = self.refl_table["bbox"][sidx]
      target_box_size = 0 # dummy value not actually used XXX

      # Get the initialization data for fitting the pixels on one shoebox
      shoebox = self.refl_table["shoebox"][sidx]
      peakslow = int(shoebox.centroid_all().px.position[1]) # approx slow-position of peak
      peakfast = int(shoebox.centroid_all().px.position[0]) # approx fast-position of peak
      FIT = fit_background_one_spot(boxsize = target_box_size, peakslow=peakslow, peakfast=peakfast,
                                    gain=gain)
      for ipanel, islow, ifast in self.per_shoebox_border_iterator(sidx):
        value = experimental[ipanel][islow,ifast]
        FIT.addpixel(islow,ifast,value)
      FIT.initialize_A()
      try:
        FIT.initialize_and_run()
      except RuntimeError as e:
        print("skipping spot"); continue
      model = self.view["bragg_plus_background"]
      for ipanel, islow, ifast in self.per_shoebox_border_iterator(sidx):
        model[ipanel,islow,ifast] = FIT.model_T(islow,ifast) * gain
      # XXX Fixme we need to get these values back to the shoebox in the result refl table iterating over whitelist

  def minimize_planes_single_lattice(self, raw_pixels, params,
      publish_to="bragg_plus_background"):
    print ("fitting a single lattice with %d shoeboxes"%(len(self.refl_table)))
    from planes_one_lattice import fit_parameters_one_lattice_v1
    POL = fit_parameters_one_lattice_v1()
    for label in params.exafel.refpar.label:
      obj = getattr(params.exafel.refpar, label)
      if obj.scope == "spot": # discovered a refinable parameter that goes by spot
        for sidx in range(len(self.refl_table)): #loop through the shoeboxes
          # Get the initialization data for fitting the pixels on one shoebox
          shoebox = self.refl_table["shoebox"][sidx]
          G = globals()[label](shoebox, self.expt) # get a class instance representing a parameter group refined by spot
          if obj.slice_init == "border":
            G.set_data(raw_pixels = raw_pixels, index = sidx,
                       opt_iterator = self.per_shoebox_border_iterator,
                       pub_iterator = self.per_shoebox_whitelist_iterator,
                       model = self.view[publish_to])
          elif obj.slice_init == "all":
            G.set_data(raw_pixels = raw_pixels, index = sidx,
                       opt_iterator = self.per_shoebox_whitelist_iterator,
                       pub_iterator = self.per_shoebox_whitelist_iterator,
                       model = self.view[publish_to])
          POL.register(G)
    print("The POL has registered %d groups"%(len(POL.groups)))
    POL.cold_initialize()
    POL.run()
    POL.publish_model()

  def select_greenlist_subset(self, array):
    if self.greenlist:
      return array.select(self.greenlist)
    else:
      return array

  def db_callback(self): # the diffBragg callback, get simulation and derivatives from current model
    #P = Profiler("%40s"%"callback diffBragg with int_whitelist")
    self.dB.add_diffBragg_spots(self.panel_fast_slow)
    whitelist_dB = self.dB.raw_pixels_roi
    return whitelist_dB

  def nb_callback(self): # the nanoBragg callback, get the static simulation once
    return self.whitelist_only

  def refine_bragg_single_lattice(self, raw_pixels, params, background_from, publish_to, edge="mono"):
    whitelist_dB = None
    if edge == "mono": #choose to take structure factors at one energy only
      x = 0 # multiple wavelengths in the beam only address one structure factor channel in gpu_amplitudes
      ichannels = flex.int(len(self.nbBeam_spectrum), x)
    elif edge == "poly": # access separate structure factors at each energy
      ichannels = flex.int(range(len(self.nbBeam_spectrum)))
      print ("CONFIRMING %d channels"%(len(ichannels)))
    mask_array = self.monolithic_mask_whole_dectector_as_int_whitelist
    print("relevant whitelist is", len(self.relevant_whitelist_order))
    P = Profiler("%40s"%"refinery energy_multichannel_mask_allpanel with int_whitelist")
    self.gpu_simulation.add_energy_multichannel_mask_allpanel(
        ichannels = ichannels,
        gpu_amplitudes = self.gpu_channels_singleton,
        gpu_detector = self.gpu_detector,
        pixel_active_list_ints = mask_array,
        weights = flex.double([item[1] for item in self.nbBeam_spectrum]) #pick out fluxes from beam
    )
    self.whitelist_only = self.gpu_detector.get_whitelist_raw_pixels(self.whitelist_focused_idx)
    # whitelist_only: flex_double pixel values
    # whitelist_focused_idx: flex.size_t detector addresses
    self.gpu_detector.scale_in_place(0) # reset
    del P

    if params.exafel.skin:
      ### boilerplate
      slow_size = self.expt.detector[0].get_image_size()[1]
      fast_size = self.expt.detector[0].get_image_size()[0]
      panel_size = slow_size*fast_size
      panel_fast_slow = flex.size_t()
      for item in self.relevant_whitelist_order:
        panel = item // panel_size
        addr_in_panel = item % panel_size
        slow = addr_in_panel // fast_size
        fast = addr_in_panel % fast_size
        panel_fast_slow.append(panel);
        panel_fast_slow.append(fast);
        panel_fast_slow.append(slow);
      self.panel_fast_slow=panel_fast_slow
      P = Profiler("%40s"%"refinery diffBragg with int_whitelist")
      whitelist_dB = self.db_callback()
      del P
      engine_callback = self.db_callback
      #from matplotlib import pyplot as plt
      #plt.loglog(self.whitelist_only, whitelist_dB, "r.")
      #plt.show()

      assert self.whitelist_only.size() == whitelist_dB.size()  # require diffBragg patch
      correlation = flex.linear_correlation(self.whitelist_only, whitelist_dB)
      cc = correlation.coefficient()
      print("CC is %.2f %%"%(100. * cc))
      assert cc > 0.87

      # previously, the nanoBragg and diffBragg simulations were essentially equivalent
      # probably still equivalant for monochromatic but may treat polychromatic differently XXX FIXME
    else:
      engine_callback = self.nb_callback

      # Tests
      # what is the cc between these two simulations? (100%)
      # Can I refine with these pixels? (Implicitly)
      # Sources are identical
      # F_amp checks out (but parallel_for printing is unreliable)
      # Umats implicitly check out once # umats is made consistent, as C.C. between simulations is 100%
      # Low level state variables consistent in show_params()
      # Future: Double check a tiger stripes example.  Difficult b/c exafel has the external ichannels mechanism
      #   to zero out specific energy channels.  DiffBragg would renormalize sources if source channels are zeroed.

    print ("refining a single lattice with %d shoeboxes"%(len(self.refl_table)))
    POL = fit_parameters_one_lattice(engine = engine_callback, finite_diff = params.exafel.debug.finite_diff,
                                     finite_diff_eps = params.exafel.debug.eps)
    for label in params.exafel.refpar.label:
      obj = getattr(params.exafel.refpar, label)
      if obj.scope == "lattice": # discovered a refinable parameter that goes by lattice
          GCI = globals()[label](self.expt,self.select_greenlist_subset, self.dB, POL, obj)
            # get a class instance representing a parameter group refined by spot
          # input 1) experimental pixels (adu) coerced to 1D relevant array
          Ki_adu = flex.double()
          for ipanel, islow, ifast, sidx in self.per_lattice_whitelist_iterator():
            value = raw_pixels[ipanel][islow, ifast]
            Ki_adu.append(1. * value) # 1. * converts a numpy.int32 to Python float
          model_adu = whitelist_dB if params.exafel.skin else self.whitelist_only
          assert len(model_adu) == len(Ki_adu)
          # input 3) background model coerced to 1D relevant array
          background_adu = self.view[background_from].select(self.relevant_whitelist_order)
          print ("%d background px, %d positive, %d not positive"%(
                 len(background_adu), (background_adu>0.).count(True), (background_adu>0.).count(False)))
          # debug
          #for iidx,lat in enumerate(self.per_lattice_whitelist_iterator()):
          #  print(iidx,lat,"%8.3f"%(background_adu[iidx]), "%6.1f"%(Ki_adu[iidx]))

          GCI.set_data(Ki_adu = Ki_adu,
                     model_adu = model_adu,
                     background_adu = background_adu
                     )
          POL.register(GCI)
    print("The lattice-wide POL has registered %d groups"%(len(POL.groups)))
    POL.cold_initialize()
    POL.run()
    if params.exafel.hotstart and len(POL.groups) > 1: #refining more than just G
      POL.hot_initialize()
      POL.run()
    POL.publish_model()
    return POL

  @property
  def whitelist_focused_idx(self) -> flex.size_t:
    """
    Return a flex with size_t "focused" addresses of relevant whitelist pixels

    This property corrects for the fact that shoeboxes might overlap.
    The low-memory output is in the m = mask_array order over index i = 1,...,p.
    Meanwhile whitelist_idx should be of size q > p because some spots overlap.
    r = self.relevant_whitelist_order is over j = 1,...,q
    w = whitelist_idx; w[j] = mask_array.index(r[j]) for j in range(q)

    Note: Tests on simulated 4 um PSII data show this method takes ~30-40 ms
    per call and is called once per refinement (single lattice or spread).
    """
    full_idx: int  # index of a pixel in the full mask array
    focus_idx: int    # index of a pixel in simulated focused (low-mem) region
    mask_array = self.monolithic_mask_whole_dectector_as_int_whitelist
    full_to_focus = {full_idx: focus_idx for focus_idx, full_idx in enumerate(mask_array)}
    return flex.size_t([full_to_focus[full_idx]
                        for full_idx in self.relevant_whitelist_order])



class G:
  def __init__(self, expt, green, engine, lattice, params):
    self.n = 1
    self.gain = expt.detector[0].get_gain()
    self.label = "G"
    self.green_callback = green # a callback for converting a whitelist to a greenlist
    # whitelist == all pixels in DIALS shoeboxes
    # greenlist == those with positive Rossmann plane models and therefore suitable for target evaluation
    self.lattice = lattice
    self.refpar = params
  def set_data(self, Ki_adu, model_adu, background_adu):
    """API: entering this encapsulated parameter class, strictly adhere to
       1) data arrays are 1D flex doubles
       2) length should strictly be the relevant whitelist
       3) order should be the relevant whitelist order
       4) to get data in this format, might need callbacks into run manager class that owns the nanoBragg simulator
    """
    self.background_adu = background_adu
    self.model_adu = model_adu
    self.Ki_adu = Ki_adu # Ki is the experimental observation
    Ki_photons = Ki_adu/self.gain # Ki is the experimental in units of photons
    self.green_Ki_photons = self.green_callback(Ki_photons) # narrow "good-background" pixel selection for target evaluation

  def cold_initialize(self):
    ersatz_numerator = self.Ki_adu - self.background_adu
    ersatz_denominator = self.model_adu
    self.Gscale = flex.sum(ersatz_numerator) / flex.sum(ersatz_denominator)
    if self.refpar.reparameterize == "free":
      return self.Gscale,
    assert self.Gscale > 0.
    return math.log(self.Gscale),
  def hot_initialize(self):
    ersatz_numerator = self.Ki_adu - self.background_adu
    ersatz_denominator = self.lattice.model_adu
    self.Gscale = flex.sum(self.green_callback(ersatz_numerator)) / flex.sum(self.green_callback(ersatz_denominator))
    if self.refpar.reparameterize == "free":
      return self.Gscale,
    assert self.Gscale > 0.
    return math.log(self.Gscale),
  def take_result(self, n_values):
    assert len(n_values)==self.n
    if self.refpar.reparameterize == "free":
      self.Gscale, = tuple(n_values)
    else:
      assert -30 < n_values[0] < 30.
      self.Gscale = math.exp(n_values[0])

  def compute_functional_and_gradients(self):
    f = 0.;
    g = flex.double(self.n)

    f = flex.sum(self.lattice.green_lambda_photons - (self.green_Ki_photons * flex.log(self.lattice.green_lambda_photons)))
    inner_paren = flex.double(len(self.green_Ki_photons),1.) - (self.green_Ki_photons/self.lattice.green_lambda_photons)

    g_list = [flex.sum( deriv * inner_paren ) for deriv in
               [self.lattice.green_model_adu/self.gain]
             ]
    if self.refpar.reparameterize == "free":
      g = flex.double(g_list)
    else:
      g = self.Gscale * flex.double(g_list) # carry the derivative through to the inner property of l = ln G.
    self.print_step("LBFGS f=",f,g)
    return f,g
  def print_step(self,message,target,g):
    print ("%s%13.6e"%(message,target),
           "[","".join(["G=%9.3e"%a for a in [self.Gscale]]),"(%9.2e)]"%(g[0]),end="")
  def publish_model(self):
    pass

class rot:
  def __init__(self, expt, green, engine, lattice, params):
    self.n = 3
    self.gain = expt.detector[0].get_gain()
    self.label = "rot"
    self.green_callback = green # a callback for converting a whitelist to a greenlist
    self.engine = engine # diffBragg instance for derivatives
    self.lattice = lattice
    self.refpar = params
    self.working_expt = expt
  def set_data(self, Ki_adu, model_adu, background_adu):
    self.background_adu = background_adu
    Ki_adu # Ki is the experimental observation
    Ki_photons = Ki_adu/self.gain # Ki is the experimental in units of photons
    self.green_Ki_photons = self.green_callback(Ki_photons) # narrow "good-background" pixel selection for target evaluation
  def cold_initialize(self):
    self.rotX = 0.0; self.rotY = 0.0; self.rotZ = 0.0;
    return self.rotX, self.rotY, self.rotZ
  def take_result(self, n_values):
    assert len(n_values)==self.n
    self.rotX,self.rotY,self.rotZ = tuple(n_values)
    self.engine.set_value(0, self.rotX)
    self.engine.set_value(1, self.rotY)
    self.engine.set_value(2, self.rotZ)
  def compute_functional_and_gradients(self):
    f = 0.;
    g = flex.double(self.n)

    inner_paren = flex.double(len(self.green_Ki_photons),1.) - (self.green_Ki_photons/self.lattice.green_lambda_photons)
    drotX = self.green_callback(self.engine.get_derivative_pixels(0))
    drotY = self.green_callback(self.engine.get_derivative_pixels(1))
    drotZ = self.green_callback(self.engine.get_derivative_pixels(2))
    g_list = [flex.sum( deriv * inner_paren ) for deriv in
               [drotX/self.gain,drotY/self.gain,drotZ/self.gain]
             ]

    g = flex.double(g_list) * self.lattice.Gscale
    if self.refpar.restraint:
      for ioff, offset in enumerate([180.*self.rotX/math.pi,180.*self.rotY/math.pi,180*self.rotZ/math.pi]):
        f       += 0.5 * (offset / self.refpar.restraint)**2
        g[ioff] += (offset / self.refpar.restraint) * 180./(math.pi * self.refpar.restraint)
    self.print_step(" Rot",g)
    return f,g
  def print_step(self,message,g):
    print ("%s "%(message),
           "["," ".join(["%8.4f° (%8.1e)"%(a,g[i]) for i,a in enumerate([180.*self.rotX/math.pi,180.*self.rotY/math.pi,180*self.rotZ/math.pi])]),"]",end="")
  def publish_model(self):
    # Implementing the publish_model method takes our optimized crystal orientation back to the
    # experimental crystal model, which can be written to disk in the *.expt file.
    # This allows the orientation results from 9exa3a to be used in scattering factor refiment (11refine)
    # Verified the (-) sign applied to rotXYZ angles is correct (by comparison to simulation ground truth)
    # We haven't definitively proven that we are taking the exact orientation used by nano/diffBragg
    # It would be necessary to print out the C++ diffBragg orientation.
    c = self.working_expt.crystal
    c.rotate_around_origin((0,0,1),-self.rotZ,False)
    c.rotate_around_origin((0,1,0),-self.rotY,False)
    c.rotate_around_origin((1,0,0),-self.rotX,False)

from parameters import background

class fit_parameters_one_lattice: # POL refinery
  def __init__(self, engine, finite_diff=-1, finite_diff_eps=1.e-8):
    self.n = 0
    self.groups = []
    self.engine = engine
    self.finite_diff = finite_diff
    self.finite_diff_eps = finite_diff_eps
  def register(self, parameter_group):
    self.groups.append(parameter_group)
  def cold_initialize(self):
    self.iteration = 0
    self.x = flex.double()
    for group in self.groups:
      for p in group.cold_initialize():
        self.x.append(p)
        self.n += 1
  def hot_initialize(self):
    self.iteration = 0
    idx = 0
    for group in self.groups:
      if group.label=="G":
        self.x[idx] = group.hot_initialize()[0]
      idx += group.n
  def run(self):
    if 1:
      self.minimizer = scitbx.lbfgs.run(target_evaluator=self,
         termination_params=scitbx.lbfgs.termination_parameters(
         traditional_convergence_test=True,
         traditional_convergence_test_eps=1.e-3,
         max_calls=100))
    if 0:
      from scipy.optimize import minimize
      minimize(self.fun_method, list(self.x), jac=True, method="L-BFGS-B") # BFGS, CG, Newton-CG (best)
                                                                           # TNC
    if 0:
      from scipy.optimize import minimize
      minimize(self.fun_method, list(self.x), jac=True, method="Newton-CG")

    self.a = self.x
    idx = 0
    for group in self.groups:
      group.take_result(self.a[idx:idx+group.n])
      idx += group.n
  def fun_method(self, x):
    self.x = flex.double(list(x))
    return self.compute_functional_and_gradients() # wrap f,g for scipy
  def compute_functional_and_gradients(self):
    self.iteration += 1
    self.a = self.x
    f = 0.;
    g = flex.double()
    idx = 0
    for group in self.groups: # first round, parameter managers update the diffbragg engine
      group.take_result(self.a[idx:idx+group.n])
      idx += group.n

    self.model_adu = self.engine() # callback gives current model adu
    G_manager = [item for item in self.groups if item.label=="G"][0]
    self.Gscale = Gscale = G_manager.Gscale
    self.green_model_adu = G_manager.green_callback(self.model_adu)   # nanoBragg intensity eqn. 6
    self.bragg_adu = Gscale * self.model_adu
    self.lambda_adu = G_manager.background_adu + self.bragg_adu
    self.lambda_photons = self.lambda_adu / G_manager.gain # lambda is in units of photons
    self.green_lambda_photons = G_manager.green_callback(self.lambda_photons)

    for group in self.groups: # lastly, get contributions to gradient vectors
      group_f, group_g = group.compute_functional_and_gradients()
      f += group_f
      g.extend(group_g)
    print()
    assert len(g)==self.n

    if self.finite_diff == self.iteration: # test all derivatives by finite-difference if requested
      self.test_all_derivatives_by_finite_difference(f, g)

    return f,g
  def test_all_derivatives_by_finite_difference(self, f, g):
      fdidx = 0
      print("Special code to check all gradients by finite differences")
      for group in self.groups:
        print("\tBegin finite difference analysis on",group.label)
        reserve_param_group = self.a[fdidx:fdidx+group.n]
        for iparam in range(group.n):
          test_params = copy.deepcopy(reserve_param_group)
          print("\t\tparam %d of %d"%(1+iparam, group.n))
          this_derivative = g[fdidx+iparam]
          base_value =  test_params[iparam]
          print("\t\t\tF is",f, "param is", base_value,"deriv is",this_derivative)
          if this_derivative==0.: continue
          eps = self.finite_diff_eps # desired fractional change in f
          incr_value = eps * f / this_derivative
          print("\t\t\ttesting increment", incr_value, "to give",eps,"fractional change in f")
          test_params[iparam] = base_value + incr_value
          group.take_result(test_params)
          hypo_f = 0.;
          hypoidx = 0
          self.model_adu = self.engine() # callback gives current model adu
          G_manager = [item for item in self.groups if item.label=="G"][0]
          self.Gscale = Gscale = G_manager.Gscale
          self.green_model_adu = G_manager.green_callback(self.model_adu)   # nanoBragg intensity eqn. 6
          self.bragg_adu = Gscale * self.model_adu
          self.lambda_adu = G_manager.background_adu + self.bragg_adu
          self.lambda_photons = self.lambda_adu / G_manager.gain # lambda is in units of photons
          self.green_lambda_photons = G_manager.green_callback(self.lambda_photons)

          for hypothetical_group in self.groups:
            hypo_group_f, unused = hypothetical_group.compute_functional_and_gradients()
            hypo_f += hypo_group_f
            print()

          print("\t\t\tThe hypothetical functional is",hypo_f)
          hypo_grad = (hypo_f-f)/incr_value
          print("\t\t\tThe hypothetical gradient is",hypo_grad)

        group.take_result(reserve_param_group)
        fdidx += group.n

  def get_adu_model(self):
    # for now, this is the pixel model in adu.  implementation expected to change.
    # returns bragg_adu, background_adu
    return (self.bragg_adu,
            [item.background_adu for item in self.groups if item.label=="G"][0])
  def publish_model(self):
    for group in self.groups: group.publish_model()
