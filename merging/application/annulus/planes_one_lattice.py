from __future__ import division
from dials.array_family import flex
import scitbx.lbfgs

class fit_parameters_one_lattice_v1: # POL refinery
  def __init__(self):
    self.n = 0
    self.groups = []
  def register(self, parameter_group):
    self.groups.append(parameter_group)
  def cold_initialize(self):
    self.x = flex.double()
    for group in self.groups:
      for p in group.cold_initialize():
        self.x.append(p)
        self.n += 1
  def run(self):
    if 1:
      self.minimizer = scitbx.lbfgs.run(target_evaluator=self,
         termination_params=scitbx.lbfgs.termination_parameters(
         traditional_convergence_test=True,
         traditional_convergence_test_eps=1.e-3,
         max_calls=100))
    if 0:
      from scipy.optimize import minimize
      minimize(self.fun_method, list(self.x), jac=True, method="L-BFGS-B") # BFGS, CG, Newton-CG (best)
                                                                           # TNC
    if 0:
      from scipy.optimize import minimize
      minimize(self.fun_method, list(self.x), jac=True, method="Newton-CG")

    self.a = self.x
    idx = 0
    for group in self.groups:
      group.take_result(self.a[idx:idx+group.n])
      idx += group.n
  def fun_method(self, x):
    self.x = flex.double(list(x))
    return self.compute_functional_and_gradients() # wrap f,g for scipy
  def compute_functional_and_gradients(self):
    self.a = self.x
    f = 0.;
    g = flex.double()
    idx = 0
    for group in self.groups:
      group.take_result(self.a[idx:idx+group.n])
      group_f, group_g = group.compute_functional_and_gradients()
      f += group_f
      g.extend(group_g)
    assert len(g)==self.n
    return f,g
  def get_adu_model(self):
    # for now, this is the pixel model in adu.  implementation expected to change.
    # returns bragg_adu, background_adu
    return ([item.bragg_adu for item in self.groups if item.label=="G"][0],
            [item.background_adu for item in self.groups if item.label=="G"][0])
  def publish_model(self):
    for group in self.groups: group.publish_model()
