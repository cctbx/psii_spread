from __future__ import print_function, division
from scitbx.array_family import flex
from libtbx.math_utils import round2
import copy
import numpy as np
from scipy import constants
ENERGY_CONV = 1e10 * constants.c * constants.h / constants.electron_volt

# %%% boilerplate specialize to packaged big data %%%
from LS49.sim import step5_pad
from LS49.sim import step4_pad
from LS49.spectra import generate_spectra
from LS49 import ls49_big_data
step5_pad.big_data = ls49_big_data
step4_pad.big_data = ls49_big_data
generate_spectra.big_data = ls49_big_data
# %%%%%%
from LS49.sim.step5_pad import data
local_data = data()
Fe_metallic_model = local_data.get("Fe_metallic_model")
Mn_metallic_model = local_data.get("Mn_metallic_model")

from LS49.sim.fdp_plot import george_sherrell
class george_sherrell_star(george_sherrell):
  def __init__(self,energies,fp,fdp):
    self.energy = [round2(e,3) for e in energies] # FIXME why do we need to round?
    self.fp = fp
    self.fdp = fdp
  def fp_fdp_at_wavelength(self,angstroms):
    # parent class, operates on published tables, uses a cubic spline
    # in contrast this child class operates on preset energies, so uses exact lookup
    lookup_energy = round2(ENERGY_CONV/angstroms, 3) # FIXME why round?
    try:
      lookup_idx = list(self.energy).index(lookup_energy)
    except ValueError as v:
      print ("Looking for",lookup_energy,"among",list(self.energy))
    return self.fp[lookup_idx], self.fdp[lookup_idx]
  def plot_them(self,plt,label=None,**kwargs):
    E = self.energy; fp = self.fp; fdp = self.fdp
    print("PLOTTING",len(E),len(fp),len(fdp))
    if label is None:
      plt.plot(E, fp, **kwargs)
    else:
      plt.plot(E, fp, label=label, **kwargs)
    plt.plot(E, fdp, **kwargs)

class rank_0_fit_all_f_mix_in:
  @property
  def refined_metal(self):
    exafel_metal_map = {'MMO2': 'Fe', 'MMO1': 'Fe', 'PSII': 'Mn', 'PSII4': 'Mn'}
    return exafel_metal_map[self.params.exafel.metal]

  @staticmethod
  def model_as_physical_parameters(n_classes, starting_models, energies):
    n_energies = len(energies)
    n_physical_parameters = 2 * n_classes * n_energies
    physical_parameters = flex.double(n_physical_parameters) #lay out the parameters.
    for incr, energy in enumerate(energies):
      wavelength = ENERGY_CONV / energy
      for iclass, model in enumerate(starting_models):
        newfp,newfdp = model.fp_fdp_at_wavelength(angstroms=wavelength)
        physical_parameters[2 * iclass * n_energies + incr]=newfp
        physical_parameters[(2 * iclass + 1) * n_energies + incr]=newfdp
    return physical_parameters

  def rank_0_fit_all_f_setup(self,starting_models,energies):
    self.starting_models = starting_models # a list of LS49.sim.fdp_plot.george_sherrell instances
    self.energies = energies # preset energies at which the f's will be refined
    self.n_energies = len(energies)
    from psii_spread.merging.application.annulus.flex_parameters import FreeParameterFlexProxy
    alg = self.params.sauter20.LLG_evaluator.restraints.kramers_kronig.algorithm
    param_container = FreeParameterFlexProxy.registry[alg]
    print("flex_parameters class->",param_container)
    param_container.n_energies = self.n_energies  # required class attribute
    self.x = param_container( #input physical parameters to constructor, thus laying out the free parameters
               self.model_as_physical_parameters(self.n_classes,self.starting_models,self.energies)
             )
    self.n = len(self.x) # for the direct case it is 2 * self.n_classes * self.n_energies

  def reinitialize(self, logical_rank, comm_size, all_lattice_managers, all_lattice_models,
                   HKL_lookup, static_fcalcs, model_intensities):
    from libtbx import adopt_init_args
    adopt_init_args(self, locals())
    self.plot_plt_imported = False
    self.starting_params_cached = False
    if not self.starting_params_cached:
      self.starting_params_cached = copy.deepcopy(self.starting_models) # a list of george_sherrell
    self.iteration = 0
    self.macrocycle = None

  def set_macrocycle(self, value):
    # allows macrocycler to set the initial values as of cycle 1
    self.macrocycle = value

  def plot_em(self):
    free_parameters = self.x
    if not self.plot_plt_imported:
      from matplotlib import pyplot as plt
      self.plt = plt
      if self.params.sauter20.LLG_evaluator.title is None:
        self.plt.ion() # interactive - on
      self.plot_plt_imported = True
    if self.params.sauter20.LLG_evaluator.title is None:
      self.plt.cla() #clear last access
    fig = self.plt.figure(figsize=(8.4,4.8))

    import matplotlib.gridspec as gridspec
    gs = gridspec.GridSpec(nrows=1, ncols=1)
    self.axspec = fig.add_subplot(gs[0,0])

    # ground truth
    from LS49.sim.step5_pad import full_path
    if self.params.sauter20.LLG_evaluator.plot.reference:
     if self.refined_metal == 'Fe':
      GS = george_sherrell(full_path("data_sherrell/pf-rd-ox_fftkk.out"))
      GS.plot_them(self.plt,f1="b-",f2="b-")
      GS = george_sherrell(full_path("data_sherrell/pf-rd-red_fftkk.out"))
      GS.plot_them(self.plt,f1="r-",f2="r-")
      #GS = george_sherrell(full_path("data_sherrell/Fe_exafs_materials_danville.dat"))
      #GS.plot_them(self.plt,f1="g-",f2="g-") # scale, offset, and fp are not curated
      GS = george_sherrell(full_path("data_sherrell/Fe_fake.dat")) # with interpolated points
      GS.plot_them(self.plt,f1="m-",f2="m-")
     elif self.refined_metal == 'Mn':
      GS = george_sherrell(full_path("data_sherrell/MnO2_spliced.dat"))
      GS.plot_them(self.plt,f1="b-",f2="b-")
      GS = george_sherrell(full_path("data_sherrell/Mn2O3_spliced.dat"))
      GS.plot_them(self.plt,f1="r-",f2="r-")
      GS = george_sherrell(full_path("data_sherrell/Mn.dat"))
      GS.plot_them(self.plt, f1="m-", f2="m-")

    # starting values
    colors = "gcyk" # older code, less attractive colors
    colors = ["limegreen","c","gold","k","darkorange",'deeppink','deepskyblue','lime']
    if self.params.sauter20.LLG_evaluator.plot.starting_model:
     for imodel, model in enumerate(self.starting_models):
      fp = flex.double(); fdp = flex.double()
      for incr, energy in enumerate(self.energies):
        wavelength = ENERGY_CONV / energy
        newfp,newfdp = model.fp_fdp_at_wavelength(angstroms=wavelength)
        fp.append(newfp); fdp.append(newfdp)
      GS = george_sherrell_star(energies = self.energies, fp = fp, fdp = fdp)
      GS.plot_them(self.axspec, marker="x", linestyle="None", color=colors[imodel])

    # current values
    for iclass, model in enumerate(self.starting_models):
      fp = self.x.free_as_fp(iclass)
      fdp = self.x.free_as_fdp(iclass)
      GS = george_sherrell_star(self.energies, fp, fdp)
      GS.plot_them(self.plt, label=self.labels[iclass], marker=".", linestyle="-", color=colors[iclass])

    x_lims_map = {'Fe': (7088, 7152), 'Mn': (6495, 6605)}
    self.axspec.set_xlim(x_lims_map[self.refined_metal])
    self.axspec.set_ylim((-9.6,5.5))
    self.plt.title("Iteration %d"%self.iteration)
    self.plt.legend(loc="center right")
    if self.params.sauter20.LLG_evaluator.title is not None:
      macrocycle_tell = "" if self.macrocycle is None else "macrocycle_%02d_"%self.macrocycle
      fig.savefig("%s_%siteration_%02d.png"%(self.params.sauter20.LLG_evaluator.title,
                  macrocycle_tell,self.iteration))
    else:
      self.plt.draw()
      self.plt.pause(0.2)
    del fig
    #self.plt.show()

  @staticmethod
  def reso_greenlist(array, RM, uc, params):
        # previous analysis, including G and rot refinement, used no resolution filter
        # however, for scattering factors pay strict adherence to intended res limits
        # save us too, as the model structure factors haven't been calculated past the limit
        # pull in existing greenlist from the lattice manager
        if RM.greenlist is None:  RM.greenlist = flex.bool(len(RM.relevant_whitelist_millers), True)
        assert len(RM.greenlist) == len(RM.relevant_whitelist_order) # length of returned array from GPU
        resolutions = uc.d(RM.relevant_whitelist_millers)
        in_resolution = (resolutions < params.statistics.annulus.d_max).__and__(
                         resolutions > params.statistics.annulus.d_min)
        in_greenlist = (RM.greenlist).__and__(in_resolution)
        return array.select(in_greenlist)

  def compute_functional_and_gradients(self):
    free_parameters = self.x

    f = 0.;
    free_parameter_gradient = flex.double(self.n)
    physical_parameter_gradient = flex.double(2 * self.n_classes * self.n_energies)
    self.iteration += 1
    #print("inside compute rank",self.logical_rank,"for iteration",self.iteration)
    if self.logical_rank==0 or self.comm_size==1:
      print("inside compute rank",self.logical_rank,"for iteration",self.iteration)
      if self.params.sauter20.LLG_evaluator.enable_plot:  self.plot_em()
      print ("Macrocycle",self.macrocycle,"Iteration",self.iteration,
            list(self.x.free_as_physical_parameters_list(minimizer=self)))

    # can remove this after debug is complete
    from libtbx.mpi4py import MPI
    comm = MPI.COMM_WORLD
    comm.barrier()

    # recalculate model intensities for each target evaluation
    physical_models = self.x.free_as_physical_parameters(minimizer=self)
    self.model_intensities = self.get_intensity_structure(
      energy_channels=self.energies,
      static_base=self.static_fcalcs,
      preset_starting_models=physical_models,
      labels=self.labels)

    this_focus = self.model_intensities.focus()
    assert this_focus[0] == len(self.Fmodel_indices)
    assert this_focus[1] == (1 + 2 * self.n_classes) * len(self.energies)
    self.update_gpu_amplitudes(self.energies, self.model_intensities)
    assert self.gpu_polychromatic_channels_singleton.get_nchannels() == len(self.energies)

    print("inside DEBUG_COMPUTE_FUNC rank",self.logical_rank,"for iteration",self.iteration)
    for RM, POL in zip(self.all_lattice_managers, self.all_lattice_models):
      uc = self.static_miller_array.unit_cell() # for resolution filter
      edge = "poly"
      if edge == "mono": #choose to take structure factors at one energy only
        x = 0 # multiple wavelengths in the beam only address one structure factor channel in gpu_amplitudes
        ichannels = flex.int(len(RM.nbBeam_spectrum), x)
      elif edge == "poly": # access separate structure factors at each energy
        ichannels = flex.int(range(len(RM.nbBeam_spectrum)))
      mask_array = RM.monolithic_mask_whole_dectector_as_int_whitelist
      RM.gpu_simulation.add_energy_multichannel_mask_allpanel(
        ichannels = ichannels,
        gpu_amplitudes = RM.gpu_channels_singleton,
        gpu_detector = RM.gpu_detector,
        pixel_active_list_ints = mask_array,
        weights = flex.double([item[1] for item in RM.nbBeam_spectrum]) #pick out fluxes from beam
      )

      whitelist_idx = RM.whitelist_focused_idx
      model_adu_whitelist = RM.gpu_detector.get_whitelist_raw_pixels(whitelist_idx)
      RM.gpu_detector.scale_in_place(0) # reset

      G_manager = [item for item in POL.groups if item.label=="G"][0]
      Gscale = G_manager.Gscale
      model_adu_greenlist = self.reso_greenlist(model_adu_whitelist, RM, uc, self.params)#nanoBragg intensity eqn. 6
      # TODO: WARNING - `model_adu_greenlist` is never used, probably bug
      bragg_adu = Gscale * model_adu_whitelist
      lambda_adu = G_manager.background_adu + bragg_adu
      lambda_photons = lambda_adu / G_manager.gain # lambda is in units of photons
      green_lambda_photons = self.reso_greenlist(lambda_photons, RM, uc, self.params)

      Ki_photons = G_manager.Ki_adu / G_manager.gain # Ki is the experimental in units of photons
      green_Ki_photons = self.reso_greenlist(Ki_photons, RM, uc, self.params)

      f += flex.sum(green_lambda_photons - (green_Ki_photons * flex.log(green_lambda_photons)))

      # inner parenthesis of eq.(20), on greenlist pixels:
      inner_paren = flex.double(len(green_Ki_photons),1.) - (green_Ki_photons/green_lambda_photons)
      # configure an array that lists the millers for each pixel in the greenlist
      millers_greenlist = self.reso_greenlist(RM.relevant_whitelist_millers, RM, uc, self.params)
      #print("all greenlist",len(millers_greenlist))
      #for item in set(list(millers_greenlist)):
      #  print(item, "%.3f"%(self.static_miller_array.unit_cell().d(item)))
      #print("HKL_Lookup total of ",len(self.HKL_lookup))
      index_into_intensities = flex.size_t([self.HKL_lookup[item] for item in millers_greenlist])
      #print("The index into intensities is",len(index_into_intensities))

      for ienergy, energy in enumerate(self.energies):
        # figure the Lambda term arising from that energy alone
        if edge == "poly": # access separate structure factors at each energy
          ichannels = flex.int(len(self.energies), -1)
          ichannels[ienergy] = ienergy
        RM.gpu_simulation.add_energy_multichannel_mask_allpanel(
          ichannels = ichannels,
          gpu_amplitudes = RM.gpu_channels_singleton,
          gpu_detector = RM.gpu_detector,
          pixel_active_list_ints = mask_array,
          weights = flex.double([item[1] for item in RM.nbBeam_spectrum]) #pick out fluxes from beam
        )

        model_adu_whitelist = RM.gpu_detector.get_whitelist_raw_pixels(whitelist_idx)
        RM.gpu_detector.scale_in_place(0) # reset
        model_adu_greenlist = self.reso_greenlist(model_adu_whitelist, RM, uc, self.params)
        wavelength_adu = Gscale * model_adu_greenlist
        wavelength_photons = wavelength_adu / G_manager.gain # lambda is in units of photons
        assert len(wavelength_photons) == len(green_lambda_photons) # both are greenlists

        # select a list of structure factor intensities for each greenlist pixel
        greenlist_Isim = self.model_intensities.matrix_copy_block(
          i_row=0,i_column=ienergy,n_rows=self.Fmodel_indices.size(),n_columns=1
          ).as_1d().select(index_into_intensities)
        assert len(greenlist_Isim) == len(green_lambda_photons)
        partial_model_partial_Isim_at_wave = wavelength_photons / greenlist_Isim

        for iclass, model in enumerate(self.starting_models):
          partial_Isim_partial_fp_at_wave = self.model_intensities.matrix_copy_block(
          i_row=0,i_column=ienergy + ((2 * iclass + 1) * self.n_energies),n_rows=self.Fmodel_indices.size(),
          n_columns=1).as_1d().select(index_into_intensities)
          partial_Isim_partial_fdp_at_wave = self.model_intensities.matrix_copy_block(
          i_row=0,i_column=ienergy + ((2 * iclass + 2) * self.n_energies),n_rows=self.Fmodel_indices.size(),
          n_columns=1).as_1d().select(index_into_intensities)

          partial_model_partial_fp_at_wave = partial_model_partial_Isim_at_wave * partial_Isim_partial_fp_at_wave
          partial_model_partial_fdp_at_wave = partial_model_partial_Isim_at_wave * partial_Isim_partial_fdp_at_wave

          physical_parameter_gradient[self.x.fp_address(ienergy,iclass)] += flex.sum(partial_model_partial_fp_at_wave * inner_paren)
          physical_parameter_gradient[self.x.fdp_address(ienergy,iclass)] += flex.sum(partial_model_partial_fdp_at_wave * inner_paren)

    if self.logical_rank == 0 or self.comm_size==1:
      if self.params.sauter20.LLG_evaluator.restraints.fp.sigma is not None:
        # add restraints on the fp model
        for iclass, model in enumerate(self.starting_models):
          # Debug option if iclass != 1: continue
          ffp,g1fp = self.compute_functional_and_gradients_fp(
            metal_fp = self.x.free_as_fp(iclass),
            mean = self.params.sauter20.LLG_evaluator.restraints.fp.mean,
            sigma = self.params.sauter20.LLG_evaluator.restraints.fp.sigma,constrain_endpoints=True)
          f += ffp
          for gidx in range(self.n_energies):
            physical_parameter_gradient[self.x.fp_address(gidx,iclass)] += g1fp[gidx];

      if self.params.sauter20.LLG_evaluator.restraints.fdp.sigma is not None:
        # add restraints on the fdp model
        for iclass, model in enumerate(self.starting_models):
          # debug option if iclass != 0: continue
          ffp,g1fp = self.compute_functional_and_gradients_fp(
            metal_fp = self.x.free_as_fdp(iclass),
            mean = self.params.sauter20.LLG_evaluator.restraints.fdp.mean,
            sigma = self.params.sauter20.LLG_evaluator.restraints.fdp.sigma,constrain_endpoints=True)
          f += ffp
          for gidx in range(self.n_energies):
            physical_parameter_gradient[self.x.fdp_address(gidx,iclass)] += g1fp[gidx];

      if self.params.sauter20.LLG_evaluator.restraints.kramers_kronig.use:
        from cctbx.dispersion.kramers_kronig import kramers_kronig
        # Kramers-Kronig restraint
        for iclass, model in enumerate(self.starting_models):
          # Debug option if iclass != 1: continue
          ffp,g1fp, g1fdp = self.compute_functional_and_gradients_kramers_kronig(
            metal_fp = self.x.free_as_fp(iclass),
            metal_fdp = self.x.free_as_fdp(iclass)
          )
          f += ffp
          for gidx in range(self.n_energies):
            physical_parameter_gradient[self.x.fp_address(gidx,iclass)] += g1fp[gidx];
            physical_parameter_gradient[self.x.fdp_address(gidx,iclass)] += g1fdp[gidx];

    self.print_step("Rank %d LBFGS Iteration %d"%(self.logical_rank,self.iteration),f,physical_parameter_gradient)
    free_parameter_gradient += self.x.chain_rule(physical_parameter_gradient)
    return f, free_parameter_gradient

  def compute_functional_and_gradients_fp(self,metal_fp,mean,sigma,constrain_endpoints=False):
    f = 0.
    if constrain_endpoints:  effective_range=range(1,self.n_energies-1)
    else: effective_range=range(self.n_energies)
    for i in effective_range:
      f += (0.5/(sigma*sigma) * (metal_fp[i] - metal_fp[i-1] -mean)**2)
    g1 = flex.double(len(metal_fp))
    for i in range(self.n_energies):
      g1[i] += 2. * metal_fp[i];
      if i > 0:
        g1[i] -= metal_fp[i-1];
      else:
        g1[i] -= metal_fp[i];
      if i < (self.n_energies - 1):
        g1[i] -= metal_fp[i+1];
      else:
        g1[i] -= metal_fp[i];

    g1 /= sigma*sigma
    return f,g1

  def compute_functional_and_gradients_kramers_kronig(self,metal_fp,
                                                      metal_fdp
                                                      ):
    """Calculation of Kramers-Kronig restraint and associated gradient"""
    pad = self.params.sauter20.LLG_evaluator.restraints.kramers_kronig.pad
    trim = self.params.sauter20.LLG_evaluator.restraints.kramers_kronig.trim
    weighting_factor = self.params.sauter20.LLG_evaluator.restraints.kramers_kronig.weighting_factor
    import torch
    from cctbx.dispersion.kramers_kronig import kramers_kronig
    f_p_torch = torch.tensor(metal_fp,requires_grad=True)
    f_dp_torch = torch.tensor(metal_fdp, requires_grad=True)
    metallic_models = {'Fe': Fe_metallic_model, 'Mn': Mn_metallic_model}
    metallic_model = metallic_models[self.refined_metal]
    known_response_energy = np.array(metallic_model.energy)
    known_response_f_p = np.array(metallic_model.fp)
    known_response_f_dp = np.array(metallic_model.fdp)

    kk_loss = weighting_factor*kramers_kronig.get_penalty(torch.tensor(self.energies), f_p_torch, f_dp_torch,
                                                          padn=pad, trim=trim,
                                                          known_response_energy=known_response_energy,
                                                          known_response_f_p=known_response_f_p,
                                                          known_response_f_dp=known_response_f_dp,
                                                          )

    kk_loss.backward()

    g1fp = f_p_torch.grad
    g1fdp = f_dp_torch.grad

    return kk_loss.detach().numpy(),g1fp.detach().numpy(),g1fdp.detach().numpy()

  def print_step(self,message,target,g):
    print ("%s %10.4f"%(message,target),
           "["," ".join(["%10.4f"%a for a in g]),"]")

if __name__=="__main__":
  pass
