from __future__ import division
from dxtbx.model.experiment_list import ExperimentList
from dials.array_family import flex
from simtbx.diffBragg import hopper_utils
from dxtbx.imageset import ImageSetFactory
import os

msg = "Run DiffBragg Stage 1"

class prepare:
  def __init__(self, old_experiments, old_reflections, params, mpi_helper, plot=True):
    self.mpi_helper = mpi_helper
    self.params = params
    from simtbx.diffBragg import mpi_logger as diffBragg_mpi_logger
    diffBragg_mpi_logger.setup_logging_from_params(self.params.diffBragg)
    self.uc = self.params.scaling.unit_cell
    print("There are %d experiments"%(len(old_experiments)))
    import time
    BEG = time.time()
    self.result_expt = ExperimentList()
    self.result_refl = flex.reflection_table()
    # Re-generate the image sets using their format classes so we can read the raw data
    # Integrate the experiments one at a time to not use up memory
    current_imageset = None
    current_imageset_path = None
    for expt_id, expt in enumerate(old_experiments):
      assert len(expt.imageset.paths()) == 1 and len(expt.imageset) == 1
      if expt.imageset.paths()[0] != current_imageset_path:
        current_imageset_path = expt.imageset.paths()[0]
        current_imageset = ImageSetFactory.make_imageset(expt.imageset.paths())
      idx = expt.imageset.indices()[0]
      expt.imageset = current_imageset[idx:idx+1]
      S = expt.imageset.get_spectrum(0)
      refl = old_reflections.select(old_reflections['id']==expt_id)
      oneexpt, onerefl = self.thin_ds1(expt, refl)
      self.result_expt.append(oneexpt)
      self.result_refl.extend(onerefl)
      expt.imageset = None
      print()
    self.result_refl.reset_ids()
    END = time.time()
    print("TIME", END-BEG)

  def thin_ds1(self, expt, refl):
    print("thin ds1 wrapper for expt",expt.identifier)
    expt.crystal.show()
    deviceId = (self.mpi_helper.rank)%int(os.environ.get("CCTBX_GPUS_PER_NODE","1"))
    exp, ref, self.stage1_modeler, x = hopper_utils.refine(expt, refl,
                                       self.params.diffBragg,
                                       gpu_device=deviceId, return_modeler=True)
    print("DONE WITH hopper utils refine")
    (scale, rotX, rotY, rotZ, Na, Nb, Nc,
     diff_gam_a, diff_gam_b, diff_gam_c, diff_sig_a, diff_sig_b, diff_sig_c,
     a,b,c,al,be,ga, detz) = hopper_utils.get_param_from_x(x,self.stage1_modeler.SIM)
    print("DS1 parameters index Na Nb Nc %.3f %.3f %.3f"%(Na,Nb,Nc))
    print(scale, rotX, rotY, rotZ)
    print(a,b,c,al,be,ga, detz)
    #self.stage1_df = save_to_pandas(x, self.stage1_modeler.SIM, orig_exp_name, self.params.diffBragg,
    #                                self.stage1_modeler.E, 0, refls_name, None)
    #exps_out = ExperimentList()
    #exps_out.append(exp)
    return exp, ref
"""
As of Feb. 22, 2022 still:
- fundamentally DS1 refine takes 196 s per image, should only take a few seconds
- refinement of mosaicity doesn't seem to happen
- refined spot positions are way off, 2.6 px radial offset vs. 1.1 px for DIALS.
# questions
# how do I know if refinement is using the GPU?
# how do I force refinement to take my PDB model for reference structure factors?
# how to I restrain to the reference uc covariance matrix or the starting unit cell?
# how do I verify that the diffuse model is turned off?
# how do I verify that the mosaic rotation is being refined?
# how to I force it to use the experimental spectrum and verify it is being used?
# how do I specify number of mosaic UMATS?
# can I verify that the output ref table has the diffBragg model centroid position as xyzcal?

The plan:
basically, hook up diffbragg stage 1
make sure the mask works
figure out how to import in-memory objects, not files
input the new-style structure factors( pavel's latest)
validate on
  a) unit cell parameters, can we put a unit cell analysis in line?
  b) rmsd in line
  c) isotropic mosaic rotation
port ds1 to kokkos
"""
