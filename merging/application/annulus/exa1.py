from __future__ import division
from LS49.adse13_187.adse13_221.basic_runner import basic_run_manager
from LS49.sim.util_fmodel import gen_fmodel
from dxtbx.model.experiment_list import ExperimentList
from dials.array_family import flex
from scitbx.matrix import sqr
from simtbx import get_exascale
from scipy import constants
ENERGY_CONV = 1e10 * constants.c * constants.h / constants.electron_volt

msg = "Run exascale API for SPREAD trial 1"
# Required boilerplate for custom workers
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
# End boilerplate. You may import directly from the directory containing this
# module. It's not necessary to reset the path.
import utils
from dxtbx.imageset import ImageSetFactory

class prepare:
  def __init__(self, old_experiments, old_reflections, params, mpi_helper, mpi_logger, plot=True):
    self.mpi_helper = mpi_helper
    self.mpi_logger = mpi_logger
    self.params = params
    self.uc = self.params.scaling.unit_cell
    self.setup_amplitudes()
    self.result_expt = ExperimentList()
    self.result_refl = flex.reflection_table()
    # Re-generate the image sets using their format classes so we can read the raw data
    # Integrate the experiments one at a time to not use up memory
    current_imageset = None
    current_imageset_path = None
    for expt_id, expt in enumerate(old_experiments):
      assert len(expt.imageset.paths()) == 1 and len(expt.imageset) == 1
      if expt.imageset.paths()[0] != current_imageset_path:
        current_imageset_path = expt.imageset.paths()[0]
        current_imageset = ImageSetFactory.make_imageset(expt.imageset.paths())
      idx = expt.imageset.indices()[0]
      expt.imageset = current_imageset[idx:idx+1]
      S = expt.imageset.get_spectrum(0) # code not yet correct for polychromatic model
      refl = old_reflections.select(old_reflections['id']==expt_id)
      oneexpt, onerefl = self.thin_exa1(expt, refl)
      self.result_expt.append(oneexpt)
      self.result_refl.extend(onerefl)
      expt.imageset = None
    self.result_refl.reset_ids()
    del self.gpu_channels_singleton # confirmed destruction before calling kokkos::finalize()

  def setup_amplitudes(self):
    wavelength_A = 1.74 # general ballpark X-ray wavelength in Angstroms
    wavlen = flex.double([wavelength_A])
    epsilon = 0.05
    direct_algo_res_limit = self.params.statistics.annulus.d_min-epsilon
    GF = gen_fmodel(resolution=direct_algo_res_limit,
       pdb_text=open(self.params.scaling.model,"r").read(),
       algorithm="fft",wavelength=wavelength_A)
    GF.set_k_sol(0.435)
    GF.make_P1_primitive()
    sfall_channels = {}
    for x in range(len(wavlen)):
      sfall_channels[x]=GF.get_amplitudes()
    # there is an MPI-assisted algorithm for polychromatic beam

    gpu_instance = get_exascale("gpu_instance", self.params.exafel.context)
    gpu_energy_channels = get_exascale("gpu_energy_channels", self.params.exafel.context)
    self.gpu_run = gpu_instance( deviceId = int(os.environ.get("CCTBX_RECOMMEND_DEVICE", 0)) )
    self.gpu_channels_singleton = gpu_energy_channels (
         deviceId = int(os.environ.get("CCTBX_RECOMMEND_DEVICE", 0)))
    assert self.gpu_channels_singleton.get_nchannels() == 0 #uninitialized
    for x in range(1): # later, len(flux)): # code not yet correct for polychromatic model
          self.gpu_channels_singleton.structure_factors_to_GPU_direct(
           x, sfall_channels[x].indices(), sfall_channels[x].data())

  def ensure_p1(self, Crystal):
    high_symm_info = Crystal.get_space_group().info()
    self.cb_op = high_symm_info.change_of_basis_op_to_primitive_setting()
    dterm = sqr(self.cb_op.c().r().as_double()).determinant()
    if dterm != 1:
      Crystal = Crystal.change_basis(self.cb_op)
    return Crystal

  def thin_exa1(self, expt, refl):
    self.mpi_logger.log(expt.crystal.as_str())
    M = utils.basic_run_manager.from_mask_file_expt_refl(
              self.params.exafel.trusted_mask, expt, refl)
    M.gpu_channels_singleton = self.gpu_channels_singleton
    M.general_trusted_and_refl_mask(self.params.exafel)
    M.data_structures_for_refls()
    M.get_image_res_data() # get experimental data
    M.spread_modify_shoeboxes() # superimpose dials planar fit over the shoeboxes
    expt.crystal = self.ensure_p1(expt.crystal)
    self.mpi_logger.log(expt.crystal.as_str())
    nanobragg_sim = M.thin_exa_sim(mod_expt = expt) # initial Bragg simulation
    M.view["bragg_plus_background"] = M.reusable_rmsd(
      proposal=nanobragg_sim, label="ersatz_mcmc",plot=self.params.exafel.model.plot,
      legend="Unnormalized simulation: %s "%expt.identifier) # Plot 3
    M.view["Z_plot"] = M.Z_statistics(experimental=M.view["exp_data"],
                                           model=M.view["bragg_plus_background"],
                                           plot=False)
    # key point implemented here is to improve the Rossmann background over
    # DIALS model before proceeding to global minimization
    M.minimize_planes(experiment=M.view["exp_data"])
    M.Z_statistics(experimental=M.view["exp_data"],
                                           model=M.view["bragg_plus_background"],
                                           plot=False)

    if self.params.exafel.debug.lastfiles:
      M.write_hdf5(filenm = "%s.h5"%(expt.identifier))
      M.resultant_mask_to_file("%s.mask"%(expt.identifier))
      utils.write_expt_and_refl_table(expt, refl, exptpath="local22.expt", reflpath="local22.refl")

    #self.stage1_df = save_to_pandas(x, self.stage1_modeler.SIM, orig_exp_name, self.params.diffBragg,
    #                                self.stage1_modeler.E, 0, refls_name, None)
    return expt, M.refl_table
"""
make sure HDF5 output works for this use case DONE
can I actually use the refls file to display shoeboxes on the HDF5? Yes, with some limitations
can I add in a view of shoebox Rossmann backgrounds on top of experimental data DONE
do the red masks work? Yes, DONE
simply create a simulated image, one wavelength, with slim interface DONE
follow: basic_runner:ersatz_MCMC-->case_run:case_job_runner-->cyto_batch:multipanel_sim DONE
new: merge:exa1_prepare-->utils:basic_run_manager-->cyto_batch:multipanel_sim-->nanoBragg:sim_data
can I view the simulated image in HDF5? DONE
confirm using either kokkos::CUDA or Kokkos::OpenMP.  YES, controlled by NERSC_HOST=perlmutter or unknown

if so, then use kokkos::OpenMP in the Azure pipeline (--enable_Kokkos=True)
absolutely make sure I'm using Kokkos and that Kokkos / CUDA are identical.
Refer to Aaron about .h5 + refl display
DAN: follow Derek suggestions to get DS1 to work.  See README.md plus "plan for the paper"
To Derek:For my part, still intend to get some sort of a minimal reproducer to exercise
diffBragg on one of my experiment files.  However it may still take a few days to put this together.

start here Feb 8:
Develop a target function with choice Sa20, Me21, or Sa22
Refine the readout noise hyperparameter and/or gain on bkgrd pixels, allow other algorithms like coset
On the bkgrd pixels, confirm Z is distributed with mean == 0, std dev==1

Feb 11:
Add the spectrum but for now with constant structure factors
  - put the flex int mask creation up front for greater efficiency
  - first try to do it within the existing gpu_channels with a single set of F.
  - accumlator mechanism: I += Gi * i(channel)
  - how about a new mechanism for populating F(energy) on GPU from scattering factors?
  - and also giving the derivatives at the same time?
create an updated interface for the SIM that inputs an initial subset of parameters (just G)
manage choice of which parameters to refine (G)
refine the parameters (just G)--come up with a sensible G initializer and refinery
Then re-evaluate where I am with Z-score, and refine the mosaic rotation please

Figure a way to create wavelength-dependent structure factors on GPU
agree on the fitted parameters: f' f" a b c G uc Rx Ry Rz Ncells_abc eta_abc
Create a skeleton structure that manages parameters
ONe possible avenue to stage 2 is a shared model of unit cell + mosaicity
create an updated interface for the SIM that inputs the fitted parameters
Goal for the end of January:  Stage 1 refinement in exafel/kokkos, with good Z-scores

# why does ds1 take 10-15 minutes, instead of 30 second?
# how do I know if refinement is using the GPU?
# how do I force refinement to take my PDB model for reference structure factors?
# how to I restrain to the reference uc covariance matrix or the starting unit cell?
# how do I verify that the diffuse model is turned off?
# how do I verify that the mosaic rotation is being refined?
# how to I force it to use the experimental spectrum and verify it is being used?
# how do I specify number of mosaic UMATS?
# can I verify that the output ref table has the diffBragg model centroid position as xyzcal?

The plan:
basically, hook up diffbragg stage 1
make sure the mask works
figure out how to import in-memory objects, not files
input the new-style structure factors( pavel's latest)
validate on
  a) unit cell parameters, can we put a unit cell analysis in line?
  b) rmsd in line
  c) isotropic mosaic rotation
port ds1 to kokkos
"""

def basic_runner_run(params):
    basename = "%s_%05d."%(params.output.label, params.output.index)
    M = basic_run_manager.from_files(params.trusted_mask, params.refl, params.expt)
    M.get_trusted_and_refl_mask()
    M.refl_analysis(params.cryst) # new
    M.simple_rmsd(plot=params.model.plot,
                  legend="Basic run stat result: %d "%params.output.index) # new Plot 1
    #M.plot_pixel_histograms() # new
    M.get_lunus_repl()
    M.get_image_res_data()
    M.modify_shoeboxes() # new
    M.view["sim_mock"] = M.simulation_mockup(M.view["exp_data"],plot=params.model.plot,
                  legend="Baseline control: %d "%params.output.index) # new Plot 2
    nanobragg_sim = M.ersatz_MCMC(params.model) # initial Bragg simulation
    M.view["bragg_plus_background"] = M.reusable_rmsd(proposal=nanobragg_sim, label="ersatz_mcmc",plot=params.model.plot,
                  legend="Unnormalized simulation: %d "%params.output.index) # Plot 3
    M.view["renormalize_bragg_plus_background"] = M.reusable_rmsd(proposal=M.renormalize(
            proposal=nanobragg_sim,proposal_label="ersatz_mcmc",ref_label="spots_mockup"),
            label="renormalize_mcmc",plot=params.model.plot,output_refl="%s_%05d."%("renormalize_mcmc", params.output.index),
                  legend="Renormalized simulation: %d "%params.output.index)# Plot 4
    M.view["Z_plot"] = M.Z_statistics(experimental=M.view["sim_mock"],
                                           model=M.view["renormalize_bragg_plus_background"],
                                   plot=False)
    M.write_hdf5(os.path.join(params.output.output_dir,basename+"hdf5"))
    M.resultant_mask_to_file(os.path.join(params.output.output_dir,basename+"mask"))

"""
Works:
dials.image_viewer local22.expt local22.refl # files written with utils, image is *.img.gz

Does not work:
dials.image_viewer 00a62b46-a725-4a7a-a096-370ddb71ffac.h5 local22.refl

Works again when local22.expt image is switched from *.img.gz to *.h5, but only for view#0, upper slices no refl:
dials.image_viewer local22.expt local22.refl

Works when the *.h5 has only one frame:
dials.image_viewer 00a62b46-a725-4a7a-a096-370ddb71ffac.h5 local22.refl
"""
