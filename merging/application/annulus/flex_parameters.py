from __future__ import division

from abc import abstractmethod
from typing import Callable

from scitbx.array_family import flex
from psii_spread.merging.application.annulus.new_global_fdp_refinery import george_sherrell_star

"""
The purpose here is to provide a different parameterization algorithm for the scattering factors.
We begin with the physical or canonical representation:
physical parameters
[ fp(metal 1) fdp(metal 1) fp(metal 2) fdp(metal 2) ... ]
suppose there are 50 energies and 4 metal classes: the physical list therefore contains 400 parameters.
free parameters := refined by LBFGS, not necessarily the same as physical parameters
"""

class FreeParameterFlexProxy(flex.double):
  """Mostly-abstract base class for all FreeParameterFlexProxy-type classes"""
  n_energies: int = NotImplemented
  registry = {}

  @classmethod
  def register(cls, name: str) -> Callable:
    """Class decorator that saves decorated class in `cls.registry[name]`"""
    def decorator(decorated_class):
      cls.registry[name] = decorated_class
      return decorated_class
    return decorator

  @abstractmethod
  def fp_address(self, idx: int, iclass: int) -> int:
    ...

  @abstractmethod
  def fdp_address(self, idx: int, iclass: int) -> int:
    ...

  @abstractmethod
  def free_as_fp(self, iclass: int) -> 'FreeParameterFlexProxy':
    ...

  @abstractmethod
  def free_as_fdp(self, iclass: int) -> 'FreeParameterFlexProxy':
    ...

  @abstractmethod
  def free_as_physical_parameters(self, minimizer) -> list[george_sherrell_star]:
    ...

  @abstractmethod
  def free_as_physical_parameters_list(self, minimizer) -> list['FreeParameterFlexProxy']:
    ...

  def chain_rule(self, physical_parameter_gradient: flex) -> flex:
    return physical_parameter_gradient


@FreeParameterFlexProxy.register('original1')
class original1(FreeParameterFlexProxy):
  """
  We notify the program to use original1 by setting the phil parameter from LS49
  sauter20.LLG_evaluator.restraints.kramers_kronig.algorithm == "original1"

  In this design we input the physical parameters to the constructor which results in a layout of
  the free parameters. After instantiation "self" contains the free parameters.
  """

  def fp_address(self, idx, iclass):
    return idx + (2 * iclass * self.n_energies)

  def fdp_address(self, idx, iclass):
    return idx + ((2 * iclass + 1) * self.n_energies)

  def free_as_fp(self, iclass):
    return self[2 * iclass * self.n_energies : (2 * iclass + 1) * self.n_energies]

  def free_as_fdp(self, iclass):
    return self[(2 * iclass + 1) * self.n_energies : (2 * iclass + 2) * self.n_energies]

  def free_as_physical_parameters(self, minimizer): # treat self as a positional argument
    # minimizer.x is intended to be the current LBFGS (free) parameters
    current_models = []
    for iclass, model in enumerate(minimizer.starting_models):
        fp = self.free_as_fp(iclass)
        fdp = self.free_as_fdp(iclass)
        current_models.append(george_sherrell_star(minimizer.energies, fp, fdp))
    return current_models

  def free_as_physical_parameters_list(self, minimizer): # treat self as a positional argument
    # minimizer.x is intended to be the current LBFGS (free) parameters
    current_models = flex.double()
    for iclass, model in enumerate(minimizer.starting_models):
        fp = self.free_as_fp(iclass)
        fdp = self.free_as_fdp(iclass)
        current_models.extend(fp); current_models.extend(fdp)
    return current_models


@FreeParameterFlexProxy.register('hotpatch2')
class hotpatch2(original1):
  # In this example, the physical parameters will be transformed to the free parameters by a permutation.
  def __init__(self, physical_parameters):
    flex.set_random_seed(123456) # always get the same sequence in all ranks
    # self.perm = flex.random_permutation(len(physical_parameters)) # no reordering (works)
    self.perm = flex.size_t(range(len(physical_parameters))) # random reordering (works)
    # self.perm = flex.size_t(range(len(physical_parameters)-1,-1,-1)) # reverse list reordering (works)
    free = physical_parameters.select(self.perm)
    super(hotpatch2, self).__init__(free)

  def free_as_fp(self, iclass):
    free2phys = self.select(self.perm)
    return free2phys[2 * iclass * self.n_energies : (2 * iclass + 1) * self.n_energies]

  def free_as_fdp(self, iclass):
    free2phys = self.select(self.perm)
    return free2phys[(2 * iclass + 1) * self.n_energies : (2 * iclass + 2) * self.n_energies]

  def chain_rule(self,physical_parameter_gradient):
    return physical_parameter_gradient.select(self.perm)

