from __future__ import division
from dials.array_family import flex
msg = "deliverable1"

def deliverable1(experiments, reflections, params, mpi_helper, mpi_logger, model="xyzcal.px", filter_zero=None):
    uc = params.scaling.unit_cell
    #from uniform_rmsd import UR
    #deliverable 1:  simple signed offset to show if the whole detector is off
    sum_delta_x_px = 0.
    sum_delta_y_px = 0.
    sum_wt = 0.
    #deliverable 2:  resolution bins of total rms offset, radial offset, tangential offset, correlation
    #deliverable 3:  same as d2, but with SPREAD spots instead of all strong spots
    if filter_zero is not None:
      reflections = reflections.select( reflections[filter_zero] > 0.0 )
    for expt_id, expt in enumerate(experiments):
      #reflections are in this experiment
      this_expt_refls = reflections.select(reflections['id']==expt_id)
      #reflections are within the outer reslimit
      dspacings = uc.d(this_expt_refls['miller_index'])
      refl_selection = dspacings > params.statistics.annulus.d_min
      #reflections are strong
      if params.spread_roi.strong:
        I_over_sigma = this_expt_refls['intensity.sum.value.unmodified'] / \
                     flex.sqrt(this_expt_refls['intensity.sum.variance.unmodified'])
        refl_selection &= I_over_sigma > params.spread_roi.strong

      this_expt_refls = this_expt_refls.select(refl_selection)
      dspacings = uc.d(this_expt_refls['miller_index'])
      spread_expt_refls = this_expt_refls.select(dspacings < params.statistics.annulus.d_max)
      if False:
        from matplotlib import pyplot as plt
        A=plt.axes()
        A.set_aspect("equal")
        A.set_xlim(0,expt.detector[0].get_image_size()[0])
        A.set_ylim(0,expt.detector[0].get_image_size()[1])
        plt.plot( [obspx[0] for obspx in spread_expt_refls["xyzobs.px.value"]],
                  [obspx[1] for obspx in spread_expt_refls["xyzobs.px.value"]], "gx" )
        plt.plot( [calpx[0] for calpx in this_expt_refls[model]],
                  [calpx[1] for calpx in this_expt_refls[model]], "b." )
        plt.plot( [obspx[0] for obspx in this_expt_refls["xyzobs.px.value"]],
                  [obspx[1] for obspx in this_expt_refls["xyzobs.px.value"]], "r." )
        plt.show()
      #Deliverable #1
      pcal = this_expt_refls[model].parts()
      pexp = this_expt_refls["xyzobs.px.value"].parts()
      for irefl in range(len(this_expt_refls)):
        sum_delta_x_px +=(pcal[0][irefl]
                        - pexp[0][irefl])
        sum_delta_y_px +=(pcal[1][irefl]
                        - pexp[1][irefl])
        sum_wt += 1.
    sum_delta_x_px = mpi_helper.comm.reduce(sum_delta_x_px, root=0, op=mpi_helper.MPI.SUM)
    sum_delta_y_px = mpi_helper.comm.reduce(sum_delta_y_px, root=0, op=mpi_helper.MPI.SUM)
    sum_wt = mpi_helper.comm.reduce(sum_wt, root=0, op=mpi_helper.MPI.SUM)
    if mpi_helper.rank==0:
      # filter statement
      if params.spread_roi.strong is None:
        filter_statement = "cccFinal analysis of signed offset from %d strong spots out to %6.4f:"%(
                           sum_wt, params.statistics.annulus.d_min)
      else:
        filter_statement = "cccFinal analysis of signed offset from %d strong spots (I/σ > %3.1f) out to %6.4f:"%(
                           sum_wt, params.spread_roi.strong, params.statistics.annulus.d_min)

      print(filter_statement)
      print("Delta XY in pixels: (%5.2f, %5.2f)"%(sum_delta_x_px/sum_wt,
                                                sum_delta_y_px/sum_wt))

      mpi_logger.main_log(
        filter_statement + "\n" +
        "Delta XY in pixels: (%5.2f, %5.2f)"%(sum_delta_x_px/sum_wt,
                                                sum_delta_y_px/sum_wt)
      )
