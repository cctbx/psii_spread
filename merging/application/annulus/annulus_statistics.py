from __future__ import division
from statistics import mean
from xfel.merging.application.worker import worker
from dials.array_family import flex


class annulus_statistics(worker):

  def __init__(self, params, mpi_helper=None, mpi_logger=None):
    super(annulus_statistics, self).__init__(params=params, mpi_helper=mpi_helper, mpi_logger=mpi_logger)

  def __repr__(self):
    return "Calculate reflection statistics in a resolution annulus"

  def run(self, experiments, reflections):
    uc = self.params.scaling.unit_cell

    all_refl_in_annulus = flex.reflection_table()
    d_min = self.params.statistics.annulus.d_min
    d_max = self.params.statistics.annulus.d_max
    for expt_id, expt in enumerate(experiments):
      self.logger.log('Annulus statistics: ' + expt.identifier)
      refl = reflections.select(reflections['id']==expt_id)
      dspacings = uc.d(refl['miller_index'])
      gt = dspacings > d_min
      lt = dspacings < d_max
      refl = refl.select(gt & lt)
      all_refl_in_annulus.extend(refl)
      iset = expt.imageset
      self.logger.log(f'{iset.paths()[0]}:{iset.indices()[0]} : '
                      f'{refl.size()} spots in annulus')

    miller_counts = self.mpi_helper.count(all_refl_in_annulus['miller_index'])
    if self.mpi_helper.rank == 0:
      print(f'total shoeboxes: {sum(miller_counts.values())}')
      print(f'unique miller indices: {len(miller_counts)}')
      print(f'average multiplicity: {mean(miller_counts.values()):.3f}')

    return experiments, reflections
