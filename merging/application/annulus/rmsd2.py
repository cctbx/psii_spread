from __future__ import division
from pandas import DataFrame as df
import numpy as np
import math
from dials.array_family import flex
from scitbx.matrix import col
msg = "deliverable2"

class deliverable2:
  def __init__(self,experiments, old_reflections, n_bins, d_max, d_min, params, mpi_helper, mpi_logger, header=True,
               model="xyzcal.px", filter_zero=None):
    self.mpi_helper = mpi_helper
    self.mpi_logger = mpi_logger
    self.params = params
    self.uc = self.params.scaling.unit_cell
    reflections = old_reflections.copy() # will apply local filters, so don't touch original data
    if filter_zero is not None:
      reflections = reflections.select( reflections[filter_zero] > 0.0 ) # get rid of shoeboxes where model==0
    if params.spread_roi.strong:
      I_over_sigma = reflections['intensity.sum.value.unmodified'] / \
                     flex.sqrt(reflections['intensity.sum.variance.unmodified'])
      sele = I_over_sigma > params.spread_roi.strong
      reflections = reflections.select(sele)

    from cctbx.crystal import symmetry
    S = symmetry(unit_cell=self.params.scaling.unit_cell,
                 space_group_info=self.params.scaling.space_group)

    H = ref_no_merge_anom = S.build_miller_set(anomalous_flag=True,
      d_min=self.params.statistics.annulus.d_min, d_max=self.params.statistics.annulus.d_max)

    J = H.setup_binner(d_max = d_max or 0, d_min = d_min, n_bins = n_bins)

    refl_bins = flex.int([J.get_i_bin(MI) for MI in reflections["miller_index"]])
    #deliverable 2:  resolution bins of total rms offset, radial offset, tangential offset, correlation
    reflections["offset vector"] = reflections[model]-reflections["xyzobs.px.value"]
    # the z component needs to be zeroed out because it's treated inconsistently between dials & merge
    parts = reflections["offset vector"].parts()
    reflections["offset vector"] = flex.vec2_double([(parts[0][idx],parts[1][idx]) for idx in range(len(reflections))
    ])

    reflections["squared diff"] = reflections["offset vector"].dot(reflections["offset vector"])

    # geometry
    E0 = experiments[0]
    B = E0.beam
    D = E0.detector
    s0_vector = -col(B.get_sample_to_source_direction())
    """
    S1_vector
    better to just project these two vectors on to the detector, then convert mm to pixels, then take the
    vector diff in pixels to get radial direction.  Then rotate 90 degrees on the normal to get tangential.
    """
    s0_on_panel_px = flex.vec2_double([D[ipn].get_ray_intersection_px(s0_vector)
                     for ipn in reflections["panel"]])
    s1_on_panel_px = flex.vec2_double(
                     [D[reflections["panel"][idx]].get_ray_intersection_px(reflections["s1"][idx])
                     for idx in range(len(reflections))])
    radial_direction_unit = (s1_on_panel_px - s0_on_panel_px).each_normalize()
    tangential_direction_unit = radial_direction_unit.rotate_around_origin(math.pi/2.)

    radial_offset = (reflections["offset vector"].dot(radial_direction_unit)) # scalar offset length, not vector
    tangential_offset = reflections["offset vector"].dot(tangential_direction_unit) # scalar offset length
    radial_sq_diff = radial_offset * radial_offset
    tangential_sq_diff = tangential_offset * tangential_offset

    # end of distributed calculations.  Now gather everything
    refl_bins = mpi_helper.aggregate_flex(refl_bins, flex.int)
    radial_offset = mpi_helper.aggregate_flex(radial_offset, flex.double)
    tangential_offset = mpi_helper.aggregate_flex(tangential_offset, flex.double)
    delpsical_rad = mpi_helper.aggregate_flex(reflections['delpsical.rad'], flex.double)
    sq_diff = mpi_helper.aggregate_flex(reflections["squared diff"], flex.double)
    radial_sq_diff = mpi_helper.aggregate_flex(radial_sq_diff, flex.double)
    tangential_sq_diff = mpi_helper.aggregate_flex(tangential_sq_diff, flex.double)
    have_Z = "mean_Z" in reflections and "sigma_Z" in reflections
    if have_Z:
      mean_Z = mpi_helper.aggregate_flex(reflections["mean_Z"], flex.double)
      sigma_Z = mpi_helper.aggregate_flex(reflections["sigma_Z"], flex.double)

    # now display with pandas
    self.TF = None
    self.DF = None
    if mpi_helper.rank == 0:
      refl_cnt = [refl_bins.count(i) for i in J.range_used()]
      bin_rang = [J.bin_legend(i, show_bin_number=False, show_counts=False) for i in J.range_used()]
      cnt_comp = [J.counts_complete()[i] for i in J.range_used()]

      correls = {"Correl ΔR,ΔΨ":[],"Correl ΔT,ΔΨ":[]}
      from xfel.metrology.panel_fitting import three_feature_fit
      for i in J.range_used():
        if (refl_bins==i).count(True)<3:
          correls["Correl ΔR,ΔΨ"].append(None); correls["Correl ΔT,ΔΨ"].append(None); continue
        bin_radial_offset = radial_offset.select(refl_bins==i)
        bin_tangential_offset = tangential_offset.select(refl_bins==i)
        bin_delpsi_deg = (delpsical_rad.select(refl_bins==i))*(180/math.pi)
        self.TF = three_feature_fit(delta_radial = bin_radial_offset,
                             delta_transverse = bin_tangential_offset,
                             delta_psi = bin_delpsi_deg, i_panel=None, verbose=False)
        correls["Correl ΔR,ΔΨ"].append(100.*self.TF.cross_correl[2])
        correls["Correl ΔT,ΔΨ"].append(100.*self.TF.cross_correl[1])

      keyval = [("Resolution range", bin_rang),("N possible", cnt_comp), ("refl_cnt", refl_cnt),
                ("Correl ΔR,ΔΨ",correls["Correl ΔR,ΔΨ"]),
                ("Correl ΔT,ΔΨ",correls["Correl ΔT,ΔΨ"])
               ]

      raw = dict(keyval)
      DF = df(raw)
       # binwise calculations in pandas
      DF["sum_sq_diff"] = [ flex.sum( sq_diff.select(refl_bins == i) ) for i in J.range_used()]
      DF["rmsd"] = np.sqrt(DF["sum_sq_diff"]/DF["refl_cnt"])
      DF["sum_radial_sq_diff"] = [ flex.sum( radial_sq_diff.select(refl_bins==i) ) for i in J.range_used()]
      DF["sum_tangential_sq_diff"] = [ flex.sum( tangential_sq_diff.select(refl_bins==i) ) for i in J.range_used()]
      DF["rms_radial_offset"] = np.sqrt( DF["sum_radial_sq_diff"]/DF["refl_cnt"] )
      DF["rms_tangential_offset"] = np.sqrt( DF["sum_tangential_sq_diff"]/DF["refl_cnt"] )
      listed_columns = ["Resolution range","N possible","refl_cnt","rmsd","rms_radial_offset","rms_tangential_offset",
                  "Correl ΔR,ΔΨ","Correl ΔT,ΔΨ"
         ]
      if have_Z:
        DF["<μZ>"] = [ flex.mean( mean_Z.select(refl_bins == i) ) for i in J.range_used()]
        DF["<σZ>"] = [ flex.mean( sigma_Z.select(refl_bins == i) ) for i in J.range_used()]
        listed_columns += ["<μZ>","<σZ>"]
      if header: listed_headers = listed_columns
      else: listed_headers = [" "*len(item) for item in listed_columns]
      print(DF.to_string(index=False, justify="center",
         formatters={"rmsd":"{:,.2f}px".format,"rms_radial_offset":"{:,.2f}px".format,"rms_tangential_offset":"{:,.2f}px".format,
                     "Correl ΔR,ΔΨ":"{:,.1f}%".format,"Correl ΔT,ΔΨ":"{:,.1f}%".format,
                     "<μZ>":"{:,.4f}".format,"<σZ>":"{:,.4f}".format},
         header=listed_headers,
         columns=listed_columns,
      ))
      self.mpi_logger.main_log(
        DF.to_string(index=False, justify="center",
         formatters={"rmsd":"{:,.2f}px".format,"rms_radial_offset":"{:,.2f}px".format,"rms_tangential_offset":"{:,.2f}px".format,
                     "Correl ΔR,ΔΨ":"{:,.1f}%".format,"Correl ΔT,ΔΨ":"{:,.1f}%".format,
                     "<μZ>":"{:,.4f}".format,"<σZ>":"{:,.4f}".format},
         header=listed_headers,
         columns=listed_columns,
        )
      )
      self.DF = DF # propagate results to caller

    self.DF, self.TF = mpi_helper.comm.bcast( (self.DF, self.TF), root=0 )

sample_output="""
final lattices: 96
total shoeboxes: 1495
total shoebox pixels: 3655534

Not merging Friedel mates:
  1350 unique indices of 8234 theoretical, 16.4% complete with multiplicity   0.2
Merging Friedel mates:
  1212 unique indices of 4195 theoretical, 28.9% complete with multiplicity   0.4

Global total of refls 7253, of which the SPREAD ROI contains 1495
cccFinal analysis of signed offset from 5455 strong spots:
Delta XY in pixels: ( 0.03,  0.02)
Resolution range  N possible  refl_cnt  rmsd  rms_radial_offset rms_tangential_offset Correl ΔR,ΔΨ Correl ΔT,ΔΨ
2.5000 - 2.4452      826        164    1.49px       1.46px              0.29px           -78.8%       -1.5%
2.4452 - 2.3950      836        156    1.44px       1.41px              0.30px           -81.9%       -6.6%
2.3950 - 2.3486      814        179    1.50px       1.47px              0.31px           -76.2%        2.8%
2.3486 - 2.3056      838        172    1.62px       1.59px              0.31px           -75.6%       -1.9%
2.3056 - 2.2656      806        155    1.50px       1.47px              0.31px           -74.4%       14.1%
2.2656 - 2.2282      808        146    1.54px       1.50px              0.31px           -79.2%       -7.1%
2.2282 - 2.1932      845        137    1.74px       1.71px              0.29px           -76.7%       -6.9%
2.1932 - 2.1603      797        129    1.75px       1.72px              0.32px           -71.1%       -8.4%
2.1603 - 2.1293      823        135    1.78px       1.74px              0.38px           -74.2%        1.9%
2.1293 - 2.1000      841        133    1.65px       1.62px              0.34px           -78.6%        4.3%
"""
