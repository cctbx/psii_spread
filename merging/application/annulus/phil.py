from __future__ import division

phil_str = """
statistics {
  annulus {
    d_min = 2.1
      .type = float
    d_max = 2.5
      .type = float
  }
}
spread_roi {
  enable = False
    .type = bool
    .help = activate alternate analysis for use by LY99 SPREAD.
    .help = requires input.persistent_refl_cols=shoebox
  plot = False
    .type = bool
    .help = for debug, plot I over sigma histogram after gathering by rank 0
  strong = 0.5
    .type = float(value_min=0)
    .help = if defined, SPREAD roi will only include integrated spots with higher I/sigma
    .help = Can be set to None to disable this filter
  min_spots = 5
    .type = int(value_min=3)
    .help = for SPREAD, only accept a lattice if the number of ROI spots exceeds this floor
}
sauter20 {
  include scope LS49.ML_push.phil.phil_scope
}
exafel
  .help = If included .model.Nabc.value or .mosaic_spread.value are libtbx.Auto,
  .help = during spread refinement source them from individual expts.
 {

  include scope LS49.adse13_187.adse13_221.mcmc_runner.phil_scope

  context = *kokkos_gpu kokkos_cpu cuda
    .type = choice
    .help = backend for parallel execution

  debug {
    lastfiles = False
      .type = bool
      .help = write within-worker files for dials.image_viewer/dials.show
    verbose = False
      .type = bool
      .help = stdout within-worker
    finite_diff = -1
      .type = int
      .help = iteration number, if >0 pause after this LBFGS iteration and confirm derivatives by finite difference analysis
    eps = 1.e-8
      .type = float
      .help = epsilon, the fractional-difference target for finite-difference testing of the derivatives
    format_offset = 0
      .type = float
      .help = Use 0 for PAD detector, 10 for SMV-format simulation.  Apparently, get_raw_data needs to
      .help = be fooled by offsetting our special data for the Z-view.
    energy_offset_eV = 0.
      .type = float
      .help = Add energy offset in eV to stated energy scale from the shot-to-shot spectrometer
    energy_stride_eV = 0.5
      .type = float
      .help = Desired spacing between energy channels used for the forward simulation, in eV
    energy_plot = False
      .type = bool
    write_mtz = False
      .type = bool
      .help = Choice to write out the structure factors for monochromatic work (exa3A) or polychromatic (sw1).
  }

  diffbragg_pickles_glob = None
    .type = str
    .help = If given, instead of using crystal detail (mosaic pars, A matrices)
    .help = from expts or phil scope, collect them from the diffBragg results
    .help = stored in this pickled pandas dataframe instead.

  trusted_pixels = *all used used_trimmed
    .type = choice
    .help = In addition to obeying exafel.trusted_mask, trust only certain pixels.
    .help = Choice all (default) trusts all pixels accepted by pixel mask.
    .help = Choice used reject pixels other than Foreground or BackgroundUsed on any frame.
    .help = Choice used_trimmed trusts used pixels with at least 3 used neighbors.

  scenario = 0 00 1 *2 3 3A S1 ds1 4
    .type = choice
    .help = Debugging products for development purpose.
    .help = Choice 0 accesses spectra and raw data, then exits.
    .help = Choice 00 accesses spectra and raw data in the downstream worker only, then exits.
    .help = Choice 1 calculates SPREAD-annulus statistics and exits.
    .help = Choice 2 performs a per-spot Rossmann plane optimization on the shoebox border.
    .help = Choice 3 performs the plane optimizations in parallel within each image.
    .help = Choice 3A combines optimal planes with polychromatic forward model.
    .help = Choice S1 trial spread refinement
    .help = Choice ds1 initial trial of diffBragg stage 1
    .help = Choice 4 implements the global scope

  skin = False
    .type = bool
    .help = whether or not to use add_diffBragg_spots for simulation + derivatives
    .help = False use nanoBragg sim only, with G as the only refinable parameter
  hotstart = False
    .type = bool
    .help = Ask for a second round of parameter refinement with hot-initialization
    .help = May not help: hot initialization of G in second round may lead to meaningless divergent G, if the rot angles
    .help = have already diverged.  Let's try to focus on rot angles.  Later we can think about having a second round.
  refpar
    .help = Refinable parameters for exafel model optimization
    {
    label = background nonsense G rot
      .type = choice(multi=True)
    background {
        fixed = False
          .type = bool
        algorithm = rossmann_2d_linear
          .type = choice
        scope = *spot lattice global
          .type = choice
        slice_init = *all border
          .type = choice
        slice = *all border
          .type = choice
    }
    G {
        fixed = False
          .type = bool
        scope = spot *lattice global
          .type = choice
        reparameterize = bound *free
          .type = choice
          .help = The scale factor can be free, taking any value, or parameterized by ln(G)
          .help = to give a lower bound of zero.
    }
    rot {
        fixed = False
          .type = bool
        scope = spot *lattice global
          .type = choice
        restraint = None
          .type = float
          .help = impose a likelihood prior on the rotation, restraining each rotation x,y,z to a prior standard deviation
          .help = 1.0-degree is a very light restraint, 0.1 degree is a conformant restraint, 0.05 degree is stiff
    }
    nonsense {
        fixed = False
          .type = bool
        algorithm = rossmann_2d_linear
          .type = choice
        scope = *spot lattice global
          .type = choice
        slice_init = *all border
          .type = choice
        slice = *all border
          .type = choice
    }
    }
  static_fcalcs
    .help = Energy-dependent Fcalcs take long enough to calculate that it is convenient to park them in a file
    {
    path = None
      .type = path
      .help = Very large flex complex double with Fmodel at every energy
    whole_path = None
      .type = path
      .help = The whole-structure Miller array, but only calculated on the lower energy bound
    action = *write read
      .type = choice
      .help = write, calculate them once, then write to file in rank 0
      .help = read, access from file in rank 0, then broadcast to all ranks
    }
  metal = *MMO2 MMO1 PSII PSII4
    .type = choice
    .help = use case for SPREAD metal.  MMO2 is two irons in MMO labeled as 601 and 602
    .help =                             MMO1 is both irons in MMO treated as spectroscopically identical labeled FE
    .help =                             PSII is 8 Mn atoms in PSII treated as spectroscopically identical
    .help =                             PSII4 is 8 Mn atoms in PSII treated as 4 spectroscopic classes
}
"""
