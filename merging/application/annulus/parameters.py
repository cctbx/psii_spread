from __future__ import division
from dials.array_family import flex

class background:
  def __init__(self, shoebox, expt):
    self.peakslow = int(shoebox.centroid_all().px.position[1]) # approx slow-position of peak
    self.peakfast = int(shoebox.centroid_all().px.position[0]) # approx fast-position of peak
    self.n = 3
    self.gain = expt.detector[0].get_gain()
    self.SP = flex.double()
    self.FP = flex.double()
    self.KI = flex.double()
    self.B = 0.; self.C = 0.;
  def set_data(self, raw_pixels, index, opt_iterator, pub_iterator, model):
    self.raw_pixels = raw_pixels
    self.index = index
    self.opt_iterator = opt_iterator
    self.pub_iterator = pub_iterator
    self.model = model # destination for refined model
    for ipanel, islow, ifast in self.opt_iterator(self.index):
      value = self.raw_pixels[ipanel][islow,ifast]
      self.addpixel(islow,ifast,value)
  def addpixel(self, slow, fast, value):
    self.SP.append(slow-self.peakslow)
    self.FP.append(fast-self.peakfast)
    self.KI.append(value/self.gain) # KI is experimental in units of photons
  def cold_initialize(self):
    self.A = self.gain * flex.mean(self.KI) # initial fit is to set the average, in units of ADU
    return self.A, self.B, self.C
  def take_result(self, n_values):
    assert len(n_values)==self.n
    self.A,self.B,self.C, = tuple(n_values)
  def model_T(self,slow,fast): # units of ADU
    sprime = slow-self.peakslow; fprime = fast-self.peakfast
    return self.A + sprime*self.B + fprime*self.C
  def publish_model(self): # publish for downstream use in units of ADU
    for ipanel, islow, ifast in self.pub_iterator(self.index):
      self.model[ipanel,islow,ifast] = self.model_T(islow,ifast)
  def compute_functional_and_gradients(self):
    f = 0.;
    g = flex.double(self.n)
    vector_T = flex.double(len(self.SP),self.A) + self.SP*self.B + self.FP*self.C
    vector_lambda = vector_T/self.gain # lambda is in units of photons
    masked_pixels = vector_lambda > 0.
    if (masked_pixels).count(False)>0:
      print ("Warning: ignoring %d pixels having log(value<=0)"%(masked_pixels.count(False)))
      vector_lambda = vector_lambda.select(masked_pixels)
      working_KI = self.KI.select(masked_pixels)
      working_SP = self.SP.select(masked_pixels)
      working_FP = self.FP.select(masked_pixels)
    else:
      working_KI = self.KI; working_SP = self.SP; working_FP = self.FP

    f = flex.sum(vector_lambda - (working_KI * flex.log(vector_lambda)))
    inner_paren = flex.double(len(working_KI),1.) - (working_KI/vector_lambda)
    g_list = [flex.sum( deriv * inner_paren ) for deriv in
               [flex.double(len(working_KI),1.), working_SP, working_FP]]
    if self.index==0: self.print_step("LBFGS stp",f)
    g = flex.double(g_list)
    return f,g
  def print_step(self,message,target):
    print ("%s %10.4f"%(message,target),
           "["," ".join(["%10.4f"%a for a in [self.A, self.B, self.C]]),"]")
