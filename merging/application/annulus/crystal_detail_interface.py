"""
In most cases identifying the source of psii_spread input is straightforward.
Details such as resolution limits or convergence criteria are common for
all experiments and thus supplied via phil parameter.
Meanwhile, details such as crystal orientation or shoeboxes lists are
unique for every expt/refl file and, thus, are passed as expt/refl pairs.

While the distinction between phil- and expt-based details is simple on paper,
there are some details that might come from either of phil or expt depending
on the approach taken; it might be even preferable to source some parameters
from external files other than expt or phil altogether.

This module interfaces exactly those cases where the origin of crystal detail
might come from multiple different sources, even external ones. At the time
of writing, it handles the following data sources and crystal details:

- Handled data sources:
  - expt files (default for variable details)
  - pickled diffBragg DataFrame (alternative for variable details)
  - phil (common details)
- Handled crystal details:
  - crystal orientation, so-called "A_matrix"
  - mosaic domain size, so-called "N_cells" or "N_abc"
  - mosaic domain spread, so-called "eta" or "η"

The source of handled details can be modified via phil parameters as follows:
- By assigning numerical values to `params.exafel.model.mosaic_spread.value`
  and `params.exafel.model.Nabc.value` (the default approach), the mosaicity
  of every crystal will be set identical and equal to this value(s).
- By setting either of the phil parameters mentioned above to `Auto`,
  instead of using a constant common to all experiments, the value will be set
  individually for each experiment based on the "source". Since the default
  source of detail is an expt file, the mosaicity of each experiment will be
  unique and based on the attributes of the respective `expt.crystal` object.
- By setting the path of `params.exafel.diffbragg_pickles_glob`,
  the default source of crystal detail will be switched from an `expt` file
  to the pickled diffBragg DataFrame. In the particular case of this source,
  since `diffBragg` calculates crystal orientation as well, expt orientations
  will be replaced with respective diffBragg `Amats` as well.

Example:
The phil scope was set to the following:
```
exafel.diffbragg_pickles_glob=/global/cfs/cdirs/m3562/der/psii/knight.*/pandas/*.pkl
exafel.model.mosaic_spread.value=Auto
exafel.model.Nabc.value=16,16,16
```
Here, the value of "N_cells" has been fixed explicitly and thus will be
initialized as [16,16,16] for all experiments. The value of "eta" was set
to Auto and thus will be set individually for each experiment.
As `exafel.diffbragg_pickles_glob` was used to specify this detail source,
individual values of "eta" will be taken from these diffBragg DataFrames.
Since in this way the source of data has been set to diffBragg,
experimental orientations will be replaced with diffBragg "Amats" as well.
"""


from collections import UserDict
from enum import Enum
from itertools import combinations
from pathlib import Path
import pickle as pkl
from glob import iglob
from typing import Any, Callable, Iterable, NamedTuple

import numpy as np
import pandas as pd

from libtbx import Auto
from libtbx.mpi4py import MPI
from xfel.util.preference import flatten


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ UTILITY FUNCTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


def geometric_mean(iterable: Iterable) -> float:
  """Return a geometric mean (n-th root of n-values product) of an iterable"""
  return (a := np.array(iterable)).prod() ** (1.0 / len(a))


DiffBraggDataFrame = pd.DataFrame
"""
This is a type alias for a `pd.DataFrame` containing results of diffBragg
and typically stored in a pickle. This dataframe is by default indexed using
a meaningless index and contains `pd.Series` of the following name and type:
- Amats: tuple[float, float, float, float, float, float, float, float, float]
- eta_abc: tuple[float, float, float] (in degrees)
- identifier: str
- ncells: tuple[float, float, float]
"""


def collect_diffbragg_dataframe(pickle_glob: str) -> DiffBraggDataFrame:
  """Collect the full results pd.DataFrame from scattered diffBragg pickles"""
  comm = MPI.COMM_WORLD
  pickle_paths = [Path(p) for p in iglob(pickle_glob)]
  df: DiffBraggDataFrame
  dfs: list[DiffBraggDataFrame] = []
  for pickle_path in pickle_paths[comm.rank::comm.size]:
    with open(pickle_path, 'rb') as pickle_file:
      dfs.append(pkl.load(pickle_file))
  df = pd.concat(dfs, axis=0, ignore_index=True) if dfs else None
  dfs = [d for d in comm.allgather(df) if d is not None]
  if not dfs:
    raise ValueError(f'Could not find any diffBragg results at {pickle_glob=}')
  df = pd.concat(dfs, axis=0, ignore_index=True)
  df.set_index('identifier', inplace=True)
  return df


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ SINGLETON METACLASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


class Singleton(type):
  """Singleton type initialized only once; return instance at each new call"""
  _instances: dict[type: Any] = {}

  def __call__(cls, *args, **kwargs) -> Any:
    if cls not in cls._instances:
      cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
    return cls._instances[cls]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REGISTRY CLASSES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


class FallbackSubkeyDict(UserDict[tuple, Any]):
  """
  Dict indexed using a composite tuple key that looks for a partially matching
  key (with `None`s marking "fallback" values) if an exact match is missing:
  For example, if d = DefaultSubkeyDict({(1, None): 2, (1, 3): 3}),
  then d[1, 2] == 2, d[1, 3] == 3, but d[0, 3] & d[None, None] will raise.
  """
  def __getitem__(self, key: tuple):
    for fallbacks_count in range(len(key) + 1):
      for fallbacks_indices in combinations(range(len(key)), fallbacks_count):
        try:
          k = [None if i in fallbacks_indices else k for i, k in enumerate(key)]
          return super().__getitem__(tuple(k))
        except KeyError:
          continue
    raise KeyError(f'Could find even a partial fallback match to {key}')


class CrystalDetailCollectors(FallbackSubkeyDict[tuple, Callable]):
  """Registry of `CrystalDetailSingleton`'s private "_collect..." methods"""
  def collects(self,
               source: 'CrystalDetailSingleton.Source' = None,
               column: 'CrystalDetailSingleton.Column' = None):
    def collects_decorator(collect_method) -> Callable:
      """
      A decorator that registers methods under a tuple `(source, column)` key.
      Each decorated method must accept `self`, full `params`, `expts`.
      Each decorated method must return `Column`s of `CrystalDetailDataFrame`.
      """
      self[source, column] = collect_method
      return collect_method
    return collects_decorator


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CRYSTAL DETAIL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


CrystalDetailDataFrame = pd.DataFrame
"""
This is a type alias for a `pd.DataFrame` containing various details
of the crystal structure. This dataframe is by default indexed using
`expt.identifiers` and contains `pd.Series` of the name and type
as given in the definition of `CrystalDetail` below.
"""


class CrystalDetailSingleton(metaclass=Singleton):
  """Singleton that auto-collects details from source if phil param. is Auto"""

  class CrystalDetail(NamedTuple):
    """Protocol for type-hinting, the actual class is created by pandas below"""
    Amats: tuple[float, float, float, float, float, float, float, float, float]
    eta: float  # in degrees
    n_cells: tuple[float, float, float]

  class CrystalDetailMissing(KeyError):
    pass

  class Column(Enum):
    """`CrystalDetailDataFrame` columns to be collected from source or phil"""
    Amats = 'Amats'
    eta = 'eta'
    ncells = 'ncells'

  class Source(Enum):
    """Accepted data sources indicating where to pull the `Column` data from"""
    expt = 'expt'
    diffbragg = 'diffbragg'

    @classmethod
    def from_params(cls, params) -> 'CrystalDetailSingleton.Source':
      if params.exafel.diffbragg_pickles_glob is not None:
        return cls.diffbragg
      else:  # if Auto in params but diffBragg pickle glob undefined
        return cls.expt

  collectors = CrystalDetailCollectors()
  """Registry of the methods to be called to collect `Column` from `Source`"""

  def __init__(self, params=None, expts=None) -> None:
    self.source = self.Source.from_params(params)
    self.df = self.collect_df(params, expts)

  def collect_df(self, params, expts=None) -> CrystalDetailDataFrame:
    """Auto-collect `self.df` using methods registered in `self.collectors`"""
    collectors = {self.collectors[self.source, col] for col in self.Column}
    auto_columns = flatten([coll(self, params, expts) for coll in collectors])
    df = pd.concat(auto_columns, axis=1)
    if non_auto_columns := self._collect_non_auto_from_params(params, expts):
      df.update(pd.concat(non_auto_columns, axis=1))
    if missing_cols := (set(col.value for col in self.Column) - set(df.columns)):
      raise self.CrystalDetailMissing(f'{self} missing columns {missing_cols}')
    return df

  @collectors.collects(source=Source.diffbragg)
  def _collect_all_from_diffbragg(self, params, expts) -> list[pd.Series]:
    db_df: DiffBraggDataFrame
    db_df = collect_diffbragg_dataframe(params.exafel.diffbragg_pickles_glob)
    db_df = db_df[db_df.index.isin([expt.identifier for expt in expts])]
    a_mats = db_df['Amats'].copy()
    eta = db_df['eta_abc'].copy().apply(geometric_mean).rename('eta')
    n_cells = db_df['ncells'].copy()
    del db_df
    return [a_mats, eta, n_cells]

  @collectors.collects(source=Source.expt, column=Column.Amats)
  def _collect_a_matrices_from_expts(self, _, expts) -> list[pd.Series]:
    a_mats = {e.identifier: e.crystal.get_A() for e in expts}
    return [pd.Series(a_mats, name='Amats')]

  @collectors.collects(source=Source.expt)
  def _collect_mosaicity_from_expts(self, _, expts) -> list[pd.Series]:
    eta, n_cells = {}, {}
    for expt in expts:
      abc = np.array(expt.crystal.get_unit_cell().parameters()[:3])
      eta[expt.identifier] = 2 * expt.crystal.get_half_mosaicity_deg()
      n_cells[expt.identifier] = tuple(expt.crystal.get_domain_size_ang() / abc)
    return [pd.Series(eta, name='eta'), pd.Series(n_cells, name='ncells')]

  def _collect_non_auto_from_params(self, params, expts) -> list[pd.Series]:
    non_auto_columns = []
    if (eta_value := params.exafel.model.mosaic_spread.value) is not Auto:
      eta = {expt.identifier: eta_value for expt in expts}
      non_auto_columns.append(pd.Series(eta, name='eta'))
    if (n_cells_value := params.exafel.model.Nabc.value) is not Auto:
      n_cells = {expt.identifier: tuple(n_cells_value) for expt in expts}
      non_auto_columns.append(pd.Series(n_cells, name='ncells'))
    return non_auto_columns

  def get_detail(self, expt_id: str) -> CrystalDetail:
    """Return a row of `self.df` as 'CrystalDetail` tuple indexed w/`Column`"""
    return next(self.df.loc[[expt_id]].itertuples(name='CrystalDetail'))
