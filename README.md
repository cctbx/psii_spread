# psii_spread

Spread analysis

## Configuration

The workers in `merging/application` can be linked in `~/.cctbx.xfel` and used for `cctbx.xfel.merge` jobs. At present (9/15/2021) `cctbx_project` must be on branch `custom_worker` but this will be merged into cctbx master shortly.

To use the custom workers, follow these steps:
```
$ cd $MODULES
$ git clone https://gitlab.com/cctbx/psii_spread.git
$ mkdir ~/.cctbx.xfel
$ ln -s $PWD/psii_spread/merging ~/.cctbx.xfel/
```

## Example processing pipeline

### Indexing/integration

A script is provided to dispatch multiple stills_process jobs in a single
Slurm interactive (`salloc`) allocation. The input is a todo file containing
pairs of image and phil paths.

```
$ salloc --nodes=64  --constraint=haswell --time=4:00:00 -A m3562 -q interactive --bb="capacity=500G access=striped type=scratch"
$ for f in $CFS/m3562/dwpaley/master_files/run_*.h5; do \
    echo "$f $CFS/m3562/dwpaley/phil_files/index.phil" >> todo.txt \
  done
$ source /global/common/software/m3562/dials/build/conda_setpaths.sh
$ module load parallel
$ psii_spread.slurm_multirun nodes_per_run=7 todo_file=todo.txt output.output_dir=001
```
The full 185 runs should take about 2-3 hours. At present this script returns a
couple minutes early and we have to monitor the remaining file transfers:
```
while true; do if [[ ! $(squeue -u$(whoami) -s | grep -l -m1 tar_) ]]; then echo ok; sleep 10; fi; done
```

Then we do reindexing, unit cell filtering, "Group A" analysis, and three-
component (radial, transverse, delta-psi) reflection filtering:
```
$ export CL=/global/common/software/m3562/dials/modules/psii_spread/command_line
$ $CL/reindex.sh 001
$ $CL/ucfilter.sh 001 /global/common/software/m3562/util/covariance_0_cells.pickle
$ $CL/move_unmatched.sh 001 
$ $CL/groupA.sh 001
$ $CL/jfmetrics.sh 001
```
A convenience script is provided to setup and run a `cctbx.xfel.merge` job for
statistics in a resolution annulus:
```
$ $CL/merge.sh 001
```
The merging logs may be scraped for a desired spot count in the annulus. Here
we find images with >=10 spots:
```
egrep '[[:digit:]]{2} spots in annulus' *.log |head -1
iobs_rank_001024_000271.log:Rank 271: Thu Sep 23 10:42:49 2021 /global/cfs/cdirs/m3562/dwpaley/master_files/run_000455.JF07T32V01_master.h5:578 : 13 spots in annulus
```
Finally, a script is provided to view an image, given the indexing output parent
folder, run number, image index, and reflection file suffix:
```
$ $CL/view_f_i.sh 001 455 578 strong.refl
```


