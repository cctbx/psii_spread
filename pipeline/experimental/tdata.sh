#!/bin/bash -l
#SBATCH -N 1                # Number of nodes on Perlmutter
#SBATCH -J psii_tdata
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m4734            # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -q debug            # debug queue
#SBATCH -t 00:10:00         # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 256 -c 4"

TRIAL=${1:-002}
RUNGROUP=${2:-007}
TDER_TASK=${3:-487}

mkdir -p "$SLURM_JOB_ID"; cd "$SLURM_JOB_ID" || exit
export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/pscratch/sd/p/psdatmgr/data/pmscr
export CCTBX_GPUS_PER_NODE=1
export MPI4PY_RC_RECV_MPROBE='False' # compensates for current missing MPI functions
env > env.out

PATHS=''  # BUILD INPUT PATHS
for f in ${CFS}/m3562/users/dtchon/p20231/common/ensemble1/r0*/${TRIAL}_rg${RUNGROUP}/task${TDER_TASK}/combine_experiments_t${TRIAL}/intermediates/
do
  PATHS="${PATHS}\n  path=$(realpath $f)"
done

echo """
tdata.output_path=exp_cells

input {$PATHS
  experiments_suffix=refined.expt
  reflections_suffix=refined.refl
  parallel_file_load.method=uniform
}
dispatch.step_list = input balance tdata
output {
  output_dir=tdata
  prefix=run_tdata
  log_level=0
}
""" > tdata.phil

echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge tdata.phil
echo "jobend $(date)";pwd
