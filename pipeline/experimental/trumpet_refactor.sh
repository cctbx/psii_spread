#!/bin/bash -l
#SBATCH -N 16               # Number of nodes on Perlmutter
#SBATCH -J psii_trumpet
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m4734            # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -q regular          # regular queue
#SBATCH -t 00:20:00         # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 1024 -c 4"

TRIAL=${1:-002}
RUNGROUP=${2:-007}
TDER_TASK=${3:-487}

mkdir -p "$SLURM_JOB_ID"; cd "$SLURM_JOB_ID" || exit
export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export CCTBX_GPUS_PER_NODE=1
export MPI4PY_RC_RECV_MPROBE='False' # compensates for current missing MPI functions
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application # User must export $MODULES path
env > env.out

PATHS=''  # BUILD INPUT PATHS
for f in ${CFS}/m3562/users/dtchon/p20231/common/ensemble1/r0*/${TRIAL}_rg${RUNGROUP}/task${TDER_TASK}/combine_experiments_t${TRIAL}/intermediates/
do
  PATHS="${PATHS}\n  path=$(realpath $f)"
done

echo "
input {$PATHS
  experiments_suffix=refined.expt
  reflections_suffix=refined.refl
  parallel_file_load.method=uniform
  parallel_file_load.balance=global2
  parallel_file_load.balance_verbose=True
  parallel_file_load.balance_mpi_alltoall_slices = 2
}
dispatch.step_list = input arrange drefine ucinline statistics_unitcell model_statistics annulus trumpet
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
arrange.sort.by=imageset_path
# tdata.output_path=PSII_cells # output the cell data as text for debug
uc_metrics.cluster.dbscan.eps=0.5 # the distance parameter normally entered interactively in the GUI
uc_metrics.file_name=None # The input tdata. Normally None, or provide file name to override
uc_metrics.input.space_group=Pmmm # Mandatory, only accept cells having this symmetry
uc_metrics.input.feature_vector=a,b,c
uc_metrics.write_covariance=False
uc_metrics.plot.outliers=True
uc_metrics.show_plot=False # True for interactive session, False for batch
filter.unit_cell.cluster.covariance.component=0
filter.unit_cell.cluster.covariance.mahalanobis=4.0
ucinline.cluster_only=False # True for debug only, normally False, which applies the mahalanobis distance filter
scaling.model=$MODULES/ls49_big_data/7RF1_refine_030_Aa_refine_032_refine_034.pdb
scaling.unit_cell=117.463  222.609  309.511  90.00  90.00  90.00
scaling.space_group=Pmmm
scaling.resolution_scalar=0.96
merging.d_max=5.0
merging.d_min=3.2
statistics.annulus.d_max=5.0
statistics.annulus.d_min=3.2
spread_roi.enable=True
spread_roi.strong=1.0
output.log_level=0
exafel.trusted_mask=None
exafel.scenario=1
output.output_dir=out
output.prefix=trumpet
output.save_experiments_and_reflections=True
trumpet.plot_all.enable=True
trumpet.plot_all.savepng=True
trumpet.dials_refine=False # dials refine now done in separate worker drefine, not trumpet
trumpet.debug_rank_0=False
trumpet.spectrum.recalibration=1.95
trumpet.spectrum.nbins=60
trumpet.spectrum.range=6500.0,6600.0
trumpet.spectrum.energy_delta_tolerance=1000
trumpet.residuals.cutoff=5.
trumpet.outlier.stddev=1.
trumpet.outlier.deff=2000.
trumpet.outlier.ucell=4.0
trumpet.outlier.tff=0
trumpet.outlier.remove=True
refinement {
  parameterisation {
    auto_reduction {
      min_nref_per_parameter = 1
      action = fix
    }
    beam.fix = all
    detector {
      fix_list = Tau1,Tau2,Tau3
      hierarchy_level = 0
    }
    crystal {
      unit_cell {
        restraints {
        tie_to_target {
            values = 117.463 222.609 309.511 90 90 90
            sigmas = 0.00117463 0.00222609 0.00309511 0.0 0.0 0.0 # drastically tighten the restraints
            id = None # apply to all
          }
        }
      }
    }
  }
  reflections {
    weighting_strategy.override = stills
    outlier.algorithm = null
  }
}
" > trumpet.phil

echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge trumpet.phil
echo "jobend $(date)";pwd
