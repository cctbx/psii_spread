#!/bin/bash -l
#SBATCH -N 1                # Number of nodes
#SBATCH -J annulus
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m4734            # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -q regular          # queue
#SBATCH -t 00:15:00         # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err           # copied from LS49/LY99/spread1/annulus2.sh
SRUN="srun -n 64 -c 4"

TRIAL=${1:-002}
RUNGROUP=${2:-007}
TDER_TASK=${3:-487}

mkdir -p "$SLURM_JOB_ID"; cd "$SLURM_JOB_ID" || exit
export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export CCTBX_GPUS_PER_NODE=1
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application
env > env.out

# NOTE: rg002 and mask both assume use of runs 70+ only
WORK=$CFS/m3562/users/dtchon/p20231/common/ensemble1
PATHS=''
for f in ${WORK}/r0*/${TRIAL}_rg${RUNGROUP}/task${TDER_TASK}/combine_experiments_t${TRIAL}/intermediates/
do
        PATHS="${PATHS}\n  path=$(realpath $f)"
done

echo -e "
input {$PATHS
  experiments_suffix=refined.expt
  reflections_suffix=refined.refl
  parallel_file_load.method=uniform
  parallel_file_load.balance=global1
}
dispatch.step_list = input balance filter statistics_unitcell model_statistics annulus
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
input.parallel_file_load.method=uniform
filter.algorithm=unit_cell
filter.unit_cell.algorithm=cluster
filter.unit_cell.cluster.covariance.file=$WORK/SPREAD/2mahalanobis/merge_get_tdata/ABatch14_AB14_TDER/covariance_ABatch14_AB14_TDER_eps03.pickle
filter.unit_cell.cluster.covariance.component=0
filter.unit_cell.cluster.covariance.mahalanobis=2.0
scaling.model=$CFS/m3562/users/dtchon/p20231/common/ensemble1/SPREAD2l/7RF1_refine_020.pdb
scaling.unit_cell=117.463  222.609  309.511  90.00  90.00  90.00
scaling.space_group=Pmmm
scaling.resolution_scalar=0.96
merging.d_min=3.5
statistics.annulus.d_max=4.0
statistics.annulus.d_min=3.5
spread_roi.enable=True
spread_roi.strong=1.0
output.log_level=0
exafel.trusted_mask=$CFS/m3562/dwpaley/processing_files_2022/mask/69plus.mask
exafel.scenario=1
output.output_dir=out
output.save_experiments_and_reflections=True
" > annulus.phil
echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge annulus.phil
echo "jobend $(date)";pwd
