# SPREAD analysis of experimental p20231 photosystem II data

This file documents the steps necessary to run the full SPREAD pipeline
on experimental p20231 photosystem II data, collected in 2022 on SwissFEL.
The order, naming, and indexing of operations reflects this
presented in a sister file
[README.md](https://github.com/nksauter/LS49/blob/master/LY99/spread1/README.md)
in LY99 repository.



## Project structure
The following document as well as published scripts assume
and suggest to follow a certain file structure described below.

File structure of the most important files on community file system:
```
$SPREAD/../
 ├─ r${RUN}
 │   └─ ${TRIAL}_rg${RUNGROUP}
 │       ├─ out/
 │       │   ├─ idx-*_refined.expt
 │       │   ├─ idx-*_indexed.refl
 │       │   ├─ idx-*_integrated.expt
 │       │   └─ idx-*_integrated.refl
 │       ├─ task${TDER_TASK}/
 │       │   └─ combine_experiments_t*/
 │       │       └─ intermediates/
 │       │           ├─ *_reintegrated_*.expt
 │       │           └─ *_reintegrated_*.refl
 │       └─ task${SCALING_TASK}
 │           └─ out/
 │               ├─ scaling*.expt
 │               └─ scaling*.refl
 ├─ ${MERGE_NAME}
 │   └─ v${LAST_VERSION}
 │       └─ ${MERGE_NAME}_v${LAST_VERSION}_all.mtz
 └─ SPREAD/
     ├─ 2mahalanobis/
     │   └─ merge_get_tdata/
     │       └─ merge_get_tdata/
     │           └─ SPREAD3_pre_scaling/
     │               ├─ merge_get_tdata.sh
     │               ├─ run080_cells.tdata
     │               └─ covariance_run080_cells.pickle
     ├─ 5annulus/
     │   ├─ annulus.sh
     │   └─ $JOB_ID_ANNULUS/
     │       └─ out/
     │           ├─ annulus_*.expt
     │           └─ annulus_*.refl
     ├─ 6trumpet/
     │   ├─ trumpet_refactor.sh
     │   └─ $JOB_ID_TRUMPET/
     │       ├─ *.png
     │       └─ out/
     │           ├─ trumpet_*.expt
     │           └─ trumpet_*.refl
     ├─ 7substitute/
     │   ├─ substitute2024.sh
     │   └─ $JOB_ID_SUBSTITUTE/
     │       └─ out/
     │           ├─ substitute_*.expt
     │           └─ substitute_*.refl
     ├─ 8mosaic/
     │   └─ mosaic.sh
     │       └─ $MOSAIC_NAME/
     │           └─ ${JOB_ID_MOSAIC}_e*a*b*c*/
     │               └─ out/
     │                   └─ *main*.log
     ├─ 8diffBragg/
     │       └─ (to be implemented)
     ├─ 9exa3a/
     │   └─ ${MERGE_NAME}_e*a*b*c*/
     │       ├─ exa3a.sh
     │       └─ ${JOB_ID_EXA3A}_eps*a*b*c*/
     │           └─ out/
     │               ├─ trial8_scenario3A_*.expt
     │               └─ trial8_scenario3A_*.refl
     ├─ 10sfactors/
     │   └─ ${MERGE_NAME}_eps*a*b*c*/
     │       ├─ sfactors.sh
     │       ├─ $JOB_ID_SFACTORS
     │       │   ├─ psii_miller_array.pickle
     │       │   └─ psii_static_fcalcs.pickle
     │       └─ reference/
     │           ├─ psii_miller_array.pickle
     │           └─ psii_static_fcalcs.pickle
     └─ 11refine
         └─ ${MERGE_NAME}_eps*a*b*c*/
             ├─ refine.sh
             └─ $JOB_ID_REFINE
                 └─ tell_macrocycle_01_iteration_*.png
```



## Prerequisites

In order to successfully run the jobs listed in the pipeline below,
the following CCTBX `modules:branches` are required:
- `cctbx:memory_policy` – to limit the GPU memory allocation per image;
- `dials:mcd_stills_nv_reject_outliers` -
  to use mcd and nave outlier rejection;
- `exafel_project` – required dependency for `psii_spread`;
- `LS49` – required dependency for `psii_spread`;
- `ls49_big_data` – required dependency for `psii_spread`;
- `psii_spread:main` – contains all used workers and other code.

Apart from selecting appropriate branches,
[cctbx.diff](../cctbx.diff) needs to be applied to cctbx_project
and [dials.diff](../dials.diff) might need to be applied to dials (see below)
before (re)running `make`.

In addition to environment variables defined locally at the top of each
individual script to control the settings,
the user must define the following environment variables
(either globally or locally before running scripts which use them):
- `$MODULES` - path to cctbx modules directory;
- `$SPREAD` - directory with the results of SPREAD calculations;

Furthermore, apart from `simulate.sh`, individual shell scripts also require
information about trial/rungroup/task ID or slurm ID of the predecessor jobs,
e.g. `$RUN` or `$JOB_ID_TRUMPET`.
They can be written directly in the script
(where they act as the default values),
or provided directly to the script
as a positional variable: `sbatch annulus.sh 002 007 487`.



## 0. Optimizing metrology
Optimizing detector metrology was performed at the beginning,
before processing the p20231 data, and has been discussed starting on page 32
in the p20231 Spread reprocessing journal.
Since it is unlikely it will have to be ever repeated for PSII,
and it varies between samples and end-stations,
it has not been described here.



## 1. Index diffraction data
Due to the nature of input data, indexing, ensemble refinement, scaling,
and merging were performed in the `cctbx.xfel` graphical interface.
In order to utilize existing database, log in using credentials available
in the Google doc: look for "`db {`" on page 8 or near page 138.

Create a new trial with phil parameters described in
[index_trial.phil](index_trial.phil) and default options.
Within the new trial, create a new run group with phil parameters described in
[index_rungroup.phil](index_rungroup.phil) and the following options:
- Start run: 27 (i.e. r0069)
- End run: 147 (i.e. r0256)
- two_theta_low: 12.5
- two_theta_high: 22.8
- Untrusted Pixel Mask:
  `/global/cfs/cdirs/m3562/dwpaley/processing_files_2022/mask/69plus.mask`

Note the run group number as `$RUNGROUP`
and trial number as `$TRIAL` for future use.

After the data is indexed, we want to run ensemble refinement of lattice
and detector parameters to make sure our final models reflect truth
as closely as possible. To run ensemble refinement, in `cctbx.xfel` GUI
create a new dataset that includes data tagged as `69plus`.
Experiments belonging to earlier runs are not numerous as well as unreliable
since they were collected using different incident beam energy.

Create and run a new task using phil parameters given in file
[ensemble_refinement.phil](ensemble_refinement.phil).
Note the task number as `$TDER_TASK` for future use.

Both indexing and TDER were run using 64 processors per node
and 4 indexing nodes, with extra submission arguments
`--time=120 -A m3562 -C cpu --qos=regular`.
While this should be completely enough, both indexing and TDER described here
scales well so higher values can be used as needed.
For example, in the most recent processing as of today (August 2, 2024)
processing some runs with many experiments required 8 nodes to finish on time.



## 2. Quality control on the unit cell
After indexing, using mentioned parameters, the unit cell distribution
should be strongly focused around desired parameters.
In order to remove any unit cells that still do not match,
create a cluster following [tdata.sh](tdata.sh),
changing variables `$TRIAL`, `$RUNGROUP`, and `$TDER_TASK` as needed.
This will generate a `.tdata` file (e.g. `run080_cells.tdata`),
which can be then used to generate a tight cluster for the purpose
of unit cell filtering via:
`uc_metrics.dbscan eps=0.5 file_name=run080_cells.tdata space_group=Pmmm 
feature_vector=a,b,c write_covariance=True plot.outliers=True`

This step is needed only for the diagnostic purposes or if
scaling and merging, as discussed in the next subsection, are desired.
Otherwise, for the purpose of SPREAD processing only,
the mahalanobis filtering is performed independently
during the following trumpet step and does not need to be run here.



## 3. Conventional merging
Conventional merging, i.e. "local" scaling and "global" merging,
are necessary only in order to produce an `.mtz` file used to later
solve and refine crystal structure. It is not required if the reference
structure is already known, in which case it can be skipped.

For scaling, in any dataset which includes previously-performed ensemble
refinement create a new scaling task. Populate the phil parameters window
with parameters given in [scaling.phil](scaling.phil) and run.

For merging, in any dataset which includes previously-performed ensemble
refinement and scaling, create a new merging task.
Populate the phil parameters window with parameters given in
[merging.phil](merging.phil) and run – preferably after the scaling has already
finished to save computational resources and simplify file structure.

All scaling and merging jobs should finish within 30 minutes using 4 nodes
and 64 ranks per node, but the number of nodes can be scaled up if desired.



## 4. Refinement of the atomic model
Refer to information provided by Nicholas Sauter and in LY99
[README.md](https://github.com/nksauter/LS49/blob/master/LY99/spread1/README.md).



## 5. Annulus worker evaluation
If desired, the distribution of d_min of individual experiments
can be performed using annulus worker. This purely-diagnostic step
reject reflections outside the specified resolution annulus
as well as experiments with no reflections remaining after this operation.
Consequently, the annulus worker can be used as a first estimate to see
what resolution range could be ultimately investigated in spread refinement.

Once a reasonable resolution range for further analysis is found,
the annulus worker can be skipped, as the following trumpet worker
needs to be run anyway and includes all funcitonality of the annulus.

This script is available as [annulus.sh](annulus.sh)
and should be run from directory `5annulus/`.
It requires known positional variables
`$TRIAL`, `$RUNGROUP`, and `$TDER_TASK`.



## 6. Trumpet plot and filtering

This is the central diagnostics and filtering step run on GPU before
any per-pixel GPU refinement takes place.
This script defines a target distribution based on unit cell parameter's
covariance ("refactor" only), refines all unit cells to common mean parameters,
removes outlier experiments, calculates unit cell and model statistics,
evaluates annulus statistics, removes experiments with insufficient
reflection count above target resolution, and finally removes experiments
based on their reflection profile, spectral profile, and unit cell offset.
During the refinement, a very strong unit cell target
(sigmas equal 0.001% of UC parameters)
and mahalanobis distance cutoff (4 with epsilon=2.0) are applied. 

The script is available in as [trumpet_refactor.sh](trumpet_refactor.sh)
and should be run from directory `6trumpet/` with all variables set.
PNG files can be converted to an animated GIF if ImageMagick is present:
```shell
convert -delay 12 -loop 0 *.png trumpet.gif
```

This script is available as [trumpet_refactor.sh](trumpet_refactor.sh)
and should be run from directory `6trumpet/`.
It requires known positional variables
`$TRIAL`, `$RUNGROUP`, and `$TDER_TASK`.



## 7. Substitute the shoeboxes
After a well-behaved set of refined experiments and indexed refls
covering appropriate resolution annulus has been selected,
use `substitute` worker to swap the indexed refls
with the ones coming from integration.

This script is available as [substitute.sh](substitute.sh)
and should be run from directory `7substitute/`.
It requires known environment or positional variables
`$TRIAL`, `RUNGROUP`, `TDER_TASK`, and `$JOB_ID_TRUMPET`.



## 8. Any flavor of mosaic parameter estimation
In step 8 we aim to estimate the values of mosaic parameters eta
and Deff/Ncells/Nabc in order to fix them later during anomalous
dispersion parameter refinement. This can be performed in multiple ways,
some of which are the matter of current consideration.

Classically, we would estimate the single uniform value of median Na, Nb, Nc,\
the average number of unit cells along
each crystallographic dimension in a single coherent crystalline unit,
as well as eta, the average rotational divergence of coherent domains,
by running step 9 on a small subset of data with gridded input parameters.
After refining the orientation with diffBragg as well as determining ΔR,
Correlation(ΔR,ΔΨ), and <σZ> on the specific SPREAD annulus,
we would pick the best global values for subsequent work.
This old approach can be still achieved by serially running script
[mosaic_legacy.sh](mosaic_legacy.sh), then [exa3A_legacy.sh](exa3A_legacy.sh),
then [refine_legacy.sh](refine_legacy.sh), adapting instructions from
[README.md](https://github.com/nksauter/LS49/blob/master/LY99/spread1/README.md).
```shell
for e in 0.0064 0.0128 0.0256 0.0384 0.0512; do \ # loop over eta values in degrees
for a in 24 48 72; do \ # loop over NcellsA
for b in 16 32 48; do \ # loop over NcellsB
for c in 8 16 24; do \ # loop over NcellsC
sbatch mosaic.sh ${e} ${a} ${b} ${c}; sleep 10; done; done; done; done
```
For this you need a `moving_to_summit` branch of cctbx.

In order to refine mosaic parameters more accurately and individually for
every experiment, a better approach requires refining Na, Bc, Nc, eta,
and orientations using diffBragg.
This will produce a pickled diffBragg DataFrame with the results.
Supplemented with these results, the refine step can be then run directly
based on the substitute data, as described below.
The diffBragg refinement for the last experimental data was done in 4 batches
by Derek Mendez, its results available in `$CFS/m3562/der/psii/knight.[0-3]`.

The process of running diffBragg refinement includes the following steps:
- run [diffBragg_split.sh](diffBragg_split.sh), let `$1` be directory with input data;
- if desired, split resulting `input.txt` spec-file into several batches;
- srun [diffBragg_refine.sh](../diffBragg_refine.sh), pointing to spec-file,
  output directory.

Both scripts should be run from directory `8diffBragg/`.
They require known environment or positional variable `$JOB_ID_SUBSTITUTE`.



## 10. Prepare model structure factors
The following text assumes that diffBragg has been used to estimate mosaicity.
Cache the base structure factors to a pickle file
to speed up the run time of the S1 final step.
In this way, the full list of reference structure factors does not need
to be computed each time the S1 scenario of annulus worker is run,
but instead can be created once and saved for later.

This script is available as [sfactors_diffBragg.sh](sfactors_diffBragg.sh)
and should be run from directory `10sfactors/`.
It requires known environment or positional variables
`$TRIAL`, `RUNGROUP`, `TDER_TASK`, and `$JOB_ID_TRUMPET`.

This SLURM job uses little multiprocessing, will fail by design and,
can be killed as soon as two .pickle files are generated.
The pickles produced by this script should be linked or copied
to the directory `10sfactors/reference/` for `11refine` to work.

Currently, this and the following step need to be run on branch
channels rebased on top of the current master. Hopefully this will
be fixed in the future.



## 11. Run scattering factor refinement
Run the refinement for four independent Mn sites
on a 2 eV grid around Mn K-alpha absorption edge.
The spacing and number of sites are controlled by phil parameters,
and updating them will require re-running step 10 as well.

This script is available as [refine_diffBragg.sh](refine_diffBragg.sh)
and should be run from directory `11refine/`.
It requires known environment or positional variables
`$JOB_ID_SUBSTUTE` and `$JOB_ID_DIFFBRAGG`,
as well as pickles from the previous step linked into  `10sfactors/reference/`.
