#!/bin/bash -l
#SBATCH -N 16               # Number of nodes
#SBATCH -J mosaic
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m3562_g          # allocation
#SBATCH -C gpu
#SBATCH -q regular          # regular queue
#SBATCH -t 02:00:00         # wall clock time limit
#SBATCH --ntasks-per-gpu=1
#SBATCH -o %j.out
#SBATCH -e %j.err

JOB_DIR=${SLURM_JOB_ID}_e${1}a${2}b${3}c${4}
mkdir -p $JOB_DIR; cd $JOB_DIR || exit

export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export CCTBX_GPUS_PER_NODE=1
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application # User must export $MODULES path
export WERK=/global/cfs/cdirs/m3562/users/dtchon/p20231/common/ensemble1

echo "
dispatch.step_list = input balance annulus
input.path=$WERK/SPREAD/7substitute/5690290/out/
input.experiments_suffix=.expt
input.reflections_suffix=.refl
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
input.parallel_file_load.method=uniform
input.parallel_file_load.balance_mpi_alltoall_slices = 10
scaling.model=$CFS/m3562/users/dtchon/p20231/common/ensemble1/SPREAD2l/7RF1_refine_020.pdb
scaling.unit_cell=117.463  222.609  309.511  90.00  90.00  90.00
scaling.space_group=P212121
scaling.resolution_scalar=0.96
filter.unit_cell.cluster.covariance.file=$WERK/SPREAD/2mahalanobis/merge_get_tdata/ABatch14_AB14_TDER/covariance_ABatch14_AB14_TDER_eps03.pickle
filter.unit_cell.cluster.covariance.component=0
filter.unit_cell.cluster.covariance.mahalanobis=2.0
merging.d_max=None
merging.d_min=3.2
statistics.annulus.d_max=5.0
statistics.annulus.d_min=3.2
spread_roi.enable=True
# spread_roi.strong=1.0 # only use for initial annulus definition, not subsequent
output.log_level=0 # 0 = stdout stderr, 1 = terminal
output.output_dir=out
output.prefix=trial8_scenario3A
output.save_experiments_and_reflections=False
exafel.scenario=3A
exafel.trusted_mask=$CFS/m3562/dwpaley/processing_files_2022/mask/69plus.mask
exafel.shoebox_border=0
exafel.context=kokkos_gpu
exafel.model.plot=False
exafel.model.mosaic_spread.value=${1}
exafel.model.Nabc.value=${2},${3},${4}
exafel.debug.lastfiles=False # write out *.h5, *.mask for each image
exafel.debug.verbose=False
exafel.debug.finite_diff=-1
exafel.debug.eps=1.e-8
exafel.debug.energy_offset_eV=0
exafel.skin=True # whether to use diffBragg
exafel{
  refpar{
    label = *background *G *rot
    background {
      algorithm=rossmann_2d_linear
      scope=spot
      slice_init=border
      slice=all
    }
    G {
      scope=lattice
    }
  }
}
trumpet.plot_all.enable=True
trumpet.plot_all.savepng=True
" > trial8.phil
echo "jobstart $(date)";pwd
srun -n 64 -c 4 cctbx.xfel.merge trial8.phil
echo "jobend $(date)";pwd
