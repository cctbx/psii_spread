#!/bin/bash -l
#SBATCH -N 128              # Number of nodes on Perlmutter
#SBATCH -J psii_refine
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m4734_g          # allocation: m3562_g or m4734_g
#SBATCH -C gpu
#SBATCH -q regular          # debug queue
#SBATCH -t 02:00:00         # wall clock time limit, ~enough for 10-20 cycles?
#SBATCH --ntasks-per-gpu=2  # used to be 1 – due to memory?
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 1024 -c 16 --ntasks-per-gpu=2"

JOB_ID_SUBSTITUTE=${1:-$JOB_ID_SUBSTITUTE}
JOB_ID_DIFFBRAGG=${2:-$JOB_ID_DIFFBRAGG}

export MERGE_NAME=SPREAD3
mkdir -p "$SLURM_JOB_ID"; cd "$SLURM_JOB_ID" || exit
export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export SIT_ROOT=/reg/g/psdm
export SIT_DATA=/global/common/software/lcls/psdm/data
export CCTBX_GPUS_PER_NODE=1
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application # User must export $MODULES path
env > env.out

echo "
dispatch.step_list = input arrange modify_reindex_to_abc annulus
input.path=$SPREAD/7substitute/${JOB_ID_SUBSTITUTE}/out
input.experiments_suffix=.expt
input.reflections_suffix=.refl
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
input.parallel_file_load.balance_verbose=False
input.parallel_file_load.balance_mpi_alltoall_slices = 2
arrange.sort.by=imageset_path
arrange.filter.mode=match                                     # Uncomment the following to use parts of data, use filter.mode=filter to exclude instead
# arrange.filter.imageset_path=\/(?:0\d\d|1[0-2]\d|13[01])\/  # stripe 1:  runs 070–131; batches 5, 6, 7, 8, 9, 10, 12, 13, 16
# arrange.filter.imageset_path=\/0(?:7\d|8[0-4])\/            # stripe 1a: runs 070–084; batches 5, 6, 7
# arrange.filter.imageset_path=\/(?:08[5-9]|09\d|1[0-2]\d|13[01])\/  # 1b: runs 085–131; batches 8, 9, 10, 12, 13, 16
# arrange.filter.imageset_path=\/13[456]\/                    # stripe 2:  runs 134–136; batch 14 (first half)
# arrange.filter.imageset_path=\/13[789]\/                    # stripe 3:  runs 137–139; batch 14 (second half)
# arrange.filter.imageset_path=\/1(?:48|[67]\d|8[02])\/       # stripe 4:  runs 148–182; batches 15, 19, 20, 21
# arrange.filter.imageset_path=\/(?:187|19\d|2[01]\d)\/       # stripe 5:  runs 187–214; batches 23, 24, 25, 26
# arrange.filter.imageset_path=\/2(?:[23]\d|40)\/             # stripe 6:  runs 227–240; batches 28, 29
# arrange.filter.imageset_path=\/2(?:4[4-9]|5\d)\/            # stripe 7:  runs 244–256; batches 30, 31
arrange.filter.imageset_path=\/(?:148|1[6-9]\d|2\d\d)\/     # stri. 4-7: runs 148–256; batches 15, 19, 20, 21, 23, 24, 25, 26, 28, 29, 30, 31
scaling.model=$CFS/m3562/users/dtchon/p20231/common/ensemble1/SPREAD2l/7RF1_refine_030_Aa_refine_032_refine_034.pdb
scaling.unit_cell=117.463  222.609  309.511  90.00  90.00  90.00
scaling.space_group=P212121
scaling.resolution_scalar=0.96
scaling.pdb.k_sol=0.435
merging.d_max=None
merging.d_min=3.2
statistics.annulus.d_max=5.0
statistics.annulus.d_min=3.2
spread_roi.enable=True
# spread_roi.strong=1.0 # only use for initial annulus definition, not subsequent
output.log_level=0 # 0 = stdout stderr, 1 = terminal
output.output_dir=out
output.prefix=drift
output.save_experiments_and_reflections=True
exafel.scenario=S1
exafel.static_fcalcs.path=$SPREAD/10sfactors/$MERGE_NAME/reference100eV/psii_static_fcalcs.pickle
exafel.static_fcalcs.whole_path=$SPREAD/10sfactors/$MERGE_NAME/reference100eV/psii_miller_array.pickle
exafel.static_fcalcs.action=read
exafel.trusted_mask=$CFS/m3562/dwpaley/processing_files_2022/mask/69plus.mask
exafel.shoebox_border=0
exafel.context=kokkos_gpu
exafel.model.plot=False
exafel.model.mosaic_spread.value=Auto
exafel.model.Nabc.value=Auto
#exafel.diffbragg_pickles_glob=/global/cfs/cdirs/m3562/der/psii/knight.*/pandas/*.pkl
exafel.diffbragg_pickles_glob=$SPREAD/8diffBragg/$JOB_ID_DIFFBRAGG/pandas/*.pkl
exafel.debug.lastfiles=False # write out *.h5, *.mask for each image
exafel.debug.verbose=False
exafel.debug.finite_diff=-1
exafel.debug.eps=1.e-8
exafel.debug.format_offset=0
exafel.debug.energy_offset_eV=0
exafel.debug.energy_stride_eV=2.00
exafel.skin=False # whether to use diffBragg
exafel{
  refpar{
    label = *background *G
    background {
      algorithm=rossmann_2d_linear
      scope=spot
      slice_init=border
      slice=all
    }
    G {
      scope=lattice
      reparameterize=bound
    }
  }
}
exafel.metal=PSII4 # PSII
sauter20.LLG_evaluator.enable_plot=True
sauter20.LLG_evaluator.title=tell
sauter20.LLG_evaluator.restraints.fp.mean=0.0
sauter20.LLG_evaluator.restraints.fp.sigma=None # 0.04
sauter20.LLG_evaluator.restraints.fdp.mean=0.1
sauter20.LLG_evaluator.restraints.fdp.sigma=None # 0.08
sauter20.LLG_evaluator.restraints.kramers_kronig.algorithm=original1
sauter20.LLG_evaluator.restraints.kramers_kronig.use=True
sauter20.LLG_evaluator.restraints.kramers_kronig.pad=100
sauter20.LLG_evaluator.restraints.kramers_kronig.trim=0
sauter20.LLG_evaluator.restraints.kramers_kronig.weighting_factor=50000.0
sauter20.LLG_evaluator.max_calls=30
modify.reindex_to_abc.space_group=P212121
" > refine.phil
echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge refine.phil
echo "jobend $(date)";pwd