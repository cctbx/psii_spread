#!/bin/bash
#SBATCH -N 256              # Number of nodes on Perlmutter
#SBATCH -J psii_refine
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A lcls_g           # allocation: m3562 or m4734
#SBATCH -C gpu
#SBATCH -q regular          # debug queue
#SBATCH -t 01:00:00         # wall clock time limit, ~enough for 10-20 cycles?
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun --cpus-per-gpu=2 --ntasks-per-node=8"

JOB_ID_SUBSTITUTE=${1:-$JOB_ID_SUBSTITUTE}
MAX_PROCESS=${2:--1}
SPEC_PATH=${PWD}/${JOB_ID_SUBSTITUTE}.txt  # input exp_ref_spec_file from psii_split.sh

mkdir -p "$SLURM_JOB_ID"; cd "$SLURM_JOB_ID" || exit

export MTZ_PATH=/global/cfs/cdirs/m3562/users/dtchon/p20231/common/ensemble1/SPREAD2l/v000/SPREAD2l_v000_all.mtz
export MASK=/global/cfs/cdirs/m3562/dwpaley/processing_files_2022/mask/69plus.mask

export DEVICES_PER_NODE=4
export DIFFBRAGG_USE_KOKKOS=1
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export MPI4PY_RC_RECV_MPROBE='False'
env > env.out

echo "
shots_per_chunk = 15
lbfgs_maxiter = 300
filter_during_refinement {
  threshold = 5
}
filter_after_refinement {
  enable = True
  max_attempts = 2
  min_prev_niter = 500
  max_prev_sigz = 4
  threshold = 7
}
symmetrize_Flatt = False
debug_mode = False
debug_mode_rank0_only=False
spectrum_from_imageset = True
downsamp_spec {
  delta_en = 1
}
method = 'L-BFGS-B'
sigmas {
  Nabc = 1 1 1
  ucell = 1 1 1
}
init {
  Nabc = 9 9 9
  G = 1e6
  eta_abc = [.1,.1,.1]
}
mins {
  detz_shift = -1.5
  RotXYZ = -3.14 -3.14 -3.14
  G = 0.01
}
maxs {
  detz_shift = 1.5
  Nabc = 100 100 80
  RotXYZ = 3.14 3.14 3.14
  G = 1e+20
  eta_abc = 360 360 360
}
centers {
  ucell_a = 117.463
  ucell_b = 222.609
  ucell_c = 309.511
  ucell_alpha = 90
  ucell_beta = 90
  ucell_gamma = 90
}
betas
{
  ucell_a = 0.0000013798  # (ucell_a * 10e-5) ** 2
  ucell_b = 0.0000049555  # (ucell_b * 10e-5) ** 2
  ucell_c = 0.0000095797  # (ucell_c * 10e-5) ** 2
  ucell_alpha = 1e-7      # should be irrelevant
  ucell_beta = 1e-7       # should be irrelevant
  ucell_gamma = 1e-7      # should be irrelevant
}
fix.eta_abc = False
ucell_edge_perc = 15
ucell_ang_abs = 1
no_Nabc_scale = True
space_group = P212121
use_restraints = True
logging {
  rank0_level = low normal *high
}
simulator {
  oversample = 1
  crystal {
    has_isotropic_mosaicity = True
    num_mosaicity_samples = 30
  }
  structure_factors {
    mtz_name = ${MTZ_PATH}
    mtz_column = 'Iobs(+),SIGIobs(+),Iobs(-),SIGIobs(-)'
  }
  beam {
    size_mm = 0.001
  }
}
refiner {
  num_devices = ${DEVICES_PER_NODE}
  sigma_r = 3
  adu_per_photon = 6.551
}
roi {
  mask_outside_trusted_range = True
  fit_tilt = True
  pad_shoebox_for_background_estimation = 8
  reject_edge_reflections = False
  reject_roi_with_hotpix = False
  hotpixel_mask = ${MASK}
}
perRoi_finish=False

" > stage1.phil


echo "jobstart $(date)";pwd
TSTART=$SECONDS
$SRUN hopper stage1.phil exp_ref_spec_file="$SPEC_PATH" outdir="$PWD" max_process="$MAX_PROCESS"
echo "jobend $(date)";pwd
echo Full job took $(($SECONDS-$TSTART)) seconds!