#!/bin/bash -l
#SBATCH -N 8          # Number of nodes
#SBATCH -J merge
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m4734            # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -q debug      # regular queue
#SBATCH -t 20
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 256 -c 8"

export JOB_ID_INDEX=$1
export JOB_ID_TDATA=$2
mkdir -p "$SLURM_JOB_ID"
cd "$SLURM_JOB_ID" || exit

export TRIAL=psii_sim
export OUT_DIR=${PWD}
env > env.out

echo "input.path=${SPREAD}/SIM/1index/${JOB_ID_INDEX}/out
input.experiments_suffix=_integrated.expt
input.reflections_suffix=_integrated.refl
input.parallel_file_load.method=uniform
filter.algorithm=unit_cell
filter.unit_cell.algorithm=cluster
filter.unit_cell.cluster.covariance.file=${SPREAD}/SIM/2mahalanobis/${JOB_ID_TDATA}/covariance_sim_cells.pickle
filter.unit_cell.cluster.covariance.component=0
filter.unit_cell.cluster.covariance.mahalanobis=4.0
filter.outlier.min_corr=-1.0
select.algorithm=significance_filter
select.significance_filter.sigma=-0.5
scaling.model=${MODULES}/ls49_big_data/7RF1_refine_030_Aa_refine_032_refine_034.pdb
scaling.resolution_scalar=0.993420862158964
postrefinement.enable=True
postrefinement.algorithm=rs
merging.d_min=2.5
merging.merge_anomalous=False
merging.set_average_unit_cell=True
merging.error.model=mm24
statistics.n_bins=20
statistics.report_ML=True
output.prefix=${TRIAL}
output.output_dir=${OUT_DIR}/out
output.tmp_dir=${OUT_DIR}/tmp
output.do_timing=True
output.log_level=0
output.save_experiments_and_reflections=True
parallel.a2a=1
" > merge.phil

mkdir -p ${OUT_DIR}/${TRIAL}/out
mkdir -p ${OUT_DIR}/${TRIAL}/tmp

echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge merge.phil
echo "jobend $(date)";pwd

