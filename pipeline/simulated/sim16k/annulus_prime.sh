#!/bin/bash -l
#SBATCH -N 1                # Number of nodes on Perlmutter
#SBATCH -J psii_annulus
#SBATCH -A m3562            # allocation
#SBATCH -C cpu
#SBATCH -q debug            # regular queue
#SBATCH -t 00:15:00         # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 64 -c 4"

JOB_ID_INDEX=${1:-$JOB_ID_INDEX}

mkdir -p "$SLURM_JOB_ID"
cd "$SLURM_JOB_ID" || exit

export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export CCTBX_GPUS_PER_NODE=1
export MPI4PY_RC_RECV_MPROBE='False' # compensates for current missing MPI functions
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application
env > env.out

echo "
input {
  path=$SPREAD/SIM/1index/$JOB_ID_INDEX/out
  experiments_suffix=refined.expt
  reflections_suffix=indexed.refl
  parallel_file_load.method=uniform
  parallel_file_load.balance=global2
}
dispatch.step_list = input balance drefine ucinline statistics_unitcell model_statistics annulus
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
tdata.output_path=PSII_cells
uc_metrics.cluster.dbscan.eps=2.0 # the distance parameter normally entered interactively in the GUI
uc_metrics.file_name=None # The input tdata. Normally None, or provide file name to override
uc_metrics.input.space_group=Pmmm # Mandatory, only accept cells having this symmetry
uc_metrics.input.feature_vector=a,b,c
uc_metrics.write_covariance=False
uc_metrics.plot.outliers=True
uc_metrics.show_plot=True # True for interactive session, False for batch
filter.unit_cell.cluster.covariance.component=0
filter.unit_cell.cluster.covariance.mahalanobis=4.0
scaling.model=$MODULES/ls49_big_data/7RF1_refine_030_Aa_refine_032_refine_034.pdb
scaling.unit_cell=117.463  222.609  309.511  90.00  90.00  90.00
scaling.space_group=Pmmm
scaling.resolution_scalar=0.96
merging.d_max=5.0
merging.d_min=3.2
statistics.annulus.d_max=5.0
statistics.annulus.d_min=3.2
spread_roi.enable=True
spread_roi.strong=1.0
output.log_level=0
exafel.trusted_mask=None
exafel.scenario=1
output.output_dir=out
output.save_experiments_and_reflections=True
refinement {
  parameterisation {
    auto_reduction {
      min_nref_per_parameter = 1
      action = fix
    }
    beam.fix = all
    detector {
      fix_list = Tau1,Tau2,Tau3
      hierarchy_level = 0
    }
    crystal {
      unit_cell {
        restraints {
        tie_to_target {
            values = 117.463 222.609 309.511 90 90 90
            sigmas = 0.00117463 0.00222609 0.00309511 0.0 0.0 0.0 # drastically tighten the restraints
            id = None # apply to all
          }
        }
      }
    }
  }
  reflections {
    weighting_strategy.override = stills
    outlier.algorithm = null
  }
}
" > annnulus.phil

echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge annnulus.phil
echo "jobend $(date)";pwd
