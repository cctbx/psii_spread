# SPREAD analysis of simulated photosystem II data

This file documents the steps necessary to run the full SPREAD pipeline
on the simulated photosystem II data, generated based on the
2022 experimental p20231 data collected on SwissFEL.
The order, naming, and indexing of operations reflects this
presented in a sister file
[README.md](https://github.com/nksauter/LS49/blob/master/LY99/spread1/README.md)
in LY99 repository.



## Project structure
The following document as well as published scripts assume
and suggest to follow a certain file structure described below.

File structure of the most important files on scratch:
```
$SCRATCH/psii_sim/
 └─ $JOB_ID_SIMULATE/
     └─ image_rank_*.h5
```

File structure of the most important files on community file system:
```
$SPREAD/SIM/
 ├─ 0simulate/
 │   └─ simulate.sh
 ├─ 1index/
 │   ├─ index.sh
 │   ├─ index_mcd.sh
 │   └─ $JOB_ID_INDEX/
 │       └─ out/
 │           ├─ idx-*_refined.expt
 │           ├─ idx-*_indexed.refl
 │           ├─ idx-*_integrated.expt
 │           └─ idx-*_integrated.refl
 ├─ 2mahalanobis/
 │   ├─ tdata.sh
 │   └─ $JOB_ID_MAHALANOBIS/
 │       └─ covariance_sim_cells.pickle
 ├─ 5annulus/
 │   └─ annulus.sh
 ├─ 6trumpet/
 │   ├─ trumpet.sh
 │   ├─ trumpet_refactor.sh
 │   └─ $JOB_ID_TRUMPET/
 │       ├─ *.png
 │       └─ out/
 │           ├─ trumpet_*.expt
 │           └─ trumpet_*.refl
 ├─ 7substitute/
 │   ├─ substitute.sh
 │   └─ $JOB_ID_SUBSTITUTE/
 │       └─ out/
 │           ├─ substitute_*.expt
 │           └─ substitute_*.refl
 ├─ 8mosaic/
 │   └─ mosaic.sh
 ├─ 9exa3a/
 │   ├─ exa3A.sh
 │   └─ ${JOB_ID_EXA3A}_e0.05Nabc16/
 │       └─ out/
 │           ├─ exa3a_*.expt
 │           └─ exa3a_*.refl
 ├─ 10sfactors/
 │   ├─ sfactors.sh
 │   └─ $JOB_ID_SFACTORS/
 │       ├─ psii_miller_array.pickle
 │       └─ psii_static_fcalcs.pickle
 └─ 11refine
     └─ refine.sh
         └─ tell_macrocycle_01_iteration_*.png
```



## Prerequisites

In order to successfully run the jobs listed in the pipeline below,
the following CCTBX `modules:branches` are required:
- `cctbx:memory_policy` – to limit the GPU memory allocation per image;
- `dials:dsp_oldstriping` or `dials:dsp_oldstriping_mcd_stills` (see below) -
  to index using more than 256 ranks concurrently;
- `exafel_project` – to simulate initial data;
- `LS49` – generally necessary for simulation;
- `ls49_big_data` – for reference structure and absorption spectra;
- `psii_spread:main` – contains all used workers and other code.

Apart from selecting appropriate branches,
[cctbx.diff](../cctbx.diff) needs to be applied to cctbx_project
and [dials.diff](../dials.diff) might need to be applied to dials (see below)
before (re)running `make`.

In addition to environment variables defined locally at the top of each
individual script to control the settings,
the user must define the following environment variables
(either globally or locally before running scripts which use them):
- `$MODULES` - path to cctbx modules directory;
- `$SCRATCH` - scratch directory with fast read/write access;
- `$SPREAD` - directory with the results of SPREAD calculations;

Furthermore, apart from `simulate.sh`, individual shell scripts also require
information about the slurm ID of the predecessor jobs, e.g. `$JOB_ID_INDEX`.
It can be supplied either as an environment variable,
using `export JOB_ID_INDEX=123456`, or provided directly to the script
as a positional variable: `sbatch annulus.sh 123456`.


## 0. Simulate the data
Since in this pipeline we start with known crystal structure instead
of unknown data, we need to start by simulating data to analyze itself.
To this aim, use python simulation interface
[LY99_batch.py](https://github.com/ExaFEL/exafel_project/blob/master/kpp_utils/LY99_batch.py)
available in the exafel_project repository.

The script used to generate 65536 images of photosystem II
is available as [simulate.sh](simulate.sh)
and should be run from directory `0simulate/`.



## 1. Index diffraction data
While typically the first step would start from optimizing detector metrology,
in the case of simulated data this is unnecessary, as the reference geometry
is the one used to simulate data and, as such, is a known ground truth.
Instead, we can go directly to indexing the data.

Two scripts used to index the data are available as
[index.sh](index.sh) and [index_mcd.sh](index_mcd.sh).
The former offers original index + refine + integrate approach.
The latter additionally incorporates a mcd outlier rejection algorithm,
which significantly improves indexing rates compared to former method.
Either script (preferably the latter) should be run from directory `1index/`.
It requires a known environment or positional variable `$JOB_ID_SIMULATE`.

***Note:** While indexing initially, I used a reference geometry with modified
detector distance, but ever since then we discussed that this should be
not necessary and the geometry should be pulled directly from h5 instead.
The current indexing script reflects that.*

***Note:** Indexing should be done on dials branch
`dsp_oldstriping_mcd_stills`* with [dials.diff](../dials.diff) applied
if using [index_mcd.sh](index_mcd.sh) or on dials branch 
`dsp_oldstriping`* without [dials.diff](../dials.diff) applied
if using [index.sh](index.sh).


## 2. Quality control on the unit cell
For experimental data, we would want to limit the size
of accepted unit cells to lie withing one standard deviation
from a mean derived from a single well-behaved run.
This filters out experiments with wrong geometry and different crystal forms.
In the case of simulated data, the ground truth unit cell parameters
are known and any deviation is introduced by errors with indexing.
Hence, we will limit the reject the unit cells which have disagreement
above 0.1% of the absolute known ground truth value.

This is mostly a legacy step.
The indexing procedure as given in `index_mcd.sh`
has been updated to jointly refine the unit cells
and introduce a mcd outlier rejection algorithm.
The rejection of unit cells based on the lattice constants, on the other hand,
has been incorporated into [trumpet_refactor.sh](trumpet_refactor.sh).
It has been preserved as a template in case a separate mahalanobis
filtering step will be required in the experimental pipeline
or is required due to using older [trumpet.sh](trumpet.sh) approach.

The script used to get tdata from indexed images
is available as [tdata.sh](tdata.sh)
and should be run from directory `2mahalanobis/`.
It requires a known environment or positional variable `$JOB_ID_INDEX`.

After writing tdata, generate a mahalanobis cluster using command:
`uc_metrics.dbscan eps=2.0 file_name=sim_cells.tdata space_group=Pmmm 
feature_vector=a,b,c write_covariance=True plot.outliers=True`.


## 3. Conventional merging
This step is required for experimental data in order to generate
a conventional hkl and solve the crystal structure to be used
as a reference later. Since for simulated data the crystal structure
is a known ground truth, neither merging the data nor refinement
of the atomic model is necessary.


## 4. Refinement of the atomic model
This step is unnecessary for simulated data due to reasons listed above.



## 5. Annulus worker evaluation
This is a purely-diagnostics step which aims to count the number
of experiments with strong reflection in the desired resolution annulus.
It is used to evaluate the quality and overall health of the data
and can be skipped in the main processing pipeline.

Because the subsequent trumpet step presents virtually all the same
analytic benefits of this annulus step with addition of trumpet filtering
information, this step can be considered legacy.
In experimental approach a shorter separate annulus filtering
and statistics step might be still necessary.

This script is available as [annulus.sh](annulus.sh)
and should be run from directory `5annulus/`.
It requires known environment or positional variables
`$JOB_ID_INDEX` and `$JOB_ID_MAHALANOBIS`.



## 6. Trumpet plot and filtering
This is the central diagnostics and filtering step run on GPU before
any per-pixel GPU refinement takes place.
This script defines a target distribution based on unit cell parameter's
covariance ("refactor" only), refines all unit cells to common mean parameters,
removes outlier experiments, calculates unit cell and model statistics,
evaluates annulus statistics, removes experiments with insufficient
reflection count above target resolution, and finally removes experiments
based on their reflection profile, spectral profile, and unit cell offset.

If using the "refactor" approach, this step is also now the only place
where a very strong unit cell target (sigmas equal 0.001% of UC parameters)
and mahalanobis distance cutoff (4 with epsilon=2.0)
are exclusively and ultimately applied. 

The script is available in two flavors, [trumpet.sh](trumpet.sh)
and [trumpet_refactor.sh](trumpet_refactor.sh). The latter is improved
and does not require separate mahalanobis step, but should be only ran on an
interactive node, as it requires one to manually close the clustering plot.

PNG files can be converted to an animated GIF if ImageMagick is present:
```shell
convert -delay 12 -loop 0 *.png trumpet.gif
```

This script is available as [trumpet.sh](trumpet.sh)
or [trumpet_refactor.sh](trumpet_refactor.sh)
and should be run from directory `6trumpet/`.
It requires known environment or positional variable `$JOB_ID_INDEX`.



## 7. Substitute the shoeboxes
After a well-behaved set of refined experiments and indexed refls
covering appropriate resolution annulus has been selected,
use `substitute` worker to swap the indexed refls
with the ones coming from integration.

This script is available as [substitute.sh](substitute.sh)
and should be run from directory `7substitute/`.
It requires known environment or positional variables
`$JOB_ID_INDEX` and `$JOB_ID_TRUMPET`.



### 7½. Generating an empty mask
Annulus (spread ROI) worker's scenarios 3A and S1 used below require
an actual mask to work and do not accept `None`. For experimental data,
we would use an actual mask derived during an experiment.
However, since theoretical data is fully correct and all pixels are valid,
a "void" mask for Jungfrau 16M detector is needed and can be generated using
the following command. In case multiple masks are produced, only one under
given name is needed and should be preserved, as all of them are identical.
```shell
cd $SPREAD/SIM/
dials.generate_mask input.experiments=7substitute/$JOB_ID_SUBSTITUTE/substitute_000000.expt output.mask=JungFrau16_void.mask
```



## 8. Grid search on mosaic parameters
In the case of experimental data, this would be a step where we estimate
the value of Na, Nb, Nc, the average number of unit cells along
each crystallographic dimension in a single coherent crystalline unit,
as well as eta, the average rotational divergence of coherent domains.
After refining the orientation with diffBragg as well as determining ΔR,
Correlation(ΔR,ΔΨ), and <σZ> on the specific SPREAD annulus,
we would pick the best global values for subsequent work.

In the case of simulated data, there is no need for a grid search
of mosaic parameters, as the values of Na, Nb, Nc, and eta are a known
ground truth. The values of Na, Nb, Nc are returned by the simulation script
based on a hard-coded crystal volume and all equal 16 in the case of PSII.
In the most recent version of the code as of today,
the value of eta is also hard-coded and equal 0.05.
Consequently, this process acts as a small-scale debug/test stage
which is run before the next full-scale step,
thus confirming its feasibility and estimating necessary wall-time.

If you would like to run diffBragg refinement for simulated data nonetheless,
please refer to the instructions in [../experimental](../experimental).

This script is available as [mosaic.sh](mosaic.sh)
and should be run from directory `8mosaic/`.
It requires known environment or positional variable `$JOB_ID_SUBSTITUTE`.



## 9. Exa3A: full-scale crystal orientation refinement
Re-run the 3A scenario of the annulus worker utilized previously
on the entirety of data, but with the number of energy channels
narrowed down to a number used in later processing, such as 256.
Use diffBragg's first derivatives to refine crystal orientation,
refine background and G-scale, reject diverging experiments.
Additionally, revert the space group from patterson symmetry "Pmmm"
back to the actual space group "P212121" for the purpose of further analysis.
Produce expt and refl files that are going to be used in the final refinement.

This script is available as [exa3A.sh](exa3A.sh)
and should be run from directory `9exa3a/`.
It requires known environment or positional variable `$JOB_ID_SUBSTITUTE`.

***Note:** in case the SPREAD refinement is not quite right
and generates centrosymmetric data, I wonder if change of the space group
doesn't need to be performed earlier, potentially even after indexing.*



## 10. Prepare model structure factors
Cache the base structure factors to a pickle file
to speed up the run time of the S1 final step.
In this way, the full list of reference structure factors does not need
to be computed each time the S1 scenario of annulus worker is run,
but instead can be created once and saved for later.

For an initial count of 65536 images simulated for a 40 micrometer crystal,
this step should accept approximately 54299 input experiments.
With this input size, the following two output files should be produced:
- `psii_static_fcalcs.pickle` containing 521MB of `flex.miller_index`es
  for the structure in P1 as well as a `flex.complex_double` array
  of the anomalous contribution to the structure factors,
  where each column corresponds to a given energy in the range.
- `psii_miller_array.pickle` containing 27MB `flex.miller_array`
  with complex structure factors in P1 calculated at the lowest energy channel
  i.e. remote baseline without a significant anomalous contribution.

The final values used to start the SPREAD refinement are a sum
of this second "remote" and first "variable" contributions.

This SLURM job will fail by design and, in fact, can be killed
as soon as the aforementioned two files are generated.

This script is available as [sfactors.sh](sfactors.sh)
and should be run from directory `10sfactors/`.
It requires a known environment or positional variable `$JOB_ID_EXA3A`.



## 11. Run scattering factor refinement
This script will run the refinement for one-to-four independent Mn sites
on a (by default) 4 eV grid around Mn K-alpha absorption edge.
The spacing and number of sites are controlled by phil parameters,
and updating them might require re-running step 10 as well.

This script is available as [refine.sh](refine.sh)
and should be run from directory `11refine/`.
It requires known environment or positional variables
`$JOB_ID_EXA3A` and `$JOB_ID_SFACTORS`.
