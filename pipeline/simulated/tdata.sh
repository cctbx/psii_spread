#!/bin/bash -l
#SBATCH -N 4
#SBATCH -J psii_tdata
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m3562            # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -q debug
#SBATCH -t 00:10:00
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 256 -c 4"

JOB_ID_INDEX=${1:-$JOB_ID_INDEX}

mkdir -p "$SLURM_JOB_ID"
cd "$SLURM_JOB_ID" || exit

export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/pscratch/sd/p/psdatmgr/data/pmscr
export CCTBX_GPUS_PER_NODE=1
export MPI4PY_RC_RECV_MPROBE='False' # compensates for current missing MPI functions
env > env.out

echo "
tdata.output_path=sim_cells

input {
  path=$SPREAD/SIM/1index/$JOB_ID_INDEX/out/
  experiments_suffix=refined.expt
  reflections_suffix=indexed.refl
  parallel_file_load.method=uniform
}
dispatch {
  step_list = input balance tdata
}
output {
  output_dir=tdata
  prefix=run_tdata
  log_level=0
}
" > tdata.phil

echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge tdata.phil
echo "jobend $(date)";pwd
