from __future__ import absolute_import, division, print_function
from dxtbx.model.experiment_list import ExperimentListFactory
from dxtbx.imageset import ImageSetFactory
from dials.array_family import flex
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.colors as colors
import matplotlib.gridspec as gridspec
from cctbx import factor_ev_angstrom
import math
from scitbx.matrix import col

# Indexed files directly from Daniel Tchon
file_expt = "/global/cfs/cdirs/m3562/users/dtchon/p20231/common/ensemble1/r0148/002_rg007/task487/combine_experiments_t002/intermediates/t002_rg007_chunk000_refined.expt"
file_refl = "/global/cfs/cdirs/m3562/users/dtchon/p20231/common/ensemble1/r0148/002_rg007/task487/combine_experiments_t002/intermediates/t002_rg007_chunk000_refined.refl"
file_expt = "/global/cfs/cdirs/m3562/users/dtchon/p20231/common/ensemble1/r0250/002_rg007/task487/combine_experiments_t002/intermediates/t002_rg007_chunk000_refined.expt"
file_refl = "/global/cfs/cdirs/m3562/users/dtchon/p20231/common/ensemble1/r0250/002_rg007/task487/combine_experiments_t002/intermediates/t002_rg007_chunk000_refined.refl"

# Files after 6trumpet worker
file_expt = "/global/cfs/cdirs/lcls/sauter/LY99/spread/SIM/6trumpet/35958483/out/iobs_000100.expt"
file_refl = "/global/cfs/cdirs/lcls/sauter/LY99/spread/SIM/6trumpet/35958483/out/iobs_000100.refl"

def scatter1(polar_angle, two_thetas, not_low_res, title):
      # Create polar plot
      fig, ax = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(10, 10))

      # Scatter plot
      ax.scatter(polar_angle, two_thetas, s=2, c="b")
      ax.scatter(polar_angle.select(not_low_res), two_thetas.select(not_low_res), s=3, c="r")

      # Customize the plot (optional)
      ax.set_title(title)
      ax.set_xlabel("Polar angle (degrees)")
      ax.set_xticks(np.arange(0, 2*np.pi, np.pi/4)) # Set theta ticks
      ax.set_yticks(np.arange(5, 35, 5))            # Set r ticks
      ax.grid(True)

      # Display the plot
      plt.show()
def scatter2(fig, gs, polar_angle, two_thetas, tt, all_energies, expt, not_low_res, title):
      # Create polar plot
      #fig, ax = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(10, 10))
      ax = fig.add_subplot(gs[0,0], projection='polar')
      # Scatter plot
      norm = colors.Normalize(vmin=6525, vmax=6575)
      scatter = ax.scatter(polar_angle, two_thetas, s=3, c=all_energies, cmap='turbo_r', norm=norm)
      # Customize the plot (optional)
      cbar = plt.colorbar(scatter, label="Energy (eV)", shrink=0.8)
      # Get the colorbar axes
      cax = cbar.ax
      cax.axhline(y=factor_ev_angstrom  / expt.beam.get_wavelength(), color='k', linestyle='-')
      banner = get_title(expt)
      ax.text(0.70*math.pi,63,banner)
      ax.set_title(title)
      ax.set_xlabel("Polar angle (degrees)")
      ax.set_xticks(np.arange(0, 2*np.pi, np.pi/4)) # Set theta ticks
      ax.set_yticks(np.arange(5, 35, 5))            # Set r ticks
      ax.set_rlim(0,40)
      ax.bar(0,tt[1]-tt[0],width=2.*math.pi,bottom=tt[0],color='red',alpha=0.05) # spread ROI in terms of resolution range
      ax.grid(True)

def scatter3(fig, gs, polar_angle, two_thetas, tt, all_energies, expt, refl, title):
      # Create polar plot
      #fig, ax = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(10, 10))
      ax = fig.add_subplot(gs[0,1], projection='polar')

      norm = colors.Normalize(vmin=-0.4, vmax=0.4)
      scatter = ax.scatter(polar_angle, two_thetas, s=3, c=(180./math.pi)*refl["delpsical.rad"], cmap='turbo_r', norm=norm)
      # Customize the plot (optional)
      cbar = plt.colorbar(scatter, label="$\Delta\Psi$($\circ$)",shrink=0.8)
      # Get the colorbar axes
      cax = cbar.ax
      cax.axhline(y=factor_ev_angstrom  / expt.beam.get_wavelength(), color='k', linestyle='-')

      ax.set_title(title)
      ax.set_xlabel("Polar angle (degrees)")
      ax.set_xticks(np.arange(0, 2*np.pi, np.pi/4)) # Set theta ticks
      ax.set_yticks(np.arange(5, 35, 5))            # Set r ticks
      ax.set_rlim(0,40)
      ax.bar(0,tt[1]-tt[0],width=2.*math.pi,bottom=tt[0],color='red',alpha=0.05) # spread ROI in terms of resolution range
      ax.grid(True)

experiments = ExperimentListFactory.from_json_file(file_expt, check_format=False)
reflections = flex.reflection_table.from_file(file_refl)

def mock_energies(expt,refl):
      s0 = expt.beam.get_s0()
      uc = expt.crystal.get_unit_cell()
      dspacings = uc.d(refl['miller_index'])

      s0vecs = flex.vec3_double(len(refl),s0)
      s1vecs = refl['s1']
      bragg_theta = 0.5 * flex.acos((s0vecs.dot(s1vecs))/(s0vecs.norms()*s1vecs.norms())) # cosine rule to get bragg angle
      bragg_waveln = 2. * dspacings *flex.sin(bragg_theta)
      all_energies = factor_ev_angstrom / bragg_waveln
      all_energies = flex.double()
      #print("Beam energy", factor_ev_angstrom  / expt.beam.get_wavelength())
      for irefl in range(len(refl)):
        info = refl[irefl]
        ipanel = info["panel"]
        panel = expt.detector[ipanel]
        s0_lab = col((0,0,-1)) # panel.get_beam_centre_lab(s0))
        #from IPython import embed; embed()
        # method 1, naive take s1 from refl column
        # s1_lab = col(info['s1']) # spot energy always equals beam energy
        # bragg_theta = 0.5 * math.acos((s0_lab.dot(s1_lab))/(s0_lab.length()*s1_lab.length())) # cosine rule to get bragg angle
        # method 2, get lab coodinates from observed pixel coords
        # s1_lab = col(panel.get_pixel_lab_coord((info["xyzobs.px.value"][0:2]))) # spot energy on average too high by 10-30 eV
        # bragg_theta = 0.5 * math.acos((s0_lab.dot(s1_lab))/(s0_lab.length()*s1_lab.length())) # cosine rule to get bragg angle
        # recheck whether I got coords swapped
        # method 3, get theta from observed pixel coords assuming s0 (which contains the mean wavelength)
        bragg_theta = 0.5 * panel.get_two_theta_at_pixel(s0, info["xyzobs.px.value"][0:2])
        bragg_waveln = 2. * dspacings[irefl] * math.sin(bragg_theta)
        all_energies.append( factor_ev_angstrom / bragg_waveln )
        #print(dspacings[irefl], bragg_waveln, all_energies[irefl])
      #print("Beam energy", factor_ev_angstrom  / expt.beam.get_wavelength(), "average spot", flex.mean(all_energies))
      #print("S0 lab", panel.get_beam_centre_lab(s0))
      # this little plot makes it easy to see if the implied Bragg energies are distributed too widely or unevenly with resolution,
      # as is the case with a poorly refined detector position (i.e. if the drefine worker is not run)
      #   from matplotlib import pyplot as plt
      #   plt.plot(dspacings,all_energies,"b.")
      #   plt.plot([3.2,10],[factor_ev_angstrom  / expt.beam.get_wavelength(),factor_ev_angstrom  / expt.beam.get_wavelength()])
      return all_energies

def get_title(expt):
  iset = expt.imageset
  assert len(iset) == 1
  image_index = iset.indices()[0]
  path = iset.paths()[0]
  """In this scenario, the input worker is asked to keep_imagesets but not read_image_headers.
    The downstream worker then reconstructs the imageset and reads the spectra and raw data arrays."""
    # Re-generate the image sets using their format classes so we can read the raw data
    # Use the experiments one at a time to not use up memory
  #CC = ImageSetFactory.make_imageset(filenames=iset.paths(), single_file_indices=expt.imageset.indices())
  #spectrum = CC.get_spectrum(0)

  return '%s, event %d'%(path,image_index)


def simple_plot(experiments,reflections):
  for expt_id, expt in enumerate(experiments):

      uc = expt.crystal.get_unit_cell()
      d_min = 3.2
      d_max = 5.0

      #print('Annulus statistics: ' + expt.identifier)
      refl = reflections.select(reflections['id']==expt_id)
      refl = refl.select(10 > uc.d(refl['miller_index']))
      #print("There are %d reflections"%len(refl))
      dspacings = uc.d(refl['miller_index'])
      two_thetas = uc.two_theta(refl['miller_index'], wavelength=expt.beam.get_wavelength(),deg=True)
      gt = dspacings > d_min
      lt = dspacings < d_max
      two_theta_inner = (180./math.pi)*2*math.asin(expt.beam.get_wavelength()/(2.*d_max))
      two_theta_outer = (180./math.pi)*2*math.asin(expt.beam.get_wavelength()/(2.*d_min))
      tt = [two_theta_inner,two_theta_outer]

      print(lt.count(False),"in the low res tail", gt.count(False),"in high res tail, leaving", (gt&lt).count(True))
      min_spots = 10 #5
      if lt.count(True) < min_spots: continue

      xproj = refl['s1'].parts()[0]
      yproj = refl['s1'].parts()[1]
      polar_angle = flex.atan2(yproj,xproj)
      all_energies = mock_energies(expt,refl)
      #scatter1(polar_angle, two_thetas, not_low_res=lt, title="Strong refls on image %s"%expt_id)
      # This little gem plots the correlation between energy calculated from spot position with Bragg's law (all energies),
      # and calculated delta psi.  With poorly refined detector position the negative correlation weakens
      #   plt.plot(all_energies, (180./math.pi)*refl["delpsical.rad"], "r.")
      #   plt.show()
      fig = plt.figure(figsize=(14,10))
      gs = gridspec.GridSpec(nrows=1, ncols=2, height_ratios=[1,])
      scatter2(fig, gs, polar_angle, two_thetas, tt,all_energies, expt = expt, not_low_res=lt, title="Strong refls on image %s"%expt_id)
      scatter3(fig, gs, polar_angle, two_thetas, tt,all_energies, expt, refl, title="Strong refls on image %s"%expt_id)
      plt.show()

if __name__=="__main__":
  simple_plot(experiments, reflections)
