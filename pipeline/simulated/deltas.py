# LIBTBX_SET_DISPATCHER_NAME spread.ground_truth_delta
from __future__ import absolute_import, division, print_function
# %%% boilerplate specialize to packaged big data %%%
from LS49.sim import step5_pad
from LS49.sim import step4_pad
from LS49.spectra import generate_spectra
from LS49 import ls49_big_data
step5_pad.big_data = ls49_big_data
step4_pad.big_data = ls49_big_data
generate_spectra.big_data = ls49_big_data
# %%%%%%
from scitbx.array_family import flex
from iotbx.phil import parse
import os
from LS49.sim.fdp_plot import george_sherrell
from LS49.sim.step5_pad import full_path
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import math
from scipy import constants
ENERGY_CONV = 1e10 * constants.c * constants.h / constants.electron_volt

master_phil = """
job_id = 28903428,28919448,28919462,28919466,28919475,28919485
  .type = str
  .multiple = False
  .help = job run within the $SPREAD/SIM/11refine folder
legend = 0.08,0.16,0.64,1.25,2.5,5
  .type = str
title = "Variable f-double-prime with restrained f-prime=0.04 electrons/interval"
  .type = str
classes = []
  .type = str
  .multiple = True
  .help = the metal scattering factors used for each metal site as ground truth in the simulation
  .help = choose from "data_sherrell/MnO2_spliced.dat"(Mn+4) or "data_sherrell/Mn2O3_spliced.dat"(Mn+3)
"""
from psii_spread.merging.application.annulus.phil import phil_str as annulus_phil_str
phil_scope = parse(master_phil + annulus_phil_str)

def app_set_up():

    help_message = '''Basic spread delta to the simulated ground truth'''

    # The script usage
    import libtbx.load_env
    usage = "usage: %s [options] [param.phil] " % libtbx.env.dispatcher_name
    parser = None
    from dials.util.options import ArgumentParser
    # Create the parser
    parser = ArgumentParser( usage=usage, phil=phil_scope, epilog=help_message)

    # Parse the command line. quick_parse is required for MPI compatibility
    params, options = parser.parse_args(show_diff_phil=True,quick_parse=False)
    return params, options

def get_log_files(params):
  # loop over jobs:
 for job in params.job_id.split(','):
  rank0dir = os.path.join(os.getenv("SPREAD"),"SIM","11refine",job,"out","rank_0.out")
  if not os.path.isfile(rank0dir):
    print("Check environment variable $SPREAD and phil parameter job_id=xxx on command line")
    print("Cannot find %s\n"%rank0dir)
  with open(rank0dir,"r") as F:
    yield F

class plotter:
        pass

def run_detail(params):
  fig = plt.figure(figsize=(8.4,4.8))
  gs = gridspec.GridSpec(nrows=1, ncols=1)
  axspecd = fig.add_subplot(gs[0,0])
  axspecd.set_title(params.title)
  axspecd.set_xlabel("Iteration")
  axspecd.set_ylabel("r.m.s.d. fit to ground truth, e-")

  for label,refinement in zip(params.legend.split(','),get_log_files(params)):
    all_values = []
    for line in refinement:
      tokens = line.strip().split()
      if "Macrocycle" in tokens and "Iteration" in tokens:
        # assume Macrocycle 1 Iteration 7 [-4.993...
        # need some more ad hoc parsing to isolate the f" and f'
        macro = " ".join(tokens[:4])
        joined = " ".join(tokens[4:])
        stripped = joined.replace("[","").replace("]","").split(", ")
        values = flex.double([float(f) for f in stripped])
        all_values.append(values)
        #axspec.plot(values)
        #plt.show()
      if "output" in tokens and "energies" in tokens:
        # assume output 25 energies from 6502.00 to 6598.00, spaced 4.00 eV
        N_energies = int(tokens[1])
        e_spacing = float(tokens[8])
        energy_1 = float(tokens[4])
    # for debug assert e_spacing == params.exafel.debug.energy_stride_eV
    #double check the number of result parameters
    assert len(set(len(v) for v in all_values)) == 1
    for values in all_values:
      assert len(values) == len(("fp","fdp")) * len(params.classes) * N_energies
    #develop the ground truth baseline
    gtb = flex.double(len(all_values[0]),0.)
    presumptive_energies = [energy_1 + x * e_spacing for x in range(N_energies)]
    for iclassgt,classgt in enumerate(params.classes):
      GS = george_sherrell(full_path(classgt))
      for ieV, eV in enumerate([energy_1 + x * e_spacing for x in range(N_energies)]):
        gtb[2*iclassgt*N_energies + ieV], gtb[(2*iclassgt+1)*N_energies + ieV] = GS.fp_fdp_at_eV_energy(eV)
    # so now we have refined (all_values) and ground truth (gtb)
    iterative_delta=[]
    for values in all_values:
      delta = math.sqrt(flex.mean(flex.pow(values-gtb,2.))) # root mean squared diff
      iterative_delta.append(delta)
      if len(iterative_delta) == len(all_values):
        print (iterative_delta)
        axspecd.plot(iterative_delta, marker=".",label=label)
  axspecd.legend(loc="upper right")
  plt.show()
  print("Ok")

if __name__=="__main__":
  params, options = app_set_up()
  # ground truth scattering factors for each class of metal in the simulation
  # +3+4+4+4
  #params.classes = ["data_sherrell/Mn2O3_spliced.dat", "data_sherrell/MnO2_spliced.dat",
  #                 "data_sherrell/MnO2_spliced.dat", "data_sherrell/MnO2_spliced.dat"]
  # +4+4+3+3
  #params.classes = ["data_sherrell/MnO2_spliced.dat", "data_sherrell/MnO2_spliced.dat",
  #                 "data_sherrell/Mn2O3_spliced.dat", "data_sherrell/Mn2O3_spliced.dat"]
  # +4+3+3+3
  params.classes = ["data_sherrell/MnO2_spliced.dat", "data_sherrell/Mn2O3_spliced.dat",
                   "data_sherrell/Mn2O3_spliced.dat", "data_sherrell/Mn2O3_spliced.dat"]
  run_detail(params)
