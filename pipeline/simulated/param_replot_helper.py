import pickle
import glob
import argparse
import torch
import matplotlib.pyplot as plt
import numpy as np
import libtbx.load_env
import os

from torchBragg.kramers_kronig.convert_fdp_full_cubic_spline_helper import get_coeff_cubic_spline, \
    reformat_fdp, get_physical_params_fdp
from torchBragg.kramers_kronig.convert_fdp_helper_vectorize import fdp_fp_integrate

colors = ["limegreen","c","gold","k","darkorange",'deeppink','deepskyblue','lime']

# ground truth
from LS49.sim.fdp_plot import george_sherrell
GS_IV = george_sherrell(os.path.join(libtbx.env.find_in_repositories("ls49_big_data"),
        "data_vgan","MnO2_spliced.dat"))
GS_III = george_sherrell(os.path.join(libtbx.env.find_in_repositories("ls49_big_data"),
        "data_vgan","Mn2O3_spliced.dat"))

def postmortem_plot(pkl_filename,global_weights_quantiles, all_energies_quantiles, device = 'cpu'):
    with open(pkl_filename, "rb") as F:
        newx = pickle.load(F)
    pkl_filebase = pkl_filename.split(".")[0]
    pkl_iteration = pkl_filebase.split("_")[-1]
    energy_vec_free = newx['energy_vec_free'].to(device)
    energy_vec_physical = newx['energy_vec_physical'].to(device)
    free_parameters = newx['free_parameters']
    scale = newx['scale']
    num_free_params = len(energy_vec_free) + 1
    free_parameters = torch.tensor(free_parameters, device=device)
    energy_vec_plot = [i for i in np.arange(energy_vec_physical[0].cpu().numpy()-100.5, energy_vec_physical[-1].cpu().numpy()+100.5, 1) if i not in energy_vec_free.cpu().numpy()]
    energy_vec_plot = torch.tensor(energy_vec_plot, device=device)

    plt.figure(figsize=(8.4,4.8))
    for iclass in range(4):
        scale_i = torch.tensor(scale[iclass * num_free_params : (iclass + 1) * num_free_params], device=device, requires_grad=False)
        free_params_i = free_parameters[iclass * num_free_params : (iclass + 1) * num_free_params]/scale_i

        fdp_vec_free = free_params_i[:-1]
        relativistic_correction = free_params_i[-1]

        params_free_cubic_spline = get_coeff_cubic_spline(energy_vec_free, fdp_vec_free)

        physical_parameters_fdp = get_physical_params_fdp(energy_vec_plot, energy_vec_free, params_free_cubic_spline)


        intervals_mat, coeff_mat, powers_mat = reformat_fdp(energy_vec_free, params_free_cubic_spline, device=device)
        physical_parameters_fp = fdp_fp_integrate(energy_vec_plot, intervals_mat, coeff_mat, powers_mat, relativistic_correction, device=device)
        # plt.plot(energy_vec_free,fdp_vec_free,'.',color='black')
        plt.plot(energy_vec_plot, physical_parameters_fdp, linestyle="-", color=colors[iclass])
        plt.plot(energy_vec_plot, physical_parameters_fp, linestyle="-", color=colors[iclass])
        # ground truth
        GS_IV.plot_them(plt,f1="b,",f2="b,")
        GS_III.plot_them(plt,f1="r,",f2="r,")
    normalize = max(list(global_weights_quantiles))
    for energy,weight in zip(all_energies_quantiles,global_weights_quantiles):
      plt.plot([energy, energy],[-2,-2+2*weight/normalize],"-",color="limegreen")

    plt.xlim([energy_vec_plot[0],energy_vec_plot[-1]])
    plt.xlim((6495.,6605))
    plt.ylim((-9.6,5.5))
    plt.title("Iteration %s"%pkl_iteration)
    plt.savefig(pkl_filename.split('.')[0] + '.png')    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--folder', type=str, default='31240113',
                        help='job ID where the results of an optimization are saved',
                        )
    parser.add_argument('--spectra', type=str, default='34848955',
                        help='job ID where the results of an optimization are saved',
                        )
    parser.add_argument('--nbins', type=int, default='32',
                        help='job ID where the results of an optimization are saved',
                        )                        
    args = parser.parse_args()

    folder = args.folder
    spectra = args.spectra
    nbins = args.nbins

    loss_files = np.sort(glob.glob(folder + '/loss*.npy'))
    loss_vec = [np.load(file).item() for file in loss_files]
    plt.figure()
    plt.plot(loss_vec)
    plt.savefig(folder + '/loss.png')    

    spectra_filein = "/global/cfs/cdirs/lcls/sauter/LY99/spread/SIM/11refine/%s/incident_spectra.pickle"%spectra
    with open(spectra_filein,"rb") as F:
      fdata = pickle.load(F)
      global_shots = fdata["global_shots"]
      all_energies = fdata["all_energies"]
      global_weights = fdata["global_weights"]

    from psii_spread.merging.application.spectra import spectra as spectra_util
    cdf_quantiles, global_weights_quantiles, all_energies_quantiles=spectra_util.get_cdf(
                      global_shots, all_energies, global_weights, nbins=nbins)

    pickle_files = glob.glob(folder + '/*.pickle')
    for file in pickle_files:
        postmortem_plot(file,global_weights_quantiles, all_energies_quantiles)
    
