#!/bin/bash -l
#SBATCH -N 16               # Number of nodes on Perlmutter
#SBATCH -J psii_substitute
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m4734            # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -q regular          # regular queue
#SBATCH -t 00:10:00         # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 256 -c 16"

JOB_ID_INDEX=${1:-$JOB_ID_INDEX}
JOB_ID_TRUMPET=${2:-$JOB_ID_TRUMPET}

mkdir -p "$SLURM_JOB_ID"
cd "$SLURM_JOB_ID" || exit

export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export CCTBX_GPUS_PER_NODE=1
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application # User must export $MODULES path
env > env.out

echo "
input.path=$SPREAD/SIM/6trumpet/$JOB_ID_TRUMPET/out
input.experiments_suffix=.expt
input.reflections_suffix=.refl
output.output_dir=out
output.prefix=substitute
output.save_experiments_and_reflections=True
dispatch.step_list = input arrange substitute
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
input.parallel_file_load.method=uniform
arrange.sort.by=imageset_path
output.log_level=0 # stdout stderr
substitute.input=$SPREAD/SIM/1index/$JOB_ID_INDEX/out/*_integrated.refl
" > substitute.phil

echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge substitute.phil
echo "jobend $(date)";pwd
