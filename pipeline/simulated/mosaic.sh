#!/bin/bash -l
#SBATCH -N 32               # Number of nodes
#SBATCH -J psii_mosaic
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m3562_g          # allocation: m3562_g or m4734_g
#SBATCH -C gpu
#SBATCH -q regular          # regular queue
#SBATCH -t 01:00:00         # wall clock time limit
#SBATCH --ntasks-per-gpu=4
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 512 -c 8"

JOB_ID_SUBSTITUTE=${1:-$JOB_ID_SUBSTITUTE}

export ETA=0.05
export NABC=16
JOB_DIR=${SLURM_JOB_ID}_e${ETA}Nabc${NABC}
mkdir -p "$JOB_DIR"
cd "$JOB_DIR" || exit

export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export CCTBX_GPUS_PER_NODE=1
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application # User must export $MODULES path
env > env.out

echo "
dispatch.step_list = input balance annulus
input.path=$SPREAD/SIM/7substitute/$JOB_ID_SUBSTITUTE/out/
input.experiments_suffix=0.expt  # 10% of data
input.reflections_suffix=0.refl  # 10% of data
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
input.parallel_file_load.method=uniform
input.parallel_file_load.balance_mpi_alltoall_slices = 10
scaling.model=$MODULES/ls49_big_data/7RF1_refine_030_Aa_refine_032_refine_034.pdb
scaling.unit_cell=117.463  222.609  309.511  90.00  90.00  90.00  # as used in simulation
scaling.space_group=Pmmm  # P212121
scaling.resolution_scalar=0.96
merging.d_max=None
merging.d_min=3.2
statistics.annulus.d_max=5.0
statistics.annulus.d_min=3.2
spread_roi.enable=True
output.log_level=0 # 0 = stdout stderr, 1 = terminal
output.output_dir=out
output.prefix=mosaic
output.save_experiments_and_reflections=False
exafel.scenario=3A
exafel.trusted_mask=$SPREAD/SIM/JungFrau16_void.mask
exafel.shoebox_border=0
exafel.context=kokkos_gpu
exafel.model.plot=False
exafel.model.mosaic_spread.value=$ETA
exafel.model.Nabc.value=$NABC,$NABC,$NABC
exafel.debug.lastfiles=False # write out *.h5, *.mask for each image
exafel.debug.verbose=False
exafel.debug.finite_diff=-1
exafel.debug.eps=1.e-8
exafel.debug.energy_offset_eV=0
exafel.debug.energy_stride_eV=1.0
exafel.skin=True # whether to use diffBragg
exafel{
  refpar{
    label = *background *G *rot
    background {
      algorithm=rossmann_2d_linear
      scope=spot
      slice_init=border
      slice=all
    }
    G {
      scope=lattice
    }
  }
}
" > mosaic.phil
echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge mosaic.phil
echo "jobend $(date)";pwd
