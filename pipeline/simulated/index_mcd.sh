#!/bin/bash
#SBATCH -N 8             # Number of nodes
#SBATCH -J psii_index
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m4734            # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -q regular       # regular or special queue
#SBATCH -t 00:15:00      # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 1024 -c 2"

JOB_ID_SIMULATE=${1:-$JOB_ID_SIMULATE}

mkdir -p "$SLURM_JOB_ID"
cd "$SLURM_JOB_ID" || exit
mkdir -p out
mkdir -p stdout
NPROC=$(echo "$SRUN" | cut -d " " -f 3)

export MPI4PY_RC_RECV_MPROBE='False'
env > env.out

echo "
dispatch {
  pre_import = True
  index = True
  refine = True
  integrate = True
  # hit_finder.minimum_number_of_reflections = 40
}
input {
  glob = $SPREAD/SIM/0simulate/$JOB_ID_SIMULATE/image*.h5
}
spotfinder {
  lookup.mask = None
  threshold {
    dispersion {
      gain = 1.0
      sigma_background=2
      sigma_strong=2
      global_threshold=10
      kernel_size=6 6
    }
  }
  filter.min_spot_size=3
  filter.d_min=2.5
}
indexing {
  stills.nv_reject_outliers=False
  stills.refine_all_candidates=False
  stills.refine_candidates_with_known_symmetry=True
  known_symmetry {
    space_group = P212121
    unit_cell = 117.463 222.609 309.511 90 90 90
  }
  stills.reflection_subsampling {
    enable = True
    step_start = 100
    step_stop = 20
    step_size = 1
    n_attempts_per_step = 3
  }
}
refinement {
  reflections {
    outlier {
      algorithm=mcd
      mcd {
        coordinates = deltatt_transverse
        threshold_probability=0.999
  } } }
  parameterisation {
    crystal {
      unit_cell {
        restraints {
          tie_to_target {
            values = 117.463 222.609 309.511 90 90 90
            sigmas = 0.117 0.223 0.309 0 0 0  # 0.1% of absolute values
}}}}}}
integration {
  background.simple.outlier.plane.n_sigma=10
  debug.output = True
  debug.separate_files = False
  debug.delete_shoeboxes = False
  summation.detector_gain = 1.0
}
profile.gaussian_rs.centroid_definition = *com s1
mp.method = mpi
output {
  composite_output=True
  integration_pickle=None
  output_dir=./out/
  logging_dir=./stdout/
  logging_option=suppressed
}
" > index.phil

echo "jobstart $(date)";pwd
$SRUN --cpu-bind=cores dials.stills_process index.phil mp.nproc="$NPROC"
echo "jobend $(date)";pwd
