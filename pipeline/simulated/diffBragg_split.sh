#!/bin/bash -l
#SBATCH -N 4                # Number of nodes on Perlmutter
#SBATCH -J psii_trumpet
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A lcls             # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -t 00:05:00         # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -c 2"

JOB_ID_SUBSTITUTE=${1:-$JOB_ID_SUBSTITUTE}

INPUT_PATH=${SPREAD}/SIM/7substitute/${JOB_ID_SUBSTITUTE}/out
SPEC_PATH=${PWD}/${JOB_ID_SUBSTITUTE}.txt

echo "jobstart $(date)";pwd
$SRUN diffBragg.make_input_file "${INPUT_PATH}" "${SPEC_PATH}" --exptSuffix ".expt"  --reflSuffix ".refl"
echo "jobend $(date)";pwd