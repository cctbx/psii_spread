# LIBTBX_SET_DISPATCHER_NAME spread.ground_truth_delta
from __future__ import absolute_import, division, print_function
# %%% boilerplate specialize to packaged big data %%%
from LS49.sim import step5_pad
from LS49.sim import step4_pad
from LS49.spectra import generate_spectra
from LS49 import ls49_big_data
step5_pad.big_data = ls49_big_data
step4_pad.big_data = ls49_big_data
generate_spectra.big_data = ls49_big_data
from LS49.sim.step5_pad import data
local_data = data()
# %%%%%%
from scitbx.array_family import flex
from iotbx.phil import parse
import os
from matplotlib import pyplot as plt
from scitbx.array_family import flex
from scipy import constants
ENERGY_CONV = 1e10 * constants.c * constants.h / constants.electron_volt

master_phil = """
jobid = 00000000
  .type = str
  .multiple = False
  .help = job run within the $SPREAD/SIM/11refine folder
classes = []
  .type = str
  .multiple = True
  .help = the metal scattering factors used for each metal site as ground truth in the simulation
  .help = choose from "data_sherrell/MnO2_spliced.dat"(Mn+4) or "data_sherrell/Mn2O3_spliced.dat"(Mn+3)
"""
from psii_spread.merging.application.annulus.phil import phil_str as annulus_phil_str
phil_scope = parse(master_phil + annulus_phil_str, process_includes = True)

def app_set_up():

    help_message = '''Basic spread delta to the simulated ground truth'''

    # The script usage
    import libtbx.load_env
    usage = "usage: %s [options] [param.phil] " % libtbx.env.dispatcher_name
    parser = None
    from dials.util.options import ArgumentParser
    # Create the parser
    parser = ArgumentParser( usage=usage, phil=phil_scope, epilog=help_message)

    # Parse the command line. quick_parse is required for MPI compatibility
    params, options = parser.parse_args(show_diff_phil=True,quick_parse=False)

    return params, options

def get_log_files(params):
  rank0dir = os.path.join(os.getenv("SPREAD"),"SIM","11refine",params.jobid,"out","rank_0.out")
  if not os.path.isfile(rank0dir):
    print("Check environment variable $SPREAD and phil parameter jobid=xxx on command line")
    print("Cannot find %s\n"%rank0dir)
  # Attempt to write all output to the jobid directory
  os.chdir(os.path.join(os.getenv("SPREAD"),"SIM","11refine",params.jobid))
  with open(rank0dir,"r") as F:
    yield F

from psii_spread.merging.application.annulus.new_global_fdp_refinery import rank_0_fit_all_f_mix_in
class myplot(rank_0_fit_all_f_mix_in):
  def __init__(self, params):
    self.params = params
    # case specific parameters
    self.params.exafel.debug.energy_stride_eV = 2.0
    self.params.sauter20.LLG_evaluator.title = "post"
    self.params.exafel.metal = "PSII4"
    self.n_classes = 4 # four atom classes for four distinct Mn sites
    self.labels = ["MN1", "MN2", "MN3", "MN4"] # to later set new_model_intensities
    self.macrocycle = None
    self.plot_plt_imported = True
    self.plt = plt

def run_detail(params):
  MP = myplot(params)
  energies = flex.double([6501.0 + 2.0*idx for idx in range(50)])
  case_specific_starting_models = [local_data.get("Mn_metallic_model")]*MP.n_classes

  MP.rank_0_fit_all_f_setup( starting_models = case_specific_starting_models, energies = energies )
  for refinement in get_log_files(params):
    all_values = []
    for line in refinement:
      tokens = line.strip().split()
      if "Macrocycle" in tokens and "Iteration" in tokens:
        MP.iteration = int(tokens[3])
        # assume Macrocycle 1 Iteration 7 [-4.993...
        # need some more ad hoc parsing to isolate the f" and f'
        macro = " ".join(tokens[:4])
        joined = " ".join(tokens[4:])
        stripped = joined.replace("[","").replace("]","").split(", ")
        values = flex.double([float(f) for f in stripped])
        all_values.append(values)
        assert len(values)==len(MP.x)
        for ival,val in enumerate(values):
          MP.x[ival] = val
        MP.plot_em()
  print("Ok")

if __name__=="__main__":
  # Usage:
  # export SPREAD=<working spread directory name>
  # libtbx.python postproc.py jobid=33085981
  params, options = app_set_up()
  # ground truth scattering factors for each class of metal in the simulation
  # +3+4+4+4
  #params.classes = ["data_sherrell/Mn2O3_spliced.dat", "data_sherrell/MnO2_spliced.dat",
  #                 "data_sherrell/MnO2_spliced.dat", "data_sherrell/MnO2_spliced.dat"]
  # +4+4+3+3
  params.classes = ["data_sherrell/MnO2_spliced.dat", "data_sherrell/MnO2_spliced.dat",
                   "data_sherrell/Mn2O3_spliced.dat", "data_sherrell/Mn2O3_spliced.dat"]
  params.classes = ["data_vgan/MnO2_spliced.dat", "data_vgan/MnO2_spliced.dat",
                   "data_vgan/Mn2O3_spliced.dat", "data_vgan/Mn2O3_spliced.dat"]
  run_detail(params)
