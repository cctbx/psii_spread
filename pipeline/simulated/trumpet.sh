#!/bin/bash -l
#SBATCH -N 4                # Number of nodes on Perlmutter
#SBATCH -J psii_trumpet
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A m3562            # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -t 00:30:00         # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 256 -c 4"

JOB_ID_INDEX=${1:-$JOB_ID_INDEX}
JOB_ID_MAHALANOBIS=${2:-$JOB_ID_MAHALANOBIS}

mkdir -p "$SLURM_JOB_ID"
cd "$SLURM_JOB_ID" || exit

export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export CCTBX_GPUS_PER_NODE=1
export MPI4PY_RC_RECV_MPROBE='False' # compensates for current missing MPI functions
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application # User must export $MODULES path
env > env.out

echo "
input {
  path=$SPREAD/SIM/1index/$JOB_ID_INDEX/out
  experiments_suffix=refined.expt
  reflections_suffix=indexed.refl
  parallel_file_load.method=uniform
  parallel_file_load.balance=global2
}
dispatch.step_list = input balance filter statistics_unitcell model_statistics annulus trumpet
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
scaling.model=$MODULES/ls49_big_data/7RF1_refine_030_Aa_refine_032_refine_034.pdb
scaling.unit_cell=117.463  222.609  309.511  90.00  90.00  90.00
scaling.space_group=Pmmm
scaling.resolution_scalar=0.96
filter.algorithm=unit_cell
filter.unit_cell.algorithm=cluster
filter.unit_cell.cluster.covariance.file=$SPREAD/SIM/2mahalanobis/$JOB_ID_MAHALANOBIS/covariance_sim_cells.pickle
filter.unit_cell.cluster.covariance.component=0
filter.unit_cell.cluster.covariance.mahalanobis=2.0
merging.d_max=5.0
merging.d_min=3.2
statistics.annulus.d_max=5.0
statistics.annulus.d_min=3.2
spread_roi.enable=True
spread_roi.strong=1.0
output.log_level=0
exafel.trusted_mask=None
exafel.scenario=1
output.output_dir=out
output.save_experiments_and_reflections=True
trumpet.plot_all.enable=True
trumpet.plot_all.savepng=True
trumpet.dials_refine=True
trumpet.debug_rank_0=False
trumpet.spectrum.recalibration=1.95
trumpet.spectrum.nbins=60
trumpet.spectrum.range=6500.0,6600.0
#trumpet.spectrum.energy_delta_tolerance=1000
trumpet.residuals.cutoff=5.
trumpet.outlier.stddev=3.
trumpet.outlier.deff=2000.
trumpet.outlier.ucell=2.
trumpet.outlier.tff=0
trumpet.outlier.remove=True
refinement {
  parameterisation {
    auto_reduction {
      min_nref_per_parameter = 1
      action = fix
    }
    beam.fix = all
    detector {
      fix_list = Tau1,Tau2,Tau3
      hierarchy_level = 0
    }
    crystal {
      unit_cell {
        restraints {
        tie_to_target {
            values = 117.463 222.609 309.511 90 90 90
            sigmas = 0.0117463 0.0222609 0.0309511 0.0 0.0 0.0
            id = None # apply to all
          }
        }
      }
    }
  }
  reflections {
    weighting_strategy.override = stills
    outlier.algorithm = null
  }
}

" > trumpet.phil

echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge trumpet.phil
echo "jobend $(date)";pwd
