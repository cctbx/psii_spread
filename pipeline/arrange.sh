#!/bin/bash -l
#SBATCH -N 16               # Number of nodes on Perlmutter
#SBATCH -J arrange
#SBATCH -L SCRATCH          # job requires SCRATCH files
#SBATCH -A lcls             # allocation: m3562 or m4734
#SBATCH -C cpu
#SBATCH -q regular          # regular queue
#SBATCH -t 00:05:00         # wall clock time limit
#SBATCH -o %j.out
#SBATCH -e %j.err
SRUN="srun -n 1024 -c 4"

# A raw arrange job; useful to equalize the size of expt/refl files if needed
INPUT_PATHS=""
for INPUT_PATH in "$@"  # array of arguments
do INPUT_PATHS+="input.path="$(realpath "$INPUT_PATH")"\n"; done

mkdir -p "$SLURM_JOB_ID"; cd "$SLURM_JOB_ID" || exit
export CCTBX_NO_UUID=1
export DIFFBRAGG_USE_CUDA=1
export CUDA_LAUNCH_BLOCKING=1
export NUMEXPR_MAX_THREADS=128
export SLURM_CPU_BIND=cores # critical to force ranks onto different cores. verify with ps -o psr <pid>
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export SIT_PSDM_DATA=/global/cfs/cdirs/lcls/psdm-sauter
export CCTBX_GPUS_PER_NODE=1
export XFEL_CUSTOM_WORKER_PATH=$MODULES/psii_spread/merging/application # User must export $MODULES path
env > env.out

echo -e "
dispatch.step_list = input arrange
$INPUT_PATHS
input.experiments_suffix=_integrated.expt
input.reflections_suffix=_integrated.refl
input.keep_imagesets=True
input.read_image_headers=False
input.persistent_refl_cols=shoebox
input.persistent_refl_cols=bbox
input.persistent_refl_cols=xyzcal.px
input.persistent_refl_cols=xyzcal.mm
input.persistent_refl_cols=xyzobs.px.value
input.persistent_refl_cols=xyzobs.mm.value
input.persistent_refl_cols=xyzobs.mm.variance
input.persistent_refl_cols=delpsical.rad
input.persistent_refl_cols=panel
input.parallel_file_load.method=uniform
input.parallel_file_load.balance_verbose=True
arrange.sort.by=imageset_path
output.output_dir=out
output.log_level=0 # stdout stderr
output.prefix=arrange
output.save_experiments_and_reflections=True
" > arrange.phil

echo "jobstart $(date)";pwd
$SRUN cctbx.xfel.merge arrange.phil
echo "jobend $(date)";pwd